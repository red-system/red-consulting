module.exports = function(grunt) {

  var default_js = [
      'assets/js/vendor/jquery.min.js',
      'assets/js/vendor/jquery-migrate.min.js',
      'assets/js/custom/custom.js',
      'assets/js/vendor/superfish.js',
      'assets/js/custom/_utils.js',
      'assets/js/custom/_init.js'
  ];

  var default_css = [
      'assets/css/font-lora-poppins.css',
      'assets/css/fontello/css/fontello.css',
      'assets/css/style.css',
      'assets/css/style-ex.css',
      'assets/css/colors.css',
      'assets/css/shortcodes.css',
      'assets/css/animation.css',
      'assets/css/responsive.css'
  ];

  // Begin beranda

  var beranda_js = [
      'assets/js/vendor/essgrid/jquery.themepunch.tools.min.js',
      'assets/js/vendor/essgrid/jquery.themepunch.essential.min.js',
      'assets/js/vendor/revslider/jquery.themepunch.revolution.min.js',
      'assets/js/vendor/revslider/revolution.extension.slideanims.min.js', 
      'assets/js/vendor/revslider/revolution.extension.layeranimation.min.js',
      'assets/js/vendor/revslider/revolution.extension.navigation.min.js', 
      'assets/js/vendor/isotope.js',
      'assets/js/custom/_shortcodes.js',
      'assets/js/vendor/swiper/swiper.js',
      'assets/js/custom/scroll-section.js'
  ];
  beranda_js = default_js.concat(beranda_js);
  var beranda_css = [
      'assets/css/vendor/essgrid/ess-grid.css',
      'assets/css/vendor/revslider/rev-slider.css',
      'assets/css/vendor/swiper/swiper.css'
  ];
  beranda_css = default_css.concat(beranda_css);
  var beranda_file = beranda_js.concat(beranda_css);

  // End beranda
  // Begin layanan_detail

  var layanan_detail_js = [
      'assets/js/custom/_shortcodes.js', 
      'assets/js/vendor/swiper/swiper.js',
      'assets/sweet-alert/sweet-alert.min.js',
      'assets/js/custom/kirim_email_konsultasi.js'
  ];
  layanan_detail_js = default_js.concat(layanan_detail_js);
  var layanan_detail_css = [
      'assets/css/vendor/swiper/swiper.css',
      'assets/sweet-alert/sweet-alert.css',
      'assets/css/loader-send-email.css',
  ];
  layanan_detail_css = default_css.concat(layanan_detail_css);
  var layanan_detail_file = layanan_detail_js.concat(layanan_detail_css);

  // End layanan_detail
  // Begin galeri

  var galeri_js = [
      'assets/js/vendor/essgrid/jquery.themepunch.tools.min.js', 
      'assets/js/vendor/essgrid/jquery.themepunch.essential.min.js'
  ];
  galeri_js = default_js.concat(galeri_js);
  var galeri_css = [
      'assets/css/vendor/essgrid/ess-grid.css'
  ];
  galeri_css = default_css.concat(galeri_css);
  var galeri_file = galeri_js.concat(galeri_css);

  // End galeri
  // Begin artikel

  var artikel_js = [
      'assets/js/vendor/isotope.js', 
      'assets/js/custom/_shortcodes.js'
  ];
  artikel_js = default_js.concat(artikel_js);
  var artikel_file = artikel_js;

  // End artikel
  // Begin artikel_detail

  var artikel_detail_js = [
      'assets/js/custom/_shortcodes.js', 
      'assets/js/vendor/swiper/swiper.js'
  ];
  artikel_detail_js = default_js.concat(artikel_detail_js);
  var artikel_detail_css = [
      'assets/css/vendor/swiper/swiper.css'
  ];
  artikel_detail_css = default_css.concat(artikel_detail_css);
  var artikel_detail_file = artikel_detail_js.concat(artikel_detail_css);

  // End artikel_detail
  // Begin faq

  var faq_js = [
      'assets/css/custom/faq-accordion.js'
  ];
  faq_js = default_js.concat(faq_js);
  var faq_file = faq_js;

  // End faq
  // Begin tentang_kami

  var tentang_kami_js = [
      'assets/js/custom/_shortcodes.js',
      'assets/js/vendor/swiper/swiper.js'
  ];
  tentang_kami_js = default_js.concat(tentang_kami_js);
  var tentang_kami_file = tentang_kami_js;

  // End tentang_kami
  // Begin kontak

  var kontak_js = [
      'assets/sweet-alert/sweet-alert.min.js',
      'assets/js/custom/kirim_email.js',
      'assets/js/custom/iframe.js'
  ];
  kontak_js = default_js.concat(kontak_js);
  var kontak_css = [
      'assets/sweet-alert/sweet-alert.css',
      'assets/css/loader-send-email.css'
  ];
  kontak_css = default_css.concat(kontak_css);
  var kontak_file = kontak_js.concat(kontak_css);

  // End kontak

  var all_file = layanan_detail_file.concat(galeri_file);

  grunt.initConfig({
    jsDistDir: 'assets/js/',
    cssDistDir: 'assets/css/',
    pkg: grunt.file.readJSON('package.json'),
    concat: {
            default_js: {
                options: {
                    separator: ';'
                },
                src: default_js,
                dest: '<%=jsDistDir%>default.js'
            },
            default_css: {
                src: default_css,
                dest: '<%=cssDistDir%>default.css'
            },
            beranda_js: {
                options: {
                    separator: ';'
                },
                src: beranda_js,
                dest: '<%=jsDistDir%>beranda.js'
            },
            beranda_css: {
                src: beranda_css,
                dest: '<%=cssDistDir%>beranda.css'
            },
            layanan_detail_js: {
                options: {
                    separator: ';'
                },
                src: layanan_detail_js,
                dest: '<%=jsDistDir%>layanan_detail.js'
            },
            layanan_detail_css: {
                src: layanan_detail_css,
                dest: '<%=cssDistDir%>layanan_detail.css'
            },
            galeri_js: {
                options: {
                    separator: ';'
                },
                src: galeri_js,
                dest: '<%=jsDistDir%>galeri.js'
            },
            galeri_css: {
                src: galeri_css,
                dest: '<%=cssDistDir%>galeri.css'
            },
            artikel_js: {
                options: {
                    separator: ';'
                },
                src: artikel_js,
                dest: '<%=jsDistDir%>artikel.js'
            },
            artikel_detail_js: {
                options: {
                    separator: ';'
                },
                src: artikel_detail_js,
                dest: '<%=jsDistDir%>artikel_detail.js'
            },
            artikel_detail_css: {
                src: artikel_detail_css,
                dest: '<%=cssDistDir%>artikel_detail.css'
            },
            faq_js: {
                options: {
                    separator: ';'
                },
                src: faq_js,
                dest: '<%=jsDistDir%>faq.js'
            },
            tentang_kami_js: {
                options: {
                    separator: ';'
                },
                src: tentang_kami_js,
                dest: '<%=jsDistDir%>tentang_kami.js'
            },
            kontak_js: {
                options: {
                    separator: ';'
                },
                src: kontak_js,
                dest: '<%=jsDistDir%>kontak.js'
            },
            kontak_css: {
                src: kontak_css,
                dest: '<%=cssDistDir%>kontak.css'
            },
        },
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%=grunt.template.today("dd-mm-yyyy") %> */\n'
      },
      dist: {
        files: {
          '<%=jsDistDir%>default.min.js': ['<%= concat.default_js.dest %>'],
          '<%=jsDistDir%>beranda.min.js': ['<%= concat.beranda_js.dest %>'],
          '<%=jsDistDir%>layanan_detail.min.js': ['<%= concat.layanan_detail_js.dest %>'],
          '<%=jsDistDir%>galeri.min.js': ['<%= concat.galeri_js.dest %>'],
          '<%=jsDistDir%>artikel.min.js': ['<%= concat.artikel_js.dest %>'],
          '<%=jsDistDir%>artikel_detail.min.js': ['<%= concat.artikel_detail_js.dest %>'],
          '<%=jsDistDir%>faq.min.js': ['<%= concat.faq_js.dest %>'],
          '<%=jsDistDir%>tentang_kami.min.js': ['<%= concat.tentang_kami_js.dest %>'],
          '<%=jsDistDir%>kontak.min.js': ['<%= concat.kontak_js.dest %>']
        }
      }
    },
    cssmin: {
      add_banner: {
        options: {
          banner: '/*! <%= pkg.name %> <%=grunt.template.today("dd-mm-yyyy") %> */\n'
        },
        files: {
          '<%=cssDistDir%>default.min.css': ['<%= concat.default_css.dest %>'],
          '<%=cssDistDir%>beranda.min.css': ['<%= concat.beranda_css.dest %>'],
          '<%=cssDistDir%>layanan_detail.min.css': ['<%= concat.layanan_detail_css.dest %>'],
          '<%=cssDistDir%>galeri.min.css': ['<%= concat.galeri_css.dest %>'],
          '<%=cssDistDir%>artikel_detail.min.css': ['<%= concat.artikel_detail_css.dest %>'],
          '<%=cssDistDir%>kontak.min.css': ['<%= concat.kontak_css.dest %>']
        }
      }
    },
    watch: {
      files: all_file,
      tasks: ['concat', 'uglify', 'cssmin']
    }
  });

  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-watch');

  grunt.registerTask('default', [
      'concat',
      'uglify',
      'cssmin',
      'watch'
  ]);

};
  