$(document).ready(function(e) {
  $("#kirim_email_konsultasi").submit(function(e) {
  		e.preventDefault();
        vardata = $(this).serialize();
        $('.loader-send').css('display','block');
		$.ajax({
			url: $(this).attr("action"),
			type: 'POST',
			data: vardata,
			beforeSend: function(){
				$('.loader-send').css('display','block');
			},
			success: function(data){
				$('.loader-send').css('display','none');
				var json = JSON.parse(data);
				var ke   = "ke";
				if(json.status == 'error'){
					swal("Failed", json.alert, "warning");
					
					$("span.form-error:eq(0)").html(json.nama).fadeIn("normal");
					$("span.form-error:eq(1)").html(json.email).fadeIn("normal");
					$("span.form-error:eq(2)").html(json.phone).fadeIn("normal");
					$("span.form-error:eq(3)").html(json.comment).fadeIn("normal");
					$("span.form-error:eq(4)").html(json.captcha).fadeIn("normal");
					
					setTimeout(function(){
						$("input, textarea").removeClass("error-border");
						
					}, 5000);
				}else if(json.status == 'success'){
					
					swal({
						title: "Success",
						text: json.alert,
						type: "success",
					}, function() {
						window.location.reload();
					});
				}
			}
		});
		return false;
  });
});

