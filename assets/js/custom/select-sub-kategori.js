$(document).ready(function(){ 
    $('.kategori_layanan_red').change(function(){ 
        var id=$(this).val(); 
        var base_url = $('#base-value').data('base-url');
        $.ajax({
            url : base_url+"Layanan/get_subkategori",
            method : "POST",
            data : {id: id},
            async : false,
            dataType : 'json',
            success: function(data){
                var html = '';
                var i;
                for(i=0; i<data.length; i++){
                    html += '<option>'+data[i]+'</option>';
                }
                $('.subkategori').html(html);
                 
            }
        });
    });
});