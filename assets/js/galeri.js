/*! jQuery v3.4.1 | (c) JS Foundation and other contributors | jquery.org/license */
!function(e,t){"use strict";"object"==typeof module&&"object"==typeof module.exports?module.exports=e.document?t(e,!0):function(e){if(!e.document)throw new Error("jQuery requires a window with a document");return t(e)}:t(e)}("undefined"!=typeof window?window:this,function(C,e){"use strict";var t=[],E=C.document,r=Object.getPrototypeOf,s=t.slice,g=t.concat,u=t.push,i=t.indexOf,n={},o=n.toString,v=n.hasOwnProperty,a=v.toString,l=a.call(Object),y={},m=function(e){return"function"==typeof e&&"number"!=typeof e.nodeType},x=function(e){return null!=e&&e===e.window},c={type:!0,src:!0,nonce:!0,noModule:!0};function b(e,t,n){var r,i,o=(n=n||E).createElement("script");if(o.text=e,t)for(r in c)(i=t[r]||t.getAttribute&&t.getAttribute(r))&&o.setAttribute(r,i);n.head.appendChild(o).parentNode.removeChild(o)}function w(e){return null==e?e+"":"object"==typeof e||"function"==typeof e?n[o.call(e)]||"object":typeof e}var f="3.4.1",k=function(e,t){return new k.fn.init(e,t)},p=/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;function d(e){var t=!!e&&"length"in e&&e.length,n=w(e);return!m(e)&&!x(e)&&("array"===n||0===t||"number"==typeof t&&0<t&&t-1 in e)}k.fn=k.prototype={jquery:f,constructor:k,length:0,toArray:function(){return s.call(this)},get:function(e){return null==e?s.call(this):e<0?this[e+this.length]:this[e]},pushStack:function(e){var t=k.merge(this.constructor(),e);return t.prevObject=this,t},each:function(e){return k.each(this,e)},map:function(n){return this.pushStack(k.map(this,function(e,t){return n.call(e,t,e)}))},slice:function(){return this.pushStack(s.apply(this,arguments))},first:function(){return this.eq(0)},last:function(){return this.eq(-1)},eq:function(e){var t=this.length,n=+e+(e<0?t:0);return this.pushStack(0<=n&&n<t?[this[n]]:[])},end:function(){return this.prevObject||this.constructor()},push:u,sort:t.sort,splice:t.splice},k.extend=k.fn.extend=function(){var e,t,n,r,i,o,a=arguments[0]||{},s=1,u=arguments.length,l=!1;for("boolean"==typeof a&&(l=a,a=arguments[s]||{},s++),"object"==typeof a||m(a)||(a={}),s===u&&(a=this,s--);s<u;s++)if(null!=(e=arguments[s]))for(t in e)r=e[t],"__proto__"!==t&&a!==r&&(l&&r&&(k.isPlainObject(r)||(i=Array.isArray(r)))?(n=a[t],o=i&&!Array.isArray(n)?[]:i||k.isPlainObject(n)?n:{},i=!1,a[t]=k.extend(l,o,r)):void 0!==r&&(a[t]=r));return a},k.extend({expando:"jQuery"+(f+Math.random()).replace(/\D/g,""),isReady:!0,error:function(e){throw new Error(e)},noop:function(){},isPlainObject:function(e){var t,n;return!(!e||"[object Object]"!==o.call(e))&&(!(t=r(e))||"function"==typeof(n=v.call(t,"constructor")&&t.constructor)&&a.call(n)===l)},isEmptyObject:function(e){var t;for(t in e)return!1;return!0},globalEval:function(e,t){b(e,{nonce:t&&t.nonce})},each:function(e,t){var n,r=0;if(d(e)){for(n=e.length;r<n;r++)if(!1===t.call(e[r],r,e[r]))break}else for(r in e)if(!1===t.call(e[r],r,e[r]))break;return e},trim:function(e){return null==e?"":(e+"").replace(p,"")},makeArray:function(e,t){var n=t||[];return null!=e&&(d(Object(e))?k.merge(n,"string"==typeof e?[e]:e):u.call(n,e)),n},inArray:function(e,t,n){return null==t?-1:i.call(t,e,n)},merge:function(e,t){for(var n=+t.length,r=0,i=e.length;r<n;r++)e[i++]=t[r];return e.length=i,e},grep:function(e,t,n){for(var r=[],i=0,o=e.length,a=!n;i<o;i++)!t(e[i],i)!==a&&r.push(e[i]);return r},map:function(e,t,n){var r,i,o=0,a=[];if(d(e))for(r=e.length;o<r;o++)null!=(i=t(e[o],o,n))&&a.push(i);else for(o in e)null!=(i=t(e[o],o,n))&&a.push(i);return g.apply([],a)},guid:1,support:y}),"function"==typeof Symbol&&(k.fn[Symbol.iterator]=t[Symbol.iterator]),k.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "),function(e,t){n["[object "+t+"]"]=t.toLowerCase()});var h=function(n){var e,d,b,o,i,h,f,g,w,u,l,T,C,a,E,v,s,c,y,k="sizzle"+1*new Date,m=n.document,S=0,r=0,p=ue(),x=ue(),N=ue(),A=ue(),D=function(e,t){return e===t&&(l=!0),0},j={}.hasOwnProperty,t=[],q=t.pop,L=t.push,H=t.push,O=t.slice,P=function(e,t){for(var n=0,r=e.length;n<r;n++)if(e[n]===t)return n;return-1},R="checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",M="[\\x20\\t\\r\\n\\f]",I="(?:\\\\.|[\\w-]|[^\0-\\xa0])+",W="\\["+M+"*("+I+")(?:"+M+"*([*^$|!~]?=)"+M+"*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|("+I+"))|)"+M+"*\\]",$=":("+I+")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|"+W+")*)|.*)\\)|)",F=new RegExp(M+"+","g"),B=new RegExp("^"+M+"+|((?:^|[^\\\\])(?:\\\\.)*)"+M+"+$","g"),_=new RegExp("^"+M+"*,"+M+"*"),z=new RegExp("^"+M+"*([>+~]|"+M+")"+M+"*"),U=new RegExp(M+"|>"),X=new RegExp($),V=new RegExp("^"+I+"$"),G={ID:new RegExp("^#("+I+")"),CLASS:new RegExp("^\\.("+I+")"),TAG:new RegExp("^("+I+"|[*])"),ATTR:new RegExp("^"+W),PSEUDO:new RegExp("^"+$),CHILD:new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\("+M+"*(even|odd|(([+-]|)(\\d*)n|)"+M+"*(?:([+-]|)"+M+"*(\\d+)|))"+M+"*\\)|)","i"),bool:new RegExp("^(?:"+R+")$","i"),needsContext:new RegExp("^"+M+"*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\("+M+"*((?:-\\d)?\\d*)"+M+"*\\)|)(?=[^-]|$)","i")},Y=/HTML$/i,Q=/^(?:input|select|textarea|button)$/i,J=/^h\d$/i,K=/^[^{]+\{\s*\[native \w/,Z=/^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,ee=/[+~]/,te=new RegExp("\\\\([\\da-f]{1,6}"+M+"?|("+M+")|.)","ig"),ne=function(e,t,n){var r="0x"+t-65536;return r!=r||n?t:r<0?String.fromCharCode(r+65536):String.fromCharCode(r>>10|55296,1023&r|56320)},re=/([\0-\x1f\x7f]|^-?\d)|^-$|[^\0-\x1f\x7f-\uFFFF\w-]/g,ie=function(e,t){return t?"\0"===e?"\ufffd":e.slice(0,-1)+"\\"+e.charCodeAt(e.length-1).toString(16)+" ":"\\"+e},oe=function(){T()},ae=be(function(e){return!0===e.disabled&&"fieldset"===e.nodeName.toLowerCase()},{dir:"parentNode",next:"legend"});try{H.apply(t=O.call(m.childNodes),m.childNodes),t[m.childNodes.length].nodeType}catch(e){H={apply:t.length?function(e,t){L.apply(e,O.call(t))}:function(e,t){var n=e.length,r=0;while(e[n++]=t[r++]);e.length=n-1}}}function se(t,e,n,r){var i,o,a,s,u,l,c,f=e&&e.ownerDocument,p=e?e.nodeType:9;if(n=n||[],"string"!=typeof t||!t||1!==p&&9!==p&&11!==p)return n;if(!r&&((e?e.ownerDocument||e:m)!==C&&T(e),e=e||C,E)){if(11!==p&&(u=Z.exec(t)))if(i=u[1]){if(9===p){if(!(a=e.getElementById(i)))return n;if(a.id===i)return n.push(a),n}else if(f&&(a=f.getElementById(i))&&y(e,a)&&a.id===i)return n.push(a),n}else{if(u[2])return H.apply(n,e.getElementsByTagName(t)),n;if((i=u[3])&&d.getElementsByClassName&&e.getElementsByClassName)return H.apply(n,e.getElementsByClassName(i)),n}if(d.qsa&&!A[t+" "]&&(!v||!v.test(t))&&(1!==p||"object"!==e.nodeName.toLowerCase())){if(c=t,f=e,1===p&&U.test(t)){(s=e.getAttribute("id"))?s=s.replace(re,ie):e.setAttribute("id",s=k),o=(l=h(t)).length;while(o--)l[o]="#"+s+" "+xe(l[o]);c=l.join(","),f=ee.test(t)&&ye(e.parentNode)||e}try{return H.apply(n,f.querySelectorAll(c)),n}catch(e){A(t,!0)}finally{s===k&&e.removeAttribute("id")}}}return g(t.replace(B,"$1"),e,n,r)}function ue(){var r=[];return function e(t,n){return r.push(t+" ")>b.cacheLength&&delete e[r.shift()],e[t+" "]=n}}function le(e){return e[k]=!0,e}function ce(e){var t=C.createElement("fieldset");try{return!!e(t)}catch(e){return!1}finally{t.parentNode&&t.parentNode.removeChild(t),t=null}}function fe(e,t){var n=e.split("|"),r=n.length;while(r--)b.attrHandle[n[r]]=t}function pe(e,t){var n=t&&e,r=n&&1===e.nodeType&&1===t.nodeType&&e.sourceIndex-t.sourceIndex;if(r)return r;if(n)while(n=n.nextSibling)if(n===t)return-1;return e?1:-1}function de(t){return function(e){return"input"===e.nodeName.toLowerCase()&&e.type===t}}function he(n){return function(e){var t=e.nodeName.toLowerCase();return("input"===t||"button"===t)&&e.type===n}}function ge(t){return function(e){return"form"in e?e.parentNode&&!1===e.disabled?"label"in e?"label"in e.parentNode?e.parentNode.disabled===t:e.disabled===t:e.isDisabled===t||e.isDisabled!==!t&&ae(e)===t:e.disabled===t:"label"in e&&e.disabled===t}}function ve(a){return le(function(o){return o=+o,le(function(e,t){var n,r=a([],e.length,o),i=r.length;while(i--)e[n=r[i]]&&(e[n]=!(t[n]=e[n]))})})}function ye(e){return e&&"undefined"!=typeof e.getElementsByTagName&&e}for(e in d=se.support={},i=se.isXML=function(e){var t=e.namespaceURI,n=(e.ownerDocument||e).documentElement;return!Y.test(t||n&&n.nodeName||"HTML")},T=se.setDocument=function(e){var t,n,r=e?e.ownerDocument||e:m;return r!==C&&9===r.nodeType&&r.documentElement&&(a=(C=r).documentElement,E=!i(C),m!==C&&(n=C.defaultView)&&n.top!==n&&(n.addEventListener?n.addEventListener("unload",oe,!1):n.attachEvent&&n.attachEvent("onunload",oe)),d.attributes=ce(function(e){return e.className="i",!e.getAttribute("className")}),d.getElementsByTagName=ce(function(e){return e.appendChild(C.createComment("")),!e.getElementsByTagName("*").length}),d.getElementsByClassName=K.test(C.getElementsByClassName),d.getById=ce(function(e){return a.appendChild(e).id=k,!C.getElementsByName||!C.getElementsByName(k).length}),d.getById?(b.filter.ID=function(e){var t=e.replace(te,ne);return function(e){return e.getAttribute("id")===t}},b.find.ID=function(e,t){if("undefined"!=typeof t.getElementById&&E){var n=t.getElementById(e);return n?[n]:[]}}):(b.filter.ID=function(e){var n=e.replace(te,ne);return function(e){var t="undefined"!=typeof e.getAttributeNode&&e.getAttributeNode("id");return t&&t.value===n}},b.find.ID=function(e,t){if("undefined"!=typeof t.getElementById&&E){var n,r,i,o=t.getElementById(e);if(o){if((n=o.getAttributeNode("id"))&&n.value===e)return[o];i=t.getElementsByName(e),r=0;while(o=i[r++])if((n=o.getAttributeNode("id"))&&n.value===e)return[o]}return[]}}),b.find.TAG=d.getElementsByTagName?function(e,t){return"undefined"!=typeof t.getElementsByTagName?t.getElementsByTagName(e):d.qsa?t.querySelectorAll(e):void 0}:function(e,t){var n,r=[],i=0,o=t.getElementsByTagName(e);if("*"===e){while(n=o[i++])1===n.nodeType&&r.push(n);return r}return o},b.find.CLASS=d.getElementsByClassName&&function(e,t){if("undefined"!=typeof t.getElementsByClassName&&E)return t.getElementsByClassName(e)},s=[],v=[],(d.qsa=K.test(C.querySelectorAll))&&(ce(function(e){a.appendChild(e).innerHTML="<a id='"+k+"'></a><select id='"+k+"-\r\\' msallowcapture=''><option selected=''></option></select>",e.querySelectorAll("[msallowcapture^='']").length&&v.push("[*^$]="+M+"*(?:''|\"\")"),e.querySelectorAll("[selected]").length||v.push("\\["+M+"*(?:value|"+R+")"),e.querySelectorAll("[id~="+k+"-]").length||v.push("~="),e.querySelectorAll(":checked").length||v.push(":checked"),e.querySelectorAll("a#"+k+"+*").length||v.push(".#.+[+~]")}),ce(function(e){e.innerHTML="<a href='' disabled='disabled'></a><select disabled='disabled'><option/></select>";var t=C.createElement("input");t.setAttribute("type","hidden"),e.appendChild(t).setAttribute("name","D"),e.querySelectorAll("[name=d]").length&&v.push("name"+M+"*[*^$|!~]?="),2!==e.querySelectorAll(":enabled").length&&v.push(":enabled",":disabled"),a.appendChild(e).disabled=!0,2!==e.querySelectorAll(":disabled").length&&v.push(":enabled",":disabled"),e.querySelectorAll("*,:x"),v.push(",.*:")})),(d.matchesSelector=K.test(c=a.matches||a.webkitMatchesSelector||a.mozMatchesSelector||a.oMatchesSelector||a.msMatchesSelector))&&ce(function(e){d.disconnectedMatch=c.call(e,"*"),c.call(e,"[s!='']:x"),s.push("!=",$)}),v=v.length&&new RegExp(v.join("|")),s=s.length&&new RegExp(s.join("|")),t=K.test(a.compareDocumentPosition),y=t||K.test(a.contains)?function(e,t){var n=9===e.nodeType?e.documentElement:e,r=t&&t.parentNode;return e===r||!(!r||1!==r.nodeType||!(n.contains?n.contains(r):e.compareDocumentPosition&&16&e.compareDocumentPosition(r)))}:function(e,t){if(t)while(t=t.parentNode)if(t===e)return!0;return!1},D=t?function(e,t){if(e===t)return l=!0,0;var n=!e.compareDocumentPosition-!t.compareDocumentPosition;return n||(1&(n=(e.ownerDocument||e)===(t.ownerDocument||t)?e.compareDocumentPosition(t):1)||!d.sortDetached&&t.compareDocumentPosition(e)===n?e===C||e.ownerDocument===m&&y(m,e)?-1:t===C||t.ownerDocument===m&&y(m,t)?1:u?P(u,e)-P(u,t):0:4&n?-1:1)}:function(e,t){if(e===t)return l=!0,0;var n,r=0,i=e.parentNode,o=t.parentNode,a=[e],s=[t];if(!i||!o)return e===C?-1:t===C?1:i?-1:o?1:u?P(u,e)-P(u,t):0;if(i===o)return pe(e,t);n=e;while(n=n.parentNode)a.unshift(n);n=t;while(n=n.parentNode)s.unshift(n);while(a[r]===s[r])r++;return r?pe(a[r],s[r]):a[r]===m?-1:s[r]===m?1:0}),C},se.matches=function(e,t){return se(e,null,null,t)},se.matchesSelector=function(e,t){if((e.ownerDocument||e)!==C&&T(e),d.matchesSelector&&E&&!A[t+" "]&&(!s||!s.test(t))&&(!v||!v.test(t)))try{var n=c.call(e,t);if(n||d.disconnectedMatch||e.document&&11!==e.document.nodeType)return n}catch(e){A(t,!0)}return 0<se(t,C,null,[e]).length},se.contains=function(e,t){return(e.ownerDocument||e)!==C&&T(e),y(e,t)},se.attr=function(e,t){(e.ownerDocument||e)!==C&&T(e);var n=b.attrHandle[t.toLowerCase()],r=n&&j.call(b.attrHandle,t.toLowerCase())?n(e,t,!E):void 0;return void 0!==r?r:d.attributes||!E?e.getAttribute(t):(r=e.getAttributeNode(t))&&r.specified?r.value:null},se.escape=function(e){return(e+"").replace(re,ie)},se.error=function(e){throw new Error("Syntax error, unrecognized expression: "+e)},se.uniqueSort=function(e){var t,n=[],r=0,i=0;if(l=!d.detectDuplicates,u=!d.sortStable&&e.slice(0),e.sort(D),l){while(t=e[i++])t===e[i]&&(r=n.push(i));while(r--)e.splice(n[r],1)}return u=null,e},o=se.getText=function(e){var t,n="",r=0,i=e.nodeType;if(i){if(1===i||9===i||11===i){if("string"==typeof e.textContent)return e.textContent;for(e=e.firstChild;e;e=e.nextSibling)n+=o(e)}else if(3===i||4===i)return e.nodeValue}else while(t=e[r++])n+=o(t);return n},(b=se.selectors={cacheLength:50,createPseudo:le,match:G,attrHandle:{},find:{},relative:{">":{dir:"parentNode",first:!0}," ":{dir:"parentNode"},"+":{dir:"previousSibling",first:!0},"~":{dir:"previousSibling"}},preFilter:{ATTR:function(e){return e[1]=e[1].replace(te,ne),e[3]=(e[3]||e[4]||e[5]||"").replace(te,ne),"~="===e[2]&&(e[3]=" "+e[3]+" "),e.slice(0,4)},CHILD:function(e){return e[1]=e[1].toLowerCase(),"nth"===e[1].slice(0,3)?(e[3]||se.error(e[0]),e[4]=+(e[4]?e[5]+(e[6]||1):2*("even"===e[3]||"odd"===e[3])),e[5]=+(e[7]+e[8]||"odd"===e[3])):e[3]&&se.error(e[0]),e},PSEUDO:function(e){var t,n=!e[6]&&e[2];return G.CHILD.test(e[0])?null:(e[3]?e[2]=e[4]||e[5]||"":n&&X.test(n)&&(t=h(n,!0))&&(t=n.indexOf(")",n.length-t)-n.length)&&(e[0]=e[0].slice(0,t),e[2]=n.slice(0,t)),e.slice(0,3))}},filter:{TAG:function(e){var t=e.replace(te,ne).toLowerCase();return"*"===e?function(){return!0}:function(e){return e.nodeName&&e.nodeName.toLowerCase()===t}},CLASS:function(e){var t=p[e+" "];return t||(t=new RegExp("(^|"+M+")"+e+"("+M+"|$)"))&&p(e,function(e){return t.test("string"==typeof e.className&&e.className||"undefined"!=typeof e.getAttribute&&e.getAttribute("class")||"")})},ATTR:function(n,r,i){return function(e){var t=se.attr(e,n);return null==t?"!="===r:!r||(t+="","="===r?t===i:"!="===r?t!==i:"^="===r?i&&0===t.indexOf(i):"*="===r?i&&-1<t.indexOf(i):"$="===r?i&&t.slice(-i.length)===i:"~="===r?-1<(" "+t.replace(F," ")+" ").indexOf(i):"|="===r&&(t===i||t.slice(0,i.length+1)===i+"-"))}},CHILD:function(h,e,t,g,v){var y="nth"!==h.slice(0,3),m="last"!==h.slice(-4),x="of-type"===e;return 1===g&&0===v?function(e){return!!e.parentNode}:function(e,t,n){var r,i,o,a,s,u,l=y!==m?"nextSibling":"previousSibling",c=e.parentNode,f=x&&e.nodeName.toLowerCase(),p=!n&&!x,d=!1;if(c){if(y){while(l){a=e;while(a=a[l])if(x?a.nodeName.toLowerCase()===f:1===a.nodeType)return!1;u=l="only"===h&&!u&&"nextSibling"}return!0}if(u=[m?c.firstChild:c.lastChild],m&&p){d=(s=(r=(i=(o=(a=c)[k]||(a[k]={}))[a.uniqueID]||(o[a.uniqueID]={}))[h]||[])[0]===S&&r[1])&&r[2],a=s&&c.childNodes[s];while(a=++s&&a&&a[l]||(d=s=0)||u.pop())if(1===a.nodeType&&++d&&a===e){i[h]=[S,s,d];break}}else if(p&&(d=s=(r=(i=(o=(a=e)[k]||(a[k]={}))[a.uniqueID]||(o[a.uniqueID]={}))[h]||[])[0]===S&&r[1]),!1===d)while(a=++s&&a&&a[l]||(d=s=0)||u.pop())if((x?a.nodeName.toLowerCase()===f:1===a.nodeType)&&++d&&(p&&((i=(o=a[k]||(a[k]={}))[a.uniqueID]||(o[a.uniqueID]={}))[h]=[S,d]),a===e))break;return(d-=v)===g||d%g==0&&0<=d/g}}},PSEUDO:function(e,o){var t,a=b.pseudos[e]||b.setFilters[e.toLowerCase()]||se.error("unsupported pseudo: "+e);return a[k]?a(o):1<a.length?(t=[e,e,"",o],b.setFilters.hasOwnProperty(e.toLowerCase())?le(function(e,t){var n,r=a(e,o),i=r.length;while(i--)e[n=P(e,r[i])]=!(t[n]=r[i])}):function(e){return a(e,0,t)}):a}},pseudos:{not:le(function(e){var r=[],i=[],s=f(e.replace(B,"$1"));return s[k]?le(function(e,t,n,r){var i,o=s(e,null,r,[]),a=e.length;while(a--)(i=o[a])&&(e[a]=!(t[a]=i))}):function(e,t,n){return r[0]=e,s(r,null,n,i),r[0]=null,!i.pop()}}),has:le(function(t){return function(e){return 0<se(t,e).length}}),contains:le(function(t){return t=t.replace(te,ne),function(e){return-1<(e.textContent||o(e)).indexOf(t)}}),lang:le(function(n){return V.test(n||"")||se.error("unsupported lang: "+n),n=n.replace(te,ne).toLowerCase(),function(e){var t;do{if(t=E?e.lang:e.getAttribute("xml:lang")||e.getAttribute("lang"))return(t=t.toLowerCase())===n||0===t.indexOf(n+"-")}while((e=e.parentNode)&&1===e.nodeType);return!1}}),target:function(e){var t=n.location&&n.location.hash;return t&&t.slice(1)===e.id},root:function(e){return e===a},focus:function(e){return e===C.activeElement&&(!C.hasFocus||C.hasFocus())&&!!(e.type||e.href||~e.tabIndex)},enabled:ge(!1),disabled:ge(!0),checked:function(e){var t=e.nodeName.toLowerCase();return"input"===t&&!!e.checked||"option"===t&&!!e.selected},selected:function(e){return e.parentNode&&e.parentNode.selectedIndex,!0===e.selected},empty:function(e){for(e=e.firstChild;e;e=e.nextSibling)if(e.nodeType<6)return!1;return!0},parent:function(e){return!b.pseudos.empty(e)},header:function(e){return J.test(e.nodeName)},input:function(e){return Q.test(e.nodeName)},button:function(e){var t=e.nodeName.toLowerCase();return"input"===t&&"button"===e.type||"button"===t},text:function(e){var t;return"input"===e.nodeName.toLowerCase()&&"text"===e.type&&(null==(t=e.getAttribute("type"))||"text"===t.toLowerCase())},first:ve(function(){return[0]}),last:ve(function(e,t){return[t-1]}),eq:ve(function(e,t,n){return[n<0?n+t:n]}),even:ve(function(e,t){for(var n=0;n<t;n+=2)e.push(n);return e}),odd:ve(function(e,t){for(var n=1;n<t;n+=2)e.push(n);return e}),lt:ve(function(e,t,n){for(var r=n<0?n+t:t<n?t:n;0<=--r;)e.push(r);return e}),gt:ve(function(e,t,n){for(var r=n<0?n+t:n;++r<t;)e.push(r);return e})}}).pseudos.nth=b.pseudos.eq,{radio:!0,checkbox:!0,file:!0,password:!0,image:!0})b.pseudos[e]=de(e);for(e in{submit:!0,reset:!0})b.pseudos[e]=he(e);function me(){}function xe(e){for(var t=0,n=e.length,r="";t<n;t++)r+=e[t].value;return r}function be(s,e,t){var u=e.dir,l=e.next,c=l||u,f=t&&"parentNode"===c,p=r++;return e.first?function(e,t,n){while(e=e[u])if(1===e.nodeType||f)return s(e,t,n);return!1}:function(e,t,n){var r,i,o,a=[S,p];if(n){while(e=e[u])if((1===e.nodeType||f)&&s(e,t,n))return!0}else while(e=e[u])if(1===e.nodeType||f)if(i=(o=e[k]||(e[k]={}))[e.uniqueID]||(o[e.uniqueID]={}),l&&l===e.nodeName.toLowerCase())e=e[u]||e;else{if((r=i[c])&&r[0]===S&&r[1]===p)return a[2]=r[2];if((i[c]=a)[2]=s(e,t,n))return!0}return!1}}function we(i){return 1<i.length?function(e,t,n){var r=i.length;while(r--)if(!i[r](e,t,n))return!1;return!0}:i[0]}function Te(e,t,n,r,i){for(var o,a=[],s=0,u=e.length,l=null!=t;s<u;s++)(o=e[s])&&(n&&!n(o,r,i)||(a.push(o),l&&t.push(s)));return a}function Ce(d,h,g,v,y,e){return v&&!v[k]&&(v=Ce(v)),y&&!y[k]&&(y=Ce(y,e)),le(function(e,t,n,r){var i,o,a,s=[],u=[],l=t.length,c=e||function(e,t,n){for(var r=0,i=t.length;r<i;r++)se(e,t[r],n);return n}(h||"*",n.nodeType?[n]:n,[]),f=!d||!e&&h?c:Te(c,s,d,n,r),p=g?y||(e?d:l||v)?[]:t:f;if(g&&g(f,p,n,r),v){i=Te(p,u),v(i,[],n,r),o=i.length;while(o--)(a=i[o])&&(p[u[o]]=!(f[u[o]]=a))}if(e){if(y||d){if(y){i=[],o=p.length;while(o--)(a=p[o])&&i.push(f[o]=a);y(null,p=[],i,r)}o=p.length;while(o--)(a=p[o])&&-1<(i=y?P(e,a):s[o])&&(e[i]=!(t[i]=a))}}else p=Te(p===t?p.splice(l,p.length):p),y?y(null,t,p,r):H.apply(t,p)})}function Ee(e){for(var i,t,n,r=e.length,o=b.relative[e[0].type],a=o||b.relative[" "],s=o?1:0,u=be(function(e){return e===i},a,!0),l=be(function(e){return-1<P(i,e)},a,!0),c=[function(e,t,n){var r=!o&&(n||t!==w)||((i=t).nodeType?u(e,t,n):l(e,t,n));return i=null,r}];s<r;s++)if(t=b.relative[e[s].type])c=[be(we(c),t)];else{if((t=b.filter[e[s].type].apply(null,e[s].matches))[k]){for(n=++s;n<r;n++)if(b.relative[e[n].type])break;return Ce(1<s&&we(c),1<s&&xe(e.slice(0,s-1).concat({value:" "===e[s-2].type?"*":""})).replace(B,"$1"),t,s<n&&Ee(e.slice(s,n)),n<r&&Ee(e=e.slice(n)),n<r&&xe(e))}c.push(t)}return we(c)}return me.prototype=b.filters=b.pseudos,b.setFilters=new me,h=se.tokenize=function(e,t){var n,r,i,o,a,s,u,l=x[e+" "];if(l)return t?0:l.slice(0);a=e,s=[],u=b.preFilter;while(a){for(o in n&&!(r=_.exec(a))||(r&&(a=a.slice(r[0].length)||a),s.push(i=[])),n=!1,(r=z.exec(a))&&(n=r.shift(),i.push({value:n,type:r[0].replace(B," ")}),a=a.slice(n.length)),b.filter)!(r=G[o].exec(a))||u[o]&&!(r=u[o](r))||(n=r.shift(),i.push({value:n,type:o,matches:r}),a=a.slice(n.length));if(!n)break}return t?a.length:a?se.error(e):x(e,s).slice(0)},f=se.compile=function(e,t){var n,v,y,m,x,r,i=[],o=[],a=N[e+" "];if(!a){t||(t=h(e)),n=t.length;while(n--)(a=Ee(t[n]))[k]?i.push(a):o.push(a);(a=N(e,(v=o,m=0<(y=i).length,x=0<v.length,r=function(e,t,n,r,i){var o,a,s,u=0,l="0",c=e&&[],f=[],p=w,d=e||x&&b.find.TAG("*",i),h=S+=null==p?1:Math.random()||.1,g=d.length;for(i&&(w=t===C||t||i);l!==g&&null!=(o=d[l]);l++){if(x&&o){a=0,t||o.ownerDocument===C||(T(o),n=!E);while(s=v[a++])if(s(o,t||C,n)){r.push(o);break}i&&(S=h)}m&&((o=!s&&o)&&u--,e&&c.push(o))}if(u+=l,m&&l!==u){a=0;while(s=y[a++])s(c,f,t,n);if(e){if(0<u)while(l--)c[l]||f[l]||(f[l]=q.call(r));f=Te(f)}H.apply(r,f),i&&!e&&0<f.length&&1<u+y.length&&se.uniqueSort(r)}return i&&(S=h,w=p),c},m?le(r):r))).selector=e}return a},g=se.select=function(e,t,n,r){var i,o,a,s,u,l="function"==typeof e&&e,c=!r&&h(e=l.selector||e);if(n=n||[],1===c.length){if(2<(o=c[0]=c[0].slice(0)).length&&"ID"===(a=o[0]).type&&9===t.nodeType&&E&&b.relative[o[1].type]){if(!(t=(b.find.ID(a.matches[0].replace(te,ne),t)||[])[0]))return n;l&&(t=t.parentNode),e=e.slice(o.shift().value.length)}i=G.needsContext.test(e)?0:o.length;while(i--){if(a=o[i],b.relative[s=a.type])break;if((u=b.find[s])&&(r=u(a.matches[0].replace(te,ne),ee.test(o[0].type)&&ye(t.parentNode)||t))){if(o.splice(i,1),!(e=r.length&&xe(o)))return H.apply(n,r),n;break}}}return(l||f(e,c))(r,t,!E,n,!t||ee.test(e)&&ye(t.parentNode)||t),n},d.sortStable=k.split("").sort(D).join("")===k,d.detectDuplicates=!!l,T(),d.sortDetached=ce(function(e){return 1&e.compareDocumentPosition(C.createElement("fieldset"))}),ce(function(e){return e.innerHTML="<a href='#'></a>","#"===e.firstChild.getAttribute("href")})||fe("type|href|height|width",function(e,t,n){if(!n)return e.getAttribute(t,"type"===t.toLowerCase()?1:2)}),d.attributes&&ce(function(e){return e.innerHTML="<input/>",e.firstChild.setAttribute("value",""),""===e.firstChild.getAttribute("value")})||fe("value",function(e,t,n){if(!n&&"input"===e.nodeName.toLowerCase())return e.defaultValue}),ce(function(e){return null==e.getAttribute("disabled")})||fe(R,function(e,t,n){var r;if(!n)return!0===e[t]?t.toLowerCase():(r=e.getAttributeNode(t))&&r.specified?r.value:null}),se}(C);k.find=h,k.expr=h.selectors,k.expr[":"]=k.expr.pseudos,k.uniqueSort=k.unique=h.uniqueSort,k.text=h.getText,k.isXMLDoc=h.isXML,k.contains=h.contains,k.escapeSelector=h.escape;var T=function(e,t,n){var r=[],i=void 0!==n;while((e=e[t])&&9!==e.nodeType)if(1===e.nodeType){if(i&&k(e).is(n))break;r.push(e)}return r},S=function(e,t){for(var n=[];e;e=e.nextSibling)1===e.nodeType&&e!==t&&n.push(e);return n},N=k.expr.match.needsContext;function A(e,t){return e.nodeName&&e.nodeName.toLowerCase()===t.toLowerCase()}var D=/^<([a-z][^\/\0>:\x20\t\r\n\f]*)[\x20\t\r\n\f]*\/?>(?:<\/\1>|)$/i;function j(e,n,r){return m(n)?k.grep(e,function(e,t){return!!n.call(e,t,e)!==r}):n.nodeType?k.grep(e,function(e){return e===n!==r}):"string"!=typeof n?k.grep(e,function(e){return-1<i.call(n,e)!==r}):k.filter(n,e,r)}k.filter=function(e,t,n){var r=t[0];return n&&(e=":not("+e+")"),1===t.length&&1===r.nodeType?k.find.matchesSelector(r,e)?[r]:[]:k.find.matches(e,k.grep(t,function(e){return 1===e.nodeType}))},k.fn.extend({find:function(e){var t,n,r=this.length,i=this;if("string"!=typeof e)return this.pushStack(k(e).filter(function(){for(t=0;t<r;t++)if(k.contains(i[t],this))return!0}));for(n=this.pushStack([]),t=0;t<r;t++)k.find(e,i[t],n);return 1<r?k.uniqueSort(n):n},filter:function(e){return this.pushStack(j(this,e||[],!1))},not:function(e){return this.pushStack(j(this,e||[],!0))},is:function(e){return!!j(this,"string"==typeof e&&N.test(e)?k(e):e||[],!1).length}});var q,L=/^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]+))$/;(k.fn.init=function(e,t,n){var r,i;if(!e)return this;if(n=n||q,"string"==typeof e){if(!(r="<"===e[0]&&">"===e[e.length-1]&&3<=e.length?[null,e,null]:L.exec(e))||!r[1]&&t)return!t||t.jquery?(t||n).find(e):this.constructor(t).find(e);if(r[1]){if(t=t instanceof k?t[0]:t,k.merge(this,k.parseHTML(r[1],t&&t.nodeType?t.ownerDocument||t:E,!0)),D.test(r[1])&&k.isPlainObject(t))for(r in t)m(this[r])?this[r](t[r]):this.attr(r,t[r]);return this}return(i=E.getElementById(r[2]))&&(this[0]=i,this.length=1),this}return e.nodeType?(this[0]=e,this.length=1,this):m(e)?void 0!==n.ready?n.ready(e):e(k):k.makeArray(e,this)}).prototype=k.fn,q=k(E);var H=/^(?:parents|prev(?:Until|All))/,O={children:!0,contents:!0,next:!0,prev:!0};function P(e,t){while((e=e[t])&&1!==e.nodeType);return e}k.fn.extend({has:function(e){var t=k(e,this),n=t.length;return this.filter(function(){for(var e=0;e<n;e++)if(k.contains(this,t[e]))return!0})},closest:function(e,t){var n,r=0,i=this.length,o=[],a="string"!=typeof e&&k(e);if(!N.test(e))for(;r<i;r++)for(n=this[r];n&&n!==t;n=n.parentNode)if(n.nodeType<11&&(a?-1<a.index(n):1===n.nodeType&&k.find.matchesSelector(n,e))){o.push(n);break}return this.pushStack(1<o.length?k.uniqueSort(o):o)},index:function(e){return e?"string"==typeof e?i.call(k(e),this[0]):i.call(this,e.jquery?e[0]:e):this[0]&&this[0].parentNode?this.first().prevAll().length:-1},add:function(e,t){return this.pushStack(k.uniqueSort(k.merge(this.get(),k(e,t))))},addBack:function(e){return this.add(null==e?this.prevObject:this.prevObject.filter(e))}}),k.each({parent:function(e){var t=e.parentNode;return t&&11!==t.nodeType?t:null},parents:function(e){return T(e,"parentNode")},parentsUntil:function(e,t,n){return T(e,"parentNode",n)},next:function(e){return P(e,"nextSibling")},prev:function(e){return P(e,"previousSibling")},nextAll:function(e){return T(e,"nextSibling")},prevAll:function(e){return T(e,"previousSibling")},nextUntil:function(e,t,n){return T(e,"nextSibling",n)},prevUntil:function(e,t,n){return T(e,"previousSibling",n)},siblings:function(e){return S((e.parentNode||{}).firstChild,e)},children:function(e){return S(e.firstChild)},contents:function(e){return"undefined"!=typeof e.contentDocument?e.contentDocument:(A(e,"template")&&(e=e.content||e),k.merge([],e.childNodes))}},function(r,i){k.fn[r]=function(e,t){var n=k.map(this,i,e);return"Until"!==r.slice(-5)&&(t=e),t&&"string"==typeof t&&(n=k.filter(t,n)),1<this.length&&(O[r]||k.uniqueSort(n),H.test(r)&&n.reverse()),this.pushStack(n)}});var R=/[^\x20\t\r\n\f]+/g;function M(e){return e}function I(e){throw e}function W(e,t,n,r){var i;try{e&&m(i=e.promise)?i.call(e).done(t).fail(n):e&&m(i=e.then)?i.call(e,t,n):t.apply(void 0,[e].slice(r))}catch(e){n.apply(void 0,[e])}}k.Callbacks=function(r){var e,n;r="string"==typeof r?(e=r,n={},k.each(e.match(R)||[],function(e,t){n[t]=!0}),n):k.extend({},r);var i,t,o,a,s=[],u=[],l=-1,c=function(){for(a=a||r.once,o=i=!0;u.length;l=-1){t=u.shift();while(++l<s.length)!1===s[l].apply(t[0],t[1])&&r.stopOnFalse&&(l=s.length,t=!1)}r.memory||(t=!1),i=!1,a&&(s=t?[]:"")},f={add:function(){return s&&(t&&!i&&(l=s.length-1,u.push(t)),function n(e){k.each(e,function(e,t){m(t)?r.unique&&f.has(t)||s.push(t):t&&t.length&&"string"!==w(t)&&n(t)})}(arguments),t&&!i&&c()),this},remove:function(){return k.each(arguments,function(e,t){var n;while(-1<(n=k.inArray(t,s,n)))s.splice(n,1),n<=l&&l--}),this},has:function(e){return e?-1<k.inArray(e,s):0<s.length},empty:function(){return s&&(s=[]),this},disable:function(){return a=u=[],s=t="",this},disabled:function(){return!s},lock:function(){return a=u=[],t||i||(s=t=""),this},locked:function(){return!!a},fireWith:function(e,t){return a||(t=[e,(t=t||[]).slice?t.slice():t],u.push(t),i||c()),this},fire:function(){return f.fireWith(this,arguments),this},fired:function(){return!!o}};return f},k.extend({Deferred:function(e){var o=[["notify","progress",k.Callbacks("memory"),k.Callbacks("memory"),2],["resolve","done",k.Callbacks("once memory"),k.Callbacks("once memory"),0,"resolved"],["reject","fail",k.Callbacks("once memory"),k.Callbacks("once memory"),1,"rejected"]],i="pending",a={state:function(){return i},always:function(){return s.done(arguments).fail(arguments),this},"catch":function(e){return a.then(null,e)},pipe:function(){var i=arguments;return k.Deferred(function(r){k.each(o,function(e,t){var n=m(i[t[4]])&&i[t[4]];s[t[1]](function(){var e=n&&n.apply(this,arguments);e&&m(e.promise)?e.promise().progress(r.notify).done(r.resolve).fail(r.reject):r[t[0]+"With"](this,n?[e]:arguments)})}),i=null}).promise()},then:function(t,n,r){var u=0;function l(i,o,a,s){return function(){var n=this,r=arguments,e=function(){var e,t;if(!(i<u)){if((e=a.apply(n,r))===o.promise())throw new TypeError("Thenable self-resolution");t=e&&("object"==typeof e||"function"==typeof e)&&e.then,m(t)?s?t.call(e,l(u,o,M,s),l(u,o,I,s)):(u++,t.call(e,l(u,o,M,s),l(u,o,I,s),l(u,o,M,o.notifyWith))):(a!==M&&(n=void 0,r=[e]),(s||o.resolveWith)(n,r))}},t=s?e:function(){try{e()}catch(e){k.Deferred.exceptionHook&&k.Deferred.exceptionHook(e,t.stackTrace),u<=i+1&&(a!==I&&(n=void 0,r=[e]),o.rejectWith(n,r))}};i?t():(k.Deferred.getStackHook&&(t.stackTrace=k.Deferred.getStackHook()),C.setTimeout(t))}}return k.Deferred(function(e){o[0][3].add(l(0,e,m(r)?r:M,e.notifyWith)),o[1][3].add(l(0,e,m(t)?t:M)),o[2][3].add(l(0,e,m(n)?n:I))}).promise()},promise:function(e){return null!=e?k.extend(e,a):a}},s={};return k.each(o,function(e,t){var n=t[2],r=t[5];a[t[1]]=n.add,r&&n.add(function(){i=r},o[3-e][2].disable,o[3-e][3].disable,o[0][2].lock,o[0][3].lock),n.add(t[3].fire),s[t[0]]=function(){return s[t[0]+"With"](this===s?void 0:this,arguments),this},s[t[0]+"With"]=n.fireWith}),a.promise(s),e&&e.call(s,s),s},when:function(e){var n=arguments.length,t=n,r=Array(t),i=s.call(arguments),o=k.Deferred(),a=function(t){return function(e){r[t]=this,i[t]=1<arguments.length?s.call(arguments):e,--n||o.resolveWith(r,i)}};if(n<=1&&(W(e,o.done(a(t)).resolve,o.reject,!n),"pending"===o.state()||m(i[t]&&i[t].then)))return o.then();while(t--)W(i[t],a(t),o.reject);return o.promise()}});var $=/^(Eval|Internal|Range|Reference|Syntax|Type|URI)Error$/;k.Deferred.exceptionHook=function(e,t){C.console&&C.console.warn&&e&&$.test(e.name)&&C.console.warn("jQuery.Deferred exception: "+e.message,e.stack,t)},k.readyException=function(e){C.setTimeout(function(){throw e})};var F=k.Deferred();function B(){E.removeEventListener("DOMContentLoaded",B),C.removeEventListener("load",B),k.ready()}k.fn.ready=function(e){return F.then(e)["catch"](function(e){k.readyException(e)}),this},k.extend({isReady:!1,readyWait:1,ready:function(e){(!0===e?--k.readyWait:k.isReady)||(k.isReady=!0)!==e&&0<--k.readyWait||F.resolveWith(E,[k])}}),k.ready.then=F.then,"complete"===E.readyState||"loading"!==E.readyState&&!E.documentElement.doScroll?C.setTimeout(k.ready):(E.addEventListener("DOMContentLoaded",B),C.addEventListener("load",B));var _=function(e,t,n,r,i,o,a){var s=0,u=e.length,l=null==n;if("object"===w(n))for(s in i=!0,n)_(e,t,s,n[s],!0,o,a);else if(void 0!==r&&(i=!0,m(r)||(a=!0),l&&(a?(t.call(e,r),t=null):(l=t,t=function(e,t,n){return l.call(k(e),n)})),t))for(;s<u;s++)t(e[s],n,a?r:r.call(e[s],s,t(e[s],n)));return i?e:l?t.call(e):u?t(e[0],n):o},z=/^-ms-/,U=/-([a-z])/g;function X(e,t){return t.toUpperCase()}function V(e){return e.replace(z,"ms-").replace(U,X)}var G=function(e){return 1===e.nodeType||9===e.nodeType||!+e.nodeType};function Y(){this.expando=k.expando+Y.uid++}Y.uid=1,Y.prototype={cache:function(e){var t=e[this.expando];return t||(t={},G(e)&&(e.nodeType?e[this.expando]=t:Object.defineProperty(e,this.expando,{value:t,configurable:!0}))),t},set:function(e,t,n){var r,i=this.cache(e);if("string"==typeof t)i[V(t)]=n;else for(r in t)i[V(r)]=t[r];return i},get:function(e,t){return void 0===t?this.cache(e):e[this.expando]&&e[this.expando][V(t)]},access:function(e,t,n){return void 0===t||t&&"string"==typeof t&&void 0===n?this.get(e,t):(this.set(e,t,n),void 0!==n?n:t)},remove:function(e,t){var n,r=e[this.expando];if(void 0!==r){if(void 0!==t){n=(t=Array.isArray(t)?t.map(V):(t=V(t))in r?[t]:t.match(R)||[]).length;while(n--)delete r[t[n]]}(void 0===t||k.isEmptyObject(r))&&(e.nodeType?e[this.expando]=void 0:delete e[this.expando])}},hasData:function(e){var t=e[this.expando];return void 0!==t&&!k.isEmptyObject(t)}};var Q=new Y,J=new Y,K=/^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,Z=/[A-Z]/g;function ee(e,t,n){var r,i;if(void 0===n&&1===e.nodeType)if(r="data-"+t.replace(Z,"-$&").toLowerCase(),"string"==typeof(n=e.getAttribute(r))){try{n="true"===(i=n)||"false"!==i&&("null"===i?null:i===+i+""?+i:K.test(i)?JSON.parse(i):i)}catch(e){}J.set(e,t,n)}else n=void 0;return n}k.extend({hasData:function(e){return J.hasData(e)||Q.hasData(e)},data:function(e,t,n){return J.access(e,t,n)},removeData:function(e,t){J.remove(e,t)},_data:function(e,t,n){return Q.access(e,t,n)},_removeData:function(e,t){Q.remove(e,t)}}),k.fn.extend({data:function(n,e){var t,r,i,o=this[0],a=o&&o.attributes;if(void 0===n){if(this.length&&(i=J.get(o),1===o.nodeType&&!Q.get(o,"hasDataAttrs"))){t=a.length;while(t--)a[t]&&0===(r=a[t].name).indexOf("data-")&&(r=V(r.slice(5)),ee(o,r,i[r]));Q.set(o,"hasDataAttrs",!0)}return i}return"object"==typeof n?this.each(function(){J.set(this,n)}):_(this,function(e){var t;if(o&&void 0===e)return void 0!==(t=J.get(o,n))?t:void 0!==(t=ee(o,n))?t:void 0;this.each(function(){J.set(this,n,e)})},null,e,1<arguments.length,null,!0)},removeData:function(e){return this.each(function(){J.remove(this,e)})}}),k.extend({queue:function(e,t,n){var r;if(e)return t=(t||"fx")+"queue",r=Q.get(e,t),n&&(!r||Array.isArray(n)?r=Q.access(e,t,k.makeArray(n)):r.push(n)),r||[]},dequeue:function(e,t){t=t||"fx";var n=k.queue(e,t),r=n.length,i=n.shift(),o=k._queueHooks(e,t);"inprogress"===i&&(i=n.shift(),r--),i&&("fx"===t&&n.unshift("inprogress"),delete o.stop,i.call(e,function(){k.dequeue(e,t)},o)),!r&&o&&o.empty.fire()},_queueHooks:function(e,t){var n=t+"queueHooks";return Q.get(e,n)||Q.access(e,n,{empty:k.Callbacks("once memory").add(function(){Q.remove(e,[t+"queue",n])})})}}),k.fn.extend({queue:function(t,n){var e=2;return"string"!=typeof t&&(n=t,t="fx",e--),arguments.length<e?k.queue(this[0],t):void 0===n?this:this.each(function(){var e=k.queue(this,t,n);k._queueHooks(this,t),"fx"===t&&"inprogress"!==e[0]&&k.dequeue(this,t)})},dequeue:function(e){return this.each(function(){k.dequeue(this,e)})},clearQueue:function(e){return this.queue(e||"fx",[])},promise:function(e,t){var n,r=1,i=k.Deferred(),o=this,a=this.length,s=function(){--r||i.resolveWith(o,[o])};"string"!=typeof e&&(t=e,e=void 0),e=e||"fx";while(a--)(n=Q.get(o[a],e+"queueHooks"))&&n.empty&&(r++,n.empty.add(s));return s(),i.promise(t)}});var te=/[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,ne=new RegExp("^(?:([+-])=|)("+te+")([a-z%]*)$","i"),re=["Top","Right","Bottom","Left"],ie=E.documentElement,oe=function(e){return k.contains(e.ownerDocument,e)},ae={composed:!0};ie.getRootNode&&(oe=function(e){return k.contains(e.ownerDocument,e)||e.getRootNode(ae)===e.ownerDocument});var se=function(e,t){return"none"===(e=t||e).style.display||""===e.style.display&&oe(e)&&"none"===k.css(e,"display")},ue=function(e,t,n,r){var i,o,a={};for(o in t)a[o]=e.style[o],e.style[o]=t[o];for(o in i=n.apply(e,r||[]),t)e.style[o]=a[o];return i};function le(e,t,n,r){var i,o,a=20,s=r?function(){return r.cur()}:function(){return k.css(e,t,"")},u=s(),l=n&&n[3]||(k.cssNumber[t]?"":"px"),c=e.nodeType&&(k.cssNumber[t]||"px"!==l&&+u)&&ne.exec(k.css(e,t));if(c&&c[3]!==l){u/=2,l=l||c[3],c=+u||1;while(a--)k.style(e,t,c+l),(1-o)*(1-(o=s()/u||.5))<=0&&(a=0),c/=o;c*=2,k.style(e,t,c+l),n=n||[]}return n&&(c=+c||+u||0,i=n[1]?c+(n[1]+1)*n[2]:+n[2],r&&(r.unit=l,r.start=c,r.end=i)),i}var ce={};function fe(e,t){for(var n,r,i,o,a,s,u,l=[],c=0,f=e.length;c<f;c++)(r=e[c]).style&&(n=r.style.display,t?("none"===n&&(l[c]=Q.get(r,"display")||null,l[c]||(r.style.display="")),""===r.style.display&&se(r)&&(l[c]=(u=a=o=void 0,a=(i=r).ownerDocument,s=i.nodeName,(u=ce[s])||(o=a.body.appendChild(a.createElement(s)),u=k.css(o,"display"),o.parentNode.removeChild(o),"none"===u&&(u="block"),ce[s]=u)))):"none"!==n&&(l[c]="none",Q.set(r,"display",n)));for(c=0;c<f;c++)null!=l[c]&&(e[c].style.display=l[c]);return e}k.fn.extend({show:function(){return fe(this,!0)},hide:function(){return fe(this)},toggle:function(e){return"boolean"==typeof e?e?this.show():this.hide():this.each(function(){se(this)?k(this).show():k(this).hide()})}});var pe=/^(?:checkbox|radio)$/i,de=/<([a-z][^\/\0>\x20\t\r\n\f]*)/i,he=/^$|^module$|\/(?:java|ecma)script/i,ge={option:[1,"<select multiple='multiple'>","</select>"],thead:[1,"<table>","</table>"],col:[2,"<table><colgroup>","</colgroup></table>"],tr:[2,"<table><tbody>","</tbody></table>"],td:[3,"<table><tbody><tr>","</tr></tbody></table>"],_default:[0,"",""]};function ve(e,t){var n;return n="undefined"!=typeof e.getElementsByTagName?e.getElementsByTagName(t||"*"):"undefined"!=typeof e.querySelectorAll?e.querySelectorAll(t||"*"):[],void 0===t||t&&A(e,t)?k.merge([e],n):n}function ye(e,t){for(var n=0,r=e.length;n<r;n++)Q.set(e[n],"globalEval",!t||Q.get(t[n],"globalEval"))}ge.optgroup=ge.option,ge.tbody=ge.tfoot=ge.colgroup=ge.caption=ge.thead,ge.th=ge.td;var me,xe,be=/<|&#?\w+;/;function we(e,t,n,r,i){for(var o,a,s,u,l,c,f=t.createDocumentFragment(),p=[],d=0,h=e.length;d<h;d++)if((o=e[d])||0===o)if("object"===w(o))k.merge(p,o.nodeType?[o]:o);else if(be.test(o)){a=a||f.appendChild(t.createElement("div")),s=(de.exec(o)||["",""])[1].toLowerCase(),u=ge[s]||ge._default,a.innerHTML=u[1]+k.htmlPrefilter(o)+u[2],c=u[0];while(c--)a=a.lastChild;k.merge(p,a.childNodes),(a=f.firstChild).textContent=""}else p.push(t.createTextNode(o));f.textContent="",d=0;while(o=p[d++])if(r&&-1<k.inArray(o,r))i&&i.push(o);else if(l=oe(o),a=ve(f.appendChild(o),"script"),l&&ye(a),n){c=0;while(o=a[c++])he.test(o.type||"")&&n.push(o)}return f}me=E.createDocumentFragment().appendChild(E.createElement("div")),(xe=E.createElement("input")).setAttribute("type","radio"),xe.setAttribute("checked","checked"),xe.setAttribute("name","t"),me.appendChild(xe),y.checkClone=me.cloneNode(!0).cloneNode(!0).lastChild.checked,me.innerHTML="<textarea>x</textarea>",y.noCloneChecked=!!me.cloneNode(!0).lastChild.defaultValue;var Te=/^key/,Ce=/^(?:mouse|pointer|contextmenu|drag|drop)|click/,Ee=/^([^.]*)(?:\.(.+)|)/;function ke(){return!0}function Se(){return!1}function Ne(e,t){return e===function(){try{return E.activeElement}catch(e){}}()==("focus"===t)}function Ae(e,t,n,r,i,o){var a,s;if("object"==typeof t){for(s in"string"!=typeof n&&(r=r||n,n=void 0),t)Ae(e,s,n,r,t[s],o);return e}if(null==r&&null==i?(i=n,r=n=void 0):null==i&&("string"==typeof n?(i=r,r=void 0):(i=r,r=n,n=void 0)),!1===i)i=Se;else if(!i)return e;return 1===o&&(a=i,(i=function(e){return k().off(e),a.apply(this,arguments)}).guid=a.guid||(a.guid=k.guid++)),e.each(function(){k.event.add(this,t,i,r,n)})}function De(e,i,o){o?(Q.set(e,i,!1),k.event.add(e,i,{namespace:!1,handler:function(e){var t,n,r=Q.get(this,i);if(1&e.isTrigger&&this[i]){if(r.length)(k.event.special[i]||{}).delegateType&&e.stopPropagation();else if(r=s.call(arguments),Q.set(this,i,r),t=o(this,i),this[i](),r!==(n=Q.get(this,i))||t?Q.set(this,i,!1):n={},r!==n)return e.stopImmediatePropagation(),e.preventDefault(),n.value}else r.length&&(Q.set(this,i,{value:k.event.trigger(k.extend(r[0],k.Event.prototype),r.slice(1),this)}),e.stopImmediatePropagation())}})):void 0===Q.get(e,i)&&k.event.add(e,i,ke)}k.event={global:{},add:function(t,e,n,r,i){var o,a,s,u,l,c,f,p,d,h,g,v=Q.get(t);if(v){n.handler&&(n=(o=n).handler,i=o.selector),i&&k.find.matchesSelector(ie,i),n.guid||(n.guid=k.guid++),(u=v.events)||(u=v.events={}),(a=v.handle)||(a=v.handle=function(e){return"undefined"!=typeof k&&k.event.triggered!==e.type?k.event.dispatch.apply(t,arguments):void 0}),l=(e=(e||"").match(R)||[""]).length;while(l--)d=g=(s=Ee.exec(e[l])||[])[1],h=(s[2]||"").split(".").sort(),d&&(f=k.event.special[d]||{},d=(i?f.delegateType:f.bindType)||d,f=k.event.special[d]||{},c=k.extend({type:d,origType:g,data:r,handler:n,guid:n.guid,selector:i,needsContext:i&&k.expr.match.needsContext.test(i),namespace:h.join(".")},o),(p=u[d])||((p=u[d]=[]).delegateCount=0,f.setup&&!1!==f.setup.call(t,r,h,a)||t.addEventListener&&t.addEventListener(d,a)),f.add&&(f.add.call(t,c),c.handler.guid||(c.handler.guid=n.guid)),i?p.splice(p.delegateCount++,0,c):p.push(c),k.event.global[d]=!0)}},remove:function(e,t,n,r,i){var o,a,s,u,l,c,f,p,d,h,g,v=Q.hasData(e)&&Q.get(e);if(v&&(u=v.events)){l=(t=(t||"").match(R)||[""]).length;while(l--)if(d=g=(s=Ee.exec(t[l])||[])[1],h=(s[2]||"").split(".").sort(),d){f=k.event.special[d]||{},p=u[d=(r?f.delegateType:f.bindType)||d]||[],s=s[2]&&new RegExp("(^|\\.)"+h.join("\\.(?:.*\\.|)")+"(\\.|$)"),a=o=p.length;while(o--)c=p[o],!i&&g!==c.origType||n&&n.guid!==c.guid||s&&!s.test(c.namespace)||r&&r!==c.selector&&("**"!==r||!c.selector)||(p.splice(o,1),c.selector&&p.delegateCount--,f.remove&&f.remove.call(e,c));a&&!p.length&&(f.teardown&&!1!==f.teardown.call(e,h,v.handle)||k.removeEvent(e,d,v.handle),delete u[d])}else for(d in u)k.event.remove(e,d+t[l],n,r,!0);k.isEmptyObject(u)&&Q.remove(e,"handle events")}},dispatch:function(e){var t,n,r,i,o,a,s=k.event.fix(e),u=new Array(arguments.length),l=(Q.get(this,"events")||{})[s.type]||[],c=k.event.special[s.type]||{};for(u[0]=s,t=1;t<arguments.length;t++)u[t]=arguments[t];if(s.delegateTarget=this,!c.preDispatch||!1!==c.preDispatch.call(this,s)){a=k.event.handlers.call(this,s,l),t=0;while((i=a[t++])&&!s.isPropagationStopped()){s.currentTarget=i.elem,n=0;while((o=i.handlers[n++])&&!s.isImmediatePropagationStopped())s.rnamespace&&!1!==o.namespace&&!s.rnamespace.test(o.namespace)||(s.handleObj=o,s.data=o.data,void 0!==(r=((k.event.special[o.origType]||{}).handle||o.handler).apply(i.elem,u))&&!1===(s.result=r)&&(s.preventDefault(),s.stopPropagation()))}return c.postDispatch&&c.postDispatch.call(this,s),s.result}},handlers:function(e,t){var n,r,i,o,a,s=[],u=t.delegateCount,l=e.target;if(u&&l.nodeType&&!("click"===e.type&&1<=e.button))for(;l!==this;l=l.parentNode||this)if(1===l.nodeType&&("click"!==e.type||!0!==l.disabled)){for(o=[],a={},n=0;n<u;n++)void 0===a[i=(r=t[n]).selector+" "]&&(a[i]=r.needsContext?-1<k(i,this).index(l):k.find(i,this,null,[l]).length),a[i]&&o.push(r);o.length&&s.push({elem:l,handlers:o})}return l=this,u<t.length&&s.push({elem:l,handlers:t.slice(u)}),s},addProp:function(t,e){Object.defineProperty(k.Event.prototype,t,{enumerable:!0,configurable:!0,get:m(e)?function(){if(this.originalEvent)return e(this.originalEvent)}:function(){if(this.originalEvent)return this.originalEvent[t]},set:function(e){Object.defineProperty(this,t,{enumerable:!0,configurable:!0,writable:!0,value:e})}})},fix:function(e){return e[k.expando]?e:new k.Event(e)},special:{load:{noBubble:!0},click:{setup:function(e){var t=this||e;return pe.test(t.type)&&t.click&&A(t,"input")&&De(t,"click",ke),!1},trigger:function(e){var t=this||e;return pe.test(t.type)&&t.click&&A(t,"input")&&De(t,"click"),!0},_default:function(e){var t=e.target;return pe.test(t.type)&&t.click&&A(t,"input")&&Q.get(t,"click")||A(t,"a")}},beforeunload:{postDispatch:function(e){void 0!==e.result&&e.originalEvent&&(e.originalEvent.returnValue=e.result)}}}},k.removeEvent=function(e,t,n){e.removeEventListener&&e.removeEventListener(t,n)},k.Event=function(e,t){if(!(this instanceof k.Event))return new k.Event(e,t);e&&e.type?(this.originalEvent=e,this.type=e.type,this.isDefaultPrevented=e.defaultPrevented||void 0===e.defaultPrevented&&!1===e.returnValue?ke:Se,this.target=e.target&&3===e.target.nodeType?e.target.parentNode:e.target,this.currentTarget=e.currentTarget,this.relatedTarget=e.relatedTarget):this.type=e,t&&k.extend(this,t),this.timeStamp=e&&e.timeStamp||Date.now(),this[k.expando]=!0},k.Event.prototype={constructor:k.Event,isDefaultPrevented:Se,isPropagationStopped:Se,isImmediatePropagationStopped:Se,isSimulated:!1,preventDefault:function(){var e=this.originalEvent;this.isDefaultPrevented=ke,e&&!this.isSimulated&&e.preventDefault()},stopPropagation:function(){var e=this.originalEvent;this.isPropagationStopped=ke,e&&!this.isSimulated&&e.stopPropagation()},stopImmediatePropagation:function(){var e=this.originalEvent;this.isImmediatePropagationStopped=ke,e&&!this.isSimulated&&e.stopImmediatePropagation(),this.stopPropagation()}},k.each({altKey:!0,bubbles:!0,cancelable:!0,changedTouches:!0,ctrlKey:!0,detail:!0,eventPhase:!0,metaKey:!0,pageX:!0,pageY:!0,shiftKey:!0,view:!0,"char":!0,code:!0,charCode:!0,key:!0,keyCode:!0,button:!0,buttons:!0,clientX:!0,clientY:!0,offsetX:!0,offsetY:!0,pointerId:!0,pointerType:!0,screenX:!0,screenY:!0,targetTouches:!0,toElement:!0,touches:!0,which:function(e){var t=e.button;return null==e.which&&Te.test(e.type)?null!=e.charCode?e.charCode:e.keyCode:!e.which&&void 0!==t&&Ce.test(e.type)?1&t?1:2&t?3:4&t?2:0:e.which}},k.event.addProp),k.each({focus:"focusin",blur:"focusout"},function(e,t){k.event.special[e]={setup:function(){return De(this,e,Ne),!1},trigger:function(){return De(this,e),!0},delegateType:t}}),k.each({mouseenter:"mouseover",mouseleave:"mouseout",pointerenter:"pointerover",pointerleave:"pointerout"},function(e,i){k.event.special[e]={delegateType:i,bindType:i,handle:function(e){var t,n=e.relatedTarget,r=e.handleObj;return n&&(n===this||k.contains(this,n))||(e.type=r.origType,t=r.handler.apply(this,arguments),e.type=i),t}}}),k.fn.extend({on:function(e,t,n,r){return Ae(this,e,t,n,r)},one:function(e,t,n,r){return Ae(this,e,t,n,r,1)},off:function(e,t,n){var r,i;if(e&&e.preventDefault&&e.handleObj)return r=e.handleObj,k(e.delegateTarget).off(r.namespace?r.origType+"."+r.namespace:r.origType,r.selector,r.handler),this;if("object"==typeof e){for(i in e)this.off(i,t,e[i]);return this}return!1!==t&&"function"!=typeof t||(n=t,t=void 0),!1===n&&(n=Se),this.each(function(){k.event.remove(this,e,n,t)})}});var je=/<(?!area|br|col|embed|hr|img|input|link|meta|param)(([a-z][^\/\0>\x20\t\r\n\f]*)[^>]*)\/>/gi,qe=/<script|<style|<link/i,Le=/checked\s*(?:[^=]|=\s*.checked.)/i,He=/^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;function Oe(e,t){return A(e,"table")&&A(11!==t.nodeType?t:t.firstChild,"tr")&&k(e).children("tbody")[0]||e}function Pe(e){return e.type=(null!==e.getAttribute("type"))+"/"+e.type,e}function Re(e){return"true/"===(e.type||"").slice(0,5)?e.type=e.type.slice(5):e.removeAttribute("type"),e}function Me(e,t){var n,r,i,o,a,s,u,l;if(1===t.nodeType){if(Q.hasData(e)&&(o=Q.access(e),a=Q.set(t,o),l=o.events))for(i in delete a.handle,a.events={},l)for(n=0,r=l[i].length;n<r;n++)k.event.add(t,i,l[i][n]);J.hasData(e)&&(s=J.access(e),u=k.extend({},s),J.set(t,u))}}function Ie(n,r,i,o){r=g.apply([],r);var e,t,a,s,u,l,c=0,f=n.length,p=f-1,d=r[0],h=m(d);if(h||1<f&&"string"==typeof d&&!y.checkClone&&Le.test(d))return n.each(function(e){var t=n.eq(e);h&&(r[0]=d.call(this,e,t.html())),Ie(t,r,i,o)});if(f&&(t=(e=we(r,n[0].ownerDocument,!1,n,o)).firstChild,1===e.childNodes.length&&(e=t),t||o)){for(s=(a=k.map(ve(e,"script"),Pe)).length;c<f;c++)u=e,c!==p&&(u=k.clone(u,!0,!0),s&&k.merge(a,ve(u,"script"))),i.call(n[c],u,c);if(s)for(l=a[a.length-1].ownerDocument,k.map(a,Re),c=0;c<s;c++)u=a[c],he.test(u.type||"")&&!Q.access(u,"globalEval")&&k.contains(l,u)&&(u.src&&"module"!==(u.type||"").toLowerCase()?k._evalUrl&&!u.noModule&&k._evalUrl(u.src,{nonce:u.nonce||u.getAttribute("nonce")}):b(u.textContent.replace(He,""),u,l))}return n}function We(e,t,n){for(var r,i=t?k.filter(t,e):e,o=0;null!=(r=i[o]);o++)n||1!==r.nodeType||k.cleanData(ve(r)),r.parentNode&&(n&&oe(r)&&ye(ve(r,"script")),r.parentNode.removeChild(r));return e}k.extend({htmlPrefilter:function(e){return e.replace(je,"<$1></$2>")},clone:function(e,t,n){var r,i,o,a,s,u,l,c=e.cloneNode(!0),f=oe(e);if(!(y.noCloneChecked||1!==e.nodeType&&11!==e.nodeType||k.isXMLDoc(e)))for(a=ve(c),r=0,i=(o=ve(e)).length;r<i;r++)s=o[r],u=a[r],void 0,"input"===(l=u.nodeName.toLowerCase())&&pe.test(s.type)?u.checked=s.checked:"input"!==l&&"textarea"!==l||(u.defaultValue=s.defaultValue);if(t)if(n)for(o=o||ve(e),a=a||ve(c),r=0,i=o.length;r<i;r++)Me(o[r],a[r]);else Me(e,c);return 0<(a=ve(c,"script")).length&&ye(a,!f&&ve(e,"script")),c},cleanData:function(e){for(var t,n,r,i=k.event.special,o=0;void 0!==(n=e[o]);o++)if(G(n)){if(t=n[Q.expando]){if(t.events)for(r in t.events)i[r]?k.event.remove(n,r):k.removeEvent(n,r,t.handle);n[Q.expando]=void 0}n[J.expando]&&(n[J.expando]=void 0)}}}),k.fn.extend({detach:function(e){return We(this,e,!0)},remove:function(e){return We(this,e)},text:function(e){return _(this,function(e){return void 0===e?k.text(this):this.empty().each(function(){1!==this.nodeType&&11!==this.nodeType&&9!==this.nodeType||(this.textContent=e)})},null,e,arguments.length)},append:function(){return Ie(this,arguments,function(e){1!==this.nodeType&&11!==this.nodeType&&9!==this.nodeType||Oe(this,e).appendChild(e)})},prepend:function(){return Ie(this,arguments,function(e){if(1===this.nodeType||11===this.nodeType||9===this.nodeType){var t=Oe(this,e);t.insertBefore(e,t.firstChild)}})},before:function(){return Ie(this,arguments,function(e){this.parentNode&&this.parentNode.insertBefore(e,this)})},after:function(){return Ie(this,arguments,function(e){this.parentNode&&this.parentNode.insertBefore(e,this.nextSibling)})},empty:function(){for(var e,t=0;null!=(e=this[t]);t++)1===e.nodeType&&(k.cleanData(ve(e,!1)),e.textContent="");return this},clone:function(e,t){return e=null!=e&&e,t=null==t?e:t,this.map(function(){return k.clone(this,e,t)})},html:function(e){return _(this,function(e){var t=this[0]||{},n=0,r=this.length;if(void 0===e&&1===t.nodeType)return t.innerHTML;if("string"==typeof e&&!qe.test(e)&&!ge[(de.exec(e)||["",""])[1].toLowerCase()]){e=k.htmlPrefilter(e);try{for(;n<r;n++)1===(t=this[n]||{}).nodeType&&(k.cleanData(ve(t,!1)),t.innerHTML=e);t=0}catch(e){}}t&&this.empty().append(e)},null,e,arguments.length)},replaceWith:function(){var n=[];return Ie(this,arguments,function(e){var t=this.parentNode;k.inArray(this,n)<0&&(k.cleanData(ve(this)),t&&t.replaceChild(e,this))},n)}}),k.each({appendTo:"append",prependTo:"prepend",insertBefore:"before",insertAfter:"after",replaceAll:"replaceWith"},function(e,a){k.fn[e]=function(e){for(var t,n=[],r=k(e),i=r.length-1,o=0;o<=i;o++)t=o===i?this:this.clone(!0),k(r[o])[a](t),u.apply(n,t.get());return this.pushStack(n)}});var $e=new RegExp("^("+te+")(?!px)[a-z%]+$","i"),Fe=function(e){var t=e.ownerDocument.defaultView;return t&&t.opener||(t=C),t.getComputedStyle(e)},Be=new RegExp(re.join("|"),"i");function _e(e,t,n){var r,i,o,a,s=e.style;return(n=n||Fe(e))&&(""!==(a=n.getPropertyValue(t)||n[t])||oe(e)||(a=k.style(e,t)),!y.pixelBoxStyles()&&$e.test(a)&&Be.test(t)&&(r=s.width,i=s.minWidth,o=s.maxWidth,s.minWidth=s.maxWidth=s.width=a,a=n.width,s.width=r,s.minWidth=i,s.maxWidth=o)),void 0!==a?a+"":a}function ze(e,t){return{get:function(){if(!e())return(this.get=t).apply(this,arguments);delete this.get}}}!function(){function e(){if(u){s.style.cssText="position:absolute;left:-11111px;width:60px;margin-top:1px;padding:0;border:0",u.style.cssText="position:relative;display:block;box-sizing:border-box;overflow:scroll;margin:auto;border:1px;padding:1px;width:60%;top:1%",ie.appendChild(s).appendChild(u);var e=C.getComputedStyle(u);n="1%"!==e.top,a=12===t(e.marginLeft),u.style.right="60%",o=36===t(e.right),r=36===t(e.width),u.style.position="absolute",i=12===t(u.offsetWidth/3),ie.removeChild(s),u=null}}function t(e){return Math.round(parseFloat(e))}var n,r,i,o,a,s=E.createElement("div"),u=E.createElement("div");u.style&&(u.style.backgroundClip="content-box",u.cloneNode(!0).style.backgroundClip="",y.clearCloneStyle="content-box"===u.style.backgroundClip,k.extend(y,{boxSizingReliable:function(){return e(),r},pixelBoxStyles:function(){return e(),o},pixelPosition:function(){return e(),n},reliableMarginLeft:function(){return e(),a},scrollboxSize:function(){return e(),i}}))}();var Ue=["Webkit","Moz","ms"],Xe=E.createElement("div").style,Ve={};function Ge(e){var t=k.cssProps[e]||Ve[e];return t||(e in Xe?e:Ve[e]=function(e){var t=e[0].toUpperCase()+e.slice(1),n=Ue.length;while(n--)if((e=Ue[n]+t)in Xe)return e}(e)||e)}var Ye=/^(none|table(?!-c[ea]).+)/,Qe=/^--/,Je={position:"absolute",visibility:"hidden",display:"block"},Ke={letterSpacing:"0",fontWeight:"400"};function Ze(e,t,n){var r=ne.exec(t);return r?Math.max(0,r[2]-(n||0))+(r[3]||"px"):t}function et(e,t,n,r,i,o){var a="width"===t?1:0,s=0,u=0;if(n===(r?"border":"content"))return 0;for(;a<4;a+=2)"margin"===n&&(u+=k.css(e,n+re[a],!0,i)),r?("content"===n&&(u-=k.css(e,"padding"+re[a],!0,i)),"margin"!==n&&(u-=k.css(e,"border"+re[a]+"Width",!0,i))):(u+=k.css(e,"padding"+re[a],!0,i),"padding"!==n?u+=k.css(e,"border"+re[a]+"Width",!0,i):s+=k.css(e,"border"+re[a]+"Width",!0,i));return!r&&0<=o&&(u+=Math.max(0,Math.ceil(e["offset"+t[0].toUpperCase()+t.slice(1)]-o-u-s-.5))||0),u}function tt(e,t,n){var r=Fe(e),i=(!y.boxSizingReliable()||n)&&"border-box"===k.css(e,"boxSizing",!1,r),o=i,a=_e(e,t,r),s="offset"+t[0].toUpperCase()+t.slice(1);if($e.test(a)){if(!n)return a;a="auto"}return(!y.boxSizingReliable()&&i||"auto"===a||!parseFloat(a)&&"inline"===k.css(e,"display",!1,r))&&e.getClientRects().length&&(i="border-box"===k.css(e,"boxSizing",!1,r),(o=s in e)&&(a=e[s])),(a=parseFloat(a)||0)+et(e,t,n||(i?"border":"content"),o,r,a)+"px"}function nt(e,t,n,r,i){return new nt.prototype.init(e,t,n,r,i)}k.extend({cssHooks:{opacity:{get:function(e,t){if(t){var n=_e(e,"opacity");return""===n?"1":n}}}},cssNumber:{animationIterationCount:!0,columnCount:!0,fillOpacity:!0,flexGrow:!0,flexShrink:!0,fontWeight:!0,gridArea:!0,gridColumn:!0,gridColumnEnd:!0,gridColumnStart:!0,gridRow:!0,gridRowEnd:!0,gridRowStart:!0,lineHeight:!0,opacity:!0,order:!0,orphans:!0,widows:!0,zIndex:!0,zoom:!0},cssProps:{},style:function(e,t,n,r){if(e&&3!==e.nodeType&&8!==e.nodeType&&e.style){var i,o,a,s=V(t),u=Qe.test(t),l=e.style;if(u||(t=Ge(s)),a=k.cssHooks[t]||k.cssHooks[s],void 0===n)return a&&"get"in a&&void 0!==(i=a.get(e,!1,r))?i:l[t];"string"===(o=typeof n)&&(i=ne.exec(n))&&i[1]&&(n=le(e,t,i),o="number"),null!=n&&n==n&&("number"!==o||u||(n+=i&&i[3]||(k.cssNumber[s]?"":"px")),y.clearCloneStyle||""!==n||0!==t.indexOf("background")||(l[t]="inherit"),a&&"set"in a&&void 0===(n=a.set(e,n,r))||(u?l.setProperty(t,n):l[t]=n))}},css:function(e,t,n,r){var i,o,a,s=V(t);return Qe.test(t)||(t=Ge(s)),(a=k.cssHooks[t]||k.cssHooks[s])&&"get"in a&&(i=a.get(e,!0,n)),void 0===i&&(i=_e(e,t,r)),"normal"===i&&t in Ke&&(i=Ke[t]),""===n||n?(o=parseFloat(i),!0===n||isFinite(o)?o||0:i):i}}),k.each(["height","width"],function(e,u){k.cssHooks[u]={get:function(e,t,n){if(t)return!Ye.test(k.css(e,"display"))||e.getClientRects().length&&e.getBoundingClientRect().width?tt(e,u,n):ue(e,Je,function(){return tt(e,u,n)})},set:function(e,t,n){var r,i=Fe(e),o=!y.scrollboxSize()&&"absolute"===i.position,a=(o||n)&&"border-box"===k.css(e,"boxSizing",!1,i),s=n?et(e,u,n,a,i):0;return a&&o&&(s-=Math.ceil(e["offset"+u[0].toUpperCase()+u.slice(1)]-parseFloat(i[u])-et(e,u,"border",!1,i)-.5)),s&&(r=ne.exec(t))&&"px"!==(r[3]||"px")&&(e.style[u]=t,t=k.css(e,u)),Ze(0,t,s)}}}),k.cssHooks.marginLeft=ze(y.reliableMarginLeft,function(e,t){if(t)return(parseFloat(_e(e,"marginLeft"))||e.getBoundingClientRect().left-ue(e,{marginLeft:0},function(){return e.getBoundingClientRect().left}))+"px"}),k.each({margin:"",padding:"",border:"Width"},function(i,o){k.cssHooks[i+o]={expand:function(e){for(var t=0,n={},r="string"==typeof e?e.split(" "):[e];t<4;t++)n[i+re[t]+o]=r[t]||r[t-2]||r[0];return n}},"margin"!==i&&(k.cssHooks[i+o].set=Ze)}),k.fn.extend({css:function(e,t){return _(this,function(e,t,n){var r,i,o={},a=0;if(Array.isArray(t)){for(r=Fe(e),i=t.length;a<i;a++)o[t[a]]=k.css(e,t[a],!1,r);return o}return void 0!==n?k.style(e,t,n):k.css(e,t)},e,t,1<arguments.length)}}),((k.Tween=nt).prototype={constructor:nt,init:function(e,t,n,r,i,o){this.elem=e,this.prop=n,this.easing=i||k.easing._default,this.options=t,this.start=this.now=this.cur(),this.end=r,this.unit=o||(k.cssNumber[n]?"":"px")},cur:function(){var e=nt.propHooks[this.prop];return e&&e.get?e.get(this):nt.propHooks._default.get(this)},run:function(e){var t,n=nt.propHooks[this.prop];return this.options.duration?this.pos=t=k.easing[this.easing](e,this.options.duration*e,0,1,this.options.duration):this.pos=t=e,this.now=(this.end-this.start)*t+this.start,this.options.step&&this.options.step.call(this.elem,this.now,this),n&&n.set?n.set(this):nt.propHooks._default.set(this),this}}).init.prototype=nt.prototype,(nt.propHooks={_default:{get:function(e){var t;return 1!==e.elem.nodeType||null!=e.elem[e.prop]&&null==e.elem.style[e.prop]?e.elem[e.prop]:(t=k.css(e.elem,e.prop,""))&&"auto"!==t?t:0},set:function(e){k.fx.step[e.prop]?k.fx.step[e.prop](e):1!==e.elem.nodeType||!k.cssHooks[e.prop]&&null==e.elem.style[Ge(e.prop)]?e.elem[e.prop]=e.now:k.style(e.elem,e.prop,e.now+e.unit)}}}).scrollTop=nt.propHooks.scrollLeft={set:function(e){e.elem.nodeType&&e.elem.parentNode&&(e.elem[e.prop]=e.now)}},k.easing={linear:function(e){return e},swing:function(e){return.5-Math.cos(e*Math.PI)/2},_default:"swing"},k.fx=nt.prototype.init,k.fx.step={};var rt,it,ot,at,st=/^(?:toggle|show|hide)$/,ut=/queueHooks$/;function lt(){it&&(!1===E.hidden&&C.requestAnimationFrame?C.requestAnimationFrame(lt):C.setTimeout(lt,k.fx.interval),k.fx.tick())}function ct(){return C.setTimeout(function(){rt=void 0}),rt=Date.now()}function ft(e,t){var n,r=0,i={height:e};for(t=t?1:0;r<4;r+=2-t)i["margin"+(n=re[r])]=i["padding"+n]=e;return t&&(i.opacity=i.width=e),i}function pt(e,t,n){for(var r,i=(dt.tweeners[t]||[]).concat(dt.tweeners["*"]),o=0,a=i.length;o<a;o++)if(r=i[o].call(n,t,e))return r}function dt(o,e,t){var n,a,r=0,i=dt.prefilters.length,s=k.Deferred().always(function(){delete u.elem}),u=function(){if(a)return!1;for(var e=rt||ct(),t=Math.max(0,l.startTime+l.duration-e),n=1-(t/l.duration||0),r=0,i=l.tweens.length;r<i;r++)l.tweens[r].run(n);return s.notifyWith(o,[l,n,t]),n<1&&i?t:(i||s.notifyWith(o,[l,1,0]),s.resolveWith(o,[l]),!1)},l=s.promise({elem:o,props:k.extend({},e),opts:k.extend(!0,{specialEasing:{},easing:k.easing._default},t),originalProperties:e,originalOptions:t,startTime:rt||ct(),duration:t.duration,tweens:[],createTween:function(e,t){var n=k.Tween(o,l.opts,e,t,l.opts.specialEasing[e]||l.opts.easing);return l.tweens.push(n),n},stop:function(e){var t=0,n=e?l.tweens.length:0;if(a)return this;for(a=!0;t<n;t++)l.tweens[t].run(1);return e?(s.notifyWith(o,[l,1,0]),s.resolveWith(o,[l,e])):s.rejectWith(o,[l,e]),this}}),c=l.props;for(!function(e,t){var n,r,i,o,a;for(n in e)if(i=t[r=V(n)],o=e[n],Array.isArray(o)&&(i=o[1],o=e[n]=o[0]),n!==r&&(e[r]=o,delete e[n]),(a=k.cssHooks[r])&&"expand"in a)for(n in o=a.expand(o),delete e[r],o)n in e||(e[n]=o[n],t[n]=i);else t[r]=i}(c,l.opts.specialEasing);r<i;r++)if(n=dt.prefilters[r].call(l,o,c,l.opts))return m(n.stop)&&(k._queueHooks(l.elem,l.opts.queue).stop=n.stop.bind(n)),n;return k.map(c,pt,l),m(l.opts.start)&&l.opts.start.call(o,l),l.progress(l.opts.progress).done(l.opts.done,l.opts.complete).fail(l.opts.fail).always(l.opts.always),k.fx.timer(k.extend(u,{elem:o,anim:l,queue:l.opts.queue})),l}k.Animation=k.extend(dt,{tweeners:{"*":[function(e,t){var n=this.createTween(e,t);return le(n.elem,e,ne.exec(t),n),n}]},tweener:function(e,t){m(e)?(t=e,e=["*"]):e=e.match(R);for(var n,r=0,i=e.length;r<i;r++)n=e[r],dt.tweeners[n]=dt.tweeners[n]||[],dt.tweeners[n].unshift(t)},prefilters:[function(e,t,n){var r,i,o,a,s,u,l,c,f="width"in t||"height"in t,p=this,d={},h=e.style,g=e.nodeType&&se(e),v=Q.get(e,"fxshow");for(r in n.queue||(null==(a=k._queueHooks(e,"fx")).unqueued&&(a.unqueued=0,s=a.empty.fire,a.empty.fire=function(){a.unqueued||s()}),a.unqueued++,p.always(function(){p.always(function(){a.unqueued--,k.queue(e,"fx").length||a.empty.fire()})})),t)if(i=t[r],st.test(i)){if(delete t[r],o=o||"toggle"===i,i===(g?"hide":"show")){if("show"!==i||!v||void 0===v[r])continue;g=!0}d[r]=v&&v[r]||k.style(e,r)}if((u=!k.isEmptyObject(t))||!k.isEmptyObject(d))for(r in f&&1===e.nodeType&&(n.overflow=[h.overflow,h.overflowX,h.overflowY],null==(l=v&&v.display)&&(l=Q.get(e,"display")),"none"===(c=k.css(e,"display"))&&(l?c=l:(fe([e],!0),l=e.style.display||l,c=k.css(e,"display"),fe([e]))),("inline"===c||"inline-block"===c&&null!=l)&&"none"===k.css(e,"float")&&(u||(p.done(function(){h.display=l}),null==l&&(c=h.display,l="none"===c?"":c)),h.display="inline-block")),n.overflow&&(h.overflow="hidden",p.always(function(){h.overflow=n.overflow[0],h.overflowX=n.overflow[1],h.overflowY=n.overflow[2]})),u=!1,d)u||(v?"hidden"in v&&(g=v.hidden):v=Q.access(e,"fxshow",{display:l}),o&&(v.hidden=!g),g&&fe([e],!0),p.done(function(){for(r in g||fe([e]),Q.remove(e,"fxshow"),d)k.style(e,r,d[r])})),u=pt(g?v[r]:0,r,p),r in v||(v[r]=u.start,g&&(u.end=u.start,u.start=0))}],prefilter:function(e,t){t?dt.prefilters.unshift(e):dt.prefilters.push(e)}}),k.speed=function(e,t,n){var r=e&&"object"==typeof e?k.extend({},e):{complete:n||!n&&t||m(e)&&e,duration:e,easing:n&&t||t&&!m(t)&&t};return k.fx.off?r.duration=0:"number"!=typeof r.duration&&(r.duration in k.fx.speeds?r.duration=k.fx.speeds[r.duration]:r.duration=k.fx.speeds._default),null!=r.queue&&!0!==r.queue||(r.queue="fx"),r.old=r.complete,r.complete=function(){m(r.old)&&r.old.call(this),r.queue&&k.dequeue(this,r.queue)},r},k.fn.extend({fadeTo:function(e,t,n,r){return this.filter(se).css("opacity",0).show().end().animate({opacity:t},e,n,r)},animate:function(t,e,n,r){var i=k.isEmptyObject(t),o=k.speed(e,n,r),a=function(){var e=dt(this,k.extend({},t),o);(i||Q.get(this,"finish"))&&e.stop(!0)};return a.finish=a,i||!1===o.queue?this.each(a):this.queue(o.queue,a)},stop:function(i,e,o){var a=function(e){var t=e.stop;delete e.stop,t(o)};return"string"!=typeof i&&(o=e,e=i,i=void 0),e&&!1!==i&&this.queue(i||"fx",[]),this.each(function(){var e=!0,t=null!=i&&i+"queueHooks",n=k.timers,r=Q.get(this);if(t)r[t]&&r[t].stop&&a(r[t]);else for(t in r)r[t]&&r[t].stop&&ut.test(t)&&a(r[t]);for(t=n.length;t--;)n[t].elem!==this||null!=i&&n[t].queue!==i||(n[t].anim.stop(o),e=!1,n.splice(t,1));!e&&o||k.dequeue(this,i)})},finish:function(a){return!1!==a&&(a=a||"fx"),this.each(function(){var e,t=Q.get(this),n=t[a+"queue"],r=t[a+"queueHooks"],i=k.timers,o=n?n.length:0;for(t.finish=!0,k.queue(this,a,[]),r&&r.stop&&r.stop.call(this,!0),e=i.length;e--;)i[e].elem===this&&i[e].queue===a&&(i[e].anim.stop(!0),i.splice(e,1));for(e=0;e<o;e++)n[e]&&n[e].finish&&n[e].finish.call(this);delete t.finish})}}),k.each(["toggle","show","hide"],function(e,r){var i=k.fn[r];k.fn[r]=function(e,t,n){return null==e||"boolean"==typeof e?i.apply(this,arguments):this.animate(ft(r,!0),e,t,n)}}),k.each({slideDown:ft("show"),slideUp:ft("hide"),slideToggle:ft("toggle"),fadeIn:{opacity:"show"},fadeOut:{opacity:"hide"},fadeToggle:{opacity:"toggle"}},function(e,r){k.fn[e]=function(e,t,n){return this.animate(r,e,t,n)}}),k.timers=[],k.fx.tick=function(){var e,t=0,n=k.timers;for(rt=Date.now();t<n.length;t++)(e=n[t])()||n[t]!==e||n.splice(t--,1);n.length||k.fx.stop(),rt=void 0},k.fx.timer=function(e){k.timers.push(e),k.fx.start()},k.fx.interval=13,k.fx.start=function(){it||(it=!0,lt())},k.fx.stop=function(){it=null},k.fx.speeds={slow:600,fast:200,_default:400},k.fn.delay=function(r,e){return r=k.fx&&k.fx.speeds[r]||r,e=e||"fx",this.queue(e,function(e,t){var n=C.setTimeout(e,r);t.stop=function(){C.clearTimeout(n)}})},ot=E.createElement("input"),at=E.createElement("select").appendChild(E.createElement("option")),ot.type="checkbox",y.checkOn=""!==ot.value,y.optSelected=at.selected,(ot=E.createElement("input")).value="t",ot.type="radio",y.radioValue="t"===ot.value;var ht,gt=k.expr.attrHandle;k.fn.extend({attr:function(e,t){return _(this,k.attr,e,t,1<arguments.length)},removeAttr:function(e){return this.each(function(){k.removeAttr(this,e)})}}),k.extend({attr:function(e,t,n){var r,i,o=e.nodeType;if(3!==o&&8!==o&&2!==o)return"undefined"==typeof e.getAttribute?k.prop(e,t,n):(1===o&&k.isXMLDoc(e)||(i=k.attrHooks[t.toLowerCase()]||(k.expr.match.bool.test(t)?ht:void 0)),void 0!==n?null===n?void k.removeAttr(e,t):i&&"set"in i&&void 0!==(r=i.set(e,n,t))?r:(e.setAttribute(t,n+""),n):i&&"get"in i&&null!==(r=i.get(e,t))?r:null==(r=k.find.attr(e,t))?void 0:r)},attrHooks:{type:{set:function(e,t){if(!y.radioValue&&"radio"===t&&A(e,"input")){var n=e.value;return e.setAttribute("type",t),n&&(e.value=n),t}}}},removeAttr:function(e,t){var n,r=0,i=t&&t.match(R);if(i&&1===e.nodeType)while(n=i[r++])e.removeAttribute(n)}}),ht={set:function(e,t,n){return!1===t?k.removeAttr(e,n):e.setAttribute(n,n),n}},k.each(k.expr.match.bool.source.match(/\w+/g),function(e,t){var a=gt[t]||k.find.attr;gt[t]=function(e,t,n){var r,i,o=t.toLowerCase();return n||(i=gt[o],gt[o]=r,r=null!=a(e,t,n)?o:null,gt[o]=i),r}});var vt=/^(?:input|select|textarea|button)$/i,yt=/^(?:a|area)$/i;function mt(e){return(e.match(R)||[]).join(" ")}function xt(e){return e.getAttribute&&e.getAttribute("class")||""}function bt(e){return Array.isArray(e)?e:"string"==typeof e&&e.match(R)||[]}k.fn.extend({prop:function(e,t){return _(this,k.prop,e,t,1<arguments.length)},removeProp:function(e){return this.each(function(){delete this[k.propFix[e]||e]})}}),k.extend({prop:function(e,t,n){var r,i,o=e.nodeType;if(3!==o&&8!==o&&2!==o)return 1===o&&k.isXMLDoc(e)||(t=k.propFix[t]||t,i=k.propHooks[t]),void 0!==n?i&&"set"in i&&void 0!==(r=i.set(e,n,t))?r:e[t]=n:i&&"get"in i&&null!==(r=i.get(e,t))?r:e[t]},propHooks:{tabIndex:{get:function(e){var t=k.find.attr(e,"tabindex");return t?parseInt(t,10):vt.test(e.nodeName)||yt.test(e.nodeName)&&e.href?0:-1}}},propFix:{"for":"htmlFor","class":"className"}}),y.optSelected||(k.propHooks.selected={get:function(e){var t=e.parentNode;return t&&t.parentNode&&t.parentNode.selectedIndex,null},set:function(e){var t=e.parentNode;t&&(t.selectedIndex,t.parentNode&&t.parentNode.selectedIndex)}}),k.each(["tabIndex","readOnly","maxLength","cellSpacing","cellPadding","rowSpan","colSpan","useMap","frameBorder","contentEditable"],function(){k.propFix[this.toLowerCase()]=this}),k.fn.extend({addClass:function(t){var e,n,r,i,o,a,s,u=0;if(m(t))return this.each(function(e){k(this).addClass(t.call(this,e,xt(this)))});if((e=bt(t)).length)while(n=this[u++])if(i=xt(n),r=1===n.nodeType&&" "+mt(i)+" "){a=0;while(o=e[a++])r.indexOf(" "+o+" ")<0&&(r+=o+" ");i!==(s=mt(r))&&n.setAttribute("class",s)}return this},removeClass:function(t){var e,n,r,i,o,a,s,u=0;if(m(t))return this.each(function(e){k(this).removeClass(t.call(this,e,xt(this)))});if(!arguments.length)return this.attr("class","");if((e=bt(t)).length)while(n=this[u++])if(i=xt(n),r=1===n.nodeType&&" "+mt(i)+" "){a=0;while(o=e[a++])while(-1<r.indexOf(" "+o+" "))r=r.replace(" "+o+" "," ");i!==(s=mt(r))&&n.setAttribute("class",s)}return this},toggleClass:function(i,t){var o=typeof i,a="string"===o||Array.isArray(i);return"boolean"==typeof t&&a?t?this.addClass(i):this.removeClass(i):m(i)?this.each(function(e){k(this).toggleClass(i.call(this,e,xt(this),t),t)}):this.each(function(){var e,t,n,r;if(a){t=0,n=k(this),r=bt(i);while(e=r[t++])n.hasClass(e)?n.removeClass(e):n.addClass(e)}else void 0!==i&&"boolean"!==o||((e=xt(this))&&Q.set(this,"__className__",e),this.setAttribute&&this.setAttribute("class",e||!1===i?"":Q.get(this,"__className__")||""))})},hasClass:function(e){var t,n,r=0;t=" "+e+" ";while(n=this[r++])if(1===n.nodeType&&-1<(" "+mt(xt(n))+" ").indexOf(t))return!0;return!1}});var wt=/\r/g;k.fn.extend({val:function(n){var r,e,i,t=this[0];return arguments.length?(i=m(n),this.each(function(e){var t;1===this.nodeType&&(null==(t=i?n.call(this,e,k(this).val()):n)?t="":"number"==typeof t?t+="":Array.isArray(t)&&(t=k.map(t,function(e){return null==e?"":e+""})),(r=k.valHooks[this.type]||k.valHooks[this.nodeName.toLowerCase()])&&"set"in r&&void 0!==r.set(this,t,"value")||(this.value=t))})):t?(r=k.valHooks[t.type]||k.valHooks[t.nodeName.toLowerCase()])&&"get"in r&&void 0!==(e=r.get(t,"value"))?e:"string"==typeof(e=t.value)?e.replace(wt,""):null==e?"":e:void 0}}),k.extend({valHooks:{option:{get:function(e){var t=k.find.attr(e,"value");return null!=t?t:mt(k.text(e))}},select:{get:function(e){var t,n,r,i=e.options,o=e.selectedIndex,a="select-one"===e.type,s=a?null:[],u=a?o+1:i.length;for(r=o<0?u:a?o:0;r<u;r++)if(((n=i[r]).selected||r===o)&&!n.disabled&&(!n.parentNode.disabled||!A(n.parentNode,"optgroup"))){if(t=k(n).val(),a)return t;s.push(t)}return s},set:function(e,t){var n,r,i=e.options,o=k.makeArray(t),a=i.length;while(a--)((r=i[a]).selected=-1<k.inArray(k.valHooks.option.get(r),o))&&(n=!0);return n||(e.selectedIndex=-1),o}}}}),k.each(["radio","checkbox"],function(){k.valHooks[this]={set:function(e,t){if(Array.isArray(t))return e.checked=-1<k.inArray(k(e).val(),t)}},y.checkOn||(k.valHooks[this].get=function(e){return null===e.getAttribute("value")?"on":e.value})}),y.focusin="onfocusin"in C;var Tt=/^(?:focusinfocus|focusoutblur)$/,Ct=function(e){e.stopPropagation()};k.extend(k.event,{trigger:function(e,t,n,r){var i,o,a,s,u,l,c,f,p=[n||E],d=v.call(e,"type")?e.type:e,h=v.call(e,"namespace")?e.namespace.split("."):[];if(o=f=a=n=n||E,3!==n.nodeType&&8!==n.nodeType&&!Tt.test(d+k.event.triggered)&&(-1<d.indexOf(".")&&(d=(h=d.split(".")).shift(),h.sort()),u=d.indexOf(":")<0&&"on"+d,(e=e[k.expando]?e:new k.Event(d,"object"==typeof e&&e)).isTrigger=r?2:3,e.namespace=h.join("."),e.rnamespace=e.namespace?new RegExp("(^|\\.)"+h.join("\\.(?:.*\\.|)")+"(\\.|$)"):null,e.result=void 0,e.target||(e.target=n),t=null==t?[e]:k.makeArray(t,[e]),c=k.event.special[d]||{},r||!c.trigger||!1!==c.trigger.apply(n,t))){if(!r&&!c.noBubble&&!x(n)){for(s=c.delegateType||d,Tt.test(s+d)||(o=o.parentNode);o;o=o.parentNode)p.push(o),a=o;a===(n.ownerDocument||E)&&p.push(a.defaultView||a.parentWindow||C)}i=0;while((o=p[i++])&&!e.isPropagationStopped())f=o,e.type=1<i?s:c.bindType||d,(l=(Q.get(o,"events")||{})[e.type]&&Q.get(o,"handle"))&&l.apply(o,t),(l=u&&o[u])&&l.apply&&G(o)&&(e.result=l.apply(o,t),!1===e.result&&e.preventDefault());return e.type=d,r||e.isDefaultPrevented()||c._default&&!1!==c._default.apply(p.pop(),t)||!G(n)||u&&m(n[d])&&!x(n)&&((a=n[u])&&(n[u]=null),k.event.triggered=d,e.isPropagationStopped()&&f.addEventListener(d,Ct),n[d](),e.isPropagationStopped()&&f.removeEventListener(d,Ct),k.event.triggered=void 0,a&&(n[u]=a)),e.result}},simulate:function(e,t,n){var r=k.extend(new k.Event,n,{type:e,isSimulated:!0});k.event.trigger(r,null,t)}}),k.fn.extend({trigger:function(e,t){return this.each(function(){k.event.trigger(e,t,this)})},triggerHandler:function(e,t){var n=this[0];if(n)return k.event.trigger(e,t,n,!0)}}),y.focusin||k.each({focus:"focusin",blur:"focusout"},function(n,r){var i=function(e){k.event.simulate(r,e.target,k.event.fix(e))};k.event.special[r]={setup:function(){var e=this.ownerDocument||this,t=Q.access(e,r);t||e.addEventListener(n,i,!0),Q.access(e,r,(t||0)+1)},teardown:function(){var e=this.ownerDocument||this,t=Q.access(e,r)-1;t?Q.access(e,r,t):(e.removeEventListener(n,i,!0),Q.remove(e,r))}}});var Et=C.location,kt=Date.now(),St=/\?/;k.parseXML=function(e){var t;if(!e||"string"!=typeof e)return null;try{t=(new C.DOMParser).parseFromString(e,"text/xml")}catch(e){t=void 0}return t&&!t.getElementsByTagName("parsererror").length||k.error("Invalid XML: "+e),t};var Nt=/\[\]$/,At=/\r?\n/g,Dt=/^(?:submit|button|image|reset|file)$/i,jt=/^(?:input|select|textarea|keygen)/i;function qt(n,e,r,i){var t;if(Array.isArray(e))k.each(e,function(e,t){r||Nt.test(n)?i(n,t):qt(n+"["+("object"==typeof t&&null!=t?e:"")+"]",t,r,i)});else if(r||"object"!==w(e))i(n,e);else for(t in e)qt(n+"["+t+"]",e[t],r,i)}k.param=function(e,t){var n,r=[],i=function(e,t){var n=m(t)?t():t;r[r.length]=encodeURIComponent(e)+"="+encodeURIComponent(null==n?"":n)};if(null==e)return"";if(Array.isArray(e)||e.jquery&&!k.isPlainObject(e))k.each(e,function(){i(this.name,this.value)});else for(n in e)qt(n,e[n],t,i);return r.join("&")},k.fn.extend({serialize:function(){return k.param(this.serializeArray())},serializeArray:function(){return this.map(function(){var e=k.prop(this,"elements");return e?k.makeArray(e):this}).filter(function(){var e=this.type;return this.name&&!k(this).is(":disabled")&&jt.test(this.nodeName)&&!Dt.test(e)&&(this.checked||!pe.test(e))}).map(function(e,t){var n=k(this).val();return null==n?null:Array.isArray(n)?k.map(n,function(e){return{name:t.name,value:e.replace(At,"\r\n")}}):{name:t.name,value:n.replace(At,"\r\n")}}).get()}});var Lt=/%20/g,Ht=/#.*$/,Ot=/([?&])_=[^&]*/,Pt=/^(.*?):[ \t]*([^\r\n]*)$/gm,Rt=/^(?:GET|HEAD)$/,Mt=/^\/\//,It={},Wt={},$t="*/".concat("*"),Ft=E.createElement("a");function Bt(o){return function(e,t){"string"!=typeof e&&(t=e,e="*");var n,r=0,i=e.toLowerCase().match(R)||[];if(m(t))while(n=i[r++])"+"===n[0]?(n=n.slice(1)||"*",(o[n]=o[n]||[]).unshift(t)):(o[n]=o[n]||[]).push(t)}}function _t(t,i,o,a){var s={},u=t===Wt;function l(e){var r;return s[e]=!0,k.each(t[e]||[],function(e,t){var n=t(i,o,a);return"string"!=typeof n||u||s[n]?u?!(r=n):void 0:(i.dataTypes.unshift(n),l(n),!1)}),r}return l(i.dataTypes[0])||!s["*"]&&l("*")}function zt(e,t){var n,r,i=k.ajaxSettings.flatOptions||{};for(n in t)void 0!==t[n]&&((i[n]?e:r||(r={}))[n]=t[n]);return r&&k.extend(!0,e,r),e}Ft.href=Et.href,k.extend({active:0,lastModified:{},etag:{},ajaxSettings:{url:Et.href,type:"GET",isLocal:/^(?:about|app|app-storage|.+-extension|file|res|widget):$/.test(Et.protocol),global:!0,processData:!0,async:!0,contentType:"application/x-www-form-urlencoded; charset=UTF-8",accepts:{"*":$t,text:"text/plain",html:"text/html",xml:"application/xml, text/xml",json:"application/json, text/javascript"},contents:{xml:/\bxml\b/,html:/\bhtml/,json:/\bjson\b/},responseFields:{xml:"responseXML",text:"responseText",json:"responseJSON"},converters:{"* text":String,"text html":!0,"text json":JSON.parse,"text xml":k.parseXML},flatOptions:{url:!0,context:!0}},ajaxSetup:function(e,t){return t?zt(zt(e,k.ajaxSettings),t):zt(k.ajaxSettings,e)},ajaxPrefilter:Bt(It),ajaxTransport:Bt(Wt),ajax:function(e,t){"object"==typeof e&&(t=e,e=void 0),t=t||{};var c,f,p,n,d,r,h,g,i,o,v=k.ajaxSetup({},t),y=v.context||v,m=v.context&&(y.nodeType||y.jquery)?k(y):k.event,x=k.Deferred(),b=k.Callbacks("once memory"),w=v.statusCode||{},a={},s={},u="canceled",T={readyState:0,getResponseHeader:function(e){var t;if(h){if(!n){n={};while(t=Pt.exec(p))n[t[1].toLowerCase()+" "]=(n[t[1].toLowerCase()+" "]||[]).concat(t[2])}t=n[e.toLowerCase()+" "]}return null==t?null:t.join(", ")},getAllResponseHeaders:function(){return h?p:null},setRequestHeader:function(e,t){return null==h&&(e=s[e.toLowerCase()]=s[e.toLowerCase()]||e,a[e]=t),this},overrideMimeType:function(e){return null==h&&(v.mimeType=e),this},statusCode:function(e){var t;if(e)if(h)T.always(e[T.status]);else for(t in e)w[t]=[w[t],e[t]];return this},abort:function(e){var t=e||u;return c&&c.abort(t),l(0,t),this}};if(x.promise(T),v.url=((e||v.url||Et.href)+"").replace(Mt,Et.protocol+"//"),v.type=t.method||t.type||v.method||v.type,v.dataTypes=(v.dataType||"*").toLowerCase().match(R)||[""],null==v.crossDomain){r=E.createElement("a");try{r.href=v.url,r.href=r.href,v.crossDomain=Ft.protocol+"//"+Ft.host!=r.protocol+"//"+r.host}catch(e){v.crossDomain=!0}}if(v.data&&v.processData&&"string"!=typeof v.data&&(v.data=k.param(v.data,v.traditional)),_t(It,v,t,T),h)return T;for(i in(g=k.event&&v.global)&&0==k.active++&&k.event.trigger("ajaxStart"),v.type=v.type.toUpperCase(),v.hasContent=!Rt.test(v.type),f=v.url.replace(Ht,""),v.hasContent?v.data&&v.processData&&0===(v.contentType||"").indexOf("application/x-www-form-urlencoded")&&(v.data=v.data.replace(Lt,"+")):(o=v.url.slice(f.length),v.data&&(v.processData||"string"==typeof v.data)&&(f+=(St.test(f)?"&":"?")+v.data,delete v.data),!1===v.cache&&(f=f.replace(Ot,"$1"),o=(St.test(f)?"&":"?")+"_="+kt+++o),v.url=f+o),v.ifModified&&(k.lastModified[f]&&T.setRequestHeader("If-Modified-Since",k.lastModified[f]),k.etag[f]&&T.setRequestHeader("If-None-Match",k.etag[f])),(v.data&&v.hasContent&&!1!==v.contentType||t.contentType)&&T.setRequestHeader("Content-Type",v.contentType),T.setRequestHeader("Accept",v.dataTypes[0]&&v.accepts[v.dataTypes[0]]?v.accepts[v.dataTypes[0]]+("*"!==v.dataTypes[0]?", "+$t+"; q=0.01":""):v.accepts["*"]),v.headers)T.setRequestHeader(i,v.headers[i]);if(v.beforeSend&&(!1===v.beforeSend.call(y,T,v)||h))return T.abort();if(u="abort",b.add(v.complete),T.done(v.success),T.fail(v.error),c=_t(Wt,v,t,T)){if(T.readyState=1,g&&m.trigger("ajaxSend",[T,v]),h)return T;v.async&&0<v.timeout&&(d=C.setTimeout(function(){T.abort("timeout")},v.timeout));try{h=!1,c.send(a,l)}catch(e){if(h)throw e;l(-1,e)}}else l(-1,"No Transport");function l(e,t,n,r){var i,o,a,s,u,l=t;h||(h=!0,d&&C.clearTimeout(d),c=void 0,p=r||"",T.readyState=0<e?4:0,i=200<=e&&e<300||304===e,n&&(s=function(e,t,n){var r,i,o,a,s=e.contents,u=e.dataTypes;while("*"===u[0])u.shift(),void 0===r&&(r=e.mimeType||t.getResponseHeader("Content-Type"));if(r)for(i in s)if(s[i]&&s[i].test(r)){u.unshift(i);break}if(u[0]in n)o=u[0];else{for(i in n){if(!u[0]||e.converters[i+" "+u[0]]){o=i;break}a||(a=i)}o=o||a}if(o)return o!==u[0]&&u.unshift(o),n[o]}(v,T,n)),s=function(e,t,n,r){var i,o,a,s,u,l={},c=e.dataTypes.slice();if(c[1])for(a in e.converters)l[a.toLowerCase()]=e.converters[a];o=c.shift();while(o)if(e.responseFields[o]&&(n[e.responseFields[o]]=t),!u&&r&&e.dataFilter&&(t=e.dataFilter(t,e.dataType)),u=o,o=c.shift())if("*"===o)o=u;else if("*"!==u&&u!==o){if(!(a=l[u+" "+o]||l["* "+o]))for(i in l)if((s=i.split(" "))[1]===o&&(a=l[u+" "+s[0]]||l["* "+s[0]])){!0===a?a=l[i]:!0!==l[i]&&(o=s[0],c.unshift(s[1]));break}if(!0!==a)if(a&&e["throws"])t=a(t);else try{t=a(t)}catch(e){return{state:"parsererror",error:a?e:"No conversion from "+u+" to "+o}}}return{state:"success",data:t}}(v,s,T,i),i?(v.ifModified&&((u=T.getResponseHeader("Last-Modified"))&&(k.lastModified[f]=u),(u=T.getResponseHeader("etag"))&&(k.etag[f]=u)),204===e||"HEAD"===v.type?l="nocontent":304===e?l="notmodified":(l=s.state,o=s.data,i=!(a=s.error))):(a=l,!e&&l||(l="error",e<0&&(e=0))),T.status=e,T.statusText=(t||l)+"",i?x.resolveWith(y,[o,l,T]):x.rejectWith(y,[T,l,a]),T.statusCode(w),w=void 0,g&&m.trigger(i?"ajaxSuccess":"ajaxError",[T,v,i?o:a]),b.fireWith(y,[T,l]),g&&(m.trigger("ajaxComplete",[T,v]),--k.active||k.event.trigger("ajaxStop")))}return T},getJSON:function(e,t,n){return k.get(e,t,n,"json")},getScript:function(e,t){return k.get(e,void 0,t,"script")}}),k.each(["get","post"],function(e,i){k[i]=function(e,t,n,r){return m(t)&&(r=r||n,n=t,t=void 0),k.ajax(k.extend({url:e,type:i,dataType:r,data:t,success:n},k.isPlainObject(e)&&e))}}),k._evalUrl=function(e,t){return k.ajax({url:e,type:"GET",dataType:"script",cache:!0,async:!1,global:!1,converters:{"text script":function(){}},dataFilter:function(e){k.globalEval(e,t)}})},k.fn.extend({wrapAll:function(e){var t;return this[0]&&(m(e)&&(e=e.call(this[0])),t=k(e,this[0].ownerDocument).eq(0).clone(!0),this[0].parentNode&&t.insertBefore(this[0]),t.map(function(){var e=this;while(e.firstElementChild)e=e.firstElementChild;return e}).append(this)),this},wrapInner:function(n){return m(n)?this.each(function(e){k(this).wrapInner(n.call(this,e))}):this.each(function(){var e=k(this),t=e.contents();t.length?t.wrapAll(n):e.append(n)})},wrap:function(t){var n=m(t);return this.each(function(e){k(this).wrapAll(n?t.call(this,e):t)})},unwrap:function(e){return this.parent(e).not("body").each(function(){k(this).replaceWith(this.childNodes)}),this}}),k.expr.pseudos.hidden=function(e){return!k.expr.pseudos.visible(e)},k.expr.pseudos.visible=function(e){return!!(e.offsetWidth||e.offsetHeight||e.getClientRects().length)},k.ajaxSettings.xhr=function(){try{return new C.XMLHttpRequest}catch(e){}};var Ut={0:200,1223:204},Xt=k.ajaxSettings.xhr();y.cors=!!Xt&&"withCredentials"in Xt,y.ajax=Xt=!!Xt,k.ajaxTransport(function(i){var o,a;if(y.cors||Xt&&!i.crossDomain)return{send:function(e,t){var n,r=i.xhr();if(r.open(i.type,i.url,i.async,i.username,i.password),i.xhrFields)for(n in i.xhrFields)r[n]=i.xhrFields[n];for(n in i.mimeType&&r.overrideMimeType&&r.overrideMimeType(i.mimeType),i.crossDomain||e["X-Requested-With"]||(e["X-Requested-With"]="XMLHttpRequest"),e)r.setRequestHeader(n,e[n]);o=function(e){return function(){o&&(o=a=r.onload=r.onerror=r.onabort=r.ontimeout=r.onreadystatechange=null,"abort"===e?r.abort():"error"===e?"number"!=typeof r.status?t(0,"error"):t(r.status,r.statusText):t(Ut[r.status]||r.status,r.statusText,"text"!==(r.responseType||"text")||"string"!=typeof r.responseText?{binary:r.response}:{text:r.responseText},r.getAllResponseHeaders()))}},r.onload=o(),a=r.onerror=r.ontimeout=o("error"),void 0!==r.onabort?r.onabort=a:r.onreadystatechange=function(){4===r.readyState&&C.setTimeout(function(){o&&a()})},o=o("abort");try{r.send(i.hasContent&&i.data||null)}catch(e){if(o)throw e}},abort:function(){o&&o()}}}),k.ajaxPrefilter(function(e){e.crossDomain&&(e.contents.script=!1)}),k.ajaxSetup({accepts:{script:"text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"},contents:{script:/\b(?:java|ecma)script\b/},converters:{"text script":function(e){return k.globalEval(e),e}}}),k.ajaxPrefilter("script",function(e){void 0===e.cache&&(e.cache=!1),e.crossDomain&&(e.type="GET")}),k.ajaxTransport("script",function(n){var r,i;if(n.crossDomain||n.scriptAttrs)return{send:function(e,t){r=k("<script>").attr(n.scriptAttrs||{}).prop({charset:n.scriptCharset,src:n.url}).on("load error",i=function(e){r.remove(),i=null,e&&t("error"===e.type?404:200,e.type)}),E.head.appendChild(r[0])},abort:function(){i&&i()}}});var Vt,Gt=[],Yt=/(=)\?(?=&|$)|\?\?/;k.ajaxSetup({jsonp:"callback",jsonpCallback:function(){var e=Gt.pop()||k.expando+"_"+kt++;return this[e]=!0,e}}),k.ajaxPrefilter("json jsonp",function(e,t,n){var r,i,o,a=!1!==e.jsonp&&(Yt.test(e.url)?"url":"string"==typeof e.data&&0===(e.contentType||"").indexOf("application/x-www-form-urlencoded")&&Yt.test(e.data)&&"data");if(a||"jsonp"===e.dataTypes[0])return r=e.jsonpCallback=m(e.jsonpCallback)?e.jsonpCallback():e.jsonpCallback,a?e[a]=e[a].replace(Yt,"$1"+r):!1!==e.jsonp&&(e.url+=(St.test(e.url)?"&":"?")+e.jsonp+"="+r),e.converters["script json"]=function(){return o||k.error(r+" was not called"),o[0]},e.dataTypes[0]="json",i=C[r],C[r]=function(){o=arguments},n.always(function(){void 0===i?k(C).removeProp(r):C[r]=i,e[r]&&(e.jsonpCallback=t.jsonpCallback,Gt.push(r)),o&&m(i)&&i(o[0]),o=i=void 0}),"script"}),y.createHTMLDocument=((Vt=E.implementation.createHTMLDocument("").body).innerHTML="<form></form><form></form>",2===Vt.childNodes.length),k.parseHTML=function(e,t,n){return"string"!=typeof e?[]:("boolean"==typeof t&&(n=t,t=!1),t||(y.createHTMLDocument?((r=(t=E.implementation.createHTMLDocument("")).createElement("base")).href=E.location.href,t.head.appendChild(r)):t=E),o=!n&&[],(i=D.exec(e))?[t.createElement(i[1])]:(i=we([e],t,o),o&&o.length&&k(o).remove(),k.merge([],i.childNodes)));var r,i,o},k.fn.load=function(e,t,n){var r,i,o,a=this,s=e.indexOf(" ");return-1<s&&(r=mt(e.slice(s)),e=e.slice(0,s)),m(t)?(n=t,t=void 0):t&&"object"==typeof t&&(i="POST"),0<a.length&&k.ajax({url:e,type:i||"GET",dataType:"html",data:t}).done(function(e){o=arguments,a.html(r?k("<div>").append(k.parseHTML(e)).find(r):e)}).always(n&&function(e,t){a.each(function(){n.apply(this,o||[e.responseText,t,e])})}),this},k.each(["ajaxStart","ajaxStop","ajaxComplete","ajaxError","ajaxSuccess","ajaxSend"],function(e,t){k.fn[t]=function(e){return this.on(t,e)}}),k.expr.pseudos.animated=function(t){return k.grep(k.timers,function(e){return t===e.elem}).length},k.offset={setOffset:function(e,t,n){var r,i,o,a,s,u,l=k.css(e,"position"),c=k(e),f={};"static"===l&&(e.style.position="relative"),s=c.offset(),o=k.css(e,"top"),u=k.css(e,"left"),("absolute"===l||"fixed"===l)&&-1<(o+u).indexOf("auto")?(a=(r=c.position()).top,i=r.left):(a=parseFloat(o)||0,i=parseFloat(u)||0),m(t)&&(t=t.call(e,n,k.extend({},s))),null!=t.top&&(f.top=t.top-s.top+a),null!=t.left&&(f.left=t.left-s.left+i),"using"in t?t.using.call(e,f):c.css(f)}},k.fn.extend({offset:function(t){if(arguments.length)return void 0===t?this:this.each(function(e){k.offset.setOffset(this,t,e)});var e,n,r=this[0];return r?r.getClientRects().length?(e=r.getBoundingClientRect(),n=r.ownerDocument.defaultView,{top:e.top+n.pageYOffset,left:e.left+n.pageXOffset}):{top:0,left:0}:void 0},position:function(){if(this[0]){var e,t,n,r=this[0],i={top:0,left:0};if("fixed"===k.css(r,"position"))t=r.getBoundingClientRect();else{t=this.offset(),n=r.ownerDocument,e=r.offsetParent||n.documentElement;while(e&&(e===n.body||e===n.documentElement)&&"static"===k.css(e,"position"))e=e.parentNode;e&&e!==r&&1===e.nodeType&&((i=k(e).offset()).top+=k.css(e,"borderTopWidth",!0),i.left+=k.css(e,"borderLeftWidth",!0))}return{top:t.top-i.top-k.css(r,"marginTop",!0),left:t.left-i.left-k.css(r,"marginLeft",!0)}}},offsetParent:function(){return this.map(function(){var e=this.offsetParent;while(e&&"static"===k.css(e,"position"))e=e.offsetParent;return e||ie})}}),k.each({scrollLeft:"pageXOffset",scrollTop:"pageYOffset"},function(t,i){var o="pageYOffset"===i;k.fn[t]=function(e){return _(this,function(e,t,n){var r;if(x(e)?r=e:9===e.nodeType&&(r=e.defaultView),void 0===n)return r?r[i]:e[t];r?r.scrollTo(o?r.pageXOffset:n,o?n:r.pageYOffset):e[t]=n},t,e,arguments.length)}}),k.each(["top","left"],function(e,n){k.cssHooks[n]=ze(y.pixelPosition,function(e,t){if(t)return t=_e(e,n),$e.test(t)?k(e).position()[n]+"px":t})}),k.each({Height:"height",Width:"width"},function(a,s){k.each({padding:"inner"+a,content:s,"":"outer"+a},function(r,o){k.fn[o]=function(e,t){var n=arguments.length&&(r||"boolean"!=typeof e),i=r||(!0===e||!0===t?"margin":"border");return _(this,function(e,t,n){var r;return x(e)?0===o.indexOf("outer")?e["inner"+a]:e.document.documentElement["client"+a]:9===e.nodeType?(r=e.documentElement,Math.max(e.body["scroll"+a],r["scroll"+a],e.body["offset"+a],r["offset"+a],r["client"+a])):void 0===n?k.css(e,t,i):k.style(e,t,n,i)},s,n?e:void 0,n)}})}),k.each("blur focus focusin focusout resize scroll click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup contextmenu".split(" "),function(e,n){k.fn[n]=function(e,t){return 0<arguments.length?this.on(n,null,e,t):this.trigger(n)}}),k.fn.extend({hover:function(e,t){return this.mouseenter(e).mouseleave(t||e)}}),k.fn.extend({bind:function(e,t,n){return this.on(e,null,t,n)},unbind:function(e,t){return this.off(e,null,t)},delegate:function(e,t,n,r){return this.on(t,e,n,r)},undelegate:function(e,t,n){return 1===arguments.length?this.off(e,"**"):this.off(t,e||"**",n)}}),k.proxy=function(e,t){var n,r,i;if("string"==typeof t&&(n=e[t],t=e,e=n),m(e))return r=s.call(arguments,2),(i=function(){return e.apply(t||this,r.concat(s.call(arguments)))}).guid=e.guid=e.guid||k.guid++,i},k.holdReady=function(e){e?k.readyWait++:k.ready(!0)},k.isArray=Array.isArray,k.parseJSON=JSON.parse,k.nodeName=A,k.isFunction=m,k.isWindow=x,k.camelCase=V,k.type=w,k.now=Date.now,k.isNumeric=function(e){var t=k.type(e);return("number"===t||"string"===t)&&!isNaN(e-parseFloat(e))},"function"==typeof define&&define.amd&&define("jquery",[],function(){return k});var Qt=C.jQuery,Jt=C.$;return k.noConflict=function(e){return C.$===k&&(C.$=Jt),e&&C.jQuery===k&&(C.jQuery=Qt),k},e||(C.jQuery=C.$=k),k});
;/*! jQuery Migrate v1.4.1 | (c) jQuery Foundation and other contributors | jquery.org/license */
"undefined"==typeof jQuery.migrateMute&&(jQuery.migrateMute=!0),function(a,b,c){function d(c){var d=b.console;f[c]||(f[c]=!0,a.migrateWarnings.push(c),d&&d.warn&&!a.migrateMute&&(d.warn("JQMIGRATE: "+c),a.migrateTrace&&d.trace&&d.trace()))}function e(b,c,e,f){if(Object.defineProperty)try{return void Object.defineProperty(b,c,{configurable:!0,enumerable:!0,get:function(){return d(f),e},set:function(a){d(f),e=a}})}catch(g){}a._definePropertyBroken=!0,b[c]=e}a.migrateVersion="1.4.1";var f={};a.migrateWarnings=[],b.console&&b.console.log&&b.console.log("JQMIGRATE: Migrate is installed"+(a.migrateMute?"":" with logging active")+", version "+a.migrateVersion),a.migrateTrace===c&&(a.migrateTrace=!0),a.migrateReset=function(){f={},a.migrateWarnings.length=0},"BackCompat"===document.compatMode&&d("jQuery is not compatible with Quirks Mode");var g=a("<input/>",{size:1}).attr("size")&&a.attrFn,h=a.attr,i=a.attrHooks.value&&a.attrHooks.value.get||function(){return null},j=a.attrHooks.value&&a.attrHooks.value.set||function(){return c},k=/^(?:input|button)$/i,l=/^[238]$/,m=/^(?:autofocus|autoplay|async|checked|controls|defer|disabled|hidden|loop|multiple|open|readonly|required|scoped|selected)$/i,n=/^(?:checked|selected)$/i;e(a,"attrFn",g||{},"jQuery.attrFn is deprecated"),a.attr=function(b,e,f,i){var j=e.toLowerCase(),o=b&&b.nodeType;return i&&(h.length<4&&d("jQuery.fn.attr( props, pass ) is deprecated"),b&&!l.test(o)&&(g?e in g:a.isFunction(a.fn[e])))?a(b)[e](f):("type"===e&&f!==c&&k.test(b.nodeName)&&b.parentNode&&d("Can't change the 'type' of an input or button in IE 6/7/8"),!a.attrHooks[j]&&m.test(j)&&(a.attrHooks[j]={get:function(b,d){var e,f=a.prop(b,d);return f===!0||"boolean"!=typeof f&&(e=b.getAttributeNode(d))&&e.nodeValue!==!1?d.toLowerCase():c},set:function(b,c,d){var e;return c===!1?a.removeAttr(b,d):(e=a.propFix[d]||d,e in b&&(b[e]=!0),b.setAttribute(d,d.toLowerCase())),d}},n.test(j)&&d("jQuery.fn.attr('"+j+"') might use property instead of attribute")),h.call(a,b,e,f))},a.attrHooks.value={get:function(a,b){var c=(a.nodeName||"").toLowerCase();return"button"===c?i.apply(this,arguments):("input"!==c&&"option"!==c&&d("jQuery.fn.attr('value') no longer gets properties"),b in a?a.value:null)},set:function(a,b){var c=(a.nodeName||"").toLowerCase();return"button"===c?j.apply(this,arguments):("input"!==c&&"option"!==c&&d("jQuery.fn.attr('value', val) no longer sets properties"),void(a.value=b))}};var o,p,q=a.fn.init,r=a.find,s=a.parseJSON,t=/^\s*</,u=/\[(\s*[-\w]+\s*)([~|^$*]?=)\s*([-\w#]*?#[-\w#]*)\s*\]/,v=/\[(\s*[-\w]+\s*)([~|^$*]?=)\s*([-\w#]*?#[-\w#]*)\s*\]/g,w=/^([^<]*)(<[\w\W]+>)([^>]*)$/;a.fn.init=function(b,e,f){var g,h;return b&&"string"==typeof b&&!a.isPlainObject(e)&&(g=w.exec(a.trim(b)))&&g[0]&&(t.test(b)||d("$(html) HTML strings must start with '<' character"),g[3]&&d("$(html) HTML text after last tag is ignored"),"#"===g[0].charAt(0)&&(d("HTML string cannot start with a '#' character"),a.error("JQMIGRATE: Invalid selector string (XSS)")),e&&e.context&&e.context.nodeType&&(e=e.context),a.parseHTML)?q.call(this,a.parseHTML(g[2],e&&e.ownerDocument||e||document,!0),e,f):(h=q.apply(this,arguments),b&&b.selector!==c?(h.selector=b.selector,h.context=b.context):(h.selector="string"==typeof b?b:"",b&&(h.context=b.nodeType?b:e||document)),h)},a.fn.init.prototype=a.fn,a.find=function(a){var b=Array.prototype.slice.call(arguments);if("string"==typeof a&&u.test(a))try{document.querySelector(a)}catch(c){a=a.replace(v,function(a,b,c,d){return"["+b+c+'"'+d+'"]'});try{document.querySelector(a),d("Attribute selector with '#' must be quoted: "+b[0]),b[0]=a}catch(e){d("Attribute selector with '#' was not fixed: "+b[0])}}return r.apply(this,b)};var x;for(x in r)Object.prototype.hasOwnProperty.call(r,x)&&(a.find[x]=r[x]);a.parseJSON=function(a){return a?s.apply(this,arguments):(d("jQuery.parseJSON requires a valid JSON string"),null)},a.uaMatch=function(a){a=a.toLowerCase();var b=/(chrome)[ \/]([\w.]+)/.exec(a)||/(webkit)[ \/]([\w.]+)/.exec(a)||/(opera)(?:.*version|)[ \/]([\w.]+)/.exec(a)||/(msie) ([\w.]+)/.exec(a)||a.indexOf("compatible")<0&&/(mozilla)(?:.*? rv:([\w.]+)|)/.exec(a)||[];return{browser:b[1]||"",version:b[2]||"0"}},a.browser||(o=a.uaMatch(navigator.userAgent),p={},o.browser&&(p[o.browser]=!0,p.version=o.version),p.chrome?p.webkit=!0:p.webkit&&(p.safari=!0),a.browser=p),e(a,"browser",a.browser,"jQuery.browser is deprecated"),a.boxModel=a.support.boxModel="CSS1Compat"===document.compatMode,e(a,"boxModel",a.boxModel,"jQuery.boxModel is deprecated"),e(a.support,"boxModel",a.support.boxModel,"jQuery.support.boxModel is deprecated"),a.sub=function(){function b(a,c){return new b.fn.init(a,c)}a.extend(!0,b,this),b.superclass=this,b.fn=b.prototype=this(),b.fn.constructor=b,b.sub=this.sub,b.fn.init=function(d,e){var f=a.fn.init.call(this,d,e,c);return f instanceof b?f:b(f)},b.fn.init.prototype=b.fn;var c=b(document);return d("jQuery.sub() is deprecated"),b},a.fn.size=function(){return d("jQuery.fn.size() is deprecated; use the .length property"),this.length};var y=!1;a.swap&&a.each(["height","width","reliableMarginRight"],function(b,c){var d=a.cssHooks[c]&&a.cssHooks[c].get;d&&(a.cssHooks[c].get=function(){var a;return y=!0,a=d.apply(this,arguments),y=!1,a})}),a.swap=function(a,b,c,e){var f,g,h={};y||d("jQuery.swap() is undocumented and deprecated");for(g in b)h[g]=a.style[g],a.style[g]=b[g];f=c.apply(a,e||[]);for(g in b)a.style[g]=h[g];return f},a.ajaxSetup({converters:{"text json":a.parseJSON}});var z=a.fn.data;a.fn.data=function(b){var e,f,g=this[0];return!g||"events"!==b||1!==arguments.length||(e=a.data(g,b),f=a._data(g,b),e!==c&&e!==f||f===c)?z.apply(this,arguments):(d("Use of jQuery.fn.data('events') is deprecated"),f)};var A=/\/(java|ecma)script/i;a.clean||(a.clean=function(b,c,e,f){c=c||document,c=!c.nodeType&&c[0]||c,c=c.ownerDocument||c,d("jQuery.clean() is deprecated");var g,h,i,j,k=[];if(a.merge(k,a.buildFragment(b,c).childNodes),e)for(i=function(a){return!a.type||A.test(a.type)?f?f.push(a.parentNode?a.parentNode.removeChild(a):a):e.appendChild(a):void 0},g=0;null!=(h=k[g]);g++)a.nodeName(h,"script")&&i(h)||(e.appendChild(h),"undefined"!=typeof h.getElementsByTagName&&(j=a.grep(a.merge([],h.getElementsByTagName("script")),i),k.splice.apply(k,[g+1,0].concat(j)),g+=j.length));return k});var B=a.event.add,C=a.event.remove,D=a.event.trigger,E=a.fn.toggle,F=a.fn.live,G=a.fn.die,H=a.fn.load,I="ajaxStart|ajaxStop|ajaxSend|ajaxComplete|ajaxError|ajaxSuccess",J=new RegExp("\\b(?:"+I+")\\b"),K=/(?:^|\s)hover(\.\S+|)\b/,L=function(b){return"string"!=typeof b||a.event.special.hover?b:(K.test(b)&&d("'hover' pseudo-event is deprecated, use 'mouseenter mouseleave'"),b&&b.replace(K,"mouseenter$1 mouseleave$1"))};a.event.props&&"attrChange"!==a.event.props[0]&&a.event.props.unshift("attrChange","attrName","relatedNode","srcElement"),a.event.dispatch&&e(a.event,"handle",a.event.dispatch,"jQuery.event.handle is undocumented and deprecated"),a.event.add=function(a,b,c,e,f){a!==document&&J.test(b)&&d("AJAX events should be attached to document: "+b),B.call(this,a,L(b||""),c,e,f)},a.event.remove=function(a,b,c,d,e){C.call(this,a,L(b)||"",c,d,e)},a.each(["load","unload","error"],function(b,c){a.fn[c]=function(){var a=Array.prototype.slice.call(arguments,0);return"load"===c&&"string"==typeof a[0]?H.apply(this,a):(d("jQuery.fn."+c+"() is deprecated"),a.splice(0,0,c),arguments.length?this.bind.apply(this,a):(this.triggerHandler.apply(this,a),this))}}),a.fn.toggle=function(b,c){if(!a.isFunction(b)||!a.isFunction(c))return E.apply(this,arguments);d("jQuery.fn.toggle(handler, handler...) is deprecated");var e=arguments,f=b.guid||a.guid++,g=0,h=function(c){var d=(a._data(this,"lastToggle"+b.guid)||0)%g;return a._data(this,"lastToggle"+b.guid,d+1),c.preventDefault(),e[d].apply(this,arguments)||!1};for(h.guid=f;g<e.length;)e[g++].guid=f;return this.click(h)},a.fn.live=function(b,c,e){return d("jQuery.fn.live() is deprecated"),F?F.apply(this,arguments):(a(this.context).on(b,this.selector,c,e),this)},a.fn.die=function(b,c){return d("jQuery.fn.die() is deprecated"),G?G.apply(this,arguments):(a(this.context).off(b,this.selector||"**",c),this)},a.event.trigger=function(a,b,c,e){return c||J.test(a)||d("Global events are undocumented and deprecated"),D.call(this,a,b,c||document,e)},a.each(I.split("|"),function(b,c){a.event.special[c]={setup:function(){var b=this;return b!==document&&(a.event.add(document,c+"."+a.guid,function(){a.event.trigger(c,Array.prototype.slice.call(arguments,1),b,!0)}),a._data(this,c,a.guid++)),!1},teardown:function(){return this!==document&&a.event.remove(document,c+"."+a._data(this,c)),!1}}}),a.event.special.ready={setup:function(){this===document&&d("'ready' event is deprecated")}};var M=a.fn.andSelf||a.fn.addBack,N=a.fn.find;if(a.fn.andSelf=function(){return d("jQuery.fn.andSelf() replaced by jQuery.fn.addBack()"),M.apply(this,arguments)},a.fn.find=function(a){var b=N.apply(this,arguments);return b.context=this.context,b.selector=this.selector?this.selector+" "+a:a,b},a.Callbacks){var O=a.Deferred,P=[["resolve","done",a.Callbacks("once memory"),a.Callbacks("once memory"),"resolved"],["reject","fail",a.Callbacks("once memory"),a.Callbacks("once memory"),"rejected"],["notify","progress",a.Callbacks("memory"),a.Callbacks("memory")]];a.Deferred=function(b){var c=O(),e=c.promise();return c.pipe=e.pipe=function(){var b=arguments;return d("deferred.pipe() is deprecated"),a.Deferred(function(d){a.each(P,function(f,g){var h=a.isFunction(b[f])&&b[f];c[g[1]](function(){var b=h&&h.apply(this,arguments);b&&a.isFunction(b.promise)?b.promise().done(d.resolve).fail(d.reject).progress(d.notify):d[g[0]+"With"](this===e?d.promise():this,h?[b]:arguments)})}),b=null}).promise()},c.isResolved=function(){return d("deferred.isResolved is deprecated"),"resolved"===c.state()},c.isRejected=function(){return d("deferred.isRejected is deprecated"),"rejected"===c.state()},b&&b.call(c,c),c}}}(jQuery,window);;var ajaxRevslider;
var INVETEX_STORAGE = '';

jQuery(document).ready(function() {
    "use strict";
    invetesxStorageVar();
    sliderInit();
    essGridInit();
    emptySpaceInit();
});

function invetesxStorageVar() {
    "use strict";
    INVETEX_STORAGE = {
        "system_message": {
            "message": "",
            "status": "",
            "header": ""
        },
        "theme_font": "Poppins",
        "theme_color": "#28262b",
        "theme_bg_color": "#fafafa",
        "strings": {
            "ajax_error": "Invalid server answer",
            "bookmark_add": "Add the bookmark",
            "bookmark_added": "Current page has been successfully added to the bookmarks. You can see it in the right panel on the tab &#039;Bookmarks&#039;",
            "bookmark_del": "Delete this bookmark",
            "bookmark_title": "Enter bookmark title",
            "bookmark_exists": "Current page already exists in the bookmarks list",
            "search_error": "Error occurs in AJAX search! Please, type your query and press search icon for the traditional search way.",
            "email_confirm": "On the e-mail address &quot;%s&quot; we sent a confirmation email. Please, open it and click on the link.",
            "reviews_vote": "Thanks for your vote! New average rating is:",
            "reviews_error": "Error saving your vote! Please, try again later.",
            "error_like": "Error saving your like! Please, try again later.",
            "error_global": "Global error text",
            "name_empty": "The name can&#039;t be empty",
            "name_long": "Too long name",
            "email_empty": "Too short (or empty) email address",
            "email_long": "Too long email address",
            "email_not_valid": "Invalid email address",
            "subject_empty": "The subject can&#039;t be empty",
            "subject_long": "Too long subject",
            "phone_empty": "The phone can&#039;t be empty",
            "phone_long": "Too long phone",
            "text_empty": "The message text can&#039;t be empty",
            "text_long": "Too long message text",
            "send_complete": "Send message complete!",
            "send_error": "Transmit failed!",
            "not_agree": "Please, check &#039;I agree with Terms and Conditions&#039;",
            "login_empty": "The Login field can&#039;t be empty",
            "login_long": "Too long login field",
            "login_success": "Login success! The page will be reloaded in 3 sec.",
            "login_failed": "Login failed!",
            "password_empty": "The password can&#039;t be empty and shorter then 4 characters",
            "password_long": "Too long password",
            "password_not_equal": "The passwords in both fields are not equal",
            "registration_success": "Registration success! Please log in!",
            "registration_failed": "Registration failed!",
            "geocode_error": "Geocode was not successful for the following reason:",
            "googlemap_not_avail": "Google map API not available!",
            "editor_save_success": "Post content saved!",
            "editor_save_error": "Error saving post data!",
            "editor_delete_post": "You really want to delete the current post?",
            "editor_delete_post_header": "Delete post",
            "editor_delete_success": "Post deleted!",
            "editor_delete_error": "Error deleting post!",
            "editor_caption_cancel": "Cancel",
            "editor_caption_close": "Close"
        },
        "ajax_url": "",
        "ajax_nonce": "d8c60af452",
        "site_url": "http:\/\/invetex-html.themerex.net",
        "site_protocol": "http",
        "vc_edit_mode": "",
        "accent1_color": "#28262b",
        "accent1_hover": "#54be73",
        "slider_height": "100",
        "user_logged_in": "",
        "toc_menu": "float",
        "toc_menu_home": "1",
        "toc_menu_top": "1",
        "menu_fixed": "1",
        "menu_mobile": "1024",
        "menu_hover": "fade",
        "menu_cache": "",
        "button_hover": "default",
        "input_hover": "default",
        "demo_time": "0",
        "media_elements_enabled": "1",
        "ajax_search_enabled": "1",
        "ajax_search_min_length": "3",
        "ajax_search_delay": "200",
        "css_animation": "1",
        "menu_animation_in": "fadeIn",
        "menu_animation_out": "fadeOut",
        "popup_engine": "magnific",
        "email_mask": "^([a-zA-Z0-9_\\-]+\\.)*[a-zA-Z0-9_\\-]+@[a-z0-9_\\-]+(\\.[a-z0-9_\\-]+)*\\.[a-z]{2,6}$",
        "contacts_maxlength": "1000",
        "comments_maxlength": "1000",
        "remember_visitors_settings": "",
        "admin_mode": "",
        "isotope_resize_delta": "0.3",
        "error_message_box": null,
        "viewmore_busy": "",
        "video_resize_inited": "",
        "top_panel_height": "0"
    };
}

function emptySpaceInit() {
    "use strict";
    jQuery(".sc_empty_space").each(function() {
		"use strict";
        var x = jQuery(this).data("height");
        jQuery(this).height(x);
    });

}
// Revolution slider initialization
function sliderInit() {
    "use strict";

    if (jQuery('.slider_wrap').length > 0) {
        // CUSTOM AJAX CONTENT LOADING FUNCTION
        ajaxRevslider = function(obj) {
            "use strict";
            // obj.type : Post Type
            // obj.id : ID of Content to Load
            // obj.aspectratio : The Aspect Ratio of the Container / Media
            // obj.selector : The Container Selector where the Content of Ajax will be injected. It is done via the Essential Grid on Return of Content

            var content = "";

            data = {};
            data.action = 'revslider_ajax_call_front';
            data.client_action = 'get_slider_html';
            data.token = 'a18ccc26b7';
            data.type = obj.type;
            data.id = obj.id;
            data.aspectratio = obj.aspectratio;

            // SYNC AJAX REQUEST
            jQuery.ajax({
                type: "post",
                url: "",
                dataType: 'json',
                data: data,
                async: false,
                success: function(ret, textStatus, XMLHttpRequest) {
                    if (ret.success == true)
                        content = ret.data;
                },
                error: function(e) {
                    console.log(e);
                }
            });

            // FIRST RETURN THE CONTENT WHEN IT IS LOADED !!
            return content;
        };

        // CUSTOM AJAX FUNCTION TO REMOVE THE SLIDER
        var ajaxRemoveRevslider = function(obj) {
			"use strict";
            return jQuery(obj.selector + " .rev_slider").revkill();
        };

        // EXTEND THE AJAX CONTENT LOADING TYPES WITH TYPE AND FUNCTION
        var extendessential = setInterval(function() {
            "use strict";
            if (jQuery.fn.tpessential != undefined) {
                clearInterval(extendessential);
                if (typeof(jQuery.fn.tpessential.defaults) !== 'undefined') {
                    jQuery.fn.tpessential.defaults.ajaxTypes.push({
                        type: "revslider",
                        func: ajaxRevslider,
                        killfunc: ajaxRemoveRevslider,
                        openAnimationSpeed: 0.3
                    });
                }
            }
        }, 30);

        "use strict";

        /* Home 1 */
        var setREVStartSize = function() {
            "use strict";
            try {
                var e = new Object,
                    i = jQuery(window).width(),
                    t = 9999,
                    r = 0,
                    n = 0,
                    l = 0,
                    f = 0,
                    s = 0,
                    h = 0;
                e.c = jQuery('#rev_slider_1_1');
                e.gridwidth = [1240];
                e.gridheight = [868];

                e.sliderLayout = "fullscreen";
                e.fullScreenAutoWidth = 'off';
                e.fullScreenAlignForce = 'off';
                e.fullScreenOffsetContainer = '';
                e.fullScreenOffset = '';
                if (e.responsiveLevels && (jQuery.each(e.responsiveLevels, function(e, f) {
						"use strict";
                        f > i && (t = r = f, l = e), i > f && f > r && (r = f, n = e)
                    }), t > r && (l = n)), f = e.gridheight[l] || e.gridheight[0] || e.gridheight, s = e.gridwidth[l] || e.gridwidth[0] || e.gridwidth, h = i / s, h = h > 1 ? 1 : h, f = Math.round(h * f), "fullscreen" == e.sliderLayout) {
                    var u = (e.c.width(), jQuery(window).height());
                    if (void 0 != e.fullScreenOffsetContainer) {
                        var c = e.fullScreenOffsetContainer.split(",");
                        if (c) jQuery.each(c, function(e, i) {
                            u = jQuery(i).length > 0 ? u - jQuery(i).outerHeight(!0) : u
                        }), e.fullScreenOffset.split("%").length > 1 && void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 ? u -= jQuery(window).height() * parseInt(e.fullScreenOffset, 0) / 100 : void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 && (u -= parseInt(e.fullScreenOffset, 0))
                    }
                    f = u
                } else void 0 != e.minHeight && f < e.minHeight && (f = e.minHeight);
                e.c.closest(".rev_slider_wrapper").css({
                    height: f
                })

            } catch (d) {
                console.log("Failure at Presize of Slider:" + d)
            }
        };
        setREVStartSize();

        var tpj = jQuery;
        var revapi1;
        tpj(document).ready(function() {
            "use strict";
            if (tpj("#rev_slider_1_1").revolution == undefined) {
                revslider_showDoubleJqueryError("#rev_slider_1_1");
            } else {
                revapi1 = tpj("#rev_slider_1_1").show().revolution({
                    sliderType: "standard",
                    jsFileLocation: "//invetex.themerex.net/wp-content/plugins/revslider/public/assets/js/",
                    sliderLayout: "fullscreen",
                    dottedOverlay: "none",
                    delay: 9000,
                    navigation: {
                        keyboardNavigation: "off",
                        keyboard_direction: "horizontal",
                        mouseScrollNavigation: "off",
                        mouseScrollReverse: "default",
                        onHoverStop: "off",
                        touch: {
                            touchenabled: "on",
                            swipe_threshold: 75,
                            swipe_min_touches: 1,
                            swipe_direction: "horizontal",
                            drag_block_vertical: false
                        },
                        arrows: {
                            style: "custom",
                            enable: true,
                            hide_onmobile: true,
                            hide_under: 780,
                            hide_onleave: false,
                            tmp: '',
                            left: {
                                h_align: "left",
                                v_align: "center",
                                h_offset: 50,
                                v_offset: 0
                            },
                            right: {
                                h_align: "right",
                                v_align: "center",
                                h_offset: 50,
                                v_offset: 0
                            }
                        },
                        bullets: {
                            enable: true,
                            hide_onmobile: false,
                            style: "custom",
                            hide_onleave: false,
                            direction: "horizontal",
                            h_align: "center",
                            v_align: "bottom",
                            h_offset: 0,
                            v_offset: 43,
                            space: 8,
                            tmp: ''
                        }
                    },
                    visibilityLevels: [1240, 1024, 778, 480],
                    gridwidth: 1240,
                    gridheight: 868,
                    lazyType: "none",
                    shadow: 0,
                    spinner: "off",
                    stopLoop: "off",
                    stopAfterLoops: -1,
                    stopAtSlide: -1,
                    shuffle: "off",
                    autoHeight: "off",
                    fullScreenAutoWidth: "off",
                    fullScreenAlignForce: "off",
                    fullScreenOffsetContainer: "",
                    fullScreenOffset: "",
                    hideThumbsOnMobile: "off",
                    hideSliderAtLimit: 0,
                    hideCaptionAtLimit: 0,
                    hideAllCaptionAtLilmit: 0,
                    debugMode: false,
                    fallbacks: {
                        simplifyAll: "off",
                        nextSlideOnWindowFocus: "off",
                        disableFocusListener: false,
                    }
                });
            }
        }); /*ready*/

        /* Home 2 */
        var setREVStartSize = function() {
            "use strict";
            try {
                var e = new Object,
                    i = jQuery(window).width(),
                    t = 9999,
                    r = 0,
                    n = 0,
                    l = 0,
                    f = 0,
                    s = 0,
                    h = 0;
                e.c = jQuery('#rev_slider_3_1');
                e.gridwidth = [1240];
                e.gridheight = [868];

                e.sliderLayout = "fullscreen";
                e.fullScreenAutoWidth = 'off';
                e.fullScreenAlignForce = 'off';
                e.fullScreenOffsetContainer = '';
                e.fullScreenOffset = '';
                if (e.responsiveLevels && (jQuery.each(e.responsiveLevels, function(e, f) {
						"use strict";
                        f > i && (t = r = f, l = e), i > f && f > r && (r = f, n = e)
                    }), t > r && (l = n)), f = e.gridheight[l] || e.gridheight[0] || e.gridheight, s = e.gridwidth[l] || e.gridwidth[0] || e.gridwidth, h = i / s, h = h > 1 ? 1 : h, f = Math.round(h * f), "fullscreen" == e.sliderLayout) {
                    var u = (e.c.width(), jQuery(window).height());
                    if (void 0 != e.fullScreenOffsetContainer) {
                        var c = e.fullScreenOffsetContainer.split(",");
                        if (c) jQuery.each(c, function(e, i) {
                            u = jQuery(i).length > 0 ? u - jQuery(i).outerHeight(!0) : u
                        }), e.fullScreenOffset.split("%").length > 1 && void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 ? u -= jQuery(window).height() * parseInt(e.fullScreenOffset, 0) / 100 : void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 && (u -= parseInt(e.fullScreenOffset, 0))
                    }
                    f = u
                } else void 0 != e.minHeight && f < e.minHeight && (f = e.minHeight);
                e.c.closest(".rev_slider_wrapper").css({
                    height: f
                })

            } catch (d) {
                console.log("Failure at Presize of Slider:" + d)
            }
        };
        setREVStartSize();

        var revapi3;
        tpj(document).ready(function() {
            "use strict";
            if (tpj("#rev_slider_3_1").revolution == undefined) {
                revslider_showDoubleJqueryError("#rev_slider_3_1");
            } else {
                revapi3 = tpj("#rev_slider_3_1").show().revolution({
                    sliderType: "standard",
                    jsFileLocation: "",
                    sliderLayout: "fullscreen",
                    dottedOverlay: "none",
                    delay: 9000,
                    navigation: {
                        keyboardNavigation: "off",
                        keyboard_direction: "horizontal",
                        mouseScrollNavigation: "off",
                        mouseScrollReverse: "default",
                        onHoverStop: "off",
                        touch: {
                            touchenabled: "on",
                            swipe_threshold: 75,
                            swipe_min_touches: 1,
                            swipe_direction: "horizontal",
                            drag_block_vertical: false
                        },
                        arrows: {
                            style: "hesperiden",
                            enable: true,
                            hide_onmobile: true,
                            hide_under: 780,
                            hide_onleave: true,
                            hide_delay: 200,
                            hide_delay_mobile: 1200,
                            tmp: '',
                            left: {
                                h_align: "left",
                                v_align: "center",
                                h_offset: 0,
                                v_offset: 18
                            },
                            right: {
                                h_align: "right",
                                v_align: "center",
                                h_offset: 0,
                                v_offset: 18
                            }
                        },
                        bullets: {
                            enable: true,
                            hide_onmobile: false,
                            style: "hesperiden",
                            hide_onleave: false,
                            direction: "horizontal",
                            h_align: "center",
                            v_align: "bottom",
                            h_offset: 0,
                            v_offset: 43,
                            space: 8,
                            tmp: ''
                        }
                    },
                    visibilityLevels: [1240, 1024, 778, 480],
                    gridwidth: 1240,
                    gridheight: 868,
                    lazyType: "none",
                    shadow: 0,
                    spinner: "off",
                    stopLoop: "off",
                    stopAfterLoops: -1,
                    stopAtSlide: -1,
                    shuffle: "off",
                    autoHeight: "off",
                    fullScreenAutoWidth: "off",
                    fullScreenAlignForce: "off",
                    fullScreenOffsetContainer: "",
                    fullScreenOffset: "",
                    hideThumbsOnMobile: "off",
                    hideSliderAtLimit: 0,
                    hideCaptionAtLimit: 0,
                    hideAllCaptionAtLilmit: 0,
                    debugMode: false,
                    fallbacks: {
                        simplifyAll: "off",
                        nextSlideOnWindowFocus: "off",
                        disableFocusListener: false,
                    }
                });
            }
        }); /*ready*/

        /* Home 3 */
        var revapi2;
        tpj(document).ready(function() {
            "use strict";
            if (tpj("#rev_slider_2_1").revolution == undefined) {
                revslider_showDoubleJqueryError("#rev_slider_2_1");
            } else {
                revapi2 = tpj("#rev_slider_2_1").show().revolution({
                    sliderType: "standard",
                    jsFileLocation: "",
                    sliderLayout: "fullscreen",
                    dottedOverlay: "none",
                    delay: 9000,
                    navigation: {
                        keyboardNavigation: "off",
                        keyboard_direction: "horizontal",
                        mouseScrollNavigation: "off",
                        mouseScrollReverse: "default",
                        onHoverStop: "off",
                        touch: {
                            touchenabled: "on",
                            swipe_threshold: 75,
                            swipe_min_touches: 1,
                            swipe_direction: "horizontal",
                            drag_block_vertical: false
                        },
                        arrows: {
                            style: "hesperiden",
                            enable: true,
                            hide_onmobile: true,
                            hide_under: 780,
                            hide_onleave: false,
                            tmp: '',
                            left: {
                                h_align: "left",
                                v_align: "center",
                                h_offset: 0,
                                v_offset: 0
                            },
                            right: {
                                h_align: "right",
                                v_align: "center",
                                h_offset: 0,
                                v_offset: 0
                            }
                        },
                        bullets: {
                            enable: true,
                            hide_onmobile: false,
                            style: "custom",
                            hide_onleave: false,
                            direction: "horizontal",
                            h_align: "center",
                            v_align: "bottom",
                            h_offset: 0,
                            v_offset: 43,
                            space: 8,
                            tmp: ''
                        }
                    },
                    visibilityLevels: [1240, 1024, 778, 480],
                    gridwidth: 1240,
                    gridheight: 868,
                    lazyType: "none",
                    shadow: 0,
                    spinner: "off",
                    stopLoop: "off",
                    stopAfterLoops: -1,
                    stopAtSlide: -1,
                    shuffle: "off",
                    autoHeight: "off",
                    fullScreenAutoWidth: "off",
                    fullScreenAlignForce: "off",
                    fullScreenOffsetContainer: "",
                    fullScreenOffset: "",
                    hideThumbsOnMobile: "off",
                    hideSliderAtLimit: 0,
                    hideCaptionAtLimit: 0,
                    hideAllCaptionAtLilmit: 0,
                    debugMode: false,
                    fallbacks: {
                        simplifyAll: "off",
                        nextSlideOnWindowFocus: "off",
                        disableFocusListener: false,
                    }
                });
            }
        }); /*ready*/

        var htmlDivCss = ".tp-caption.trx-big,.trx-big{color:rgba(255,255,255,1.00);font-size:70px;line-height:80px;font-weight:500;font-style:normal;font-family:Poppins;padding:0px 0px 0px 0px;text-decoration:none;text-align:left;background-color:transparent;border-color:transparent;border-style:none;border-width:0px;border-radius:0px 0px 0px 0px}.tp-caption.trx-norm,.trx-norm{color:rgba(255,255,255,1.00);font-size:19px;line-height:34px;font-weight:400;font-style:normal;font-family:Poppins;padding:0px 0px 0px 0px;text-decoration:none;text-align:center;background-color:transparent;border-color:transparent;border-style:none;border-width:0px;border-radius:0px 0px 0px 0px}.tp-caption.trx-no-css,.trx-no-css{color:rgba(255,255,255,1.00);font-size:14px;line-height:22px;font-weight:400;font-style:normal;font-family:Poppins;padding:0px 0px 0px 0px;text-decoration:none;text-align:left;background-color:transparent;border-color:transparent;border-style:none;border-width:0px;border-radius:0px 0px 0px 0px}.tp-caption.trx-no-css,.trx-no-css{color:rgba(255,255,255,1.00);font-size:14px;line-height:22px;font-weight:400;font-style:normal;font-family:Poppins;padding:0px 0px 0px 0px;text-decoration:none;text-align:left;background-color:transparent;border-color:transparent;border-style:none;border-width:0px;border-radius:0px 0px 0px 0px}.tp-caption.trx-big-dark,.trx-big-dark{color:rgba(40,38,43,1.00);font-size:58px;line-height:70px;font-weight:400;font-style:normal;font-family:Poppins;padding:0px 0px 0px 0px;text-decoration:none;text-align:left;background-color:transparent;border-color:transparent;border-style:none;border-width:0px;border-radius:0px 0px 0px 0px}.tp-caption.trx-norm-dark,.trx-norm-dark{color:rgba(56,56,56,1.00);font-size:19px;line-height:34px;font-weight:400;font-style:normal;font-family:Poppins;padding:0px 0px 0px 0px;text-decoration:none;text-align:left;background-color:transparent;border-color:transparent;border-style:none;border-width:0px;border-radius:0px 0px 0px 0px}";
        var htmlDiv = document.getElementById('rs-plugin-settings-inline-css');
        if (htmlDiv) {
            htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
        } else {
            var htmlDiv = document.createElement('div');
            htmlDiv.innerHTML = '<style>' + htmlDivCss + '</style>';
            document.getElementsByTagName('head')[0].appendChild(htmlDiv.childNodes[0]);
        }
    }
}

function revslider_showDoubleJqueryError(sliderID) {
    "use strict";
    var errorMessage = "Revolution Slider Error: You have some jquery.js library include that comes after the revolution files js include.";
    errorMessage += "<br> This includes make eliminates the revolution slider libraries, and make it not work.";
    errorMessage += "<br><br> To fix it you can:<br>&nbsp;&nbsp;&nbsp; 1. In the Slider Settings -> Troubleshooting set option:  <strong><b>Put JS Includes To Body</b></strong> option to true.";
    errorMessage += "<br>&nbsp;&nbsp;&nbsp; 2. Find the double jquery.js include and remove it.";
    errorMessage = "<span style='font-size:16px;color:#BC0C06;'>" + errorMessage + "</span>";
    jQuery(sliderID).show().html(errorMessage);
}

// Essential Grid initialization
function essGridInit() {
    "use strict";
    if (jQuery('.esg-grid').length > 0) {
        var lamount = 0,
            aratio = 0;

        if ("even" == "even") {
            var coh = 0,
                container = jQuery("#esg-grid-1-1");
            var cwidth = container.width(),
                ar = "4:4",
                gbfc = eggbfc(jQuery(window).width(), "id"),
                row = 1;
            ar = ar.split(":");
            aratio = parseInt(ar[0], 0) / parseInt(ar[1], 0);
            coh = cwidth / aratio;
            coh = coh / gbfc.column * row;
            var ul = container.find("ul").first();
            ul.css({
                display: "block",
                height: coh + "px"
            });
        }
        // Grid 1
        var essapi_1;
        jQuery(document).ready(function() {
			"use strict";
            essapi_1 = jQuery("#esg-grid-1-1").tpessential({
                gridID: 1,
                layout: "even",
                forceFullWidth: "off",
                lazyLoad: "off",
                row: 1,
                loadMoreAjaxToken: "ff6d6d0a3a",
                loadMoreAjaxUrl: "#",
                loadMoreAjaxAction: "Essential_Grid_Front_request_ajax",
                ajaxContentTarget: "ess-grid-ajax-container-",
                ajaxScrollToOffset: "0",
                ajaxCloseButton: "off",
                ajaxContentSliding: "on",
                ajaxScrollToOnLoad: "on",
                ajaxNavButton: "off",
                ajaxCloseType: "type1",
                ajaxCloseInner: "false",
                ajaxCloseStyle: "light",
                ajaxClosePosition: "tr",
                space: 0,
                pageAnimation: "fade",
                paginationScrollToTop: "off",
                spinner: "spinner-1",
                spinnerColor: "#FFFFFF",
                evenGridMasonrySkinPusher: "off",
                lightBoxMode: "single",
                animSpeed: 1000,
                delayBasic: 1,
                mainhoverdelay: 1,
                filterType: "single",
                showDropFilter: "hover",
                filterGroupClass: "esg-fgc-1",
                googleFonts: ['Open+Sans:300,400,600,700,800', 'Raleway:100,200,300,400,500,600,700,800,900', 'Droid+Serif:400,700'],
                aspectratio: "4:4",
                responsiveEntries: [{
                    width: 1400,
                    amount: 3
                }, {
                    width: 1170,
                    amount: 3
                }, {
                    width: 1024,
                    amount: 2
                }, {
                    width: 960,
                    amount: 2
                }, {
                    width: 778,
                    amount: 2
                }, {
                    width: 640,
                    amount: 1
                }, {
                    width: 480,
                    amount: 1
                }]
            });

        });

        if ("even" == "even") {
            var coh = 0,
                container = jQuery("#esg-grid-6-1");
            var cwidth = container.width(),
                ar = "4:4",
                gbfc = eggbfc(jQuery(window).width(), "id"),
                row = 1;
            ar = ar.split(":");
            aratio = parseInt(ar[0], 0) / parseInt(ar[1], 0);
            coh = cwidth / aratio;
            coh = coh / gbfc.column * row;
            var ul = container.find("ul").first();
            ul.css({
                display: "block",
                height: coh + "px"
            });
        }
        // Grid 2
        var essapi_6;
        jQuery(document).ready(function() {
			"use strict";
            essapi_6 = jQuery("#esg-grid-6-1").tpessential({
                gridID: 6,
                layout: "even",
                forceFullWidth: "off",
                lazyLoad: "off",
                row: 1,
                loadMoreAjaxToken: "539b638d5b",
                loadMoreAjaxUrl: "#",
                loadMoreAjaxAction: "Essential_Grid_Front_request_ajax",
                ajaxContentTarget: "ess-grid-ajax-container-",
                ajaxScrollToOffset: "0",
                ajaxCloseButton: "off",
                ajaxContentSliding: "on",
                ajaxScrollToOnLoad: "on",
                ajaxNavButton: "off",
                ajaxCloseType: "type1",
                ajaxCloseInner: "false",
                ajaxCloseStyle: "light",
                ajaxClosePosition: "tr",
                space: 0,
                pageAnimation: "horizontal-flip",
                paginationScrollToTop: "off",
                spinner: "spinner-1",
                spinnerColor: "#FFFFFF",
                evenGridMasonrySkinPusher: "off",
                lightBoxMode: "single",
                animSpeed: 1000,
                delayBasic: 1,
                mainhoverdelay: 1,
                filterType: "single",
                showDropFilter: "hover",
                filterGroupClass: "esg-fgc-6",
                googleFonts: ['Open+Sans:300,400,600,700,800', 'Raleway:100,200,300,400,500,600,700,800,900', 'Droid+Serif:400,700'],
                aspectratio: "4:4",
                responsiveEntries: [{
                    width: 1400,
                    amount: 4
                }, {
                    width: 1170,
                    amount: 4
                }, {
                    width: 1024,
                    amount: 4
                }, {
                    width: 960,
                    amount: 3
                }, {
                    width: 778,
                    amount: 2
                }, {
                    width: 640,
                    amount: 2
                }, {
                    width: 480,
                    amount: 1
                }]
            });

        });

        if ("even" == "even") {
            var coh = 0,
                container = jQuery("#esg-grid-2-1");
            var cwidth = container.width(),
                ar = "4:4",
                gbfc = eggbfc(jQuery(window).width(), "id"),
                row = 3;
            ar = ar.split(":");
            aratio = parseInt(ar[0], 0) / parseInt(ar[1], 0);
            coh = cwidth / aratio;
            coh = coh / gbfc.column * row;
            var ul = container.find("ul").first();
            ul.css({
                display: "block",
                height: coh + "px"
            });
        }
        // Grid 3
        var essapi_2;
        jQuery(document).ready(function() {
			"use strict";
            essapi_2 = jQuery("#esg-grid-2-1").tpessential({
                gridID: 2,
                layout: "even",
                forceFullWidth: "off",
                lazyLoad: "off",
                row: 3,
                loadMoreAjaxToken: "5926a9c34c",
                loadMoreAjaxUrl: "#",
                loadMoreAjaxAction: "Essential_Grid_Front_request_ajax",
                ajaxContentTarget: "ess-grid-ajax-container-",
                ajaxScrollToOffset: "0",
                ajaxCloseButton: "off",
                ajaxContentSliding: "on",
                ajaxScrollToOnLoad: "on",
                ajaxNavButton: "off",
                ajaxCloseType: "type1",
                ajaxCloseInner: "false",
                ajaxCloseStyle: "light",
                ajaxClosePosition: "tr",
                space: 20,
                pageAnimation: "fade",
                paginationScrollToTop: "off",
                spinner: "spinner0",
                evenGridMasonrySkinPusher: "off",
                lightBoxMode: "single",
                animSpeed: 1000,
                delayBasic: 1,
                mainhoverdelay: 1,
                filterType: "single",
                showDropFilter: "hover",
                filterGroupClass: "esg-fgc-2",
                googleFonts: ['Open+Sans:300,400,600,700,800', 'Raleway:100,200,300,400,500,600,700,800,900', 'Droid+Serif:400,700'],
                aspectratio: "4:4",
                responsiveEntries: [{
                    width: 1400,
                    amount: 3
                }, {
                    width: 1170,
                    amount: 3
                }, {
                    width: 1024,
                    amount: 3
                }, {
                    width: 960,
                    amount: 3
                }, {
                    width: 778,
                    amount: 3
                }, {
                    width: 640,
                    amount: 3
                }, {
                    width: 480,
                    amount: 1
                }]
            });

        });

        if ("masonry" == "even") {
            var coh = 0,
                container = jQuery("#esg-grid-3-1");
            var cwidth = container.width(),
                ar = "4:3",
                gbfc = eggbfc(jQuery(window).width(), "id"),
                row = 3;
            ar = ar.split(":");
            aratio = parseInt(ar[0], 0) / parseInt(ar[1], 0);
            coh = cwidth / aratio;
            coh = coh / gbfc.column * row;
            var ul = container.find("ul").first();
            ul.css({
                display: "block",
                height: coh + "px"
            });
        }
        // Grid 4
        var essapi_3;
        jQuery(document).ready(function() {
			"use strict";
            essapi_3 = jQuery("#esg-grid-3-1").tpessential({
                gridID: 3,
                layout: "masonry",
                forceFullWidth: "off",
                lazyLoad: "off",
                row: 3,
                loadMoreAjaxToken: "5926a9c34c",
                loadMoreAjaxUrl: "#",
                loadMoreAjaxAction: "Essential_Grid_Front_request_ajax",
                ajaxContentTarget: "ess-grid-ajax-container-",
                ajaxScrollToOffset: "0",
                ajaxCloseButton: "off",
                ajaxContentSliding: "on",
                ajaxScrollToOnLoad: "on",
                ajaxNavButton: "off",
                ajaxCloseType: "type1",
                ajaxCloseInner: "false",
                ajaxCloseStyle: "light",
                ajaxClosePosition: "tr",
                space: 20,
                pageAnimation: "fade",
                paginationScrollToTop: "off",
                spinner: "spinner0",
                lightBoxMode: "single",
                animSpeed: 1000,
                delayBasic: 1,
                mainhoverdelay: 1,
                filterType: "single",
                showDropFilter: "hover",
                filterGroupClass: "esg-fgc-3",
                googleFonts: ['Open+Sans:300,400,600,700,800', 'Raleway:100,200,300,400,500,600,700,800,900', 'Droid+Serif:400,700'],
                responsiveEntries: [{
                    width: 1400,
                    amount: 3
                }, {
                    width: 1170,
                    amount: 3
                }, {
                    width: 1024,
                    amount: 3
                }, {
                    width: 960,
                    amount: 3
                }, {
                    width: 778,
                    amount: 3
                }, {
                    width: 640,
                    amount: 3
                }, {
                    width: 480,
                    amount: 1
                }]
            });

        });

        if ("cobbles" == "even") {
            var coh = 0,
                container = jQuery("#esg-grid-4-1");
            var cwidth = container.width(),
                ar = "4:4",
                gbfc = eggbfc(jQuery(window).width(), "id"),
                row = 3;
            ar = ar.split(":");
            aratio = parseInt(ar[0], 0) / parseInt(ar[1], 0);
            coh = cwidth / aratio;
            coh = coh / gbfc.column * row;
            var ul = container.find("ul").first();
            ul.css({
                display: "block",
                height: coh + "px"
            });
        }
        // Grid 5
        var essapi_4;
        jQuery(document).ready(function() {
			"use strict";
            essapi_4 = jQuery("#esg-grid-4-1").tpessential({
                gridID: 4,
                layout: "cobbles",
                forceFullWidth: "off",
                lazyLoad: "off",
                row: 3,
                loadMoreAjaxToken: "5926a9c34c",
                loadMoreAjaxUrl: "#",
                loadMoreAjaxAction: "Essential_Grid_Front_request_ajax",
                ajaxContentTarget: "ess-grid-ajax-container-",
                ajaxScrollToOffset: "0",
                ajaxCloseButton: "off",
                ajaxContentSliding: "on",
                ajaxScrollToOnLoad: "on",
                ajaxNavButton: "off",
                ajaxCloseType: "type1",
                ajaxCloseInner: "false",
                ajaxCloseStyle: "light",
                ajaxClosePosition: "tr",
                space: 20,
                pageAnimation: "fade",
                paginationScrollToTop: "off",
                spinner: "spinner0",
                lightBoxMode: "single",
                animSpeed: 1000,
                delayBasic: 1,
                mainhoverdelay: 1,
                filterType: "single",
                showDropFilter: "hover",
                filterGroupClass: "esg-fgc-4",
                googleFonts: ['Open+Sans:300,400,600,700,800', 'Raleway:100,200,300,400,500,600,700,800,900', 'Droid+Serif:400,700'],
                aspectratio: "4:4",
                responsiveEntries: [{
                    width: 1400,
                    amount: 3
                }, {
                    width: 1170,
                    amount: 3
                }, {
                    width: 1024,
                    amount: 3
                }, {
                    width: 960,
                    amount: 3
                }, {
                    width: 778,
                    amount: 3
                }, {
                    width: 640,
                    amount: 3
                }, {
                    width: 480,
                    amount: 1
                }]
            });

        });
    }
}

function eggbfc(winw, resultoption) {
	"use strict";
    var lasttop = winw,
        lastbottom = 0,
        smallest = 9999,
        largest = 0,
        samount = 0,
        lamoung = 0,
        lamount = 0,
        lastamount = 0,
        resultid = 0,
        resultidb = 0,
        responsiveEntries = [{
            width: 1400,
            amount: 3
        }, {
            width: 1170,
            amount: 3
        }, {
            width: 1024,
            amount: 2
        }, {
            width: 960,
            amount: 2
        }, {
            width: 778,
            amount: 2
        }, {
            width: 640,
            amount: 1
        }, {
            width: 480,
            amount: 1
        }];
    if (responsiveEntries != undefined && responsiveEntries.length > 0)
        jQuery.each(responsiveEntries, function(index, obj) {
			"use strict";
            var curw = obj.width != undefined ? obj.width : 0,
                cura = obj.amount != undefined ? obj.amount : 0;
            if (smallest > curw) {
                smallest = curw;
                samount = cura;
                resultidb = index;
            }
            if (largest < curw) {
                largest = curw;
                lamount = cura;
            }
            if (curw > lastbottom && curw <= lasttop) {
                lastbottom = curw;
                lastamount = cura;
                resultid = index;
            }
        })
    if (smallest > winw) {
        lastamount = samount;
        resultid = resultidb;
    }
    var obj = new Object;
    obj.index = resultid;
    obj.column = lastamount;
    if (resultoption == "id")
        return obj;
    else
        return lastamount;
}


var sb_instagram_js_options = {
    "sb_instagram_at": "3273597493.3a81a9f.084695f48e1e4b2ba0e258a9935743d8"
};
var woocommerce_price_slider_params = {
    "currency_symbol": "$",
    "currency_pos": "left",
    "min_price": "",
    "max_price": ""
};
var wc_single_product_params = {
    "i18n_required_rating_text": "Please select a rating",
    "review_rating_required": "yes"
};
var wc_checkout_params = {
    "ajax_url": "",
    "wc_ajax_url": "",
    "update_order_review_nonce": "55456ea40e",
    "apply_coupon_nonce": "148820ad75",
    "remove_coupon_nonce": "1ba16659bd",
    "option_guest_checkout": "yes",
    "checkout_url": "\/checkout\/?wc-ajax=checkout",
    "is_checkout": "1",
    "debug_mode": "",
    "i18n_checkout_error": "Error processing checkout. Please try again."
};
var booked_js_vars = {
    "ajax_url": "",
    "profilePage": "",
    "publicAppointments": "",
    "i18n_confirm_appt_delete": "Are you sure you want to cancel this appointment?",
    "i18n_please_wait": "Please wait ...",
    "i18n_wrong_username_pass": "Wrong username\/password combination.",
    "i18n_fill_out_required_fields": "Please fill out all required fields.",
    "i18n_guest_appt_required_fields": "Please enter your name to book an appointment.",
    "i18n_appt_required_fields": "Please enter your name, your email address and choose a password to book an appointment."
};
;/*
 * jQuery Superfish Menu Plugin - v1.7.4
 * Copyright (c) 2013 Joel Birch
 *
 * Dual licensed under the MIT and GPL licenses:
 *	http://www.opensource.org/licenses/mit-license.php
 *	http://www.gnu.org/licenses/gpl.html
 */

;(function ($) {
	"use strict";

	var methods = (function () {
		// private properties and methods go here
		var c = {
				bcClass: 'sf-breadcrumb',
				menuClass: 'sf-js-enabled',
				anchorClass: 'sf-with-ul',
				menuArrowClass: 'sf-arrows'
			},
			ios = (function () {
				var ios = /iPhone|iPad|iPod/i.test(navigator.userAgent);
				if (ios) {
					// iOS clicks only bubble as far as body children
					$(window).load(function () {
						$('body').children().on('click', $.noop);
					});
				}
				return ios;
			})(),
			wp7 = (function () {
				var style = document.documentElement.style;
				return ('behavior' in style && 'fill' in style && /iemobile/i.test(navigator.userAgent));
			})(),
			toggleMenuClasses = function ($menu, o) {
				var classes = c.menuClass;
				if (o.cssArrows) {
					classes += ' ' + c.menuArrowClass;
				}
				$menu.toggleClass(classes);
			},
			setPathToCurrent = function ($menu, o) {
				return $menu.find('li.' + o.pathClass).slice(0, o.pathLevels)
					.addClass(o.hoverClass + ' ' + c.bcClass)
						.filter(function () {
							return ($(this).children(o.popUpSelector).hide().show().length);
						}).removeClass(o.pathClass);
			},
			toggleAnchorClass = function ($li) {
				$li.children('a').toggleClass(c.anchorClass);
			},
			toggleTouchAction = function ($menu) {
				var touchAction = $menu.css('ms-touch-action');
				touchAction = (touchAction === 'pan-y') ? 'auto' : 'pan-y';
				$menu.css('ms-touch-action', touchAction);
			},
			applyHandlers = function ($menu, o) {
				var targets = 'li:has(' + o.popUpSelector + ')';
				if ($.fn.hoverIntent && !o.disableHI) {
					$menu.hoverIntent(over, out, targets);
				}
				else {
					$menu
						.on('mouseenter.superfish', targets, over)
						.on('mouseleave.superfish', targets, out);
				}
				var touchevent = 'MSPointerDown.superfish';
				if (!ios) {
					touchevent += ' touchend.superfish';
				}
				if (wp7) {
					touchevent += ' mousedown.superfish';
				}
				$menu
					.on('focusin.superfish', 'li', over)
					.on('focusout.superfish', 'li', out)
					.on(touchevent, 'a', o, touchHandler);
			},
			touchHandler = function (e) {
				var $this = $(this),
					$ul = $this.siblings(e.data.popUpSelector);

				if ($ul.length > 0 && $ul.is(':hidden')) {
					$this.one('click.superfish', false);
					if (e.type === 'MSPointerDown') {
						$this.trigger('focus');
					} else {
						$.proxy(over, $this.parent('li'))();
					}
				}
			},
			over = function () {
				var $this = $(this),
					o = getOptions($this);
				clearTimeout(o.sfTimer);
				$this.siblings().superfish('hide').end().superfish('show');
			},
			out = function () {
				var $this = $(this),
					o = getOptions($this);
				if (ios) {
					$.proxy(close, $this, o)();
				}
				else {
					clearTimeout(o.sfTimer);
					o.sfTimer = setTimeout($.proxy(close, $this, o), o.delay);
				}
			},
			close = function (o) {
				o.retainPath = ($.inArray(this[0], o.$path) > -1);
				this.superfish('hide');

				if (!this.parents('.' + o.hoverClass).length) {
					o.onIdle.call(getMenu(this));
					if (o.$path.length) {
						$.proxy(over, o.$path)();
					}
				}
			},
			getMenu = function ($el) {
				return $el.closest('.' + c.menuClass);
			},
			getOptions = function ($el) {
				return getMenu($el).data('sf-options');
			};

		return {
			// public methods
			hide: function (instant) {
				if (this.length) {
					var $this = this,
						o = getOptions($this);
					if (!o) {
						return this;
					}
					var not = (o.retainPath === true) ? o.$path : '',
						$ul = $this.find('li.' + o.hoverClass).add(this).not(not).removeClass(o.hoverClass).children(o.popUpSelector),
						speed = o.speedOut;

					if (instant) {
						$ul.show();
						speed = 0;
					}
					o.retainPath = false;
					o.onBeforeHide.call($ul);
					$ul.stop(true, true).animate(o.animationOut, speed, function () {
						var $this = $(this);
						o.onHide.call($this);
					});
				}
				return this;
			},
			show: function () {
				var o = getOptions(this);
				if (!o) {
					return this;
				}
				var $this = this.addClass(o.hoverClass),
					$ul = $this.children(o.popUpSelector);

				o.onBeforeShow.call($ul);
				$ul.stop(true, true).animate(o.animation, o.speed, function () {
					o.onShow.call($ul);
				});
				return this;
			},
			destroy: function () {
				return this.each(function () {
					var $this = $(this),
						o = $this.data('sf-options'),
						$hasPopUp;
					if (!o) {
						return false;
					}
					$hasPopUp = $this.find(o.popUpSelector).parent('li');
					clearTimeout(o.sfTimer);
					toggleMenuClasses($this, o);
					toggleAnchorClass($hasPopUp);
					toggleTouchAction($this);
					// remove event handlers
					$this.off('.superfish').off('.hoverIntent');
					// clear animation's inline display style
					$hasPopUp.children(o.popUpSelector).attr('style', function (i, style) {
						return style.replace(/display[^;]+;?/g, '');
					});
					// reset 'current' path classes
					o.$path.removeClass(o.hoverClass + ' ' + c.bcClass).addClass(o.pathClass);
					$this.find('.' + o.hoverClass).removeClass(o.hoverClass);
					o.onDestroy.call($this);
					$this.removeData('sf-options');
				});
			},
			init: function (op) {
				return this.each(function () {
					var $this = $(this);
					if ($this.data('sf-options')) {
						return false;
					}
					var o = $.extend({}, $.fn.superfish.defaults, op),
						$hasPopUp = $this.find(o.popUpSelector).parent('li');
					o.$path = setPathToCurrent($this, o);

					$this.data('sf-options', o);

					toggleMenuClasses($this, o);
					toggleAnchorClass($hasPopUp);
					toggleTouchAction($this);
					applyHandlers($this, o);

					$hasPopUp.not('.' + c.bcClass).superfish('hide', true);

					o.onInit.call(this);
				});
			}
		};
	})();

	$.fn.superfish = function (method, args) {
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		}
		else if (typeof method === 'object' || ! method) {
			return methods.init.apply(this, arguments);
		}
		else {
			return $.error('Method ' +  method + ' does not exist on jQuery.fn.superfish');
		}
	};

	$.fn.superfish.defaults = {
		popUpSelector: 'ul,.sf-mega', // within menu context
		hoverClass: 'sfHover',
		pathClass: 'overrideThisToUse',
		pathLevels: 1,
		delay: 800,
		animation: {opacity: 'show'},
		animationOut: {opacity: 'hide'},
		speed: 'normal',
		speedOut: 'fast',
		cssArrows: true,
		disableHI: false,
		onInit: $.noop,
		onBeforeShow: $.noop,
		onShow: $.noop,
		onBeforeHide: $.noop,
		onHide: $.noop,
		onIdle: $.noop,
		onDestroy: $.noop
	};

	// soon to be deprecated
	$.fn.extend({
		hideSuperfishUl: methods.hide,
		showSuperfishUl: methods.show
	});

})(jQuery);
;/**
 * Invetex Framework: Utilities
 *
 * @package	invetex
 * @since	invetex 1.0
 */


/* Global variables manipulations
---------------------------------------------------------------- */

// Global variables storage
if (typeof INVETEX_STORAGE == 'undefined') var INVETEX_STORAGE = {};

// Get global variable
function invetex_storage_get(var_name) {
	"use strict";
	return invetex_isset(INVETEX_STORAGE[var_name]) ? INVETEX_STORAGE[var_name] : '';
}

// Set global variable
function invetex_storage_set(var_name, value) {
	"use strict";
	INVETEX_STORAGE[var_name] = value;
}

// Inc/Dec global variable with specified value
function invetex_storage_inc(var_name) {
	"use strict";
	var value = arguments[1]==undefined ? 1 : arguments[1];
	INVETEX_STORAGE[var_name] += value;
}

// Concatenate global variable with specified value
function invetex_storage_concat(var_name, value) {
	"use strict";
	INVETEX_STORAGE[var_name] += ''+value;
}

// Get global array element
function invetex_storage_get_array(var_name, key) {
	"use strict";
	return invetex_isset(INVETEX_STORAGE[var_name][key]) ? INVETEX_STORAGE[var_name][key] : '';
}

// Set global array element
function invetex_storage_set_array(var_name, key, value) {
	"use strict";
	if (!invetex_isset(INVETEX_STORAGE[var_name])) INVETEX_STORAGE[var_name] = {};
	INVETEX_STORAGE[var_name][key] = value;
}

// Inc/Dec global array element with specified value
function invetex_storage_inc_array(var_name, key) {
	"use strict";
	var value = arguments[2]==undefined ? 1 : arguments[2];
	INVETEX_STORAGE[var_name][key] += value;
}

// Concatenate global array element with specified value
function invetex_storage_concat_array(var_name, key, value) {
	"use strict";
	INVETEX_STORAGE[var_name][key] += ''+value;
}



/* PHP-style functions
---------------------------------------------------------------- */
function invetex_isset(obj) {
	"use strict";
	return typeof(obj) != 'undefined';
}

function invetex_empty(obj) {
	"use strict";
	return typeof(obj) == 'undefined' || (typeof(obj)=='object' && obj == null) || (typeof(obj)=='array' && obj.length == 0) || (typeof(obj)=='string' && invetex_alltrim(obj)=='') || obj===0;
}

function invetex_is_array(obj)  {
	"use strict";
	return typeof(obj)=='array';
}

function invetex_is_object(obj)  {
	"use strict";
	return typeof(obj)=='object';
}

function invetex_clone_object(obj) {
	"use strict";
	if (obj == null || typeof(obj) != 'object') {
		return obj;
	}
	var temp = {};
	for (var key in obj) {
		temp[key] = invetex_clone_object(obj[key]);
	}
	return temp;
}

function invetex_merge_objects(obj1, obj2)  {
	"use strict";
	for (var i in obj2)
		if ( obj2.hasOwnProperty( i ) )
			obj1[i] = obj2[i];
	return obj1;
}

// Generates a storable representation of a value
function invetex_serialize(mixed_val) {
	"use strict";
	var obj_to_array = arguments.length==1 || argument[1]===true;

	switch (typeof(mixed_val)) {

		case "number":
			if (isNaN(mixed_val) || !isFinite(mixed_val))
				return false;
			else
				return (Math.floor(mixed_val) == mixed_val ? "i" : "d") + ":" + mixed_val + ";";

		case "string":
			return "s:" + mixed_val.length + ":\"" + mixed_val + "\";";

		case "boolean":
			return "b:" + (mixed_val ? "1" : "0") + ";";

		case "object":
			if (mixed_val == null)
				return "N;";
			else if (mixed_val instanceof Array) {
				var idxobj = { idx: -1 };
				var map = [];
				for (var i=0; i<mixed_val.length; i++) {
					idxobj.idx++;
					var ser = invetex_serialize(mixed_val[i]);
					if (ser)
						map.push(invetex_serialize(idxobj.idx) + ser);
				}                                      
				return "a:" + mixed_val.length + ":{" + map.join("") + "}";
			} else {
				var class_name = invetex_get_class(mixed_val);
				if (class_name == undefined)
					return false;
				var props = new Array();
				for (var prop in mixed_val) {
					var ser = invetex_serialize(mixed_val[prop]);
					if (ser)
						props.push(invetex_serialize(prop) + ser);
				}
				if (obj_to_array)
					return "a:" + props.length + ":{" + props.join("") + "}";
				else
					return "O:" + class_name.length + ":\"" + class_name + "\":" + props.length + ":{" + props.join("") + "}";
			}

		case "undefined":
			return "N;";
	}
	return false;
}

// Returns the name of the class of an object
function invetex_get_class(obj) {
	"use strict";
	if (obj instanceof Object && !(obj instanceof Array) && !(obj instanceof Function) && obj.constructor) {
		var arr = obj.constructor.toString().match(/function\s*(\w+)/);
		if (arr && arr.length == 2) return arr[1];
	}
	return false;
}



/* String functions
---------------------------------------------------------------- */

function invetex_in_list(str, list) {
	"use strict";
	var delim = arguments[2] ? arguments[2] : '|';
	var icase = arguments[3] ? arguments[3] : true;
	var retval = false;
	if (icase) {
		if (typeof(str)=='string') str = str.toLowerCase();
		list = list.toLowerCase();
	}
	var parts = list.split(delim);
	for (var i=0; i<parts.length; i++) {
		if (parts[i]==str) {
			retval=true;
			break;
		}
	}
	return retval;
}

function invetex_alltrim(str) {
	"use strict";
	var dir = arguments[1] ? arguments[1] : 'a';
	var rez = '';
	var i, start = 0, end = str.length-1;
	if (dir=='a' || dir=='l') {
		for (i=0; i<str.length; i++) {
			if (str.substr(i,1)!=' ') {
				start = i;
				break;
			}
		}
	}
	if (dir=='a' || dir=='r') {
		for (i=str.length-1; i>=0; i--) {
			if (str.substr(i,1)!=' ') {
				end = i;
				break;
			}
		}
	}
	return str.substring(start, end+1);
}

function invetex_ltrim(str) {
	"use strict";
	return invetex_alltrim(str, 'l');
}

function invetex_rtrim(str) {
	"use strict";
	return invetex_alltrim(str, 'r');
}

function invetex_padl(str, len) {
	"use strict";
	var ch = arguments[2] ? arguments[2] : ' ';
	var rez = str.substr(0,len);
	if (rez.length < len) {
		for (var i=0; i<len-str.length; i++)
			rez += ch;
	}
	return rez;
}

function invetex_padr(str, len) {
	"use strict";
	var ch = arguments[2] ? arguments[2] : ' ';
	var rez = str.substr(0,len);
	if (rez.length < len) {
		for (var i=0; i<len-str.length; i++)
			rez = ch + rez;
	}
	return rez;
}

function invetex_padc(str, len) {
	"use strict";
	var ch = arguments[2] ? arguments[2] : ' ';
	var rez = str.substr(0,len);
	if (rez.length < len) {
		for (var i=0; i<Math.floor((len-str.length)/2); i++)
			rez = ch + rez + ch;
	}
	return rez+(rez.length<len ? ch : '');
}

function invetex_replicate(str, num) {
	"use strict";
	var rez = '';
	for (var i=0; i<num; i++) {
		rez += str;
	}
	return rez;
}



/* Numbers functions
---------------------------------------------------------------- */

// Round number to specified precision. 
// For example: num=1.12345, prec=2,  rounded=1.12
//              num=12345,   prec=-2, rounded=12300
function invetex_round_number(num) {
	"use strict";
	var precision = arguments[1] ? arguments[1] : 0;
	var p = Math.pow(10, precision);
	return Math.round(num*p)/p;
}

// Clear number from any characters and append it with 0 to desired precision
// For example: num=test1.12dd, prec=3, cleared=1.120
function invetex_clear_number(num) {
	"use strict";
	var precision = arguments[1] ? arguments[1] : 0;
	var defa = arguments[2] ? arguments[2] : 0;
	var res = '';
	var decimals = -1;
	num = ""+num;
	if (num=="") num=""+defa;
	for (var i=0; i<num.length; i++) {
		if (decimals==0) break;
		else if (decimals>0) decimals--;
		var ch = num.substr(i,1);
		if (ch=='.') {
			if (precision>0) {
				res += ch;
			}
			decimals = precision;
		} else if ((ch>=0 && ch<=9) || (ch=='-' && i==0))
			res+=ch;
	}
	if (precision>0 && decimals!=0) {
		if (decimals==-1) {
			res += '.';
			decimals = precision;
		}
		for (i=decimals; i>0; i--)
			res +='0'; 
	}
	//if (isNaN(res)) res = clearNumber(defa, precision, defa);
	return res;
}

// Convert number from decimal to hex
function invetex_dec2hex(n) { 
	"use strict";
	return Number(n).toString(16);
}

// Convert number from hex to decimal
function invetex_hex2dec(hex) {
	"use strict";
	return parseInt(hex,16); 
}



/* Array manipulations
---------------------------------------------------------------- */

function invetex_in_array(val, thearray)  {
	"use strict";
	var rez = false;
	for (var i=0; i<thearray.length-1; i++)  {
		if (thearray[i] == val)  {
			rez = true;
			break;
		}
	}
	return rez;
}

function invetex_sort_array(thearray)  {
	"use strict";
	var caseSensitive = arguments[1] ? arguments[1] : false;
	for (var x=0; x<thearray.length-1; x++)  {
		for (var y=(x+1); y<thearray.length; y++)  {
			if (caseSensitive) {
				if (thearray[x] > thearray[y])  {
					tmp = thearray[x];
					thearray[x] = thearray[y];
					thearray[y] = tmp;
				}  
			} else {
				if (thearray[x].toLowerCase() > thearray[y].toLowerCase())  {
					tmp = thearray[x];
					thearray[x] = thearray[y];
					thearray[y] = tmp;
				}  
			}
		}  
	}
	return thearray;
}



/* Date manipulations
---------------------------------------------------------------- */

// Return array[Year, Month, Day, Hours, Minutes, Seconds]
// from string: Year[-/.]Month[-/.]Day[T ]Hours:Minutes:Seconds
function invetex_parse_date(dt) {
	"use strict";
	dt = dt.replace(/\//g, '-').replace(/\./g, '-').replace(/T/g, ' ').split('+')[0];
	var dt2 = dt.split(' ');
	var d = dt2[0].split('-');
	var t = dt2[1].split(':');
	d.push(t[0], t[1], t[2]);
	return d;
}

// Return difference string between two dates
function invetex_get_date_difference(dt1) {
	"use strict";
	var dt2 = arguments[1]!==undefined ? arguments[1] : '';
	var short_date = arguments[2]!==undefined ? arguments[2] : true;
	var sec = arguments[3]!==undefined ? arguments[3] : false;
	var a1 = invetex_parse_date(dt1);
	dt1 = Date.UTC(a1[0], a1[1], a1[2], a1[3], a1[4], a1[5]);
	if (dt2 == '') {
		dt2 = new Date();
		var a2 = [dt2.getFullYear(), dt2.getMonth()+1, dt2.getDate(), dt2.getHours(), dt2.getMinutes(), dt2.getSeconds()];
	} else
		var a2 = invetex_parse_date(dt2);
	dt2 = Date.UTC(a2[0], a2[1], a2[2], a2[3], a2[4], a2[5]);
	var diff = Math.round((dt2 - dt1)/1000);
	var days = Math.floor(diff / (24*3600));
	diff -= days * 24 * 3600;
	var hours = Math.floor(diff / 3600);
	diff -= hours * 3600;
	var minutes = Math.floor(diff / 60);
	diff -= minutes * 60;
	rez = '';
	if (days > 0)
		rez += (rez!='' ? ' ' : '') + days + ' day' + (days > 1 ? 's' : '');
	if ((!short_date || rez=='') && hours > 0)
		rez += (rez!='' ? ' ' : '') + hours + ' hour' + (hours > 1 ? 's' : '');
	if ((!short_date || rez=='') && minutes > 0)
		rez +=  (rez!='' ? ' ' : '') + minutes + ' minute' + (minutes > 1 ? 's' : '');
	if (sec || rez=='')
		rez +=  rez!='' || sec ? (' ' + diff + ' second' + (diff > 1 ? 's' : '')) : 'less then minute';
	return rez;
}



/* Colors functions
---------------------------------------------------------------- */

function invetex_hex2rgb(hex) {
	"use strict";
	hex = parseInt(((hex.indexOf('#') > -1) ? hex.substring(1) : hex), 16);
	return {r: hex >> 16, g: (hex & 0x00FF00) >> 8, b: (hex & 0x0000FF)};
}

function invetex_rgb2hex(color) {
	"use strict";
	var aRGB;
	color = color.replace(/\s/g,"").toLowerCase();
	if (color=='rgba(0,0,0,0)' || color=='rgba(0%,0%,0%,0%)')
		color = 'transparent';
	if (color.indexOf('rgba(')==0)
		aRGB = color.match(/^rgba\((\d{1,3}[%]?),(\d{1,3}[%]?),(\d{1,3}[%]?),(\d{1,3}[%]?)\)$/i);
	else	
		aRGB = color.match(/^rgb\((\d{1,3}[%]?),(\d{1,3}[%]?),(\d{1,3}[%]?)\)$/i);
	
	if(aRGB) {
		color = '';
		for (var i=1; i<=3; i++) 
			color += Math.round((aRGB[i][aRGB[i].length-1]=="%"?2.55:1)*parseInt(aRGB[i],10)).toString(16).replace(/^(.)$/,'0$1');
	} else 
		color = color.replace(/^#?([\da-f])([\da-f])([\da-f])$/i, '$1$1$2$2$3$3');
	return (color.substr(0,1)!='#' ? '#' : '') + color;
}

function invetex_components2hex(r,g,b) {
	"use strict";
	return '#'+
		Number(r).toString(16).toUpperCase().replace(/^(.)$/,'0$1') +
		Number(g).toString(16).toUpperCase().replace(/^(.)$/,'0$1') +
		Number(b).toString(16).toUpperCase().replace(/^(.)$/,'0$1');
}

function invetex_rgb2components(color) {
	"use strict";
	color = invetex_rgb2hex(color);
	var matches = color.match(/^#?([\dabcdef]{2})([\dabcdef]{2})([\dabcdef]{2})$/i);
	if (!matches) return false;
	for (var i=1, rgb = new Array(3); i<=3; i++)
		rgb[i-1] = parseInt(matches[i],16);
	return rgb;
}

function invetex_hex2hsb(hex) {
	"use strict";
	return invetex_rgb2hsb(invetex_hex2rgb(hex));
}

function invetex_hsb2hex(hsb) {
	"use strict";
	var rgb = invetex_hsb2rgb(hsb);
	return invetex_components2hex(rgb.r, rgb.g, rgb.b);
}

function invetex_rgb2hsb(rgb) {
	"use strict";
	var hsb = {};
	hsb.b = Math.max(Math.max(rgb.r,rgb.g),rgb.b);
	hsb.s = (hsb.b <= 0) ? 0 : Math.round(100*(hsb.b - Math.min(Math.min(rgb.r,rgb.g),rgb.b))/hsb.b);
	hsb.b = Math.round((hsb.b /255)*100);
	if ((rgb.r==rgb.g) && (rgb.g==rgb.b))  hsb.h = 0;
	else if (rgb.r>=rgb.g && rgb.g>=rgb.b) hsb.h = 60*(rgb.g-rgb.b)/(rgb.r-rgb.b);
	else if (rgb.g>=rgb.r && rgb.r>=rgb.b) hsb.h = 60  + 60*(rgb.g-rgb.r)/(rgb.g-rgb.b);
	else if (rgb.g>=rgb.b && rgb.b>=rgb.r) hsb.h = 120 + 60*(rgb.b-rgb.r)/(rgb.g-rgb.r);
	else if (rgb.b>=rgb.g && rgb.g>=rgb.r) hsb.h = 180 + 60*(rgb.b-rgb.g)/(rgb.b-rgb.r);
	else if (rgb.b>=rgb.r && rgb.r>=rgb.g) hsb.h = 240 + 60*(rgb.r-rgb.g)/(rgb.b-rgb.g);
	else if (rgb.r>=rgb.b && rgb.b>=rgb.g) hsb.h = 300 + 60*(rgb.r-rgb.b)/(rgb.r-rgb.g);
	else 								   hsb.h = 0;
	hsb.h = Math.round(hsb.h);
	return hsb;
}

function invetex_hsb2rgb(hsb) {
	"use strict";
	var rgb = {};
	var h = Math.round(hsb.h);
	var s = Math.round(hsb.s*255/100);
	var v = Math.round(hsb.b*255/100);
	if (s == 0) {
		rgb.r = rgb.g = rgb.b = v;
	} else {
		var t1 = v;
		var t2 = (255-s)*v/255;
		var t3 = (t1-t2)*(h%60)/60;
		if (h==360) h = 0;
		if (h<60) 		{ rgb.r=t1;	rgb.b=t2;   rgb.g=t2+t3; }
		else if (h<120) { rgb.g=t1; rgb.b=t2;	rgb.r=t1-t3; }
		else if (h<180) { rgb.g=t1; rgb.r=t2;	rgb.b=t2+t3; }
		else if (h<240) { rgb.b=t1; rgb.r=t2;	rgb.g=t1-t3; }
		else if (h<300) { rgb.b=t1; rgb.g=t2;	rgb.r=t2+t3; }
		else if (h<360) { rgb.r=t1; rgb.g=t2;	rgb.b=t1-t3; }
		else 			{ rgb.r=0;  rgb.g=0;	rgb.b=0;	 }
	}
	return { r:Math.round(rgb.r), g:Math.round(rgb.g), b:Math.round(rgb.b) };
}

function invetex_color_picker(){
	"use strict";
	var id = arguments[0] ? arguments[0] : "iColorPicker"+Math.round(Math.random()*1000);
	var colors = arguments[1] ? arguments[1] : 
	'#f00,#ff0,#0f0,#0ff,#00f,#f0f,#fff,#ebebeb,#e1e1e1,#d7d7d7,#cccccc,#c2c2c2,#b7b7b7,#acacac,#a0a0a0,#959595,'
	+'#ee1d24,#fff100,#00a650,#00aeef,#2f3192,#ed008c,#898989,#7d7d7d,#707070,#626262,#555,#464646,#363636,#262626,#111,#000,'
	+'#f7977a,#fbad82,#fdc68c,#fff799,#c6df9c,#a4d49d,#81ca9d,#7bcdc9,#6ccff7,#7ca6d8,#8293ca,#8881be,#a286bd,#bc8cbf,#f49bc1,#f5999d,'
	+'#f16c4d,#f68e54,#fbaf5a,#fff467,#acd372,#7dc473,#39b778,#16bcb4,#00bff3,#438ccb,#5573b7,#5e5ca7,#855fa8,#a763a9,#ef6ea8,#f16d7e,'
	+'#ee1d24,#f16522,#f7941d,#fff100,#8fc63d,#37b44a,#00a650,#00a99e,#00aeef,#0072bc,#0054a5,#2f3192,#652c91,#91278f,#ed008c,#ee105a,'
	+'#9d0a0f,#a1410d,#a36209,#aba000,#588528,#197b30,#007236,#00736a,#0076a4,#004a80,#003370,#1d1363,#450e61,#62055f,#9e005c,#9d0039,'
	+'#790000,#7b3000,#7c4900,#827a00,#3e6617,#045f20,#005824,#005951,#005b7e,#003562,#002056,#0c004b,#30004a,#4b0048,#7a0045,#7a0026';
	var colorsList = colors.split(',');
	var tbl = '<table class="colorPickerTable"><thead>';
	for (var i=0; i<colorsList.length; i++) {
		if (i%16==0) tbl += (i>0 ? '</tr>' : '') + '<tr>';
		tbl += '<td style="background-color:'+colorsList[i]+'">&nbsp;</td>';
	}
	tbl += '</tr></thead><tbody>'
		+ '<tr style="height:60px;">'
		+ '<td colspan="8" id="'+id+'_colorPreview" style="vertical-align:middle;text-align:center;border:1px solid #000;background:#fff;">'
		+ '<input style="width:55px;color:#000;border:1px solid rgb(0, 0, 0);padding:5px;background-color:#fff;font:11px Arial, Helvetica, sans-serif;" maxlength="7" />'
		+ '<a href="#" id="'+id+'_moreColors" class="iColorPicker_moreColors"></a>'
		+ '</td>'
		+ '<td colspan="8" id="'+id+'_colorOriginal" style="vertical-align:middle;text-align:center;border:1px solid #000;background:#fff;">'
		+ '<input style="width:55px;color:#000;border:1px solid rgb(0, 0, 0);padding:5px;background-color:#fff;font:11px Arial, Helvetica, sans-serif;" readonly="readonly" />'
		+ '</td>'
		+ '</tr></tbody></table>';

	jQuery(document.createElement("div"))
		.attr("id", id)
		.css('display','none')
		.html(tbl)
		.appendTo("body")
		.addClass("iColorPickerTable")
		.on('mouseover', 'thead td', function(){
			"use strict";
			var aaa = invetex_rgb2hex(jQuery(this).css('background-color'));
			jQuery('#'+id+'_colorPreview').css('background',aaa);
			jQuery('#'+id+'_colorPreview input').val(aaa);
		})
		.on('keypress', '#'+id+'_colorPreview input', function(key){
			"use strict";
			var aaa = jQuery(this).val()
			if (aaa.length<7 && ((key.which>=48 && key.which<=57) || (key.which>=97 && key.which<=102) || (key.which===35 || aaa.length===0))) {
				aaa += String.fromCharCode(key.which);
			} else if (key.which == 8 && aaa.length>0) {
				aaa = aaa.substring(0, aaa.length-1);
			} else if (key.which===13 && (aaa.length===4 || aaa.length===7)) {
				var fld  = jQuery('#'+id).data('field');
				var func = jQuery('#'+id).data('func');
				if (func!=null && func!='undefined') {
					func(fld, aaa);
				} else {
					fld.val(aaa).css('backgroundColor', aaa).trigger('change');
				}
				jQuery('#'+id+'_Bg').fadeOut(500);
				jQuery('#'+id).fadeOut(500);
				
			} else {
				key.preventDefault();
				return false;
			}
			if (aaa.substr(0,1)==='#' && (aaa.length===4 || aaa.length===7)) {
				jQuery('#'+id+'_colorPreview').css('background',aaa);
			}
		})
		.on('click', 'thead td', function(e){
			"use strict";
			var fld  = jQuery('#'+id).data('field');
			var func = jQuery('#'+id).data('func');
			var aaa  = invetex_rgb2hex(jQuery(this).css('background-color'));
			if (func!=null && func!='undefined') {
				func(fld, aaa);
			} else {
				fld.val(aaa).css('backgroundColor', aaa).trigger('change');
			}
			jQuery('#'+id+'_Bg').fadeOut(500);
			jQuery('#'+id).fadeOut(500);
			e.preventDefault();
			return false;
		})
		.on('click', 'tbody .iColorPicker_moreColors', function(e){
			"use strict";
			var thead  = jQuery(this).parents('table').find('thead');
			var out = '';
			if (thead.hasClass('more_colors')) {
				for (var i=0; i<colorsList.length; i++) {
					if (i%16==0) out += (i>0 ? '</tr>' : '') + '<tr>';
					out += '<td style="background-color:'+colorsList[i]+'">&nbsp;</td>';
				}
				thead.removeClass('more_colors').empty().html(out+'</tr>');
				jQuery('#'+id+'_colorPreview').attr('colspan', 8);
				jQuery('#'+id+'_colorOriginal').attr('colspan', 8);
			} else {
				var rgb=[0,0,0], i=0, j=-1;	// Set j=-1 or j=0 - show 2 different colors layouts
				while (rgb[0]<0xF || rgb[1]<0xF || rgb[2]<0xF) {
					if (i%18==0) out += (i>0 ? '</tr>' : '') + '<tr>';
					i++;
					out += '<td style="background-color:'+invetex_components2hex(rgb[0]*16+rgb[0],rgb[1]*16+rgb[1],rgb[2]*16+rgb[2])+'">&nbsp;</td>';
					rgb[2]+=3;
					if (rgb[2]>0xF) {
						rgb[1]+=3;
						if (rgb[1]>(j===0 ? 6 : 0xF)) {
							rgb[0]+=3;
							if (rgb[0]>0xF) {
								if (j===0) {
									j=1;
									rgb[0]=0;
									rgb[1]=9;
									rgb[2]=0;
								} else {
									break;
								}
							} else {
								rgb[1]=(j < 1 ? 0 : 9);
								rgb[2]=0;
							}
						} else {
							rgb[2]=0;
						}
					}
				}
				thead.addClass('more_colors').empty().html(out+'<td  style="background-color:#ffffff" colspan="8">&nbsp;</td></tr>');
				jQuery('#'+id+'_colorPreview').attr('colspan', 9);
				jQuery('#'+id+'_colorOriginal').attr('colspan', 9);
			}
			jQuery('#'+id+' table.colorPickerTable thead td')
				.css({
					'width':'12px',
					'height':'14px',
					'border':'1px solid #000',
					'cursor':'pointer'
				});
			e.preventDefault();
			return false;
		});
	jQuery(document.createElement("div"))
		.attr("id", id+"_Bg")
		.on('click', function(e) {
			"use strict";
			jQuery("#"+id+"_Bg").fadeOut(500);
			jQuery("#"+id).fadeOut(500);
			e.preventDefault();
			return false;
		})
		.appendTo("body");
	jQuery('#'+id+' table.colorPickerTable thead td')
		.css({
			'width':'12px',
			'height':'14px',
			'border':'1px solid #000',
			'cursor':'pointer'
		});
	jQuery('#'+id+' table.colorPickerTable')
		.css({'border-collapse':'collapse'});
	jQuery('#'+id)
		.css({
			'border':'1px solid #ccc',
			'background':'#333',
			'padding':'5px',
			'color':'#fff',
			'z-index':999999
		});
	jQuery('#'+id+'_colorPreview')
		.css({'height':'50px'});
	return id;
}

function invetex_color_picker_show(id, fld, func) { 
	"use strict";
	if (id===null || id==='') {
		id = jQuery('.iColorPickerTable').attr('id');
	}
	var eICP = fld.offset();
	var w = jQuery('#'+id).width();
	var h = jQuery('#'+id).height();
	var l = eICP.left + w < jQuery(window).width()-10 ? eICP.left : jQuery(window).width()-10 - w;
	var t = eICP.top + fld.outerHeight() + h < jQuery(document).scrollTop() + jQuery(window).height()-10 ? eICP.top + fld.outerHeight() : eICP.top - h - 13;
	jQuery("#"+id)
		.data({field: fld, func: func})
		.css({
			'top':t+"px",
			'left':l+"px",
			'position':'absolute',
			'z-index':100001
		})
		.fadeIn(500);
	jQuery("#"+id+"_Bg")
		.css({
			'position':'fixed',
			'z-index':100000,
			'top':0,
			'left':0,
			'width':'100%',
			'height':'100%'
		})
		.fadeIn(500);
	var def = fld.val().substr(0, 1)=='#' ? fld.val() : invetex_rgb2hex(fld.css('backgroundColor'));
	jQuery('#'+id+'_colorPreview input,#'+id+'_colorOriginal input').val(def);
	jQuery('#'+id+'_colorPreview,#'+id+'_colorOriginal').css('background',def);
}



/* Cookies manipulations
---------------------------------------------------------------- */

function invetex_get_cookie(name) {
	"use strict";
	var defa = arguments[1]!=undefined ? arguments[1] : null;
	var start = document.cookie.indexOf(name + '=');
	var len = start + name.length + 1;
	if ((!start) && (name != document.cookie.substring(0, name.length))) {
		return defa;
	}
	if (start == -1)
		return defa;
	var end = document.cookie.indexOf(';', len);
	if (end == -1)
		end = document.cookie.length;
	return unescape(document.cookie.substring(len, end));
}


function invetex_set_cookie(name, value, expires, path, domain, secure) {
	"use strict";
	var expires = arguments[2]!=undefined ? arguments[2] : 0;
	var path    = arguments[3]!=undefined ? arguments[3] : '/';
	var domain  = arguments[4]!=undefined ? arguments[4] : '';
	var secure  = arguments[5]!=undefined ? arguments[5] : '';
	var today = new Date();
	today.setTime(today.getTime());
	if (expires) {
		expires = expires * 1000 * 60 * 60 * 24;
	}
	var expires_date = new Date(today.getTime() + (expires));
	document.cookie = name + '='
			+ escape(value)
			+ ((expires) ? ';expires=' + expires_date.toGMTString() : '')
			+ ((path)    ? ';path=' + path : '')
			+ ((domain)  ? ';domain=' + domain : '')
			+ ((secure)  ? ';secure' : '');
}


function invetex_del_cookie(name, path, domain) {
	"use strict";
	var path   = arguments[1]!=undefined ? arguments[1] : '/';
	var domain = arguments[2]!=undefined ? arguments[2] : '';
	if (invetex_get_cookie(name))
		document.cookie = name + '=' + ((path) ? ';path=' + path : '')
				+ ((domain) ? ';domain=' + domain : '')
				+ ';expires=Thu, 01-Jan-1970 00:00:01 GMT';
}



/* ListBox and ComboBox manipulations
---------------------------------------------------------------- */

function invetex_clear_listbox(box) {
	"use strict";
	for (var i=box.options.length-1; i>=0; i--)
		box.options[i] = null;
}

function invetex_add_listbox_item(box, val, text) {
	"use strict";
	var item = new Option();
	item.value = val;
	item.text = text;
    box.options.add(item);
}

function invetex_del_listbox_item_by_value(box, val) {
	"use strict";
	for (var i=0; i<box.options.length; i++) {
		if (box.options[i].value == val) {
			box.options[i] = null;
			break;
		}
	}
}

function invetex_del_listbox_item_by_text(box, txt) {
	"use strict";
	for (var i=0; i<box.options.length; i++) {
		if (box.options[i].text == txt) {
			box.options[i] = null;
			break;
		}
	}
}

function invetex_find_listbox_item_by_value(box, val) {
	"use strict";
	var idx = -1;
	for (var i=0; i<box.options.length; i++) {
		if (box.options[i].value == val) {
			idx = i;
			break;
		}
	}
	return idx;
}

function invetex_find_listbox_item_by_text(box, txt) {
	"use strict";
	var idx = -1;
	for (var i=0; i<box.options.length; i++) {
		if (box.options[i].text == txt) {
			idx = i;
			break;
		}
	}
	return idx;
}

function invetex_select_listbox_item_by_value(box, val) {
	"use strict";
	for (var i = 0; i < box.options.length; i++) {
		box.options[i].selected = (val == box.options[i].value);
	}
}

function invetex_select_listbox_item_by_text(box, txt) {
	"use strict";
	for (var i = 0; i < box.options.length; i++) {
		box.options[i].selected = (txt == box.options[i].text);
	}
}

function invetex_get_listbox_values(box) {
	"use strict";
	var delim = arguments[1] ? arguments[1] : ',';
	var str = '';
	for (var i=0; i<box.options.length; i++) {
		str += (str ? delim : '') + box.options[i].value;
	}
	return str;
}

function invetex_get_listbox_texts(box) {
	"use strict";
	var delim = arguments[1] ? arguments[1] : ',';
	var str = '';
	for (var i=0; i<box.options.length; i++) {
		str += (str ? delim : '') + box.options[i].text;
	}
	return str;
}

function invetex_sort_listbox(box)  {
	"use strict";
	var temp_opts = new Array();
	var temp = new Option();
	for(var i=0; i<box.options.length; i++)  {
		temp_opts[i] = box.options[i].clone();
	}
	for(var x=0; x<temp_opts.length-1; x++)  {
		for(var y=(x+1); y<temp_opts.length; y++)  {
			if(temp_opts[x].text > temp_opts[y].text)  {
				temp = temp_opts[x];
				temp_opts[x] = temp_opts[y];
				temp_opts[y] = temp;
			}  
		}  
	}
	for(var i=0; i<box.options.length; i++)  {
		box.options[i] = temp_opts[i].clone();
	}
}

function invetex_get_listbox_selected_index(box) {
	"use strict";
	for (var i = 0; i < box.options.length; i++) {
		if (box.options[i].selected)
			return i;
	}
	return -1;
}

function invetex_get_listbox_selected_value(box) {
	"use strict";
	for (var i = 0; i < box.options.length; i++) {
		if (box.options[i].selected) {
			return box.options[i].value;
		}
	}
	return null;
}

function invetex_get_listbox_selected_text(box) {
	"use strict";
	for (var i = 0; i < box.options.length; i++) {
		if (box.options[i].selected) {
			return box.options[i].text;
		}
	}
	return null;
}

function invetex_get_listbox_selected_option(box) {
	"use strict";
	for (var i = 0; i < box.options.length; i++) {
		if (box.options[i].selected) {
			return box.options[i];
		}
	}
	return null;
}



/* Radio buttons manipulations
---------------------------------------------------------------- */

function invetex_get_radio_value(radioGroupObj) {
	"use strict";
	for (var i=0; i < radioGroupObj.length; i++)
		if (radioGroupObj[i].checked) return radioGroupObj[i].value;
	return null;
}

function invetex_set_radio_checked_by_num(radioGroupObj, num) {
	"use strict";
	for (var i=0; i < radioGroupObj.length; i++)
		if (radioGroupObj[i].checked && i!=num) radioGroupObj[i].checked=false;
		else if (i==num) radioGroupObj[i].checked=true;
}

function invetex_set_radio_checked_by_value(radioGroupObj, val) {
	"use strict";
	for (var i=0; i < radioGroupObj.length; i++)
		if (radioGroupObj[i].checked && radioGroupObj[i].value!=val) radioGroupObj[i].checked=false;
		else if (radioGroupObj[i].value==val) radioGroupObj[i].checked=true;
}



/* Form manipulations
---------------------------------------------------------------- */

function invetex_form_validate(form, opt) {
	"use strict";
	var error_msg = '';
	form.find(":input").each(function() {
		"use strict";
		if (error_msg!='' && opt.exit_after_first_error) return;
		for (var i = 0; i < opt.rules.length; i++) {
			if (jQuery(this).attr("name") == opt.rules[i].field) {
				var val = jQuery(this).val();
				var error = false;
				if (typeof(opt.rules[i].min_length) == 'object') {
					if (opt.rules[i].min_length.value > 0 && val.length < opt.rules[i].min_length.value) {
						if (error_msg=='') jQuery(this).get(0).focus();
						error_msg += '<p class="error_item">' + (typeof(opt.rules[i].min_length.message)!='undefined' ? opt.rules[i].min_length.message : opt.error_message_text ) + '</p>'
						error = true;
					}
				}
				if ((!error || !opt.exit_after_first_error) && typeof(opt.rules[i].max_length) == 'object') {
					if (opt.rules[i].max_length.value > 0 && val.length > opt.rules[i].max_length.value) {
						if (error_msg=='') jQuery(this).get(0).focus();
						error_msg += '<p class="error_item">' + (typeof(opt.rules[i].max_length.message)!='undefined' ? opt.rules[i].max_length.message : opt.error_message_text ) + '</p>'
						error = true;
					}
				}
				if ((!error || !opt.exit_after_first_error) && typeof(opt.rules[i].mask) == 'object') {
					if (val.length > 0 && opt.rules[i].mask.value != '') {
						var regexp = new RegExp(opt.rules[i].mask.value);
						if (!regexp.test(val)) {
							if (error_msg=='') jQuery(this).get(0).focus();
							error_msg += '<p class="error_item">' + (typeof(opt.rules[i].mask.message)!='undefined' ? opt.rules[i].mask.message : opt.error_message_text ) + '</p>'
							error = true;
						}
					}
				}
				if ((!error || !opt.exit_after_first_error) && typeof(opt.rules[i].state) == 'object') {
					if (opt.rules[i].state.value=='checked' && !jQuery(this).get(0).checked) {
						if (error_msg=='') jQuery(this).get(0).focus();
						error_msg += '<p class="error_item">' + (typeof(opt.rules[i].state.message)!='undefined' ? opt.rules[i].state.message : opt.error_message_text ) + '</p>'
						error = true;
					}
				}
				if ((!error || !opt.exit_after_first_error) && typeof(opt.rules[i].equal_to) == 'object') {
					if (opt.rules[i].equal_to.value != '' && val!=jQuery(jQuery(this).get(0).form[opt.rules[i].equal_to.value]).val()) {
						if (error_msg=='') jQuery(this).get(0).focus();
						error_msg += '<p class="error_item">' + (typeof(opt.rules[i].equal_to.message)!='undefined' ? opt.rules[i].equal_to.message : opt.error_message_text ) + '</p>'
						error = true;
					}
				}
				if (opt.error_fields_class != '') jQuery(this).toggleClass(opt.error_fields_class, error);
			}
		}
	});
	if (error_msg!='' && opt.error_message_show) {
		var error_message_box = form.find(".result");
		if (error_message_box.length == 0) error_message_box = form.parent().find(".result");
		if (error_message_box.length == 0) {
			form.append('<div class="result"></div>');
			error_message_box = form.find(".result");
		}
		if (opt.error_message_class) error_message_box.toggleClass(opt.error_message_class, true);
		error_message_box.html(error_msg).fadeIn();
		setTimeout(function() { error_message_box.fadeOut(); }, opt.error_message_time);
	}
	return error_msg!='';
}



/* Document manipulations
---------------------------------------------------------------- */

// Animated scroll to selected id
function invetex_document_animate_to(id) {
	"use strict";
	if (id.indexOf('#')==-1) id = '#' + id;
	var obj = jQuery(id).eq(0);
	if (obj.length == 0) return;
	var oft = jQuery(id).offset().top;
	var st  = jQuery(window).scrollTop();
	var speed = Math.min(1600, Math.max(400, Math.round(Math.abs(oft-st) / jQuery(window).height() * 100)));
	jQuery('body,html').animate( {scrollTop: oft - jQuery('#wpadminbar').height() - jQuery('header.fixedTopMenu .topWrap').height()}, speed, 'swing');
}

// Change browser address without reload page
function invetex_document_set_location(curLoc){
	"use strict";
	try {
		history.pushState(null, null, curLoc);
		return;
	} catch(e) {}
	location.href = curLoc;
}

// Add hidden elements init functions after tab, accordion, toggles activate
function invetex_add_hidden_elements_handler(key, handler) {
	"use strict";
	invetex_storage_set_array('init_hidden_elements', key, handler);
}

// Init hidden elements after tab, accordion, toggles activate
function invetex_init_hidden_elements(cont) {
	"use strict";
	if (INVETEX_STORAGE['init_hidden_elements']) {
		for (key in INVETEX_STORAGE['init_hidden_elements']) {
			INVETEX_STORAGE['init_hidden_elements'][key](cont);
		}
	}
}



/* Browsers detection
---------------------------------------------------------------- */

function invetex_browser_is_mobile() {
	"use strict";
	var check = false;
	(function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od|ad)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);
	return check;
}
function invetex_browser_is_ios() {
	"use strict";
	return navigator.userAgent.match(/iPad|iPhone|iPod/i) != null;
}
function invetex_is_retina() {
	"use strict";
	var mediaQuery = '(-webkit-min-device-pixel-ratio: 1.5), (min--moz-device-pixel-ratio: 1.5), (-o-min-device-pixel-ratio: 3/2), (min-resolution: 1.5dppx)';
	return (window.devicePixelRatio > 1) || (window.matchMedia && window.matchMedia(mediaQuery).matches);
}


/* Files functions
---------------------------------------------------------------- */

function invetex_get_file_name(path) {
	"use strict";
	path = path.replace(/\\/g, '/');
	var pos = path.lastIndexOf('/');
	if (pos >= 0)
		path = path.substr(pos+1);
	return path;
}

function invetex_get_file_ext(path) {
	"use strict";
	var pos = path.lastIndexOf('.');
	path = pos >= 0 ? path.substr(pos+1) : '';
	return path;
}



/* Images functions
---------------------------------------------------------------- */

// Return true, if all images in the specified container are loaded
function invetex_check_images_complete(cont) {
	"use strict";
	var complete = true;
	cont.find('img').each(function() {
		if (!complete) return;
		if (!jQuery(this).get(0).complete) complete = false;
	});
	return complete;
}
;/* global jQuery:false */
/* global INVETEX_STORAGE:false */
jQuery(document).ready(function() {
    "use strict";
    INVETEX_STORAGE['theme_init_counter'] = 0;
    invetex_init_actions();
});

jQuery(window).on('beforeunload', function() {
    "use strict";
    // Show preloader
    jQuery('#page_preloader').css({
        display: 'block',
        opacity: 0
    }).animate({
        opacity: 0.8
    }, 300);
});


// Template init actions
function invetex_init_actions() {
    "use strict";

    if (INVETEX_STORAGE['vc_edit_mode'] && jQuery('.vc_empty-placeholder').length == 0 && INVETEX_STORAGE['theme_init_counter']++ < 30) {
        setTimeout(invetex_init_actions, 200);
        return;
    }

    // Hide preloader
    jQuery('#page_preloader').animate({
        opacity: 0
    }, 500, function() {
        jQuery(this).css({
            display: 'none'
        });
    });

    // Check for Retina display
    if (invetex_is_retina()) {
        invetex_set_cookie('invetex_retina', 1, 365);
    }

    invetex_ready_actions();

    // Add resize handlers after VC row stretch handlers on('resize.vcRowBehaviour', ...)
    setTimeout(function() {
		"use strict";
        jQuery(window).on('resize.invetex', function() {
            invetex_resize_actions();
            invetex_scroll_actions()
        }).trigger('resize.invetex');
    }, 10);

    // Scroll handlers
    jQuery(window).on('scroll.invetex', function() {
        "use strict";
        invetex_scroll_actions();
    });
}



// Template first load actions
//==============================================
function invetex_ready_actions() {
    "use strict";

    // Call theme specific action (if exists)
    //----------------------------------------------
    if (window.invetex_theme_ready_actions) invetex_theme_ready_actions();


    // Widgets decoration
    //----------------------------------------------

    // Decorate nested lists in widgets and side panels
    jQuery('.widget ul > li').each(function() {
		"use strict";
        if (jQuery(this).find('ul').length > 0) {
            jQuery(this).addClass('has_children');
        }
    });


    // Archive widget decoration
    jQuery('.widget_archive a').each(function() {
		"use strict";
        var val = jQuery(this).html().split(' ');
        if (val.length > 1) {
            val[val.length - 1] = '<span>' + val[val.length - 1] + '</span>';
            jQuery(this).html(val.join(' '))
        }
    });


    // Navigate on category change
    jQuery('.widget_subcategories').on('change', 'select', function() {
		"use strict";
        var dropdown = jQuery(this).get(0);
        if (dropdown.options[dropdown.selectedIndex].value > 0) {
            location.href = INVETEX_STORAGE['site_url'] + "/?cat=" + dropdown.options[dropdown.selectedIndex].value;
        }
    });


    // Calendar handlers - change months
    jQuery('.widget_calendar').on('click', '.month_prev a, .month_next a', function(e) {
        "use strict";
        var calendar = jQuery(this).parents('.wp-calendar');
        var m = jQuery(this).data('month');
        var y = jQuery(this).data('year');
        var l = jQuery(this).data('letter');
        var pt = jQuery(this).data('type');
        jQuery.post(INVETEX_STORAGE['ajax_url'], {
            action: 'calendar_change_month',
            nonce: INVETEX_STORAGE['ajax_nonce'],
            letter: l,
            month: m,
            year: y,
            post_type: pt
        }).done(function(response) {
			"use strict";
            var rez = {};
            try {
                rez = JSON.parse(response);
            } catch (e) {
                rez = {
                    error: INVETEX_STORAGE['ajax_error']
                };
                console.log(response);
            }
            if (rez.error === '') {
                calendar.parent().fadeOut(200, function() {
                    jQuery(this).find('.wp-calendar').remove();
                    jQuery(this).append(rez.data).fadeIn(200);
                });
            }
        });
        e.preventDefault();
        return false;
    });


    // Media setup
    //----------------------------------------------

    // Video background init
    jQuery('.video_background').each(function() {
		"use strict";
        var youtube = jQuery(this).data('youtube-code');
        if (youtube) {
            jQuery(this).tubular({
                videoId: youtube
            });
        }
    });


    // Main slider
    //----------------------------------------------
    jQuery('.slider_over_button,.slider_over_close').on('click', function(e) {
		"use strict";
        jQuery(this).parent().toggleClass('opened');
        e.preventDefault();
        return false;
    });



    // Menu
    //----------------------------------------------

    // Prepare menus
    if (INVETEX_STORAGE['menu_cache']) invetex_prepare_menus();

    // Clone side menu for responsive
	var menu_side = jQuery('ul#menu_side');
	var header_mobile = jQuery('.header_mobile');
	var side_wrap = jQuery('.header_mobile .side_wrap');
	var header_mask = jQuery('.header_mobile .mask');
	var html = jQuery('html');
	var body = jQuery('body');
	
    if (menu_side.length > 0) {
        menu_side.clone().removeAttr('id').removeClass('menu_side_nav').addClass('menu_side_responsive').insertAfter('ul#menu_side');
        invetex_show_current_menu_item(jQuery('.menu_side_responsive'), jQuery('.sidebar_outer_menu_responsive_button'));
    }
    if (header_mobile.length > 0) {
        jQuery('.header_mobile .menu_main_nav_area ul#menu_main').removeAttr('id');
        jQuery('.header_mobile .menu_button').on('click', function() {
			"use strict";
            side_wrap.toggleClass('open');
            header_mask.toggleClass('show');
            html.toggleClass('menu_mobile_open');
            // Fix for Safari
            if (invetex_browser_is_ios() && body.hasClass('menu_mobile') && side_wrap.hasClass('open')) {
                body.data('overflow', body.css('overflow')).css('overflow', 'hidden');
                body.data('position', body.css('position')).css('position', 'fixed');
            }
        });
        jQuery('.header_mobile .mask, .header_mobile .side_wrap .close').on('click', function() {
			"use strict";
            side_wrap.removeClass('open');
            header_mask.removeClass('show');
            html.removeClass('menu_mobile_open');
            // Fix for Safari
            if (invetex_browser_is_ios() && body.hasClass('menu_mobile') && !side_wrap.hasClass('open')) {
                body.css('overflow', body.data('overflow'));
                body.css('position', body.data('position'));
            }
        });
    }

    // Push menu button
    jQuery('.menu_pushy_button').on('click', function(e) {
        "use strict";
        body.addClass('pushy-active').css('overflow', 'hidden');
        jQuery('.site-overlay').fadeIn('fast');
        e.preventDefault();
        return false;
    });
    jQuery('.pushy .close-pushy,.site-overlay').on('click', function(e) {
		"use strict";
        body.removeClass('pushy-active').css('overflow', 'visible');
        jQuery('.site-overlay').fadeOut('fast');
        e.preventDefault();
        return false;
    });

    // Add arrows in responsive menu
    jQuery('.header_mobile .menu_main_nav .menu-item-has-children > a, .menu_side_responsive .menu-item-has-children > a, .menu_pushy_nav_area .menu-item-has-children > a, body:not(.woocommerce) .widget_area:not(.footer_wrap) .widget_product_categories ul.product-categories .has_children > a').prepend('<span class="open_child_menu"></span>');

    // Submenu click handler for the responsive menu
    jQuery('.header_mobile .menu_main_nav, .menu_side_responsive, .menu_pushy_nav_area, body:not(.woocommerce) .widget_area:not(.footer_wrap) .widget_product_categories').on('click', 'li a,li a .open_child_menu, ul.product-categories.plain li a .open_child_menu', function(e) {
        "use strict";
        var is_menu_main = jQuery(this).parents('.menu_main_nav').length > 0;
        var $a = jQuery(this).hasClass('open_child_menu') ? jQuery(this).parent() : jQuery(this);
        if ((!is_menu_main || body.hasClass('menu_mobile')) && ($a.parent().hasClass('menu-item-has-children') || $a.parent().hasClass('has_children'))) {
            if ($a.siblings('ul:visible').length > 0)
                $a.siblings('ul').slideUp().parent().removeClass('opened');
            else {
                jQuery(this).parents('li').siblings('li').find('ul:visible').slideUp().parent().removeClass('opened');
                $a.siblings('ul').slideDown().parent().addClass('opened');
            }
        }
        // Ignore link for parent menu items
        if (jQuery(this).hasClass('open_child_menu') || $a.attr('href') == '#') {
            e.preventDefault();
            return false;
        }
    });

    // Init superfish menus
    invetex_init_sfmenu('.menu_main_nav_area ul#menu_main, ul#menu_user, ul#menu_side, body:not(.woocommerce) .widget_area:not(.footer_wrap) .widget_product_categories ul.product-categories');

    // Slide effect for main menu
    if (INVETEX_STORAGE['menu_hover'] == 'slide_line' || INVETEX_STORAGE['menu_hover'] == 'slide_box') {
        setTimeout(function() {
            "use strict";
            jQuery('#menu_main').spasticNav({
                style: INVETEX_STORAGE['menu_hover'] == 'slide_line' ? 'line' : 'box',
                color: INVETEX_STORAGE['accent1_hover'],
                colorOverride: false
            });
        }, 500);
    }

    // Show table of contents for the current page
    if (INVETEX_STORAGE['toc_menu'] != 'hide' && INVETEX_STORAGE['toc_menu'] != 'no') {
        invetex_build_page_toc();
    }

    // One page mode for menu links (scroll to anchor)
    jQuery('#toc, ul#menu_main li, ul#menu_user li, ul#menu_side li, ul#menu_footer li, ul#menu_pushy li').on('click', 'a', function(e) {
        "use strict";
        var href = jQuery(this).attr('href');
        if (href === undefined) return;
        var pos = href.indexOf('#');
        if (pos < 0 || href.length == 1) return;
        if (jQuery(href.substr(pos)).length > 0) {
            var loc = window.location.href;
            var pos2 = loc.indexOf('#');
            if (pos2 > 0) loc = loc.substring(0, pos2);
            var now = pos == 0;
            if (!now) now = loc == href.substring(0, pos);
            if (now) {
                invetex_document_animate_to(href.substr(pos));
                invetex_document_set_location(pos == 0 ? loc + href : href);
                e.preventDefault();
                return false;
            }
        }
    });


    // Store height of the top and side panels
    INVETEX_STORAGE['top_panel_height'] = 0; 
    INVETEX_STORAGE['side_panel_height'] = 0;


    // Pagination
    //----------------------------------------------

    // Page navigation (style slider)
	var pager_slider = jQuery('.pager_slider');
    jQuery('.pager_cur').on('click', function(e) {
        "use strict";
        pager_slider.slideDown(300, function() {
            invetex_sc_init(pager_slider.eq(0));
        });
        e.preventDefault();
        return false;
    });


    // View More button
	var viewmore_link = jQuery('#viewmore_link');
    viewmore_link.on('click', function(e) {
        "use strict";
        if (!INVETEX_STORAGE['viewmore_busy'] && !jQuery(this).hasClass('viewmore_empty')) {
            jQuery(this).parent().addClass('loading');
            INVETEX_STORAGE['viewmore_busy'] = true;
            jQuery.post(INVETEX_STORAGE['ajax_url'], {
                action: 'view_more_posts',
                nonce: INVETEX_STORAGE['ajax_nonce'],
                page: INVETEX_STORAGE['viewmore_page'] + 1,
                data: INVETEX_STORAGE['viewmore_data'],
                vars: INVETEX_STORAGE['viewmore_vars']
            }).done(function(response) {
                "use strict";
                var rez = {};
                try {
                    rez = JSON.parse(response);
                } catch (e) {
                    rez = {
                        error: INVETEX_STORAGE['ajax_error']
                    };
                    console.log(response);
                }
                viewmore_link.parent().removeClass('loading');
                INVETEX_STORAGE['viewmore_busy'] = false;
                if (rez.error === '') {
                    var posts_container = jQuery('.content').eq(0);
                    if (posts_container.find('.isotope_wrap').length > 0) posts_container = posts_container.find('.isotope_wrap').eq(0);
                    if (posts_container.hasClass('isotope_wrap')) {
                        posts_container.data('last-width', 0).append(rez.data);
                        INVETEX_STORAGE['isotope_init_counter'] = 0;
                        invetex_init_appended_isotope(posts_container, rez.filters);
                    } else
                        jQuery('#viewmore').before(rez.data);

                    INVETEX_STORAGE['viewmore_page']++;
                    if (rez.no_more_data == 1) {
                        viewmore_link.addClass('viewmore_empty').parent().hide();
                    }

                    invetex_init_post_formats();
                    invetex_sc_init(posts_container);
                    invetex_scroll_actions();
                }
            });
        }
        e.preventDefault();
        return false;
    });


    // WooCommerce
    //----------------------------------------------

    // Change display mode
    jQuery('.woocommerce,.woocommerce-page').on('click', '.mode_buttons a', function(e) {
        "use strict";
        var mode = jQuery(this).hasClass('woocommerce_thumbs') ? 'thumbs' : 'list';
        jQuery.cookie('invetex_shop_mode', mode, {
            expires: 365,
            path: '/'
        });
        jQuery(this).siblings('input').val(mode).parents('form').get(0).submit();
        e.preventDefault();
        return false;
    });
    // Added to cart
    body.bind('added_to_cart', function() {
        "use strict";
        // Update amount on the cart button
        var total = jQuery('.widget_shopping_cart').eq(0).find('.total .amount').text();
        if (total != undefined) {
            jQuery('.top_panel_cart_button .cart_summa').text(total);
        }
        // Update count items on the cart button
        var cnt = 0;
        jQuery('.widget_shopping_cart_content').eq(0).find('.cart_list li').each(function() {
			"use strict";
            var q = jQuery(this).find('.quantity').html().split(' ', 2);
            if (!isNaN(q[0]))
                cnt += Number(q[0]);
        });
        var items = jQuery('.top_panel_cart_button .cart_items').eq(0).text().split(' ', 2);
        items[0] = cnt;
        jQuery('.top_panel_cart_button .cart_items').text(items[0] + ' ' + items[1]);
        // Update data-attr on button
        jQuery('.top_panel_cart_button').data({
            'items': cnt ? cnt : 0,
            'summa': total ? total : 0
        });
    });
    // Show cart 
    jQuery('.top_panel_middle .top_panel_cart_button, .header_mobile .top_panel_cart_button').on('click', function(e) {
        "use strict";
        jQuery(this).siblings('.sidebar_cart').slideToggle();
        e.preventDefault();
        return false;
    });
    // Add buttons to quantity
    jQuery('.woocommerce div.quantity,.woocommerce-page div.quantity').append('<span class="q_inc"></span><span class="q_dec"></span>');
    jQuery('.woocommerce div.quantity').on('click', '>span', function(e) {
        "use strict";
        var f = jQuery(this).siblings('input');
        if (jQuery(this).hasClass('q_inc')) {
            f.val(Math.max(0, parseInt(f.val(), 10)) + 1);
        } else {
            f.val(Math.max(1, Math.max(0, parseInt(f.val(), 10)) - 1));
        }
        e.preventDefault();
        return false;
    });
    // Add stretch behaviour to WooC tabs area
    jQuery('.single-product .woocommerce-tabs')
        .addClass('trx-stretch-width scheme_light')
        .after('<div class="trx-stretch-width-original"></div>');
    invetex_stretch_width();


    // Popup login and register windows
    //----------------------------------------------
	var popup_wrap_bg = jQuery('.popup_wrap_bg');
    jQuery('.popup_link,.popup_login_link,.popup_register_link').addClass('inited').on('click', function(e) {
		"use strict";
        var popup = jQuery(jQuery(this).attr('href'));
        if (popup.length === 1) {
            invetex_hide_popup(jQuery(popup.hasClass('popup_login') ? '.popup_registration' : '.popup_login'));
            invetex_show_popup(popup);
            if (jQuery(popup.hasClass('popup_login')) || jQuery(popup.hasClass('popup_registration'))) {
                if (popup_wrap_bg.css('display') != 'none') {
                    popup_wrap_bg.fadeOut();
                } else {
                    popup_wrap_bg.fadeIn();
                }
            }
        }
        e.preventDefault();
        return false;
    });
    jQuery('.popup_wrap').on('click', '.popup_close', function(e) {
		"use strict";
        var popup = jQuery(this).parent();
        if (popup.length === 1) {
            invetex_hide_popup(popup);
            popup_wrap_bg.fadeOut();
        }
        e.preventDefault();
        return false;
    });


    // Forms validation
    //----------------------------------------------

    // Login form
    jQuery('.popup_form.login_form').submit(function(e) {
        "use strict";
        var rez = invetex_login_validate(jQuery(this));
        if (!rez)
            e.preventDefault();
        return rez;
    });

    // Registration form
    jQuery('.popup_form.registration_form').submit(function(e) {
        "use strict";
        var rez = invetex_registration_validate(jQuery(this));
        if (!rez)
            e.preventDefault();
        return rez;
    });

    // Comment form
    jQuery("form#commentform").submit(function(e) {
        "use strict";
        var rez = invetex_comments_validate(jQuery(this));
        if (!rez)
            e.preventDefault();
        return rez;
    });



    // Bookmarks
    //----------------------------------------------

    // Add bookmark
    jQuery('.bookmarks_add').on('click', function(e) {
        "use strict";
        var title = window.document.title.split('|')[0];
        var url = window.location.href;
        var list = jQuery.cookie('invetex_bookmarks');
        var exists = false;
        if (list) {
            try {
                list = JSON.parse(list);
            } catch (e) {}
            if (list.length) {
                for (var i = 0; i < list.length; i++) {
                    if (list[i].url == url) {
                        exists = true;
                        break;
                    }
                }
            }
        } else
            list = new Array();
        if (!exists) {
            var message_popup = invetex_message_dialog('<label for="bookmark_title">' + INVETEX_STORAGE['strings']['bookmark_title'] + '</label><br><input type="text" id="bookmark_title" name="bookmark_title" value="' + title + '">', INVETEX_STORAGE['strings']['bookmark_add'], null,
                function(btn, popup) {
                    "use strict";
                    if (btn != 1) return;
                    title = message_popup.find('#bookmark_title').val();
                    list.push({
                        title: title,
                        url: url
                    });
                    jQuery('.bookmarks_list').append('<li><a href="' + url + '" class="bookmarks_item">' + title + '<span class="bookmarks_delete icon-cancel" title="' + INVETEX_STORAGE['strings']['bookmark_del'] + '"></span></a></li>');
                    jQuery.cookie('invetex_bookmarks', JSON.stringify(list), {
                        expires: 365,
                        path: '/'
                    });
                    setTimeout(function() {
						"use strict";
                        invetex_message_success(INVETEX_STORAGE['strings']['bookmark_added'], INVETEX_STORAGE['strings']['bookmark_add']);
                    }, INVETEX_STORAGE['message_timeout'] / 4);
                });
        } else
            invetex_message_warning(INVETEX_STORAGE['strings']['bookmark_exists'], INVETEX_STORAGE['strings']['bookmark_add']);
        e.preventDefault();
        return false;
    });

    // Delete bookmark
    jQuery('.bookmarks_list').on('click', '.bookmarks_delete', function(e) {
        "use strict";
        var idx = jQuery(this).parent().index();
        var list = jQuery.cookie('invetex_bookmarks');
        if (list) {
            try {
                list = JSON.parse(list);
            } catch (e) {}
            if (list.length) {
                list.splice(idx, 1);
                jQuery.cookie('invetex_bookmarks', JSON.stringify(list), {
                    expires: 365,
                    path: '/'
                });
            }
        }
        jQuery(this).parent().remove();
        e.preventDefault();
        return false;
    });


    // Other settings
    //------------------------------------

    // Scroll to top button
    jQuery('.scroll_to_top').on('click', function(e) {
        "use strict";
        jQuery('html,body').animate({
            scrollTop: 0
        }, 'slow');
        e.preventDefault();
        return false;
    });

    // AJAX views counter
    if (INVETEX_STORAGE['ajax_views_counter'] !== undefined) {
        setTimeout(function() {
			"use strict";
            jQuery.post(INVETEX_STORAGE['ajax_url'], {
                action: 'post_counter',
                nonce: INVETEX_STORAGE['ajax_nonce'],
                post_id: INVETEX_STORAGE['ajax_views_counter']['post_id'],
                views: INVETEX_STORAGE['ajax_views_counter']['post_views']
            });
        }, 10);
    }

    // Show system message
    invetex_show_system_message();

    // Init post format specific scripts
    invetex_init_post_formats();

    // Call sc init action (if exists)
    if (window.invetex_sc_init_actions) invetex_sc_init_actions();

    // Init hidden elements (if exists)
    if (window.invetex_init_hidden_elements) invetex_init_hidden_elements(body.eq(0));

} //end ready




// Scroll actions
//==============================================

// Do actions when page scrolled
function invetex_scroll_actions() {
    "use strict";

    // Call theme specific action (if exists)
    //----------------------------------------------
    if (window.invetex_theme_scroll_actions) invetex_theme_scroll_actions();

    var scroll_offset = jQuery(window).scrollTop();
    var scroll_to_top_button = jQuery('.scroll_to_top');
    var adminbar_height = Math.max(0, jQuery('#wpadminbar').height());

	var body = jQuery('body');
	var top_panel_wrap = jQuery('.top_panel_wrap');
	
    if (INVETEX_STORAGE['top_panel_height'] < 1) {
        INVETEX_STORAGE['top_panel_height'] = Math.max(0, top_panel_wrap.height());
    }

    // Scroll to top button show/hide
    if (scroll_offset > INVETEX_STORAGE['top_panel_height'])
        scroll_to_top_button.addClass('show');
    else
        scroll_to_top_button.removeClass('show');

    // Fix/unfix top panel
    if (!body.hasClass('menu_mobile') && INVETEX_STORAGE['menu_fixed']) {
        var slider_height = 0;
        if (jQuery('.top_panel_below .slider_wrap').length > 0) {
            slider_height = jQuery('.top_panel_below .slider_wrap').height();
            if (slider_height < 10) {
                slider_height = jQuery('.slider_wrap').hasClass('.slider_fullscreen') ? jQuery(window).height() : INVETEX_STORAGE['slider_height'];
            }
        }
        if (scroll_offset <= slider_height + INVETEX_STORAGE['top_panel_height']) {
            if (body.hasClass('top_panel_fixed')) {
                body.removeClass('top_panel_fixed');
            }
        } else if (scroll_offset > slider_height + INVETEX_STORAGE['top_panel_height']) {
            if (!body.hasClass('top_panel_fixed') && jQuery(document).height() > jQuery(window).height() * 1.5) {
                jQuery('.top_panel_fixed_wrap').height(INVETEX_STORAGE['top_panel_height']);
                top_panel_wrap.css('marginTop', '-150px').animate({
                    'marginTop': 0
                }, 500);
                body.addClass('top_panel_fixed');
            }
        }
    }

    // TOC current items
    jQuery('#toc .toc_item').each(function() {
        "use strict";
        var id = jQuery(this).find('a').attr('href');
        var pos = id.indexOf('#');
        if (pos < 0 || id.length == 1) return;
        var loc = window.location.href;
        var pos2 = loc.indexOf('#');
        if (pos2 > 0) loc = loc.substring(0, pos2);
        var now = pos == 0;
        if (!now) now = loc == href.substring(0, pos);
        if (!now) return;
        var off = jQuery(id).offset().top;
        var id_next = jQuery(this).next().find('a').attr('href');
        var off_next = id_next ? jQuery(id_next).offset().top : 1000000;
        if (off < scroll_offset + jQuery(window).height() * 0.8 && scroll_offset + INVETEX_STORAGE['top_panel_height'] < off_next)
            jQuery(this).addClass('current');
        else
            jQuery(this).removeClass('current');
    });

    // Infinite pagination
    invetex_infinite_scroll()

    // Parallax scroll
    invetex_parallax_scroll();

    // Call sc scroll actions (if exists)
    if (window.invetex_sc_scroll_actions) invetex_sc_scroll_actions();
}


// Infinite Scroll
function invetex_infinite_scroll() {
    "use strict";
    if (INVETEX_STORAGE['viewmore_busy']) return;
    var infinite = jQuery('#viewmore.pagination_infinite');
    if (infinite.length > 0) {
        var viewmore = infinite.find('#viewmore_link:not(.viewmore_empty)');
        if (viewmore.length > 0) {
            if (jQuery(window).scrollTop() + jQuery(window).height() + 100 >= infinite.offset().top) {
                viewmore.eq(0).trigger('click');
            }
        }
    }
}

// Parallax scroll
function invetex_parallax_scroll() {
    jQuery('.sc_parallax').each(function() {
		"use strict";
        var windowHeight = jQuery(window).height();
        var scrollTops = jQuery(window).scrollTop();
        var offsetPrx = Math.max(jQuery(this).offset().top, windowHeight);
        if (offsetPrx <= scrollTops + windowHeight) {
            var speed = Number(jQuery(this).data('parallax-speed'));
            var xpos = jQuery(this).data('parallax-x-pos');
            var ypos = Math.round((offsetPrx - scrollTops - windowHeight) * speed + (speed < 0 ? windowHeight * speed : 0));
            jQuery(this).find('.sc_parallax_content').css('backgroundPosition', xpos + ' ' + ypos + 'px');
            // Uncomment next line if you want parallax video (else - video position is static)
            jQuery(this).find('div.sc_video_bg').css('top', ypos + 'px');
        }
    });
}




// Resize actions
//==============================================

// Do actions when page scrolled
function invetex_resize_actions() {
    "use strict";

    // Call theme specific action (if exists)
    //----------------------------------------------
    if (window.invetex_theme_resize_actions) invetex_theme_resize_actions();

    // Reset stored value
    INVETEX_STORAGE['top_panel_height'] = 0;

    invetex_responsive_menu();
    invetex_vc_row_fullwidth_to_boxed();
    invetex_video_dimensions();
    invetex_resize_video_background();
    invetex_resize_fullscreen_slider();
    invetex_resize_alter_portfolio();
    invetex_stretch_width();

    // Call sc resize actions (if exists)
    if (window.invetex_sc_resize_actions) invetex_sc_resize_actions();
}

// Stretch area to full window width
function invetex_stretch_width() {
	"use strict";
    jQuery('.trx-stretch-width').each(function() {
		"use strict";
        var $el = jQuery(this);
        var $el_full = $el.next('.trx-stretch-width-original');
        var el_margin_left = parseInt($el.css('margin-left'), 10);
        var el_margin_right = parseInt($el.css('margin-right'), 10);
        var offset = 0 - $el_full.offset().left - el_margin_left;
        var width = jQuery(window).width();
        if (!$el.hasClass('inited')) {
            $el.addClass('inited invisible');
            $el.css({
                'position': 'relative',
                'box-sizing': 'border-box'
            });
        }
        $el.css({
            'left': offset,
            'width': jQuery(window).width()
        });
        if (!$el.hasClass('trx-stretch-content')) {
            var padding = Math.max(0, -1 * offset);
            var paddingRight = Math.max(0, width - padding - $el_full.width() + el_margin_left + el_margin_right);
            $el.css({
                'padding-left': padding + 'px',
                'padding-right': paddingRight + 'px'
            });
        }
        $el.removeClass('invisible');
    });
}

// Width vc_row when content boxed
function invetex_vc_row_fullwidth_to_boxed() {
    "use strict";
    if (jQuery('body').hasClass('body_style_boxed')) {
        var width_body = jQuery('body').width();
        var width_content = jQuery('.page_wrap').width();
        var width_content_wrap = jQuery('.page_content_wrap  .content_wrap').width();
        var indent = (width_content - width_content_wrap) / 2;
        if (width_body > width_content) {
            jQuery('.vc_row[data-vc-full-width="true"]').each(function() {
                "use strict";
                var mrg = parseInt(jQuery(this).css('marginLeft'), 10);
                jQuery(this).css({
                    'width': width_content,
                    'left': -indent - mrg,
                    'padding-left': indent + mrg,
                    'padding-right': indent + mrg
                });
                if (jQuery(this).attr('data-vc-stretch-content')) {
                    jQuery(this).css({
                        'padding-left': 0,
                        'padding-right': 0
                    });
                }
            });
        }
    }
}

// Check window size and do responsive menu
function invetex_responsive_menu() {
    "use strict";
	var body = jQuery('body');
	var top_panel_wrap =  jQuery('header.top_panel_wrap');
	var header_mobile = jQuery('.header_mobile');
    if (invetex_is_responsive_need(INVETEX_STORAGE['menu_mobile'])) {
        if (!body.hasClass('menu_mobile')) {
            body.removeClass('top_panel_fixed').addClass('menu_mobile');
            top_panel_wrap.hide();
            header_mobile.show();

            jQuery('header #popup_login').attr('id', 'popup_login_1');
            jQuery('header #popup_registration').attr('id', 'popup_registration_1');
            jQuery('.header_mobile #popup_login_1').attr('id', 'popup_login');
            jQuery('.header_mobile #popup_registration_1').attr('id', 'popup_registration');
        }
    } else {
        if (body.hasClass('menu_mobile')) {
            body.removeClass('menu_mobile');
            top_panel_wrap.show();
            header_mobile.hide();

            jQuery('header #popup_login_1').attr('id', 'popup_login');
            jQuery('header #popup_registration_1').attr('id', 'popup_registration');
            jQuery('.header_mobile #popup_login').attr('id', 'popup_login_1');
            jQuery('.header_mobile #popup_registration').attr('id', 'popup_registration_1');
        }
    }

    if (jQuery(window).width() < 640) {
        var pass = jQuery('.header_mobile .popup_wrap.popup_registration .registration_form > .form_right');
        if (pass.length > 0) {
            jQuery('.header_mobile .popup_wrap.popup_registration .form_left .popup_form_field.email_field').after(pass);
        }
    } else {
        var pass = jQuery('.header_mobile .popup_wrap.popup_registration .form_left > .form_right');
        if (pass.length > 0) {
            jQuery('.header_mobile .popup_wrap.popup_registration .registration_form').append(pass);
        }
    }

    if (!jQuery('.top_panel_wrap').hasClass('menu_show')) jQuery('.top_panel_wrap').addClass('menu_show');
	
    var cat_menu = jQuery('body:not(.woocommerce) .widget_area:not(.footer_wrap) .widget_product_categories ul.product-categories');
    var sb = cat_menu.parents('.widget_area');
    if (sb.length > 0 && cat_menu.length > 0) {
        if (sb.width() == sb.parents('.content_wrap').width()) {
            if (cat_menu.hasClass('inited')) {
                cat_menu.removeClass('inited').addClass('plain').superfish('destroy');
                cat_menu.find('ul.animated').removeClass('animated').addClass('no_animated');
            }
        } else {
            if (!cat_menu.hasClass('inited')) {
                cat_menu.removeClass('plain').addClass('inited');
                cat_menu.find('ul.no_animated').removeClass('no_animated').addClass('animated');
                invetex_init_sfmenu('body:not(.woocommerce) .widget_area:not(.footer_wrap) .widget_product_categories ul.product-categories');
            }
        }
    }
}


// Check if responsive menu need
function invetex_is_responsive_need(max_width) {
    "use strict";
    var rez = false;
    if (max_width > 0) {
        var w = window.innerWidth;
        if (w == undefined) {
            w = jQuery(window).width() + (jQuery(window).height() < jQuery(document).height() || jQuery(window).scrollTop() > 0 ? 16 : 0);
        }
        rez = max_width > w;
    }
    return rez;
}


// Fit video frames to document width
function invetex_video_dimensions() {
    jQuery('.sc_video_frame').each(function() {
        "use strict";
        if (jQuery(this).parents('div:hidden,article:hidden').length > 0) return;
        var frame = jQuery(this).eq(0);
        var player = frame.parent();
        var ratio = (frame.data('ratio') ? frame.data('ratio').split(':') : (frame.find('[data-ratio]').length > 0 ? frame.find('[data-ratio]').data('ratio').split(':') : [16, 9]));
        ratio = ratio.length != 2 || ratio[0] == 0 || ratio[1] == 0 ? 16 / 9 : ratio[0] / ratio[1];
        var w_attr = frame.data('width');
        var h_attr = frame.data('height');
        if (!w_attr || !h_attr) return;
        var percent = ('' + w_attr).substr(-1) == '%';
        w_attr = parseInt(w_attr, 10);
        h_attr = parseInt(h_attr, 10);
        var w_real = Math.min(percent || frame.parents('.columns_wrap').length > 0 ? 10000 : w_attr, frame.parents('div,article').width()), //player.width();
            h_real = Math.round(percent ? w_real / ratio : w_real / w_attr * h_attr);
        if (parseInt(frame.attr('data-last-width'), 10) == w_real) return;
        if (percent) {
            frame.height(h_real);
        } else {
            frame.css({
                'width': w_real + 'px',
                'height': h_real + 'px'
            });
        }
        frame.attr('data-last-width', w_real);
    });
    jQuery('video.sc_video,video.wp-video-shortcode').each(function() {
        "use strict";
        if (jQuery(this).parents('div:hidden,article:hidden').length > 0) return;
        var video = jQuery(this).eq(0);
        var ratio = (video.data('ratio') != undefined ? video.data('ratio').split(':') : [16, 9]);
        ratio = ratio.length != 2 || ratio[0] == 0 || ratio[1] == 0 ? 16 / 9 : ratio[0] / ratio[1];
        var mejs_cont = video.parents('.mejs-video');
        var frame = video.parents('.sc_video_frame');
        var w_attr = frame.length > 0 ? frame.data('width') : video.data('width');
        var h_attr = frame.length > 0 ? frame.data('height') : video.data('height');
        if (!w_attr || !h_attr) {
            w_attr = video.attr('width');
            h_attr = video.attr('height');
            if (!w_attr || !h_attr) return;
            video.data({
                'width': w_attr,
                'height': h_attr
            });
        }
        var percent = ('' + w_attr).substr(-1) == '%';
        w_attr = parseInt(w_attr, 10);
        h_attr = parseInt(h_attr, 10);
        var w_real = Math.round(mejs_cont.length > 0 ? Math.min(percent ? 10000 : w_attr, mejs_cont.parents('div,article').width()) : video.width()),
            h_real = Math.round(percent ? w_real / ratio : w_real / w_attr * h_attr);
        if (parseInt(video.attr('data-last-width'), 10) == w_real) return;
        if (mejs_cont.length > 0 && mejs) {
            invetex_set_mejs_player_dimensions(video, w_real, h_real);
        }
        if (percent) {
            video.height(h_real);
        } else {
            video.attr({
                'width': w_real,
                'height': h_real
            }).css({
                'width': w_real + 'px',
                'height': h_real + 'px'
            });
        }
        video.attr('data-last-width', w_real);
    });
    jQuery('video.sc_video_bg').each(function() {
        "use strict";
        if (jQuery(this).parents('div:hidden,article:hidden').length > 0) return;
        var video = jQuery(this).eq(0);
        var ratio = (video.data('ratio') != undefined ? video.data('ratio').split(':') : [16, 9]);
        ratio = ratio.length != 2 || ratio[0] == 0 || ratio[1] == 0 ? 16 / 9 : ratio[0] / ratio[1];
        var mejs_cont = video.parents('.mejs-video');
        var container = mejs_cont.length > 0 ? mejs_cont.parent() : video.parent();
        var w = container.width();
        var h = container.height();
        var w1 = Math.ceil(h * ratio);
        var h1 = Math.ceil(w / ratio);
        if (video.parents('.sc_parallax').length > 0) {
            var windowHeight = jQuery(window).height();
            var speed = Number(video.parents('.sc_parallax').data('parallax-speed'));
            var h_add = Math.ceil(Math.abs((windowHeight - h) * speed));
            if (h1 < h + h_add) {
                h1 = h + h_add;
                w1 = Math.ceil(h1 * ratio);
            }
        }
        if (h1 < h) {
            h1 = h;
            w1 = Math.ceil(h1 * ratio);
        }
        if (w1 < w) {
            w1 = w;
            h1 = Math.ceil(w1 / ratio);
        }
        var l = Math.round((w1 - w) / 2);
        var t = Math.round((h1 - h) / 2);
        if (parseInt(video.attr('data-last-width'), 10) == w1) return;
        if (mejs_cont.length > 0) {
            invetex_set_mejs_player_dimensions(video, w1, h1);
            mejs_cont.css({
                //'left': -l+'px',
                'top': -t + 'px'
            });
        } else
            video.css({
                //'left': -l+'px',
                'top': -t + 'px'
            });
        video.attr({
            'width': w1,
            'height': h1,
            'data-last-width': w1
        }).css({
            'width': w1 + 'px',
            'height': h1 + 'px'
        });
        if (video.css('opacity') == 0) video.animate({
            'opacity': 1
        }, 3000);
    });
    jQuery('iframe').each(function() {
        "use strict";
        if (jQuery(this).parents('div:hidden,article:hidden').length > 0) return;
        var iframe = jQuery(this).eq(0);
        var ratio = (iframe.data('ratio') != undefined ? iframe.data('ratio').split(':') : (iframe.find('[data-ratio]').length > 0 ? iframe.find('[data-ratio]').data('ratio').split(':') : [16, 9]));
        ratio = ratio.length != 2 || ratio[0] == 0 || ratio[1] == 0 ? 16 / 9 : ratio[0] / ratio[1];
        var w_attr = iframe.attr('width');
        var h_attr = iframe.attr('height');
        var frame = iframe.parents('.sc_video_frame');
        if (frame.length > 0) {
            w_attr = frame.data('width');
            h_attr = frame.data('height');
        }
        if (!w_attr || !h_attr) {
            return;
        }
        var percent = ('' + w_attr).substr(-1) == '%';
        w_attr = parseInt(w_attr, 10);
        h_attr = parseInt(h_attr, 10);
        var w_real = frame.length > 0 ? frame.width() : iframe.width(),
            h_real = Math.round(percent ? w_real / ratio : w_real / w_attr * h_attr);
        if (parseInt(iframe.attr('data-last-width'), 10) == w_real) return;
        iframe.css({
            'width': w_real + 'px',
            'height': h_real + 'px'
        });
    });
}

// Resize fullscreen video background
function invetex_resize_video_background() {
    "use strict";
    var bg = jQuery('.video_bg');
    if (bg.length < 1)
        return;
    if (INVETEX_STORAGE['media_elements_enabled'] && bg.find('.mejs-video').length == 0) {
        setTimeout(invetex_resize_video_background, 100);
        return;
    }
    var video = bg.find('video');
    var ratio = (video.data('ratio') != undefined ? video.data('ratio').split(':') : [16, 9]);
    ratio = ratio.length != 2 || ratio[0] == 0 || ratio[1] == 0 ? 16 / 9 : ratio[0] / ratio[1];
    var w = bg.width();
    var h = bg.height();
    var w1 = Math.ceil(h * ratio);
    var h1 = Math.ceil(w / ratio);
    if (h1 < h) {
        h1 = h;
        w1 = Math.ceil(h1 * ratio);
    }
    if (w1 < w) {
        w1 = w;
        h1 = Math.ceil(w1 / ratio);
    }
    var l = Math.round((w1 - w) / 2);
    var t = Math.round((h1 - h) / 2);
    if (bg.find('.mejs-container').length > 0) {
        invetex_set_mejs_player_dimensions(bg.find('video'), w1, h1);
        bg.find('.mejs-container').css({
            'left': -l + 'px',
            'top': -t + 'px'
        });
    } else
        bg.find('video').css({
            'left': -l + 'px',
            'top': -t + 'px'
        });
    bg.find('video').attr({
        'width': w1,
        'height': h1
    }).css({
        'width': w1 + 'px',
        'height': h1 + 'px'
    });
}

// Set Media Elements player dimensions
function invetex_set_mejs_player_dimensions(video, w, h) {
    "use strict";
    if (mejs) {
        for (var pl in mejs.players) {
            if (mejs.players[pl].media.src == video.attr('src')) {
                if (mejs.players[pl].media.setVideoSize) {
                    mejs.players[pl].media.setVideoSize(w, h);
                }
                mejs.players[pl].setPlayerSize(w, h);
                mejs.players[pl].setControlsSize();
            }
        }
    }
}

// Resize Fullscreen Slider
function invetex_resize_fullscreen_slider() {
    "use strict";
    var slider_wrap = jQuery('.slider_wrap.slider_fullscreen');
    if (slider_wrap.length < 1)
        return;
    var slider = slider_wrap.find('.sc_slider_swiper');
    if (slider.length < 1)
        return;
    var h = jQuery(window).height() - jQuery('#wpadminbar').height() - (jQuery('body').hasClass('top_panel_above') && !jQuery('body').hasClass('.top_panel_fixed') ? jQuery('.top_panel_wrap').height() : 0);
    slider.height(h);
}

// Resize Alter portfolio elements
function invetex_resize_alter_portfolio() {
    "use strict";
    var wrap = jQuery('.isotope_wrap.inited');
    if (wrap.length == 0) return;
    wrap.each(function() {
        "use strict";
        var alter = jQuery(this).find('.post_item_alter');
        if (alter.length == 0) return;
        var single = alter.find('.post_featured img[data-alter-items-w="1"]').eq(0);
        if (single.length != 1) return;
        var w_real = single.width();
        var h_real = single.height();
        var space = Number(single.data('alter-item-space'));
        var relayout = false;
        alter.find('.post_featured img').each(function() {
            "use strict";
            var items_w = Number(jQuery(this).data('alter-items-w'));
            var items_h = Number(jQuery(this).data('alter-items-h'));
            if (items_h > 1) {
                jQuery(this).height(Math.round(items_h * h_real + (items_h - 1) * (space + 1)));
                relayout = true;
            } else if (items_w > 1) {
                jQuery(this).height(h_real);
                relayout = true;
            }
        });
        if (relayout) {
            jQuery(this).isotope('layout');
        }
    });
}




// Navigation
//==============================================

// Init Superfish menu
function invetex_init_sfmenu(selector) {
	"use strict";
    jQuery(selector).show().each(function() {
		"use strict";
        if (invetex_is_responsive_need() && (jQuery(this).attr('id') == 'menu_main' || jQuery(this).attr('id') == 'menu_side')) return;
        jQuery(this).addClass('inited').superfish({
            delay: 500,
            animation: {
                opacity: 'show'
            },
            animationOut: {
                opacity: 'hide'
            },
            speed: INVETEX_STORAGE['css_animation'] ? 500 : 200,
            speedOut: INVETEX_STORAGE['css_animation'] ? 500 : 200,
            autoArrows: false,
            dropShadows: false,
            onBeforeShow: function(ul) {
				"use strict";
                if (jQuery(this).parents("ul").length > 1) {
                    var w = jQuery(window).width();
                    var par_offset = jQuery(this).parents("ul").offset().left;
                    var par_width = jQuery(this).parents("ul").outerWidth();
                    var ul_width = jQuery(this).outerWidth();
                    if (par_offset + par_width + ul_width > w - 20 && par_offset - ul_width > 0)
                        jQuery(this).addClass('submenu_left');
                    else
                        jQuery(this).removeClass('submenu_left');
                }
                if (INVETEX_STORAGE['css_animation']) {
                    jQuery(this).removeClass('animated fast ' + INVETEX_STORAGE['menu_animation_out']);
                    jQuery(this).addClass('animated fast ' + INVETEX_STORAGE['menu_animation_in']);
                }
            },
            onBeforeHide: function(ul) {
				"use strict";
                if (INVETEX_STORAGE['css_animation']) {
                    jQuery(this).removeClass('animated fast ' + INVETEX_STORAGE['menu_animation_in']);
                    jQuery(this).addClass('animated fast ' + INVETEX_STORAGE['menu_animation_out']);
                }
            }
        });
    });
}


// Build page TOC from the tag's id
function invetex_build_page_toc() {
    "use strict";
    var toc = '',
        toc_count = 0;
    jQuery('[id^="toc_"],.sc_anchor').each(function(idx) {
        "use strict";
        var obj = jQuery(this);
        var id = obj.attr('id');
        var url = obj.data('url');
        var icon = obj.data('icon');
        if (!icon) icon = 'icon-circle-dot';
        var title = obj.attr('title');
        var description = obj.data('description');
        var separator = obj.data('separator');
        toc_count++;
        toc += '<div class="toc_item' + (separator == 'yes' ? ' toc_separator' : '') + '">' +
            (description ? '<div class="toc_description">' + description + '</div>' : '') +
            '<a href="' + (url ? url : '#' + id) + '" class="toc_icon' + (title ? ' with_title' : '') + ' ' + icon + '">' + (title ? '<span class="toc_title">' + title + '</span>' : '') + '</a>' +
            '</div>';
    });
    if (toc_count > (INVETEX_STORAGE['toc_menu_home'] ? 1 : 0) + (INVETEX_STORAGE['toc_menu_top'] ? 1 : 0)) {
        if (jQuery('#toc').length > 0)
            jQuery('#toc .toc_inner').html(toc);
        else
            jQuery('body').append('<div id="toc" class="toc_' + INVETEX_STORAGE['toc_menu'] + '"><div class="toc_inner">' + toc + '</div></div>');
    }
}


// Show current page title on the responsive menu button
function invetex_show_current_menu_item(menu, button) {
    "use strict";
    menu.find('a').each(function() {
        var menu_link = jQuery(this);
        if (menu_link.text() == "") {
            return;
        }
        if (menu_link.attr('href') == window.location.href)
            button.text(menu_link.text());
    });
}


// Prepare menus (if menu cache is used)
function invetex_prepare_menus() {
    "use strict";
    var menus = [
        jQuery('ul#menu_main'),
        jQuery('ul#menu_user'),
        menu_side,
        jQuery('ul#menu_footer'),
        jQuery('ul#menu_pushy')
    ];
    var href = window.location.href;
    for (var m in menus) {
        if (menus[m].length == 0) continue;
        menus[m].find('li').removeClass('current-menu-ancestor current-menu-parent current-menu-item current_page_item');
        menus[m].find('a[href="' + href + '"]').each(function(idx) {
			"use strict";
            var li = jQuery(this).parent();
            li.addClass('current-menu-item');
            if (li.hasClass('menu-item-object-page')) li.addClass('current_page_item');
            var cnt = 0;
            while ((li = li.parents('li')).length > 0) {
                cnt++;
                li.addClass('current-menu-ancestor' + (cnt == 1 ? ' current-menu-parent' : ''));
            }
        });
    }
}



// Isotope
//=====================================================

// First init isotope containers
function invetex_init_isotope() {
    "use strict";

    var all_images_complete = true;

    // Check if all images in isotope wrapper are loaded
	var isotope_wrap = jQuery('.isotope_wrap:not(.inited)');
    isotope_wrap.each(function() {
        "use strict";
        all_images_complete = all_images_complete && invetex_check_images_complete(jQuery(this));
    });
    // Wait for images loading
    if (!all_images_complete && INVETEX_STORAGE['isotope_init_counter']++ < 30) {
        setTimeout(invetex_init_isotope, 200);
        return;
    }

    // Isotope filters handler
	var viewmore_link = jQuery('#viewmore_link');
    jQuery('.isotope_filters:not(.inited)').addClass('inited').on('click', 'a', function(e) {
        "use strict";
        jQuery(this).parents('.isotope_filters').find('a').removeClass('active');
        jQuery(this).addClass('active');

        var selector = jQuery(this).data('filter');
        jQuery(this).parents('.isotope_filters').siblings('.isotope_wrap').eq(0).isotope({
            filter: selector
        });

        if (selector == '*')
            viewmore_link.fadeIn();
        else
            viewmore_link.fadeOut();

        e.preventDefault();
        return false;
    });

    // Init isotope script
    isotope_wrap.each(function() {
        "use strict";

        var isotope_container = jQuery(this);

        // Init shortcodes
        invetex_sc_init(isotope_container);

        // If in scroll container - no init isotope
        if (isotope_container.parents('.sc_scroll').length > 0) {
            isotope_container.addClass('inited').find('.isotope_item').animate({
                opacity: 1
            }, 200, function() {
                jQuery(this).addClass('isotope_item_show');
            });
            return;
        }

        // Init isotope with timeout
        setTimeout(function() {
			"use strict";
            isotope_container.addClass('inited').isotope({
                itemSelector: '.isotope_item',
                animationOptions: {
                    duration: 750,
                    easing: 'linear',
                    queue: false
                }
            });

            // Show elements
            isotope_container.find('.isotope_item').animate({
                opacity: 1
            }, 200, function() {
				"use strict";
                jQuery(this).addClass('isotope_item_show');
            });

            // Resize Alter portfolio elements
            invetex_resize_alter_portfolio();

        }, 500);

    });
}

function invetex_init_appended_isotope(posts_container, filters) {
    "use strict";

    if (posts_container.parents('.sc_scroll_horizontal').length > 0) return;

    if (!invetex_check_images_complete(posts_container) && INVETEX_STORAGE['isotope_init_counter']++ < 30) {
        setTimeout(function() {
            invetex_init_appended_isotope(posts_container, filters);
        }, 200);
        return;
    }
    // Add filters
    var flt = posts_container.siblings('.isotope_filter');
    for (var i in filters) {
        if (flt.find('a[data-filter=".flt_' + i + '"]').length == 0) {
            flt.append('<a href="#" class="isotope_filters_button" data-filter=".flt_' + i + '">' + filters[i] + '</a>');
        }
    }
    // Init shortcodes in added elements
    invetex_sc_init(posts_container);
    // Get added elements
    var elems = posts_container.find('.isotope_item:not(.isotope_item_show)');
    // Notify isotope about added elements with timeout
    setTimeout(function() {
		"use strict";
        posts_container.isotope('appended', elems);
        // Show appended elements
        elems.animate({
            opacity: 1
        }, 200, function() {
            jQuery(this).addClass('isotope_item_show');
        });
    }, 500);
}



// Post formats init
//=====================================================

function invetex_init_post_formats() {
    "use strict";

    // Call theme specific action (if exists)
    if (window.invetex_theme_init_post_formats) invetex_theme_init_post_formats();

    // MediaElement init
    invetex_init_media_elements(jQuery('body'));

    // Isotope first init
    if (jQuery('.isotope_wrap:not(.inited)').length > 0) {
        INVETEX_STORAGE['isotope_init_counter'] = 0;
        invetex_init_isotope();
    }

    // Hover Effect 'Dir'
    if (jQuery('.isotope_wrap .isotope_item_content.square.effect_dir:not(.inited)').length > 0) {
        jQuery('.isotope_wrap .isotope_item_content.square.effect_dir:not(.inited)').each(function() {
            jQuery(this).addClass('inited').hoverdir();
        });
    }

    // Popup init
    if (INVETEX_STORAGE['popup_engine'] == 'pretty') {
        jQuery("a[href$='jpg'],a[href$='jpeg'],a[href$='png'],a[href$='gif']").attr('rel', 'prettyPhoto[slideshow]');
        var images = jQuery("a[rel*='prettyPhoto']:not(.inited):not(.esgbox):not([data-rel*='pretty']):not([rel*='magnific']):not([data-rel*='magnific'])").addClass('inited');
        try {
            images.prettyPhoto({
                social_tools: '',
                theme: 'facebook',
                deeplinking: false
            });
        } catch (e) {};
    } else if (INVETEX_STORAGE['popup_engine'] == 'magnific') {
        jQuery("a[href$='jpg'],a[href$='jpeg'],a[href$='png'],a[href$='gif']").attr('rel', 'magnific');
        var images = jQuery("a[rel*='magnific']:not(.inited):not(.esgbox):not(.prettyphoto):not([rel*='pretty']):not([data-rel*='pretty'])").addClass('inited');
        try {
            images.magnificPopup({
                type: 'image',
                mainClass: 'mfp-img-mobile',
                closeOnContentClick: true,
                closeBtnInside: true,
                fixedContentPos: true,
                midClick: true,
                //removalDelay: 500, 
                preloader: true,
                tLoading: INVETEX_STORAGE['strings']['magnific_loading'],
                gallery: {
                    enabled: true
                },
                image: {
                    tError: INVETEX_STORAGE['strings']['magnific_error'],
                    verticalFit: true
                }
            });
        } catch (e) {};
    }


    // Add hover icon to products thumbnails
    jQuery(".post_item_product .product .images a.woocommerce-main-image:not(.hover_icon)").addClass('hover_icon hover_icon_view');


    // Likes counter
    if (jQuery('.post_counters_likes:not(.inited)').length > 0) {
        jQuery('.post_counters_likes:not(.inited)')
            .addClass('inited')
            .on('click', function(e) {
				"use strict";
                var button = jQuery(this);
                var inc = button.hasClass('enabled') ? 1 : -1;
                var post_id = button.data('postid');
                var likes = Number(button.data('likes')) + inc;
                var cookie_likes = invetex_get_cookie('invetex_likes');
                if (cookie_likes === undefined || cookie_likes === null) cookie_likes = '';
                jQuery.post(INVETEX_STORAGE['ajax_url'], {
                    action: 'post_counter',
                    nonce: INVETEX_STORAGE['ajax_nonce'],
                    post_id: post_id,
                    likes: likes
                }).done(function(response) {
					"use strict";
                    var rez = {};
                    try {
                        rez = JSON.parse(response);
                    } catch (e) {
                        rez = {
                            error: INVETEX_STORAGE['ajax_error']
                        };
                        console.log(response);
                    }
                    if (rez.error === '') {
                        if (inc == 1) {
                            var title = button.data('title-dislike');
                            button.removeClass('enabled').addClass('disabled');
                            cookie_likes += (cookie_likes.substr(-1) != ',' ? ',' : '') + post_id + ',';
                        } else {
                            var title = button.data('title-like');
                            button.removeClass('disabled').addClass('enabled');
                            cookie_likes = cookie_likes.replace(',' + post_id + ',', ',');
                        }
                        button.data('likes', likes).attr('title', title).find('.post_counters_number').html(likes);
                        invetex_set_cookie('invetex_likes', cookie_likes, 365);
                    } else {
                        invetex_message_warning(INVETEX_STORAGE['strings']['error_like']);
                    }
                });
                e.preventDefault();
                return false;
            });
    }

    // Social share links
    if (jQuery('.sc_socials_share:not(.inited)').length > 0) {
        jQuery('.sc_socials_share:not(.inited)').each(function() {
            "use strict";
            jQuery(this).addClass('inited').on('click', '.social_item_popup > a.social_icons', function(e) {
                "use strict";
                var url = jQuery(this).data('link');
                window.open(url, '_blank', 'scrollbars=0, resizable=1, menubar=0, left=100, top=100, width=480, height=400, toolbar=0, status=0');
                e.preventDefault();
                return false;
            });
        });
    }

    // Add video on thumb click
    if (jQuery('.sc_video_play_button:not(.inited)').length > 0) {
        jQuery('.sc_video_play_button:not(.inited)').each(function() {
            "use strict";
            jQuery(this)
                .addClass('inited')
                .animate({
                    opacity: 1
                }, 1000)
                .on('click', function(e) {
                    "use strict";
                    if (!jQuery(this).hasClass('sc_video_play_button')) return;
                    var video = jQuery(this).removeClass('sc_video_play_button hover_icon hover_icon_play').data('video');
                    if (video !== '') {
                        jQuery(this).empty().html(video);
                        invetex_video_dimensions();
                        var video_tag = jQuery(this).find('video');
                        var w = video_tag.width();
                        var h = video_tag.height();
                        invetex_init_media_elements(jQuery(this));
                        // Restore WxH attributes, because Chrome broke it!
                        jQuery(this).find('video').css({
                            'width': w,
                            'height': h
                        }).attr({
                            'width': w,
                            'height': h
                        });
                    }
                    e.preventDefault();
                    return false;
                });
        });
    }
}


function invetex_init_media_elements(cont) {
	"use strict";
    if (INVETEX_STORAGE['media_elements_enabled'] && cont.find('audio,video').length > 0) {
        if (window.mejs) {
            window.mejs.MepDefaults.enableAutosize = false;
            window.mejs.MediaElementDefaults.enableAutosize = false;
            cont.find('audio:not(.wp-audio-shortcode),video:not(.wp-video-shortcode)').each(function() {
				"use strict";
                if (jQuery(this).parents('.mejs-mediaelement').length == 0) {
                    var media_tag = jQuery(this);
                    var settings = {
                        enableAutosize: true,
                        videoWidth: -1, // if set, overrides <video width>
                        videoHeight: -1, // if set, overrides <video height>
                        audioWidth: '100%', // width of audio player
                        audioHeight: 30, // height of audio player
                        success: function(mejs) {
							"use strict";
                            var autoplay, loop;
                            if ('flash' === mejs.pluginType) {
                                autoplay = mejs.attributes.autoplay && 'false' !== mejs.attributes.autoplay;
                                loop = mejs.attributes.loop && 'false' !== mejs.attributes.loop;
                                autoplay && mejs.addEventListener('canplay', function() {
                                    mejs.play();
                                }, false);
                                loop && mejs.addEventListener('ended', function() {
                                    mejs.play();
                                }, false);
                            }
                            media_tag.parents('.sc_audio,.sc_video').addClass('inited sc_show');
                        }
                    };
                    jQuery(this).mediaelementplayer(settings);
                }
            });
        } else
            setTimeout(function() {
                invetex_init_media_elements(cont);
            }, 400);
    }
}




// Popups and system messages
//==============================================

// Show system message (bubble from previous page)
function invetex_show_system_message() {
	"use strict";
    if (INVETEX_STORAGE['system_message'] && INVETEX_STORAGE['system_message']['message']) {
        if (INVETEX_STORAGE['system_message']['status'] == 'success')
            invetex_message_success(INVETEX_STORAGE['system_message']['message'], INVETEX_STORAGE['system_message']['header']);
        else if (INVETEX_STORAGE['system_message']['status'] == 'info')
            invetex_message_info(INVETEX_STORAGE['system_message']['message'], INVETEX_STORAGE['system_message']['header']);
        else if (INVETEX_STORAGE['system_message']['status'] == 'error' || INVETEX_STORAGE['system_message']['status'] == 'warning')
            invetex_message_warning(INVETEX_STORAGE['system_message']['message'], INVETEX_STORAGE['system_message']['header']);
    }
}

// Toggle popups
function invetex_toggle_popup(popup) {
	"use strict";
    if (popup.css('display') != 'none')
        invetex_hide_popup(popup);
    else
        invetex_show_popup(popup);
}

// Show popups
function invetex_show_popup(popup) {
	"use strict";
    if (popup.css('display') == 'none') {
        if (false && INVETEX_STORAGE['css_animation'])
            popup.show().removeClass('animated fast ' + INVETEX_STORAGE['menu_animation_out']).addClass('animated fast ' + INVETEX_STORAGE['menu_animation_in']);
        else
            popup.fadeIn();
    }
}

// Hide popups
function invetex_hide_popup(popup) {
	"use strict";
    if (popup.css('display') != 'none') {
        if (false && INVETEX_STORAGE['css_animation'])
            popup.removeClass('animated fast ' + INVETEX_STORAGE['menu_animation_in']).addClass('animated fast ' + INVETEX_STORAGE['menu_animation_out']).delay(500).hide();
        else
            popup.fadeOut();
    }
}




// Forms validation
//-------------------------------------------------------


// Comments form
function invetex_comments_validate(form) {
    "use strict";
    form.find('input').removeClass('error_fields_class');
    var rules = {
        error_message_text: INVETEX_STORAGE['strings']['error_global'], // Global error message text (if don't write in checked field)
        error_message_show: true, // Display or not error message
        error_message_time: 4000, // Error message display time
        error_message_class: 'sc_infobox sc_infobox_style_error', // Class appended to error message block
        error_fields_class: 'error_fields_class', // Class appended to error fields
        exit_after_first_error: false, // Cancel validation and exit after first error
        rules: [{
            field: 'comment',
            min_length: {
                value: 1,
                message: INVETEX_STORAGE['strings']['text_empty']
            },
            max_length: {
                value: INVETEX_STORAGE['comments_maxlength'],
                message: INVETEX_STORAGE['strings']['text_long']
            }
        }]
    };
    if (form.find('.comments_author input[aria-required="true"]').length > 0) {
        rules.rules.push({
            field: 'author',
            min_length: {
                value: 1,
                message: INVETEX_STORAGE['strings']['name_empty']
            },
            max_length: {
                value: 60,
                message: INVETEX_STORAGE['strings']['name_long']
            }
        });
    }
    if (form.find('.comments_email input[aria-required="true"]').length > 0) {
        rules.rules.push({
            field: 'email',
            min_length: {
                value: 7,
                message: INVETEX_STORAGE['strings']['email_empty']
            },
            max_length: {
                value: 60,
                message: INVETEX_STORAGE['strings']['email_long']
            },
            mask: {
                value: INVETEX_STORAGE['email_mask'],
                message: INVETEX_STORAGE['strings']['email_not_valid']
            }
        });
    }
    var error = invetex_form_validate(form, rules);
    return !error;
}


// Login form
function invetex_login_validate(form) {
    "use strict";
    form.find('input').removeClass('error_fields_class');
    var error = invetex_form_validate(form, {
        error_message_show: true,
        error_message_time: 4000,
        error_message_class: 'sc_infobox sc_infobox_style_error',
        error_fields_class: 'error_fields_class',
        exit_after_first_error: true,
        rules: [{
            field: "log",
            min_length: {
                value: 1,
                message: INVETEX_STORAGE['strings']['login_empty']
            },
            max_length: {
                value: 60,
                message: INVETEX_STORAGE['strings']['login_long']
            }
        }, {
            field: "pwd",
            min_length: {
                value: 4,
                message: INVETEX_STORAGE['strings']['password_empty']
            },
            max_length: {
                value: 30,
                message: INVETEX_STORAGE['strings']['password_long']
            }
        }]
    });
    if (!error) {
        jQuery.post(INVETEX_STORAGE['ajax_url'], {
            action: 'login_user',
            nonce: INVETEX_STORAGE['ajax_nonce'],
            remember: form.find('#rememberme').val(),
            user_log: form.find('#log').val(),
            user_pwd: form.find('#password').val()
        }).done(function(response) {
			"use strict";
            var rez = {};
            try {
                rez = JSON.parse(response);
            } catch (e) {
                rez = {
                    error: INVETEX_STORAGE['ajax_error']
                };
                console.log(response);
            }
            var result_box = form.find('.result');
            if (result_box.length == 0) result_box = form.siblings('.result');
            if (result_box.length == 0) result_box = form.after('<div class="result"></div>').next('.result');
            result_box.toggleClass('sc_infobox_style_error', false).toggleClass('sc_infobox_style_success', false);
            if (rez.error === '') {
                result_box.addClass('sc_infobox sc_infobox_style_success').html(INVETEX_STORAGE['strings']['login_success']);
                setTimeout(function() {
					"use strict";
                    location.reload();
                }, 3000);
            } else {
                result_box.addClass('sc_infobox sc_infobox_style_error').html(INVETEX_STORAGE['strings']['login_failed'] + '<br>' + rez.error);
            }
            result_box.fadeIn().delay(3000).fadeOut();
        });
    }
    return false;
}


// Registration form 
function invetex_registration_validate(form) {
    "use strict";
    form.find('input').removeClass('error_fields_class');
    var error = invetex_form_validate(form, {
        error_message_show: true,
        error_message_time: 4000,
        error_message_class: "sc_infobox sc_infobox_style_error",
        error_fields_class: "error_fields_class",
        exit_after_first_error: true,
        rules: [{
            field: "registration_agree",
            state: {
                value: 'checked',
                message: INVETEX_STORAGE['strings']['not_agree']
            },
        }, {
            field: "registration_username",
            min_length: {
                value: 1,
                message: INVETEX_STORAGE['strings']['login_empty']
            },
            max_length: {
                value: 60,
                message: INVETEX_STORAGE['strings']['login_long']
            }
        }, {
            field: "registration_email",
            min_length: {
                value: 7,
                message: INVETEX_STORAGE['strings']['email_empty']
            },
            max_length: {
                value: 60,
                message: INVETEX_STORAGE['strings']['email_long']
            },
            mask: {
                value: INVETEX_STORAGE['email_mask'],
                message: INVETEX_STORAGE['strings']['email_not_valid']
            }
        }, {
            field: "registration_pwd",
            min_length: {
                value: 4,
                message: INVETEX_STORAGE['strings']['password_empty']
            },
            max_length: {
                value: 30,
                message: INVETEX_STORAGE['strings']['password_long']
            }
        }, {
            field: "registration_pwd2",
            equal_to: {
                value: 'registration_pwd',
                message: INVETEX_STORAGE['strings']['password_not_equal']
            }
        }]
    });
    if (!error) {
        jQuery.post(INVETEX_STORAGE['ajax_url'], {
            action: 'registration_user',
            nonce: INVETEX_STORAGE['ajax_nonce'],
            user_name: form.find('#registration_username').val(),
            user_email: form.find('#registration_email').val(),
            user_pwd: form.find('#registration_pwd').val()
        }).done(function(response) {
			"use strict";
            var rez = {};
            try {
                rez = JSON.parse(response);
            } catch (e) {
                rez = {
                    error: INVETEX_STORAGE['ajax_error']
                };
                console.log(response);
            }
            var result_box = form.find('.result');
            if (result_box.length == 0) result_box = form.siblings('.result');
            if (result_box.length == 0) result_box = form.after('<div class="result"></div>').next('.result');
            result_box.toggleClass('sc_infobox_style_error', false).toggleClass('sc_infobox_style_success', false);
            if (rez.error === '') {
                result_box.addClass('sc_infobox sc_infobox_style_success').html(INVETEX_STORAGE['strings']['registration_success']);
                setTimeout(function() {
                    jQuery('.popup_login_link').trigger('click');
                }, 3000);
            } else {
                result_box.addClass('sc_infobox sc_infobox_style_error').html(INVETEX_STORAGE['strings']['registration_failed'] + ' ' + rez.error);
            }
            result_box.fadeIn().delay(3000).fadeOut();
        });
    }
    return false;
};/********************************************
	-	THEMEPUNCH TOOLS Ver. 1.0     -
	 Last Update of Tools 27.02.2015
*********************************************/


/*
* @fileOverview TouchSwipe - jQuery Plugin
* @version 1.6.9
*
* @author Matt Bryson http://www.github.com/mattbryson
* @see https://github.com/mattbryson/TouchSwipe-Jquery-Plugin
* @see http://labs.skinkers.com/touchSwipe/
* @see http://plugins.jquery.com/project/touchSwipe
*
* Copyright (c) 2010 Matt Bryson
* Dual licensed under the MIT or GPL Version 2 licenses.
*
*/
(function(a){if(typeof define==="function"&&define.amd&&define.amd.jQuery){define(["jquery"],a)}else{a(jQuery)}}(function(f){var y="1.6.9",p="left",o="right",e="up",x="down",c="in",A="out",m="none",s="auto",l="swipe",t="pinch",B="tap",j="doubletap",b="longtap",z="hold",E="horizontal",u="vertical",i="all",r=10,g="start",k="move",h="end",q="cancel",a="ontouchstart" in window,v=window.navigator.msPointerEnabled&&!window.navigator.pointerEnabled,d=window.navigator.pointerEnabled||window.navigator.msPointerEnabled,C="TouchSwipe";var n={fingers:1,threshold:75,cancelThreshold:null,pinchThreshold:20,maxTimeThreshold:null,fingerReleaseThreshold:250,longTapThreshold:500,doubleTapThreshold:200,swipe:null,swipeLeft:null,swipeRight:null,swipeUp:null,swipeDown:null,swipeStatus:null,pinchIn:null,pinchOut:null,pinchStatus:null,click:null,tap:null,doubleTap:null,longTap:null,hold:null,triggerOnTouchEnd:true,triggerOnTouchLeave:false,allowPageScroll:"auto",fallbackToMouseEvents:true,excludedElements:"label, button, input, select, textarea, a, .noSwipe",preventDefaultEvents:true};f.fn.swipetp=function(H){var G=f(this),F=G.data(C);if(F&&typeof H==="string"){if(F[H]){return F[H].apply(this,Array.prototype.slice.call(arguments,1))}else{f.error("Method "+H+" does not exist on jQuery.swipetp")}}else{if(!F&&(typeof H==="object"||!H)){return w.apply(this,arguments)}}return G};f.fn.swipetp.version=y;f.fn.swipetp.defaults=n;f.fn.swipetp.phases={PHASE_START:g,PHASE_MOVE:k,PHASE_END:h,PHASE_CANCEL:q};f.fn.swipetp.directions={LEFT:p,RIGHT:o,UP:e,DOWN:x,IN:c,OUT:A};f.fn.swipetp.pageScroll={NONE:m,HORIZONTAL:E,VERTICAL:u,AUTO:s};f.fn.swipetp.fingers={ONE:1,TWO:2,THREE:3,ALL:i};function w(F){if(F&&(F.allowPageScroll===undefined&&(F.swipe!==undefined||F.swipeStatus!==undefined))){F.allowPageScroll=m}if(F.click!==undefined&&F.tap===undefined){F.tap=F.click}if(!F){F={}}F=f.extend({},f.fn.swipetp.defaults,F);return this.each(function(){var H=f(this);var G=H.data(C);if(!G){G=new D(this,F);H.data(C,G)}})}function D(a5,aw){var aA=(a||d||!aw.fallbackToMouseEvents),K=aA?(d?(v?"MSPointerDown":"pointerdown"):"touchstart"):"mousedown",az=aA?(d?(v?"MSPointerMove":"pointermove"):"touchmove"):"mousemove",V=aA?(d?(v?"MSPointerUp":"pointerup"):"touchend"):"mouseup",T=aA?null:"mouseleave",aE=(d?(v?"MSPointerCancel":"pointercancel"):"touchcancel");var ah=0,aQ=null,ac=0,a2=0,a0=0,H=1,ar=0,aK=0,N=null;var aS=f(a5);var aa="start";var X=0;var aR=null;var U=0,a3=0,a6=0,ae=0,O=0;var aX=null,ag=null;try{aS.bind(K,aO);aS.bind(aE,ba)}catch(al){f.error("events not supported "+K+","+aE+" on jQuery.swipetp")}this.enable=function(){aS.bind(K,aO);aS.bind(aE,ba);return aS};this.disable=function(){aL();return aS};this.destroy=function(){aL();aS.data(C,null);aS=null};this.option=function(bd,bc){if(aw[bd]!==undefined){if(bc===undefined){return aw[bd]}else{aw[bd]=bc}}else{f.error("Option "+bd+" does not exist on jQuery.swipetp.options")}return null};function aO(be){if(aC()){return}if(f(be.target).closest(aw.excludedElements,aS).length>0){return}var bf=be.originalEvent?be.originalEvent:be;var bd,bg=bf.touches,bc=bg?bg[0]:bf;aa=g;if(bg){X=bg.length}else{be.preventDefault()}ah=0;aQ=null;aK=null;ac=0;a2=0;a0=0;H=1;ar=0;aR=ak();N=ab();S();if(!bg||(X===aw.fingers||aw.fingers===i)||aY()){aj(0,bc);U=au();if(X==2){aj(1,bg[1]);a2=a0=av(aR[0].start,aR[1].start)}if(aw.swipeStatus||aw.pinchStatus){bd=P(bf,aa)}}else{bd=false}if(bd===false){aa=q;P(bf,aa);return bd}else{if(aw.hold){ag=setTimeout(f.proxy(function(){aS.trigger("hold",[bf.target]);if(aw.hold){bd=aw.hold.call(aS,bf,bf.target)}},this),aw.longTapThreshold)}ap(true)}return null}function a4(bf){var bi=bf.originalEvent?bf.originalEvent:bf;if(aa===h||aa===q||an()){return}var be,bj=bi.touches,bd=bj?bj[0]:bi;var bg=aI(bd);a3=au();if(bj){X=bj.length}if(aw.hold){clearTimeout(ag)}aa=k;if(X==2){if(a2==0){aj(1,bj[1]);a2=a0=av(aR[0].start,aR[1].start)}else{aI(bj[1]);a0=av(aR[0].end,aR[1].end);aK=at(aR[0].end,aR[1].end)}H=a8(a2,a0);ar=Math.abs(a2-a0)}if((X===aw.fingers||aw.fingers===i)||!bj||aY()){aQ=aM(bg.start,bg.end);am(bf,aQ);ah=aT(bg.start,bg.end);ac=aN();aJ(aQ,ah);if(aw.swipeStatus||aw.pinchStatus){be=P(bi,aa)}if(!aw.triggerOnTouchEnd||aw.triggerOnTouchLeave){var bc=true;if(aw.triggerOnTouchLeave){var bh=aZ(this);bc=F(bg.end,bh)}if(!aw.triggerOnTouchEnd&&bc){aa=aD(k)}else{if(aw.triggerOnTouchLeave&&!bc){aa=aD(h)}}if(aa==q||aa==h){P(bi,aa)}}}else{aa=q;P(bi,aa)}if(be===false){aa=q;P(bi,aa)}}function M(bc){var bd=bc.originalEvent?bc.originalEvent:bc,be=bd.touches;if(be){if(be.length){G();return true}}if(an()){X=ae}a3=au();ac=aN();if(bb()||!ao()){aa=q;P(bd,aa)}else{if(aw.triggerOnTouchEnd||(aw.triggerOnTouchEnd==false&&aa===k)){bc.preventDefault();aa=h;P(bd,aa)}else{if(!aw.triggerOnTouchEnd&&a7()){aa=h;aG(bd,aa,B)}else{if(aa===k){aa=q;P(bd,aa)}}}}ap(false);return null}function ba(){X=0;a3=0;U=0;a2=0;a0=0;H=1;S();ap(false)}function L(bc){var bd=bc.originalEvent?bc.originalEvent:bc;if(aw.triggerOnTouchLeave){aa=aD(h);P(bd,aa)}}function aL(){aS.unbind(K,aO);aS.unbind(aE,ba);aS.unbind(az,a4);aS.unbind(V,M);if(T){aS.unbind(T,L)}ap(false)}function aD(bg){var bf=bg;var be=aB();var bd=ao();var bc=bb();if(!be||bc){bf=q}else{if(bd&&bg==k&&(!aw.triggerOnTouchEnd||aw.triggerOnTouchLeave)){bf=h}else{if(!bd&&bg==h&&aw.triggerOnTouchLeave){bf=q}}}return bf}function P(be,bc){var bd,bf=be.touches;if((J()||W())||(Q()||aY())){if(J()||W()){bd=aG(be,bc,l)}if((Q()||aY())&&bd!==false){bd=aG(be,bc,t)}}else{if(aH()&&bd!==false){bd=aG(be,bc,j)}else{if(aq()&&bd!==false){bd=aG(be,bc,b)}else{if(ai()&&bd!==false){bd=aG(be,bc,B)}}}}if(bc===q){ba(be)}if(bc===h){if(bf){if(!bf.length){ba(be)}}else{ba(be)}}return bd}function aG(bf,bc,be){var bd;if(be==l){aS.trigger("swipeStatus",[bc,aQ||null,ah||0,ac||0,X,aR]);if(aw.swipeStatus){bd=aw.swipeStatus.call(aS,bf,bc,aQ||null,ah||0,ac||0,X,aR);if(bd===false){return false}}if(bc==h&&aW()){aS.trigger("swipe",[aQ,ah,ac,X,aR]);if(aw.swipe){bd=aw.swipe.call(aS,bf,aQ,ah,ac,X,aR);if(bd===false){return false}}switch(aQ){case p:aS.trigger("swipeLeft",[aQ,ah,ac,X,aR]);if(aw.swipeLeft){bd=aw.swipeLeft.call(aS,bf,aQ,ah,ac,X,aR)}break;case o:aS.trigger("swipeRight",[aQ,ah,ac,X,aR]);if(aw.swipeRight){bd=aw.swipeRight.call(aS,bf,aQ,ah,ac,X,aR)}break;case e:aS.trigger("swipeUp",[aQ,ah,ac,X,aR]);if(aw.swipeUp){bd=aw.swipeUp.call(aS,bf,aQ,ah,ac,X,aR)}break;case x:aS.trigger("swipeDown",[aQ,ah,ac,X,aR]);if(aw.swipeDown){bd=aw.swipeDown.call(aS,bf,aQ,ah,ac,X,aR)}break}}}if(be==t){aS.trigger("pinchStatus",[bc,aK||null,ar||0,ac||0,X,H,aR]);if(aw.pinchStatus){bd=aw.pinchStatus.call(aS,bf,bc,aK||null,ar||0,ac||0,X,H,aR);if(bd===false){return false}}if(bc==h&&a9()){switch(aK){case c:aS.trigger("pinchIn",[aK||null,ar||0,ac||0,X,H,aR]);if(aw.pinchIn){bd=aw.pinchIn.call(aS,bf,aK||null,ar||0,ac||0,X,H,aR)}break;case A:aS.trigger("pinchOut",[aK||null,ar||0,ac||0,X,H,aR]);if(aw.pinchOut){bd=aw.pinchOut.call(aS,bf,aK||null,ar||0,ac||0,X,H,aR)}break}}}if(be==B){if(bc===q||bc===h){clearTimeout(aX);clearTimeout(ag);if(Z()&&!I()){O=au();aX=setTimeout(f.proxy(function(){O=null;aS.trigger("tap",[bf.target]);if(aw.tap){bd=aw.tap.call(aS,bf,bf.target)}},this),aw.doubleTapThreshold)}else{O=null;aS.trigger("tap",[bf.target]);if(aw.tap){bd=aw.tap.call(aS,bf,bf.target)}}}}else{if(be==j){if(bc===q||bc===h){clearTimeout(aX);O=null;aS.trigger("doubletap",[bf.target]);if(aw.doubleTap){bd=aw.doubleTap.call(aS,bf,bf.target)}}}else{if(be==b){if(bc===q||bc===h){clearTimeout(aX);O=null;aS.trigger("longtap",[bf.target]);if(aw.longTap){bd=aw.longTap.call(aS,bf,bf.target)}}}}}return bd}function ao(){var bc=true;if(aw.threshold!==null){bc=ah>=aw.threshold}return bc}function bb(){var bc=false;if(aw.cancelThreshold!==null&&aQ!==null){bc=(aU(aQ)-ah)>=aw.cancelThreshold}return bc}function af(){if(aw.pinchThreshold!==null){return ar>=aw.pinchThreshold}return true}function aB(){var bc;if(aw.maxTimeThreshold){if(ac>=aw.maxTimeThreshold){bc=false}else{bc=true}}else{bc=true}return bc}function am(bc,bd){if(aw.preventDefaultEvents===false){return}if(aw.allowPageScroll===m){bc.preventDefault()}else{var be=aw.allowPageScroll===s;switch(bd){case p:if((aw.swipeLeft&&be)||(!be&&aw.allowPageScroll!=E)){bc.preventDefault()}break;case o:if((aw.swipeRight&&be)||(!be&&aw.allowPageScroll!=E)){bc.preventDefault()}break;case e:if((aw.swipeUp&&be)||(!be&&aw.allowPageScroll!=u)){bc.preventDefault()}break;case x:if((aw.swipeDown&&be)||(!be&&aw.allowPageScroll!=u)){bc.preventDefault()}break}}}function a9(){var bd=aP();var bc=Y();var be=af();return bd&&bc&&be}function aY(){return !!(aw.pinchStatus||aw.pinchIn||aw.pinchOut)}function Q(){return !!(a9()&&aY())}function aW(){var bf=aB();var bh=ao();var be=aP();var bc=Y();var bd=bb();var bg=!bd&&bc&&be&&bh&&bf;return bg}function W(){return !!(aw.swipe||aw.swipeStatus||aw.swipeLeft||aw.swipeRight||aw.swipeUp||aw.swipeDown)}function J(){return !!(aW()&&W())}function aP(){return((X===aw.fingers||aw.fingers===i)||!a)}function Y(){return aR[0].end.x!==0}function a7(){return !!(aw.tap)}function Z(){return !!(aw.doubleTap)}function aV(){return !!(aw.longTap)}function R(){if(O==null){return false}var bc=au();return(Z()&&((bc-O)<=aw.doubleTapThreshold))}function I(){return R()}function ay(){return((X===1||!a)&&(isNaN(ah)||ah<aw.threshold))}function a1(){return((ac>aw.longTapThreshold)&&(ah<r))}function ai(){return !!(ay()&&a7())}function aH(){return !!(R()&&Z())}function aq(){return !!(a1()&&aV())}function G(){a6=au();ae=event.touches.length+1}function S(){a6=0;ae=0}function an(){var bc=false;if(a6){var bd=au()-a6;if(bd<=aw.fingerReleaseThreshold){bc=true}}return bc}function aC(){return !!(aS.data(C+"_intouch")===true)}function ap(bc){if(bc===true){aS.bind(az,a4);aS.bind(V,M);if(T){aS.bind(T,L)}}else{aS.unbind(az,a4,false);aS.unbind(V,M,false);if(T){aS.unbind(T,L,false)}}aS.data(C+"_intouch",bc===true)}function aj(bd,bc){var be=bc.identifier!==undefined?bc.identifier:0;aR[bd].identifier=be;aR[bd].start.x=aR[bd].end.x=bc.pageX||bc.clientX;aR[bd].start.y=aR[bd].end.y=bc.pageY||bc.clientY;return aR[bd]}function aI(bc){var be=bc.identifier!==undefined?bc.identifier:0;var bd=ad(be);bd.end.x=bc.pageX||bc.clientX;bd.end.y=bc.pageY||bc.clientY;return bd}function ad(bd){for(var bc=0;bc<aR.length;bc++){if(aR[bc].identifier==bd){return aR[bc]}}}function ak(){var bc=[];for(var bd=0;bd<=5;bd++){bc.push({start:{x:0,y:0},end:{x:0,y:0},identifier:0})}return bc}function aJ(bc,bd){bd=Math.max(bd,aU(bc));N[bc].distance=bd}function aU(bc){if(N[bc]){return N[bc].distance}return undefined}function ab(){var bc={};bc[p]=ax(p);bc[o]=ax(o);bc[e]=ax(e);bc[x]=ax(x);return bc}function ax(bc){return{direction:bc,distance:0}}function aN(){return a3-U}function av(bf,be){var bd=Math.abs(bf.x-be.x);var bc=Math.abs(bf.y-be.y);return Math.round(Math.sqrt(bd*bd+bc*bc))}function a8(bc,bd){var be=(bd/bc)*1;return be.toFixed(2)}function at(){if(H<1){return A}else{return c}}function aT(bd,bc){return Math.round(Math.sqrt(Math.pow(bc.x-bd.x,2)+Math.pow(bc.y-bd.y,2)))}function aF(bf,bd){var bc=bf.x-bd.x;var bh=bd.y-bf.y;var be=Math.atan2(bh,bc);var bg=Math.round(be*180/Math.PI);if(bg<0){bg=360-Math.abs(bg)}return bg}function aM(bd,bc){var be=aF(bd,bc);if((be<=45)&&(be>=0)){return p}else{if((be<=360)&&(be>=315)){return p}else{if((be>=135)&&(be<=225)){return o}else{if((be>45)&&(be<135)){return x}else{return e}}}}}function au(){var bc=new Date();return bc.getTime()}function aZ(bc){bc=f(bc);var be=bc.offset();var bd={left:be.left,right:be.left+bc.outerWidth(),top:be.top,bottom:be.top+bc.outerHeight()};return bd}function F(bc,bd){return(bc.x>bd.left&&bc.x<bd.right&&bc.y>bd.top&&bc.y<bd.bottom)}}}));

if(typeof(console) === 'undefined') {
    var console = {}
    console.log = console.error = console.info = console.debug = console.warn = console.trace = console.dir = console.dirxml = console.group = console.groupEnd = console.time = console.timeEnd = console.assert = console.profile = console.groupCollapsed = function() {};
}

if (window.tplogs==true)
	try {
		console.groupCollapsed("ThemePunch GreenSocks Logs");
	} catch(e) { }


var oldgs = window.GreenSockGlobals;
	oldgs_queue = window._gsQueue;
	
var punchgs = window.GreenSockGlobals = {};

if (window.tplogs==true)
	try {
		console.info("Build GreenSock SandBox for ThemePunch Plugins");
		console.info("GreenSock TweenLite Engine Initalised by ThemePunch Plugin");
	} catch(e) {}

/*!
 * VERSION: 1.17.0
 * DATE: 2015-05-27
 * UPDATES AND DOCS AT: http://greensock.com
 *
 * @license Copyright (c) 2008-2015, GreenSock. All rights reserved.
 * This work is subject to the terms at http://greensock.com/standard-license or for
 * Club GreenSock members, the software agreement that was issued with your membership.
 * 
 * @author: Jack Doyle, jack@greensock.com
 */
(function(t,e){"use strict";var i=t.GreenSockGlobals=t.GreenSockGlobals||t;if(!i.TweenLite){var s,n,r,a,o,l=function(t){var e,s=t.split("."),n=i;for(e=0;s.length>e;e++)n[s[e]]=n=n[s[e]]||{};return n},h=l("com.greensock"),_=1e-10,u=function(t){var e,i=[],s=t.length;for(e=0;e!==s;i.push(t[e++]));return i},m=function(){},f=function(){var t=Object.prototype.toString,e=t.call([]);return function(i){return null!=i&&(i instanceof Array||"object"==typeof i&&!!i.push&&t.call(i)===e)}}(),c={},p=function(s,n,r,a){this.sc=c[s]?c[s].sc:[],c[s]=this,this.gsClass=null,this.func=r;var o=[];this.check=function(h){for(var _,u,m,f,d=n.length,v=d;--d>-1;)(_=c[n[d]]||new p(n[d],[])).gsClass?(o[d]=_.gsClass,v--):h&&_.sc.push(this);if(0===v&&r)for(u=("com.greensock."+s).split("."),m=u.pop(),f=l(u.join("."))[m]=this.gsClass=r.apply(r,o),a&&(i[m]=f,"function"==typeof define&&define.amd?define((t.GreenSockAMDPath?t.GreenSockAMDPath+"/":"")+s.split(".").pop(),[],function(){return f}):s===e&&"undefined"!=typeof module&&module.exports&&(module.exports=f)),d=0;this.sc.length>d;d++)this.sc[d].check()},this.check(!0)},d=t._gsDefine=function(t,e,i,s){return new p(t,e,i,s)},v=h._class=function(t,e,i){return e=e||function(){},d(t,[],function(){return e},i),e};d.globals=i;var g=[0,0,1,1],T=[],y=v("easing.Ease",function(t,e,i,s){this._func=t,this._type=i||0,this._power=s||0,this._params=e?g.concat(e):g},!0),w=y.map={},P=y.register=function(t,e,i,s){for(var n,r,a,o,l=e.split(","),_=l.length,u=(i||"easeIn,easeOut,easeInOut").split(",");--_>-1;)for(r=l[_],n=s?v("easing."+r,null,!0):h.easing[r]||{},a=u.length;--a>-1;)o=u[a],w[r+"."+o]=w[o+r]=n[o]=t.getRatio?t:t[o]||new t};for(r=y.prototype,r._calcEnd=!1,r.getRatio=function(t){if(this._func)return this._params[0]=t,this._func.apply(null,this._params);var e=this._type,i=this._power,s=1===e?1-t:2===e?t:.5>t?2*t:2*(1-t);return 1===i?s*=s:2===i?s*=s*s:3===i?s*=s*s*s:4===i&&(s*=s*s*s*s),1===e?1-s:2===e?s:.5>t?s/2:1-s/2},s=["Linear","Quad","Cubic","Quart","Quint,Strong"],n=s.length;--n>-1;)r=s[n]+",Power"+n,P(new y(null,null,1,n),r,"easeOut",!0),P(new y(null,null,2,n),r,"easeIn"+(0===n?",easeNone":"")),P(new y(null,null,3,n),r,"easeInOut");w.linear=h.easing.Linear.easeIn,w.swing=h.easing.Quad.easeInOut;var b=v("events.EventDispatcher",function(t){this._listeners={},this._eventTarget=t||this});r=b.prototype,r.addEventListener=function(t,e,i,s,n){n=n||0;var r,l,h=this._listeners[t],_=0;for(null==h&&(this._listeners[t]=h=[]),l=h.length;--l>-1;)r=h[l],r.c===e&&r.s===i?h.splice(l,1):0===_&&n>r.pr&&(_=l+1);h.splice(_,0,{c:e,s:i,up:s,pr:n}),this!==a||o||a.wake()},r.removeEventListener=function(t,e){var i,s=this._listeners[t];if(s)for(i=s.length;--i>-1;)if(s[i].c===e)return s.splice(i,1),void 0},r.dispatchEvent=function(t){var e,i,s,n=this._listeners[t];if(n)for(e=n.length,i=this._eventTarget;--e>-1;)s=n[e],s&&(s.up?s.c.call(s.s||i,{type:t,target:i}):s.c.call(s.s||i))};var k=t.requestAnimationFrame,A=t.cancelAnimationFrame,S=Date.now||function(){return(new Date).getTime()},x=S();for(s=["ms","moz","webkit","o"],n=s.length;--n>-1&&!k;)k=t[s[n]+"RequestAnimationFrame"],A=t[s[n]+"CancelAnimationFrame"]||t[s[n]+"CancelRequestAnimationFrame"];v("Ticker",function(t,e){var i,s,n,r,l,h=this,u=S(),f=e!==!1&&k,c=500,p=33,d="tick",v=function(t){var e,a,o=S()-x;o>c&&(u+=o-p),x+=o,h.time=(x-u)/1e3,e=h.time-l,(!i||e>0||t===!0)&&(h.frame++,l+=e+(e>=r?.004:r-e),a=!0),t!==!0&&(n=s(v)),a&&h.dispatchEvent(d)};b.call(h),h.time=h.frame=0,h.tick=function(){v(!0)},h.lagSmoothing=function(t,e){c=t||1/_,p=Math.min(e,c,0)},h.sleep=function(){null!=n&&(f&&A?A(n):clearTimeout(n),s=m,n=null,h===a&&(o=!1))},h.wake=function(){null!==n?h.sleep():h.frame>10&&(x=S()-c+5),s=0===i?m:f&&k?k:function(t){return setTimeout(t,0|1e3*(l-h.time)+1)},h===a&&(o=!0),v(2)},h.fps=function(t){return arguments.length?(i=t,r=1/(i||60),l=this.time+r,h.wake(),void 0):i},h.useRAF=function(t){return arguments.length?(h.sleep(),f=t,h.fps(i),void 0):f},h.fps(t),setTimeout(function(){f&&5>h.frame&&h.useRAF(!1)},1500)}),r=h.Ticker.prototype=new h.events.EventDispatcher,r.constructor=h.Ticker;var R=v("core.Animation",function(t,e){if(this.vars=e=e||{},this._duration=this._totalDuration=t||0,this._delay=Number(e.delay)||0,this._timeScale=1,this._active=e.immediateRender===!0,this.data=e.data,this._reversed=e.reversed===!0,B){o||a.wake();var i=this.vars.useFrames?q:B;i.add(this,i._time),this.vars.paused&&this.paused(!0)}});a=R.ticker=new h.Ticker,r=R.prototype,r._dirty=r._gc=r._initted=r._paused=!1,r._totalTime=r._time=0,r._rawPrevTime=-1,r._next=r._last=r._onUpdate=r._timeline=r.timeline=null,r._paused=!1;var C=function(){o&&S()-x>2e3&&a.wake(),setTimeout(C,2e3)};C(),r.play=function(t,e){return null!=t&&this.seek(t,e),this.reversed(!1).paused(!1)},r.pause=function(t,e){return null!=t&&this.seek(t,e),this.paused(!0)},r.resume=function(t,e){return null!=t&&this.seek(t,e),this.paused(!1)},r.seek=function(t,e){return this.totalTime(Number(t),e!==!1)},r.restart=function(t,e){return this.reversed(!1).paused(!1).totalTime(t?-this._delay:0,e!==!1,!0)},r.reverse=function(t,e){return null!=t&&this.seek(t||this.totalDuration(),e),this.reversed(!0).paused(!1)},r.render=function(){},r.invalidate=function(){return this._time=this._totalTime=0,this._initted=this._gc=!1,this._rawPrevTime=-1,(this._gc||!this.timeline)&&this._enabled(!0),this},r.isActive=function(){var t,e=this._timeline,i=this._startTime;return!e||!this._gc&&!this._paused&&e.isActive()&&(t=e.rawTime())>=i&&i+this.totalDuration()/this._timeScale>t},r._enabled=function(t,e){return o||a.wake(),this._gc=!t,this._active=this.isActive(),e!==!0&&(t&&!this.timeline?this._timeline.add(this,this._startTime-this._delay):!t&&this.timeline&&this._timeline._remove(this,!0)),!1},r._kill=function(){return this._enabled(!1,!1)},r.kill=function(t,e){return this._kill(t,e),this},r._uncache=function(t){for(var e=t?this:this.timeline;e;)e._dirty=!0,e=e.timeline;return this},r._swapSelfInParams=function(t){for(var e=t.length,i=t.concat();--e>-1;)"{self}"===t[e]&&(i[e]=this);return i},r._callback=function(t){var e=this.vars;e[t].apply(e[t+"Scope"]||e.callbackScope||this,e[t+"Params"]||T)},r.eventCallback=function(t,e,i,s){if("on"===(t||"").substr(0,2)){var n=this.vars;if(1===arguments.length)return n[t];null==e?delete n[t]:(n[t]=e,n[t+"Params"]=f(i)&&-1!==i.join("").indexOf("{self}")?this._swapSelfInParams(i):i,n[t+"Scope"]=s),"onUpdate"===t&&(this._onUpdate=e)}return this},r.delay=function(t){return arguments.length?(this._timeline.smoothChildTiming&&this.startTime(this._startTime+t-this._delay),this._delay=t,this):this._delay},r.duration=function(t){return arguments.length?(this._duration=this._totalDuration=t,this._uncache(!0),this._timeline.smoothChildTiming&&this._time>0&&this._time<this._duration&&0!==t&&this.totalTime(this._totalTime*(t/this._duration),!0),this):(this._dirty=!1,this._duration)},r.totalDuration=function(t){return this._dirty=!1,arguments.length?this.duration(t):this._totalDuration},r.time=function(t,e){return arguments.length?(this._dirty&&this.totalDuration(),this.totalTime(t>this._duration?this._duration:t,e)):this._time},r.totalTime=function(t,e,i){if(o||a.wake(),!arguments.length)return this._totalTime;if(this._timeline){if(0>t&&!i&&(t+=this.totalDuration()),this._timeline.smoothChildTiming){this._dirty&&this.totalDuration();var s=this._totalDuration,n=this._timeline;if(t>s&&!i&&(t=s),this._startTime=(this._paused?this._pauseTime:n._time)-(this._reversed?s-t:t)/this._timeScale,n._dirty||this._uncache(!1),n._timeline)for(;n._timeline;)n._timeline._time!==(n._startTime+n._totalTime)/n._timeScale&&n.totalTime(n._totalTime,!0),n=n._timeline}this._gc&&this._enabled(!0,!1),(this._totalTime!==t||0===this._duration)&&(this.render(t,e,!1),z.length&&$())}return this},r.progress=r.totalProgress=function(t,e){return arguments.length?this.totalTime(this.duration()*t,e):this._time/this.duration()},r.startTime=function(t){return arguments.length?(t!==this._startTime&&(this._startTime=t,this.timeline&&this.timeline._sortChildren&&this.timeline.add(this,t-this._delay)),this):this._startTime},r.endTime=function(t){return this._startTime+(0!=t?this.totalDuration():this.duration())/this._timeScale},r.timeScale=function(t){if(!arguments.length)return this._timeScale;if(t=t||_,this._timeline&&this._timeline.smoothChildTiming){var e=this._pauseTime,i=e||0===e?e:this._timeline.totalTime();this._startTime=i-(i-this._startTime)*this._timeScale/t}return this._timeScale=t,this._uncache(!1)},r.reversed=function(t){return arguments.length?(t!=this._reversed&&(this._reversed=t,this.totalTime(this._timeline&&!this._timeline.smoothChildTiming?this.totalDuration()-this._totalTime:this._totalTime,!0)),this):this._reversed},r.paused=function(t){if(!arguments.length)return this._paused;var e,i,s=this._timeline;return t!=this._paused&&s&&(o||t||a.wake(),e=s.rawTime(),i=e-this._pauseTime,!t&&s.smoothChildTiming&&(this._startTime+=i,this._uncache(!1)),this._pauseTime=t?e:null,this._paused=t,this._active=this.isActive(),!t&&0!==i&&this._initted&&this.duration()&&this.render(s.smoothChildTiming?this._totalTime:(e-this._startTime)/this._timeScale,!0,!0)),this._gc&&!t&&this._enabled(!0,!1),this};var D=v("core.SimpleTimeline",function(t){R.call(this,0,t),this.autoRemoveChildren=this.smoothChildTiming=!0});r=D.prototype=new R,r.constructor=D,r.kill()._gc=!1,r._first=r._last=r._recent=null,r._sortChildren=!1,r.add=r.insert=function(t,e){var i,s;if(t._startTime=Number(e||0)+t._delay,t._paused&&this!==t._timeline&&(t._pauseTime=t._startTime+(this.rawTime()-t._startTime)/t._timeScale),t.timeline&&t.timeline._remove(t,!0),t.timeline=t._timeline=this,t._gc&&t._enabled(!0,!0),i=this._last,this._sortChildren)for(s=t._startTime;i&&i._startTime>s;)i=i._prev;return i?(t._next=i._next,i._next=t):(t._next=this._first,this._first=t),t._next?t._next._prev=t:this._last=t,t._prev=i,this._recent=t,this._timeline&&this._uncache(!0),this},r._remove=function(t,e){return t.timeline===this&&(e||t._enabled(!1,!0),t._prev?t._prev._next=t._next:this._first===t&&(this._first=t._next),t._next?t._next._prev=t._prev:this._last===t&&(this._last=t._prev),t._next=t._prev=t.timeline=null,t===this._recent&&(this._recent=this._last),this._timeline&&this._uncache(!0)),this},r.render=function(t,e,i){var s,n=this._first;for(this._totalTime=this._time=this._rawPrevTime=t;n;)s=n._next,(n._active||t>=n._startTime&&!n._paused)&&(n._reversed?n.render((n._dirty?n.totalDuration():n._totalDuration)-(t-n._startTime)*n._timeScale,e,i):n.render((t-n._startTime)*n._timeScale,e,i)),n=s},r.rawTime=function(){return o||a.wake(),this._totalTime};var I=v("TweenLite",function(e,i,s){if(R.call(this,i,s),this.render=I.prototype.render,null==e)throw"Cannot tween a null target.";this.target=e="string"!=typeof e?e:I.selector(e)||e;var n,r,a,o=e.jquery||e.length&&e!==t&&e[0]&&(e[0]===t||e[0].nodeType&&e[0].style&&!e.nodeType),l=this.vars.overwrite;if(this._overwrite=l=null==l?Q[I.defaultOverwrite]:"number"==typeof l?l>>0:Q[l],(o||e instanceof Array||e.push&&f(e))&&"number"!=typeof e[0])for(this._targets=a=u(e),this._propLookup=[],this._siblings=[],n=0;a.length>n;n++)r=a[n],r?"string"!=typeof r?r.length&&r!==t&&r[0]&&(r[0]===t||r[0].nodeType&&r[0].style&&!r.nodeType)?(a.splice(n--,1),this._targets=a=a.concat(u(r))):(this._siblings[n]=K(r,this,!1),1===l&&this._siblings[n].length>1&&J(r,this,null,1,this._siblings[n])):(r=a[n--]=I.selector(r),"string"==typeof r&&a.splice(n+1,1)):a.splice(n--,1);else this._propLookup={},this._siblings=K(e,this,!1),1===l&&this._siblings.length>1&&J(e,this,null,1,this._siblings);(this.vars.immediateRender||0===i&&0===this._delay&&this.vars.immediateRender!==!1)&&(this._time=-_,this.render(-this._delay))},!0),E=function(e){return e&&e.length&&e!==t&&e[0]&&(e[0]===t||e[0].nodeType&&e[0].style&&!e.nodeType)},O=function(t,e){var i,s={};for(i in t)G[i]||i in e&&"transform"!==i&&"x"!==i&&"y"!==i&&"width"!==i&&"height"!==i&&"className"!==i&&"border"!==i||!(!F[i]||F[i]&&F[i]._autoCSS)||(s[i]=t[i],delete t[i]);t.css=s};r=I.prototype=new R,r.constructor=I,r.kill()._gc=!1,r.ratio=0,r._firstPT=r._targets=r._overwrittenProps=r._startAt=null,r._notifyPluginsOfEnabled=r._lazy=!1,I.version="1.17.0",I.defaultEase=r._ease=new y(null,null,1,1),I.defaultOverwrite="auto",I.ticker=a,I.autoSleep=120,I.lagSmoothing=function(t,e){a.lagSmoothing(t,e)},I.selector=t.$||t.jQuery||function(e){var i=t.$||t.jQuery;return i?(I.selector=i,i(e)):"undefined"==typeof document?e:document.querySelectorAll?document.querySelectorAll(e):document.getElementById("#"===e.charAt(0)?e.substr(1):e)};var z=[],N={},L=I._internals={isArray:f,isSelector:E,lazyTweens:z},F=I._plugins={},U=L.tweenLookup={},j=0,G=L.reservedProps={ease:1,delay:1,overwrite:1,onComplete:1,onCompleteParams:1,onCompleteScope:1,useFrames:1,runBackwards:1,startAt:1,onUpdate:1,onUpdateParams:1,onUpdateScope:1,onStart:1,onStartParams:1,onStartScope:1,onReverseComplete:1,onReverseCompleteParams:1,onReverseCompleteScope:1,onRepeat:1,onRepeatParams:1,onRepeatScope:1,easeParams:1,yoyo:1,immediateRender:1,repeat:1,repeatDelay:1,data:1,paused:1,reversed:1,autoCSS:1,lazy:1,onOverwrite:1,callbackScope:1},Q={none:0,all:1,auto:2,concurrent:3,allOnStart:4,preexisting:5,"true":1,"false":0},q=R._rootFramesTimeline=new D,B=R._rootTimeline=new D,M=30,$=L.lazyRender=function(){var t,e=z.length;for(N={};--e>-1;)t=z[e],t&&t._lazy!==!1&&(t.render(t._lazy[0],t._lazy[1],!0),t._lazy=!1);z.length=0};B._startTime=a.time,q._startTime=a.frame,B._active=q._active=!0,setTimeout($,1),R._updateRoot=I.render=function(){var t,e,i;if(z.length&&$(),B.render((a.time-B._startTime)*B._timeScale,!1,!1),q.render((a.frame-q._startTime)*q._timeScale,!1,!1),z.length&&$(),a.frame>=M){M=a.frame+(parseInt(I.autoSleep,10)||120);for(i in U){for(e=U[i].tweens,t=e.length;--t>-1;)e[t]._gc&&e.splice(t,1);0===e.length&&delete U[i]}if(i=B._first,(!i||i._paused)&&I.autoSleep&&!q._first&&1===a._listeners.tick.length){for(;i&&i._paused;)i=i._next;i||a.sleep()}}},a.addEventListener("tick",R._updateRoot);var K=function(t,e,i){var s,n,r=t._gsTweenID;if(U[r||(t._gsTweenID=r="t"+j++)]||(U[r]={target:t,tweens:[]}),e&&(s=U[r].tweens,s[n=s.length]=e,i))for(;--n>-1;)s[n]===e&&s.splice(n,1);return U[r].tweens},H=function(t,e,i,s){var n,r,a=t.vars.onOverwrite;return a&&(n=a(t,e,i,s)),a=I.onOverwrite,a&&(r=a(t,e,i,s)),n!==!1&&r!==!1},J=function(t,e,i,s,n){var r,a,o,l;if(1===s||s>=4){for(l=n.length,r=0;l>r;r++)if((o=n[r])!==e)o._gc||o._kill(null,t,e)&&(a=!0);else if(5===s)break;return a}var h,u=e._startTime+_,m=[],f=0,c=0===e._duration;for(r=n.length;--r>-1;)(o=n[r])===e||o._gc||o._paused||(o._timeline!==e._timeline?(h=h||V(e,0,c),0===V(o,h,c)&&(m[f++]=o)):u>=o._startTime&&o._startTime+o.totalDuration()/o._timeScale>u&&((c||!o._initted)&&2e-10>=u-o._startTime||(m[f++]=o)));for(r=f;--r>-1;)if(o=m[r],2===s&&o._kill(i,t,e)&&(a=!0),2!==s||!o._firstPT&&o._initted){if(2!==s&&!H(o,e))continue;o._enabled(!1,!1)&&(a=!0)}return a},V=function(t,e,i){for(var s=t._timeline,n=s._timeScale,r=t._startTime;s._timeline;){if(r+=s._startTime,n*=s._timeScale,s._paused)return-100;s=s._timeline}return r/=n,r>e?r-e:i&&r===e||!t._initted&&2*_>r-e?_:(r+=t.totalDuration()/t._timeScale/n)>e+_?0:r-e-_};r._init=function(){var t,e,i,s,n,r=this.vars,a=this._overwrittenProps,o=this._duration,l=!!r.immediateRender,h=r.ease;if(r.startAt){this._startAt&&(this._startAt.render(-1,!0),this._startAt.kill()),n={};for(s in r.startAt)n[s]=r.startAt[s];if(n.overwrite=!1,n.immediateRender=!0,n.lazy=l&&r.lazy!==!1,n.startAt=n.delay=null,this._startAt=I.to(this.target,0,n),l)if(this._time>0)this._startAt=null;else if(0!==o)return}else if(r.runBackwards&&0!==o)if(this._startAt)this._startAt.render(-1,!0),this._startAt.kill(),this._startAt=null;else{0!==this._time&&(l=!1),i={};for(s in r)G[s]&&"autoCSS"!==s||(i[s]=r[s]);if(i.overwrite=0,i.data="isFromStart",i.lazy=l&&r.lazy!==!1,i.immediateRender=l,this._startAt=I.to(this.target,0,i),l){if(0===this._time)return}else this._startAt._init(),this._startAt._enabled(!1),this.vars.immediateRender&&(this._startAt=null)}if(this._ease=h=h?h instanceof y?h:"function"==typeof h?new y(h,r.easeParams):w[h]||I.defaultEase:I.defaultEase,r.easeParams instanceof Array&&h.config&&(this._ease=h.config.apply(h,r.easeParams)),this._easeType=this._ease._type,this._easePower=this._ease._power,this._firstPT=null,this._targets)for(t=this._targets.length;--t>-1;)this._initProps(this._targets[t],this._propLookup[t]={},this._siblings[t],a?a[t]:null)&&(e=!0);else e=this._initProps(this.target,this._propLookup,this._siblings,a);if(e&&I._onPluginEvent("_onInitAllProps",this),a&&(this._firstPT||"function"!=typeof this.target&&this._enabled(!1,!1)),r.runBackwards)for(i=this._firstPT;i;)i.s+=i.c,i.c=-i.c,i=i._next;this._onUpdate=r.onUpdate,this._initted=!0},r._initProps=function(e,i,s,n){var r,a,o,l,h,_;if(null==e)return!1;N[e._gsTweenID]&&$(),this.vars.css||e.style&&e!==t&&e.nodeType&&F.css&&this.vars.autoCSS!==!1&&O(this.vars,e);for(r in this.vars){if(_=this.vars[r],G[r])_&&(_ instanceof Array||_.push&&f(_))&&-1!==_.join("").indexOf("{self}")&&(this.vars[r]=_=this._swapSelfInParams(_,this));else if(F[r]&&(l=new F[r])._onInitTween(e,this.vars[r],this)){for(this._firstPT=h={_next:this._firstPT,t:l,p:"setRatio",s:0,c:1,f:!0,n:r,pg:!0,pr:l._priority},a=l._overwriteProps.length;--a>-1;)i[l._overwriteProps[a]]=this._firstPT;(l._priority||l._onInitAllProps)&&(o=!0),(l._onDisable||l._onEnable)&&(this._notifyPluginsOfEnabled=!0)}else this._firstPT=i[r]=h={_next:this._firstPT,t:e,p:r,f:"function"==typeof e[r],n:r,pg:!1,pr:0},h.s=h.f?e[r.indexOf("set")||"function"!=typeof e["get"+r.substr(3)]?r:"get"+r.substr(3)]():parseFloat(e[r]),h.c="string"==typeof _&&"="===_.charAt(1)?parseInt(_.charAt(0)+"1",10)*Number(_.substr(2)):Number(_)-h.s||0;h&&h._next&&(h._next._prev=h)}return n&&this._kill(n,e)?this._initProps(e,i,s,n):this._overwrite>1&&this._firstPT&&s.length>1&&J(e,this,i,this._overwrite,s)?(this._kill(i,e),this._initProps(e,i,s,n)):(this._firstPT&&(this.vars.lazy!==!1&&this._duration||this.vars.lazy&&!this._duration)&&(N[e._gsTweenID]=!0),o)},r.render=function(t,e,i){var s,n,r,a,o=this._time,l=this._duration,h=this._rawPrevTime;if(t>=l)this._totalTime=this._time=l,this.ratio=this._ease._calcEnd?this._ease.getRatio(1):1,this._reversed||(s=!0,n="onComplete",i=i||this._timeline.autoRemoveChildren),0===l&&(this._initted||!this.vars.lazy||i)&&(this._startTime===this._timeline._duration&&(t=0),(0===t||0>h||h===_&&"isPause"!==this.data)&&h!==t&&(i=!0,h>_&&(n="onReverseComplete")),this._rawPrevTime=a=!e||t||h===t?t:_);else if(1e-7>t)this._totalTime=this._time=0,this.ratio=this._ease._calcEnd?this._ease.getRatio(0):0,(0!==o||0===l&&h>0)&&(n="onReverseComplete",s=this._reversed),0>t&&(this._active=!1,0===l&&(this._initted||!this.vars.lazy||i)&&(h>=0&&(h!==_||"isPause"!==this.data)&&(i=!0),this._rawPrevTime=a=!e||t||h===t?t:_)),this._initted||(i=!0);else if(this._totalTime=this._time=t,this._easeType){var u=t/l,m=this._easeType,f=this._easePower;(1===m||3===m&&u>=.5)&&(u=1-u),3===m&&(u*=2),1===f?u*=u:2===f?u*=u*u:3===f?u*=u*u*u:4===f&&(u*=u*u*u*u),this.ratio=1===m?1-u:2===m?u:.5>t/l?u/2:1-u/2}else this.ratio=this._ease.getRatio(t/l);if(this._time!==o||i){if(!this._initted){if(this._init(),!this._initted||this._gc)return;if(!i&&this._firstPT&&(this.vars.lazy!==!1&&this._duration||this.vars.lazy&&!this._duration))return this._time=this._totalTime=o,this._rawPrevTime=h,z.push(this),this._lazy=[t,e],void 0;this._time&&!s?this.ratio=this._ease.getRatio(this._time/l):s&&this._ease._calcEnd&&(this.ratio=this._ease.getRatio(0===this._time?0:1))}for(this._lazy!==!1&&(this._lazy=!1),this._active||!this._paused&&this._time!==o&&t>=0&&(this._active=!0),0===o&&(this._startAt&&(t>=0?this._startAt.render(t,e,i):n||(n="_dummyGS")),this.vars.onStart&&(0!==this._time||0===l)&&(e||this._callback("onStart"))),r=this._firstPT;r;)r.f?r.t[r.p](r.c*this.ratio+r.s):r.t[r.p]=r.c*this.ratio+r.s,r=r._next;this._onUpdate&&(0>t&&this._startAt&&t!==-1e-4&&this._startAt.render(t,e,i),e||(this._time!==o||s)&&this._callback("onUpdate")),n&&(!this._gc||i)&&(0>t&&this._startAt&&!this._onUpdate&&t!==-1e-4&&this._startAt.render(t,e,i),s&&(this._timeline.autoRemoveChildren&&this._enabled(!1,!1),this._active=!1),!e&&this.vars[n]&&this._callback(n),0===l&&this._rawPrevTime===_&&a!==_&&(this._rawPrevTime=0))}},r._kill=function(t,e,i){if("all"===t&&(t=null),null==t&&(null==e||e===this.target))return this._lazy=!1,this._enabled(!1,!1);e="string"!=typeof e?e||this._targets||this.target:I.selector(e)||e;var s,n,r,a,o,l,h,_,u,m=i&&this._time&&i._startTime===this._startTime&&this._timeline===i._timeline;if((f(e)||E(e))&&"number"!=typeof e[0])for(s=e.length;--s>-1;)this._kill(t,e[s],i)&&(l=!0);else{if(this._targets){for(s=this._targets.length;--s>-1;)if(e===this._targets[s]){o=this._propLookup[s]||{},this._overwrittenProps=this._overwrittenProps||[],n=this._overwrittenProps[s]=t?this._overwrittenProps[s]||{}:"all";break}}else{if(e!==this.target)return!1;o=this._propLookup,n=this._overwrittenProps=t?this._overwrittenProps||{}:"all"}if(o){if(h=t||o,_=t!==n&&"all"!==n&&t!==o&&("object"!=typeof t||!t._tempKill),i&&(I.onOverwrite||this.vars.onOverwrite)){for(r in h)o[r]&&(u||(u=[]),u.push(r));if((u||!t)&&!H(this,i,e,u))return!1}for(r in h)(a=o[r])&&(m&&(a.f?a.t[a.p](a.s):a.t[a.p]=a.s,l=!0),a.pg&&a.t._kill(h)&&(l=!0),a.pg&&0!==a.t._overwriteProps.length||(a._prev?a._prev._next=a._next:a===this._firstPT&&(this._firstPT=a._next),a._next&&(a._next._prev=a._prev),a._next=a._prev=null),delete o[r]),_&&(n[r]=1);!this._firstPT&&this._initted&&this._enabled(!1,!1)}}return l},r.invalidate=function(){return this._notifyPluginsOfEnabled&&I._onPluginEvent("_onDisable",this),this._firstPT=this._overwrittenProps=this._startAt=this._onUpdate=null,this._notifyPluginsOfEnabled=this._active=this._lazy=!1,this._propLookup=this._targets?{}:[],R.prototype.invalidate.call(this),this.vars.immediateRender&&(this._time=-_,this.render(-this._delay)),this},r._enabled=function(t,e){if(o||a.wake(),t&&this._gc){var i,s=this._targets;if(s)for(i=s.length;--i>-1;)this._siblings[i]=K(s[i],this,!0);else this._siblings=K(this.target,this,!0)}return R.prototype._enabled.call(this,t,e),this._notifyPluginsOfEnabled&&this._firstPT?I._onPluginEvent(t?"_onEnable":"_onDisable",this):!1},I.to=function(t,e,i){return new I(t,e,i)},I.from=function(t,e,i){return i.runBackwards=!0,i.immediateRender=0!=i.immediateRender,new I(t,e,i)},I.fromTo=function(t,e,i,s){return s.startAt=i,s.immediateRender=0!=s.immediateRender&&0!=i.immediateRender,new I(t,e,s)},I.delayedCall=function(t,e,i,s,n){return new I(e,0,{delay:t,onComplete:e,onCompleteParams:i,callbackScope:s,onReverseComplete:e,onReverseCompleteParams:i,immediateRender:!1,lazy:!1,useFrames:n,overwrite:0})},I.set=function(t,e){return new I(t,0,e)},I.getTweensOf=function(t,e){if(null==t)return[];t="string"!=typeof t?t:I.selector(t)||t;var i,s,n,r;if((f(t)||E(t))&&"number"!=typeof t[0]){for(i=t.length,s=[];--i>-1;)s=s.concat(I.getTweensOf(t[i],e));for(i=s.length;--i>-1;)for(r=s[i],n=i;--n>-1;)r===s[n]&&s.splice(i,1)}else for(s=K(t).concat(),i=s.length;--i>-1;)(s[i]._gc||e&&!s[i].isActive())&&s.splice(i,1);return s},I.killTweensOf=I.killDelayedCallsTo=function(t,e,i){"object"==typeof e&&(i=e,e=!1);for(var s=I.getTweensOf(t,e),n=s.length;--n>-1;)s[n]._kill(i,t)};var W=v("plugins.TweenPlugin",function(t,e){this._overwriteProps=(t||"").split(","),this._propName=this._overwriteProps[0],this._priority=e||0,this._super=W.prototype},!0);if(r=W.prototype,W.version="1.10.1",W.API=2,r._firstPT=null,r._addTween=function(t,e,i,s,n,r){var a,o;return null!=s&&(a="number"==typeof s||"="!==s.charAt(1)?Number(s)-Number(i):parseInt(s.charAt(0)+"1",10)*Number(s.substr(2)))?(this._firstPT=o={_next:this._firstPT,t:t,p:e,s:i,c:a,f:"function"==typeof t[e],n:n||e,r:r},o._next&&(o._next._prev=o),o):void 0},r.setRatio=function(t){for(var e,i=this._firstPT,s=1e-6;i;)e=i.c*t+i.s,i.r?e=Math.round(e):s>e&&e>-s&&(e=0),i.f?i.t[i.p](e):i.t[i.p]=e,i=i._next},r._kill=function(t){var e,i=this._overwriteProps,s=this._firstPT;if(null!=t[this._propName])this._overwriteProps=[];else for(e=i.length;--e>-1;)null!=t[i[e]]&&i.splice(e,1);for(;s;)null!=t[s.n]&&(s._next&&(s._next._prev=s._prev),s._prev?(s._prev._next=s._next,s._prev=null):this._firstPT===s&&(this._firstPT=s._next)),s=s._next;return!1},r._roundProps=function(t,e){for(var i=this._firstPT;i;)(t[this._propName]||null!=i.n&&t[i.n.split(this._propName+"_").join("")])&&(i.r=e),i=i._next},I._onPluginEvent=function(t,e){var i,s,n,r,a,o=e._firstPT;if("_onInitAllProps"===t){for(;o;){for(a=o._next,s=n;s&&s.pr>o.pr;)s=s._next;(o._prev=s?s._prev:r)?o._prev._next=o:n=o,(o._next=s)?s._prev=o:r=o,o=a}o=e._firstPT=n}for(;o;)o.pg&&"function"==typeof o.t[t]&&o.t[t]()&&(i=!0),o=o._next;return i},W.activate=function(t){for(var e=t.length;--e>-1;)t[e].API===W.API&&(F[(new t[e])._propName]=t[e]);return!0},d.plugin=function(t){if(!(t&&t.propName&&t.init&&t.API))throw"illegal plugin definition.";var e,i=t.propName,s=t.priority||0,n=t.overwriteProps,r={init:"_onInitTween",set:"setRatio",kill:"_kill",round:"_roundProps",initAll:"_onInitAllProps"},a=v("plugins."+i.charAt(0).toUpperCase()+i.substr(1)+"Plugin",function(){W.call(this,i,s),this._overwriteProps=n||[]},t.global===!0),o=a.prototype=new W(i);o.constructor=a,a.API=t.API;for(e in r)"function"==typeof t[e]&&(o[r[e]]=t[e]);return a.version=t.version,W.activate([a]),a},s=t._gsQueue){for(n=0;s.length>n;n++)s[n]();for(r in c)c[r].func||t.console.log("GSAP encountered missing dependency: com.greensock."+r)}o=!1}})("undefined"!=typeof module&&module.exports&&"undefined"!=typeof global?global:this||window,"TweenLite");

/*!
 * VERSION: 1.17.0
 * DATE: 2015-05-27
 * UPDATES AND DOCS AT: http://greensock.com
 *
 * @license Copyright (c) 2008-2015, GreenSock. All rights reserved.
 * This work is subject to the terms at http://greensock.com/standard-license or for
 * Club GreenSock members, the software agreement that was issued with your membership.
 * 
 * @author: Jack Doyle, jack@greensock.com
 */
var _gsScope="undefined"!=typeof module&&module.exports&&"undefined"!=typeof global?global:this||window;(_gsScope._gsQueue||(_gsScope._gsQueue=[])).push(function(){"use strict";_gsScope._gsDefine("TimelineLite",["core.Animation","core.SimpleTimeline","TweenLite"],function(t,e,i){var s=function(t){e.call(this,t),this._labels={},this.autoRemoveChildren=this.vars.autoRemoveChildren===!0,this.smoothChildTiming=this.vars.smoothChildTiming===!0,this._sortChildren=!0,this._onUpdate=this.vars.onUpdate;var i,s,r=this.vars;for(s in r)i=r[s],h(i)&&-1!==i.join("").indexOf("{self}")&&(r[s]=this._swapSelfInParams(i));h(r.tweens)&&this.add(r.tweens,0,r.align,r.stagger)},r=1e-10,n=i._internals,a=s._internals={},o=n.isSelector,h=n.isArray,l=n.lazyTweens,_=n.lazyRender,u=[],f=_gsScope._gsDefine.globals,c=function(t){var e,i={};for(e in t)i[e]=t[e];return i},p=a.pauseCallback=function(t,e,i,s){var n,a=t._timeline,o=a._totalTime,h=t._startTime,l=0>t._rawPrevTime||0===t._rawPrevTime&&a._reversed,_=l?0:r,f=l?r:0;if(e||!this._forcingPlayhead){for(a.pause(h),n=t._prev;n&&n._startTime===h;)n._rawPrevTime=f,n=n._prev;for(n=t._next;n&&n._startTime===h;)n._rawPrevTime=_,n=n._next;e&&e.apply(s||a.vars.callbackScope||a,i||u),(this._forcingPlayhead||!a._paused)&&a.seek(o)}},m=function(t){var e,i=[],s=t.length;for(e=0;e!==s;i.push(t[e++]));return i},d=s.prototype=new e;return s.version="1.17.0",d.constructor=s,d.kill()._gc=d._forcingPlayhead=!1,d.to=function(t,e,s,r){var n=s.repeat&&f.TweenMax||i;return e?this.add(new n(t,e,s),r):this.set(t,s,r)},d.from=function(t,e,s,r){return this.add((s.repeat&&f.TweenMax||i).from(t,e,s),r)},d.fromTo=function(t,e,s,r,n){var a=r.repeat&&f.TweenMax||i;return e?this.add(a.fromTo(t,e,s,r),n):this.set(t,r,n)},d.staggerTo=function(t,e,r,n,a,h,l,_){var u,f=new s({onComplete:h,onCompleteParams:l,callbackScope:_,smoothChildTiming:this.smoothChildTiming});for("string"==typeof t&&(t=i.selector(t)||t),t=t||[],o(t)&&(t=m(t)),n=n||0,0>n&&(t=m(t),t.reverse(),n*=-1),u=0;t.length>u;u++)r.startAt&&(r.startAt=c(r.startAt)),f.to(t[u],e,c(r),u*n);return this.add(f,a)},d.staggerFrom=function(t,e,i,s,r,n,a,o){return i.immediateRender=0!=i.immediateRender,i.runBackwards=!0,this.staggerTo(t,e,i,s,r,n,a,o)},d.staggerFromTo=function(t,e,i,s,r,n,a,o,h){return s.startAt=i,s.immediateRender=0!=s.immediateRender&&0!=i.immediateRender,this.staggerTo(t,e,s,r,n,a,o,h)},d.call=function(t,e,s,r){return this.add(i.delayedCall(0,t,e,s),r)},d.set=function(t,e,s){return s=this._parseTimeOrLabel(s,0,!0),null==e.immediateRender&&(e.immediateRender=s===this._time&&!this._paused),this.add(new i(t,0,e),s)},s.exportRoot=function(t,e){t=t||{},null==t.smoothChildTiming&&(t.smoothChildTiming=!0);var r,n,a=new s(t),o=a._timeline;for(null==e&&(e=!0),o._remove(a,!0),a._startTime=0,a._rawPrevTime=a._time=a._totalTime=o._time,r=o._first;r;)n=r._next,e&&r instanceof i&&r.target===r.vars.onComplete||a.add(r,r._startTime-r._delay),r=n;return o.add(a,0),a},d.add=function(r,n,a,o){var l,_,u,f,c,p;if("number"!=typeof n&&(n=this._parseTimeOrLabel(n,0,!0,r)),!(r instanceof t)){if(r instanceof Array||r&&r.push&&h(r)){for(a=a||"normal",o=o||0,l=n,_=r.length,u=0;_>u;u++)h(f=r[u])&&(f=new s({tweens:f})),this.add(f,l),"string"!=typeof f&&"function"!=typeof f&&("sequence"===a?l=f._startTime+f.totalDuration()/f._timeScale:"start"===a&&(f._startTime-=f.delay())),l+=o;return this._uncache(!0)}if("string"==typeof r)return this.addLabel(r,n);if("function"!=typeof r)throw"Cannot add "+r+" into the timeline; it is not a tween, timeline, function, or string.";r=i.delayedCall(0,r)}if(e.prototype.add.call(this,r,n),(this._gc||this._time===this._duration)&&!this._paused&&this._duration<this.duration())for(c=this,p=c.rawTime()>r._startTime;c._timeline;)p&&c._timeline.smoothChildTiming?c.totalTime(c._totalTime,!0):c._gc&&c._enabled(!0,!1),c=c._timeline;return this},d.remove=function(e){if(e instanceof t)return this._remove(e,!1);if(e instanceof Array||e&&e.push&&h(e)){for(var i=e.length;--i>-1;)this.remove(e[i]);return this}return"string"==typeof e?this.removeLabel(e):this.kill(null,e)},d._remove=function(t,i){e.prototype._remove.call(this,t,i);var s=this._last;return s?this._time>s._startTime+s._totalDuration/s._timeScale&&(this._time=this.duration(),this._totalTime=this._totalDuration):this._time=this._totalTime=this._duration=this._totalDuration=0,this},d.append=function(t,e){return this.add(t,this._parseTimeOrLabel(null,e,!0,t))},d.insert=d.insertMultiple=function(t,e,i,s){return this.add(t,e||0,i,s)},d.appendMultiple=function(t,e,i,s){return this.add(t,this._parseTimeOrLabel(null,e,!0,t),i,s)},d.addLabel=function(t,e){return this._labels[t]=this._parseTimeOrLabel(e),this},d.addPause=function(t,e,s,r){var n=i.delayedCall(0,p,["{self}",e,s,r],this);return n.data="isPause",this.add(n,t)},d.removeLabel=function(t){return delete this._labels[t],this},d.getLabelTime=function(t){return null!=this._labels[t]?this._labels[t]:-1},d._parseTimeOrLabel=function(e,i,s,r){var n;if(r instanceof t&&r.timeline===this)this.remove(r);else if(r&&(r instanceof Array||r.push&&h(r)))for(n=r.length;--n>-1;)r[n]instanceof t&&r[n].timeline===this&&this.remove(r[n]);if("string"==typeof i)return this._parseTimeOrLabel(i,s&&"number"==typeof e&&null==this._labels[i]?e-this.duration():0,s);if(i=i||0,"string"!=typeof e||!isNaN(e)&&null==this._labels[e])null==e&&(e=this.duration());else{if(n=e.indexOf("="),-1===n)return null==this._labels[e]?s?this._labels[e]=this.duration()+i:i:this._labels[e]+i;i=parseInt(e.charAt(n-1)+"1",10)*Number(e.substr(n+1)),e=n>1?this._parseTimeOrLabel(e.substr(0,n-1),0,s):this.duration()}return Number(e)+i},d.seek=function(t,e){return this.totalTime("number"==typeof t?t:this._parseTimeOrLabel(t),e!==!1)},d.stop=function(){return this.paused(!0)},d.gotoAndPlay=function(t,e){return this.play(t,e)},d.gotoAndStop=function(t,e){return this.pause(t,e)},d.render=function(t,e,i){this._gc&&this._enabled(!0,!1);var s,n,a,o,h,u=this._dirty?this.totalDuration():this._totalDuration,f=this._time,c=this._startTime,p=this._timeScale,m=this._paused;if(t>=u)this._totalTime=this._time=u,this._reversed||this._hasPausedChild()||(n=!0,o="onComplete",h=!!this._timeline.autoRemoveChildren,0===this._duration&&(0===t||0>this._rawPrevTime||this._rawPrevTime===r)&&this._rawPrevTime!==t&&this._first&&(h=!0,this._rawPrevTime>r&&(o="onReverseComplete"))),this._rawPrevTime=this._duration||!e||t||this._rawPrevTime===t?t:r,t=u+1e-4;else if(1e-7>t)if(this._totalTime=this._time=0,(0!==f||0===this._duration&&this._rawPrevTime!==r&&(this._rawPrevTime>0||0>t&&this._rawPrevTime>=0))&&(o="onReverseComplete",n=this._reversed),0>t)this._active=!1,this._timeline.autoRemoveChildren&&this._reversed?(h=n=!0,o="onReverseComplete"):this._rawPrevTime>=0&&this._first&&(h=!0),this._rawPrevTime=t;else{if(this._rawPrevTime=this._duration||!e||t||this._rawPrevTime===t?t:r,0===t&&n)for(s=this._first;s&&0===s._startTime;)s._duration||(n=!1),s=s._next;t=0,this._initted||(h=!0)}else this._totalTime=this._time=this._rawPrevTime=t;if(this._time!==f&&this._first||i||h){if(this._initted||(this._initted=!0),this._active||!this._paused&&this._time!==f&&t>0&&(this._active=!0),0===f&&this.vars.onStart&&0!==this._time&&(e||this._callback("onStart")),this._time>=f)for(s=this._first;s&&(a=s._next,!this._paused||m);)(s._active||s._startTime<=this._time&&!s._paused&&!s._gc)&&(s._reversed?s.render((s._dirty?s.totalDuration():s._totalDuration)-(t-s._startTime)*s._timeScale,e,i):s.render((t-s._startTime)*s._timeScale,e,i)),s=a;else for(s=this._last;s&&(a=s._prev,!this._paused||m);)(s._active||f>=s._startTime&&!s._paused&&!s._gc)&&(s._reversed?s.render((s._dirty?s.totalDuration():s._totalDuration)-(t-s._startTime)*s._timeScale,e,i):s.render((t-s._startTime)*s._timeScale,e,i)),s=a;this._onUpdate&&(e||(l.length&&_(),this._callback("onUpdate"))),o&&(this._gc||(c===this._startTime||p!==this._timeScale)&&(0===this._time||u>=this.totalDuration())&&(n&&(l.length&&_(),this._timeline.autoRemoveChildren&&this._enabled(!1,!1),this._active=!1),!e&&this.vars[o]&&this._callback(o)))}},d._hasPausedChild=function(){for(var t=this._first;t;){if(t._paused||t instanceof s&&t._hasPausedChild())return!0;t=t._next}return!1},d.getChildren=function(t,e,s,r){r=r||-9999999999;for(var n=[],a=this._first,o=0;a;)r>a._startTime||(a instanceof i?e!==!1&&(n[o++]=a):(s!==!1&&(n[o++]=a),t!==!1&&(n=n.concat(a.getChildren(!0,e,s)),o=n.length))),a=a._next;return n},d.getTweensOf=function(t,e){var s,r,n=this._gc,a=[],o=0;for(n&&this._enabled(!0,!0),s=i.getTweensOf(t),r=s.length;--r>-1;)(s[r].timeline===this||e&&this._contains(s[r]))&&(a[o++]=s[r]);return n&&this._enabled(!1,!0),a},d.recent=function(){return this._recent},d._contains=function(t){for(var e=t.timeline;e;){if(e===this)return!0;e=e.timeline}return!1},d.shiftChildren=function(t,e,i){i=i||0;for(var s,r=this._first,n=this._labels;r;)r._startTime>=i&&(r._startTime+=t),r=r._next;if(e)for(s in n)n[s]>=i&&(n[s]+=t);return this._uncache(!0)},d._kill=function(t,e){if(!t&&!e)return this._enabled(!1,!1);for(var i=e?this.getTweensOf(e):this.getChildren(!0,!0,!1),s=i.length,r=!1;--s>-1;)i[s]._kill(t,e)&&(r=!0);return r},d.clear=function(t){var e=this.getChildren(!1,!0,!0),i=e.length;for(this._time=this._totalTime=0;--i>-1;)e[i]._enabled(!1,!1);return t!==!1&&(this._labels={}),this._uncache(!0)},d.invalidate=function(){for(var e=this._first;e;)e.invalidate(),e=e._next;return t.prototype.invalidate.call(this)},d._enabled=function(t,i){if(t===this._gc)for(var s=this._first;s;)s._enabled(t,!0),s=s._next;return e.prototype._enabled.call(this,t,i)},d.totalTime=function(){this._forcingPlayhead=!0;var e=t.prototype.totalTime.apply(this,arguments);return this._forcingPlayhead=!1,e},d.duration=function(t){return arguments.length?(0!==this.duration()&&0!==t&&this.timeScale(this._duration/t),this):(this._dirty&&this.totalDuration(),this._duration)},d.totalDuration=function(t){if(!arguments.length){if(this._dirty){for(var e,i,s=0,r=this._last,n=999999999999;r;)e=r._prev,r._dirty&&r.totalDuration(),r._startTime>n&&this._sortChildren&&!r._paused?this.add(r,r._startTime-r._delay):n=r._startTime,0>r._startTime&&!r._paused&&(s-=r._startTime,this._timeline.smoothChildTiming&&(this._startTime+=r._startTime/this._timeScale),this.shiftChildren(-r._startTime,!1,-9999999999),n=0),i=r._startTime+r._totalDuration/r._timeScale,i>s&&(s=i),r=e;this._duration=this._totalDuration=s,this._dirty=!1}return this._totalDuration}return 0!==this.totalDuration()&&0!==t&&this.timeScale(this._totalDuration/t),this},d.paused=function(e){if(!e)for(var i=this._first,s=this._time;i;)i._startTime===s&&"isPause"===i.data&&(i._rawPrevTime=0),i=i._next;return t.prototype.paused.apply(this,arguments)},d.usesFrames=function(){for(var e=this._timeline;e._timeline;)e=e._timeline;return e===t._rootFramesTimeline},d.rawTime=function(){return this._paused?this._totalTime:(this._timeline.rawTime()-this._startTime)*this._timeScale},s},!0)}),_gsScope._gsDefine&&_gsScope._gsQueue.pop()(),function(t){"use strict";var e=function(){return(_gsScope.GreenSockGlobals||_gsScope)[t]};"function"==typeof define&&define.amd?define(["TweenLite"],e):"undefined"!=typeof module&&module.exports&&(require("./TweenLite.js"),module.exports=e())}("TimelineLite");


/*!
 * VERSION: beta 1.15.2
 * DATE: 2015-01-27
 * UPDATES AND DOCS AT: http://greensock.com
 *
 * @license Copyright (c) 2008-2015, GreenSock. All rights reserved.
 * This work is subject to the terms at http://greensock.com/standard-license or for
 * Club GreenSock members, the software agreement that was issued with your membership.
 * 
 * @author: Jack Doyle, jack@greensock.com
 **/
var _gsScope="undefined"!=typeof module&&module.exports&&"undefined"!=typeof global?global:this||window;(_gsScope._gsQueue||(_gsScope._gsQueue=[])).push(function(){"use strict";_gsScope._gsDefine("easing.Back",["easing.Ease"],function(t){var e,i,s,r=_gsScope.GreenSockGlobals||_gsScope,n=r.com.greensock,a=2*Math.PI,o=Math.PI/2,h=n._class,l=function(e,i){var s=h("easing."+e,function(){},!0),r=s.prototype=new t;return r.constructor=s,r.getRatio=i,s},_=t.register||function(){},u=function(t,e,i,s){var r=h("easing."+t,{easeOut:new e,easeIn:new i,easeInOut:new s},!0);return _(r,t),r},c=function(t,e,i){this.t=t,this.v=e,i&&(this.next=i,i.prev=this,this.c=i.v-e,this.gap=i.t-t)},f=function(e,i){var s=h("easing."+e,function(t){this._p1=t||0===t?t:1.70158,this._p2=1.525*this._p1},!0),r=s.prototype=new t;return r.constructor=s,r.getRatio=i,r.config=function(t){return new s(t)},s},p=u("Back",f("BackOut",function(t){return(t-=1)*t*((this._p1+1)*t+this._p1)+1}),f("BackIn",function(t){return t*t*((this._p1+1)*t-this._p1)}),f("BackInOut",function(t){return 1>(t*=2)?.5*t*t*((this._p2+1)*t-this._p2):.5*((t-=2)*t*((this._p2+1)*t+this._p2)+2)})),m=h("easing.SlowMo",function(t,e,i){e=e||0===e?e:.7,null==t?t=.7:t>1&&(t=1),this._p=1!==t?e:0,this._p1=(1-t)/2,this._p2=t,this._p3=this._p1+this._p2,this._calcEnd=i===!0},!0),d=m.prototype=new t;return d.constructor=m,d.getRatio=function(t){var e=t+(.5-t)*this._p;return this._p1>t?this._calcEnd?1-(t=1-t/this._p1)*t:e-(t=1-t/this._p1)*t*t*t*e:t>this._p3?this._calcEnd?1-(t=(t-this._p3)/this._p1)*t:e+(t-e)*(t=(t-this._p3)/this._p1)*t*t*t:this._calcEnd?1:e},m.ease=new m(.7,.7),d.config=m.config=function(t,e,i){return new m(t,e,i)},e=h("easing.SteppedEase",function(t){t=t||1,this._p1=1/t,this._p2=t+1},!0),d=e.prototype=new t,d.constructor=e,d.getRatio=function(t){return 0>t?t=0:t>=1&&(t=.999999999),(this._p2*t>>0)*this._p1},d.config=e.config=function(t){return new e(t)},i=h("easing.RoughEase",function(e){e=e||{};for(var i,s,r,n,a,o,h=e.taper||"none",l=[],_=0,u=0|(e.points||20),f=u,p=e.randomize!==!1,m=e.clamp===!0,d=e.template instanceof t?e.template:null,g="number"==typeof e.strength?.4*e.strength:.4;--f>-1;)i=p?Math.random():1/u*f,s=d?d.getRatio(i):i,"none"===h?r=g:"out"===h?(n=1-i,r=n*n*g):"in"===h?r=i*i*g:.5>i?(n=2*i,r=.5*n*n*g):(n=2*(1-i),r=.5*n*n*g),p?s+=Math.random()*r-.5*r:f%2?s+=.5*r:s-=.5*r,m&&(s>1?s=1:0>s&&(s=0)),l[_++]={x:i,y:s};for(l.sort(function(t,e){return t.x-e.x}),o=new c(1,1,null),f=u;--f>-1;)a=l[f],o=new c(a.x,a.y,o);this._prev=new c(0,0,0!==o.t?o:o.next)},!0),d=i.prototype=new t,d.constructor=i,d.getRatio=function(t){var e=this._prev;if(t>e.t){for(;e.next&&t>=e.t;)e=e.next;e=e.prev}else for(;e.prev&&e.t>=t;)e=e.prev;return this._prev=e,e.v+(t-e.t)/e.gap*e.c},d.config=function(t){return new i(t)},i.ease=new i,u("Bounce",l("BounceOut",function(t){return 1/2.75>t?7.5625*t*t:2/2.75>t?7.5625*(t-=1.5/2.75)*t+.75:2.5/2.75>t?7.5625*(t-=2.25/2.75)*t+.9375:7.5625*(t-=2.625/2.75)*t+.984375}),l("BounceIn",function(t){return 1/2.75>(t=1-t)?1-7.5625*t*t:2/2.75>t?1-(7.5625*(t-=1.5/2.75)*t+.75):2.5/2.75>t?1-(7.5625*(t-=2.25/2.75)*t+.9375):1-(7.5625*(t-=2.625/2.75)*t+.984375)}),l("BounceInOut",function(t){var e=.5>t;return t=e?1-2*t:2*t-1,t=1/2.75>t?7.5625*t*t:2/2.75>t?7.5625*(t-=1.5/2.75)*t+.75:2.5/2.75>t?7.5625*(t-=2.25/2.75)*t+.9375:7.5625*(t-=2.625/2.75)*t+.984375,e?.5*(1-t):.5*t+.5})),u("Circ",l("CircOut",function(t){return Math.sqrt(1-(t-=1)*t)}),l("CircIn",function(t){return-(Math.sqrt(1-t*t)-1)}),l("CircInOut",function(t){return 1>(t*=2)?-.5*(Math.sqrt(1-t*t)-1):.5*(Math.sqrt(1-(t-=2)*t)+1)})),s=function(e,i,s){var r=h("easing."+e,function(t,e){this._p1=t>=1?t:1,this._p2=(e||s)/(1>t?t:1),this._p3=this._p2/a*(Math.asin(1/this._p1)||0),this._p2=a/this._p2},!0),n=r.prototype=new t;return n.constructor=r,n.getRatio=i,n.config=function(t,e){return new r(t,e)},r},u("Elastic",s("ElasticOut",function(t){return this._p1*Math.pow(2,-10*t)*Math.sin((t-this._p3)*this._p2)+1},.3),s("ElasticIn",function(t){return-(this._p1*Math.pow(2,10*(t-=1))*Math.sin((t-this._p3)*this._p2))},.3),s("ElasticInOut",function(t){return 1>(t*=2)?-.5*this._p1*Math.pow(2,10*(t-=1))*Math.sin((t-this._p3)*this._p2):.5*this._p1*Math.pow(2,-10*(t-=1))*Math.sin((t-this._p3)*this._p2)+1},.45)),u("Expo",l("ExpoOut",function(t){return 1-Math.pow(2,-10*t)}),l("ExpoIn",function(t){return Math.pow(2,10*(t-1))-.001}),l("ExpoInOut",function(t){return 1>(t*=2)?.5*Math.pow(2,10*(t-1)):.5*(2-Math.pow(2,-10*(t-1)))})),u("Sine",l("SineOut",function(t){return Math.sin(t*o)}),l("SineIn",function(t){return-Math.cos(t*o)+1}),l("SineInOut",function(t){return-.5*(Math.cos(Math.PI*t)-1)})),h("easing.EaseLookup",{find:function(e){return t.map[e]}},!0),_(r.SlowMo,"SlowMo","ease,"),_(i,"RoughEase","ease,"),_(e,"SteppedEase","ease,"),p},!0)}),_gsScope._gsDefine&&_gsScope._gsQueue.pop()();


/*!
 * VERSION: 1.17.0
 * DATE: 2015-05-27
 * UPDATES AND DOCS AT: http://greensock.com
 *
 * @license Copyright (c) 2008-2015, GreenSock. All rights reserved.
 * This work is subject to the terms at http://greensock.com/standard-license or for
 * Club GreenSock members, the software agreement that was issued with your membership.
 * 
 * @author: Jack Doyle, jack@greensock.com
 */
var _gsScope="undefined"!=typeof module&&module.exports&&"undefined"!=typeof global?global:this||window;(_gsScope._gsQueue||(_gsScope._gsQueue=[])).push(function(){"use strict";_gsScope._gsDefine("plugins.CSSPlugin",["plugins.TweenPlugin","TweenLite"],function(t,e){var i,r,s,n,a=function(){t.call(this,"css"),this._overwriteProps.length=0,this.setRatio=a.prototype.setRatio},o=_gsScope._gsDefine.globals,l={},h=a.prototype=new t("css");h.constructor=a,a.version="1.17.0",a.API=2,a.defaultTransformPerspective=0,a.defaultSkewType="compensated",a.defaultSmoothOrigin=!0,h="px",a.suffixMap={top:h,right:h,bottom:h,left:h,width:h,height:h,fontSize:h,padding:h,margin:h,perspective:h,lineHeight:""};var u,f,c,p,_,d,m=/(?:\d|\-\d|\.\d|\-\.\d)+/g,g=/(?:\d|\-\d|\.\d|\-\.\d|\+=\d|\-=\d|\+=.\d|\-=\.\d)+/g,v=/(?:\+=|\-=|\-|\b)[\d\-\.]+[a-zA-Z0-9]*(?:%|\b)/gi,y=/(?![+-]?\d*\.?\d+|[+-]|e[+-]\d+)[^0-9]/g,x=/(?:\d|\-|\+|=|#|\.)*/g,T=/opacity *= *([^)]*)/i,w=/opacity:([^;]*)/i,b=/alpha\(opacity *=.+?\)/i,P=/^(rgb|hsl)/,S=/([A-Z])/g,O=/-([a-z])/gi,k=/(^(?:url\(\"|url\())|(?:(\"\))$|\)$)/gi,C=function(t,e){return e.toUpperCase()},R=/(?:Left|Right|Width)/i,A=/(M11|M12|M21|M22)=[\d\-\.e]+/gi,M=/progid\:DXImageTransform\.Microsoft\.Matrix\(.+?\)/i,D=/,(?=[^\)]*(?:\(|$))/gi,N=Math.PI/180,L=180/Math.PI,F={},X=document,z=function(t){return X.createElementNS?X.createElementNS("http://www.w3.org/1999/xhtml",t):X.createElement(t)},B=z("div"),E=z("img"),I=a._internals={_specialProps:l},Y=navigator.userAgent,W=function(){var t=Y.indexOf("Android"),e=z("a");return c=-1!==Y.indexOf("Safari")&&-1===Y.indexOf("Chrome")&&(-1===t||Number(Y.substr(t+8,1))>3),_=c&&6>Number(Y.substr(Y.indexOf("Version/")+8,1)),p=-1!==Y.indexOf("Firefox"),(/MSIE ([0-9]{1,}[\.0-9]{0,})/.exec(Y)||/Trident\/.*rv:([0-9]{1,}[\.0-9]{0,})/.exec(Y))&&(d=parseFloat(RegExp.$1)),e?(e.style.cssText="top:1px;opacity:.55;",/^0.55/.test(e.style.opacity)):!1}(),V=function(t){return T.test("string"==typeof t?t:(t.currentStyle?t.currentStyle.filter:t.style.filter)||"")?parseFloat(RegExp.$1)/100:1},j=function(t){window.console&&console.log(t)},G="",U="",q=function(t,e){e=e||B;var i,r,s=e.style;if(void 0!==s[t])return t;for(t=t.charAt(0).toUpperCase()+t.substr(1),i=["O","Moz","ms","Ms","Webkit"],r=5;--r>-1&&void 0===s[i[r]+t];);return r>=0?(U=3===r?"ms":i[r],G="-"+U.toLowerCase()+"-",U+t):null},H=X.defaultView?X.defaultView.getComputedStyle:function(){},Q=a.getStyle=function(t,e,i,r,s){var n;return W||"opacity"!==e?(!r&&t.style[e]?n=t.style[e]:(i=i||H(t))?n=i[e]||i.getPropertyValue(e)||i.getPropertyValue(e.replace(S,"-$1").toLowerCase()):t.currentStyle&&(n=t.currentStyle[e]),null==s||n&&"none"!==n&&"auto"!==n&&"auto auto"!==n?n:s):V(t)},Z=I.convertToPixels=function(t,i,r,s,n){if("px"===s||!s)return r;if("auto"===s||!r)return 0;var o,l,h,u=R.test(i),f=t,c=B.style,p=0>r;if(p&&(r=-r),"%"===s&&-1!==i.indexOf("border"))o=r/100*(u?t.clientWidth:t.clientHeight);else{if(c.cssText="border:0 solid red;position:"+Q(t,"position")+";line-height:0;","%"!==s&&f.appendChild)c[u?"borderLeftWidth":"borderTopWidth"]=r+s;else{if(f=t.parentNode||X.body,l=f._gsCache,h=e.ticker.frame,l&&u&&l.time===h)return l.width*r/100;c[u?"width":"height"]=r+s}f.appendChild(B),o=parseFloat(B[u?"offsetWidth":"offsetHeight"]),f.removeChild(B),u&&"%"===s&&a.cacheWidths!==!1&&(l=f._gsCache=f._gsCache||{},l.time=h,l.width=100*(o/r)),0!==o||n||(o=Z(t,i,r,s,!0))}return p?-o:o},$=I.calculateOffset=function(t,e,i){if("absolute"!==Q(t,"position",i))return 0;var r="left"===e?"Left":"Top",s=Q(t,"margin"+r,i);return t["offset"+r]-(Z(t,e,parseFloat(s),s.replace(x,""))||0)},K=function(t,e){var i,r,s,n={};if(e=e||H(t,null))if(i=e.length)for(;--i>-1;)s=e[i],(-1===s.indexOf("-transform")||Pe===s)&&(n[s.replace(O,C)]=e.getPropertyValue(s));else for(i in e)(-1===i.indexOf("Transform")||be===i)&&(n[i]=e[i]);else if(e=t.currentStyle||t.style)for(i in e)"string"==typeof i&&void 0===n[i]&&(n[i.replace(O,C)]=e[i]);return W||(n.opacity=V(t)),r=Xe(t,e,!1),n.rotation=r.rotation,n.skewX=r.skewX,n.scaleX=r.scaleX,n.scaleY=r.scaleY,n.x=r.x,n.y=r.y,Oe&&(n.z=r.z,n.rotationX=r.rotationX,n.rotationY=r.rotationY,n.scaleZ=r.scaleZ),n.filters&&delete n.filters,n},J=function(t,e,i,r,s){var n,a,o,l={},h=t.style;for(a in i)"cssText"!==a&&"length"!==a&&isNaN(a)&&(e[a]!==(n=i[a])||s&&s[a])&&-1===a.indexOf("Origin")&&("number"==typeof n||"string"==typeof n)&&(l[a]="auto"!==n||"left"!==a&&"top"!==a?""!==n&&"auto"!==n&&"none"!==n||"string"!=typeof e[a]||""===e[a].replace(y,"")?n:0:$(t,a),void 0!==h[a]&&(o=new pe(h,a,h[a],o)));if(r)for(a in r)"className"!==a&&(l[a]=r[a]);return{difs:l,firstMPT:o}},te={width:["Left","Right"],height:["Top","Bottom"]},ee=["marginLeft","marginRight","marginTop","marginBottom"],ie=function(t,e,i){var r=parseFloat("width"===e?t.offsetWidth:t.offsetHeight),s=te[e],n=s.length;for(i=i||H(t,null);--n>-1;)r-=parseFloat(Q(t,"padding"+s[n],i,!0))||0,r-=parseFloat(Q(t,"border"+s[n]+"Width",i,!0))||0;return r},re=function(t,e){(null==t||""===t||"auto"===t||"auto auto"===t)&&(t="0 0");var i=t.split(" "),r=-1!==t.indexOf("left")?"0%":-1!==t.indexOf("right")?"100%":i[0],s=-1!==t.indexOf("top")?"0%":-1!==t.indexOf("bottom")?"100%":i[1];return null==s?s="center"===r?"50%":"0":"center"===s&&(s="50%"),("center"===r||isNaN(parseFloat(r))&&-1===(r+"").indexOf("="))&&(r="50%"),t=r+" "+s+(i.length>2?" "+i[2]:""),e&&(e.oxp=-1!==r.indexOf("%"),e.oyp=-1!==s.indexOf("%"),e.oxr="="===r.charAt(1),e.oyr="="===s.charAt(1),e.ox=parseFloat(r.replace(y,"")),e.oy=parseFloat(s.replace(y,"")),e.v=t),e||t},se=function(t,e){return"string"==typeof t&&"="===t.charAt(1)?parseInt(t.charAt(0)+"1",10)*parseFloat(t.substr(2)):parseFloat(t)-parseFloat(e)},ne=function(t,e){return null==t?e:"string"==typeof t&&"="===t.charAt(1)?parseInt(t.charAt(0)+"1",10)*parseFloat(t.substr(2))+e:parseFloat(t)},ae=function(t,e,i,r){var s,n,a,o,l,h=1e-6;return null==t?o=e:"number"==typeof t?o=t:(s=360,n=t.split("_"),l="="===t.charAt(1),a=(l?parseInt(t.charAt(0)+"1",10)*parseFloat(n[0].substr(2)):parseFloat(n[0]))*(-1===t.indexOf("rad")?1:L)-(l?0:e),n.length&&(r&&(r[i]=e+a),-1!==t.indexOf("short")&&(a%=s,a!==a%(s/2)&&(a=0>a?a+s:a-s)),-1!==t.indexOf("_cw")&&0>a?a=(a+9999999999*s)%s-(0|a/s)*s:-1!==t.indexOf("ccw")&&a>0&&(a=(a-9999999999*s)%s-(0|a/s)*s)),o=e+a),h>o&&o>-h&&(o=0),o},oe={aqua:[0,255,255],lime:[0,255,0],silver:[192,192,192],black:[0,0,0],maroon:[128,0,0],teal:[0,128,128],blue:[0,0,255],navy:[0,0,128],white:[255,255,255],fuchsia:[255,0,255],olive:[128,128,0],yellow:[255,255,0],orange:[255,165,0],gray:[128,128,128],purple:[128,0,128],green:[0,128,0],red:[255,0,0],pink:[255,192,203],cyan:[0,255,255],transparent:[255,255,255,0]},le=function(t,e,i){return t=0>t?t+1:t>1?t-1:t,0|255*(1>6*t?e+6*(i-e)*t:.5>t?i:2>3*t?e+6*(i-e)*(2/3-t):e)+.5},he=a.parseColor=function(t){var e,i,r,s,n,a;return t&&""!==t?"number"==typeof t?[t>>16,255&t>>8,255&t]:(","===t.charAt(t.length-1)&&(t=t.substr(0,t.length-1)),oe[t]?oe[t]:"#"===t.charAt(0)?(4===t.length&&(e=t.charAt(1),i=t.charAt(2),r=t.charAt(3),t="#"+e+e+i+i+r+r),t=parseInt(t.substr(1),16),[t>>16,255&t>>8,255&t]):"hsl"===t.substr(0,3)?(t=t.match(m),s=Number(t[0])%360/360,n=Number(t[1])/100,a=Number(t[2])/100,i=.5>=a?a*(n+1):a+n-a*n,e=2*a-i,t.length>3&&(t[3]=Number(t[3])),t[0]=le(s+1/3,e,i),t[1]=le(s,e,i),t[2]=le(s-1/3,e,i),t):(t=t.match(m)||oe.transparent,t[0]=Number(t[0]),t[1]=Number(t[1]),t[2]=Number(t[2]),t.length>3&&(t[3]=Number(t[3])),t)):oe.black},ue="(?:\\b(?:(?:rgb|rgba|hsl|hsla)\\(.+?\\))|\\B#.+?\\b";for(h in oe)ue+="|"+h+"\\b";ue=RegExp(ue+")","gi");var fe=function(t,e,i,r){if(null==t)return function(t){return t};var s,n=e?(t.match(ue)||[""])[0]:"",a=t.split(n).join("").match(v)||[],o=t.substr(0,t.indexOf(a[0])),l=")"===t.charAt(t.length-1)?")":"",h=-1!==t.indexOf(" ")?" ":",",u=a.length,f=u>0?a[0].replace(m,""):"";return u?s=e?function(t){var e,c,p,_;if("number"==typeof t)t+=f;else if(r&&D.test(t)){for(_=t.replace(D,"|").split("|"),p=0;_.length>p;p++)_[p]=s(_[p]);return _.join(",")}if(e=(t.match(ue)||[n])[0],c=t.split(e).join("").match(v)||[],p=c.length,u>p--)for(;u>++p;)c[p]=i?c[0|(p-1)/2]:a[p];return o+c.join(h)+h+e+l+(-1!==t.indexOf("inset")?" inset":"")}:function(t){var e,n,c;if("number"==typeof t)t+=f;else if(r&&D.test(t)){for(n=t.replace(D,"|").split("|"),c=0;n.length>c;c++)n[c]=s(n[c]);return n.join(",")}if(e=t.match(v)||[],c=e.length,u>c--)for(;u>++c;)e[c]=i?e[0|(c-1)/2]:a[c];return o+e.join(h)+l}:function(t){return t}},ce=function(t){return t=t.split(","),function(e,i,r,s,n,a,o){var l,h=(i+"").split(" ");for(o={},l=0;4>l;l++)o[t[l]]=h[l]=h[l]||h[(l-1)/2>>0];return s.parse(e,o,n,a)}},pe=(I._setPluginRatio=function(t){this.plugin.setRatio(t);for(var e,i,r,s,n=this.data,a=n.proxy,o=n.firstMPT,l=1e-6;o;)e=a[o.v],o.r?e=Math.round(e):l>e&&e>-l&&(e=0),o.t[o.p]=e,o=o._next;if(n.autoRotate&&(n.autoRotate.rotation=a.rotation),1===t)for(o=n.firstMPT;o;){if(i=o.t,i.type){if(1===i.type){for(s=i.xs0+i.s+i.xs1,r=1;i.l>r;r++)s+=i["xn"+r]+i["xs"+(r+1)];i.e=s}}else i.e=i.s+i.xs0;o=o._next}},function(t,e,i,r,s){this.t=t,this.p=e,this.v=i,this.r=s,r&&(r._prev=this,this._next=r)}),_e=(I._parseToProxy=function(t,e,i,r,s,n){var a,o,l,h,u,f=r,c={},p={},_=i._transform,d=F;for(i._transform=null,F=e,r=u=i.parse(t,e,r,s),F=d,n&&(i._transform=_,f&&(f._prev=null,f._prev&&(f._prev._next=null)));r&&r!==f;){if(1>=r.type&&(o=r.p,p[o]=r.s+r.c,c[o]=r.s,n||(h=new pe(r,"s",o,h,r.r),r.c=0),1===r.type))for(a=r.l;--a>0;)l="xn"+a,o=r.p+"_"+l,p[o]=r.data[l],c[o]=r[l],n||(h=new pe(r,l,o,h,r.rxp[l]));r=r._next}return{proxy:c,end:p,firstMPT:h,pt:u}},I.CSSPropTween=function(t,e,r,s,a,o,l,h,u,f,c){this.t=t,this.p=e,this.s=r,this.c=s,this.n=l||e,t instanceof _e||n.push(this.n),this.r=h,this.type=o||0,u&&(this.pr=u,i=!0),this.b=void 0===f?r:f,this.e=void 0===c?r+s:c,a&&(this._next=a,a._prev=this)}),de=function(t,e,i,r,s,n){var a=new _e(t,e,i,r-i,s,-1,n);return a.b=i,a.e=a.xs0=r,a},me=a.parseComplex=function(t,e,i,r,s,n,a,o,l,h){i=i||n||"",a=new _e(t,e,0,0,a,h?2:1,null,!1,o,i,r),r+="";var f,c,p,_,d,v,y,x,T,w,b,S,O=i.split(", ").join(",").split(" "),k=r.split(", ").join(",").split(" "),C=O.length,R=u!==!1;for((-1!==r.indexOf(",")||-1!==i.indexOf(","))&&(O=O.join(" ").replace(D,", ").split(" "),k=k.join(" ").replace(D,", ").split(" "),C=O.length),C!==k.length&&(O=(n||"").split(" "),C=O.length),a.plugin=l,a.setRatio=h,f=0;C>f;f++)if(_=O[f],d=k[f],x=parseFloat(_),x||0===x)a.appendXtra("",x,se(d,x),d.replace(g,""),R&&-1!==d.indexOf("px"),!0);else if(s&&("#"===_.charAt(0)||oe[_]||P.test(_)))S=","===d.charAt(d.length-1)?"),":")",_=he(_),d=he(d),T=_.length+d.length>6,T&&!W&&0===d[3]?(a["xs"+a.l]+=a.l?" transparent":"transparent",a.e=a.e.split(k[f]).join("transparent")):(W||(T=!1),a.appendXtra(T?"rgba(":"rgb(",_[0],d[0]-_[0],",",!0,!0).appendXtra("",_[1],d[1]-_[1],",",!0).appendXtra("",_[2],d[2]-_[2],T?",":S,!0),T&&(_=4>_.length?1:_[3],a.appendXtra("",_,(4>d.length?1:d[3])-_,S,!1)));else if(v=_.match(m)){if(y=d.match(g),!y||y.length!==v.length)return a;for(p=0,c=0;v.length>c;c++)b=v[c],w=_.indexOf(b,p),a.appendXtra(_.substr(p,w-p),Number(b),se(y[c],b),"",R&&"px"===_.substr(w+b.length,2),0===c),p=w+b.length;a["xs"+a.l]+=_.substr(p)}else a["xs"+a.l]+=a.l?" "+_:_;if(-1!==r.indexOf("=")&&a.data){for(S=a.xs0+a.data.s,f=1;a.l>f;f++)S+=a["xs"+f]+a.data["xn"+f];a.e=S+a["xs"+f]}return a.l||(a.type=-1,a.xs0=a.e),a.xfirst||a},ge=9;for(h=_e.prototype,h.l=h.pr=0;--ge>0;)h["xn"+ge]=0,h["xs"+ge]="";h.xs0="",h._next=h._prev=h.xfirst=h.data=h.plugin=h.setRatio=h.rxp=null,h.appendXtra=function(t,e,i,r,s,n){var a=this,o=a.l;return a["xs"+o]+=n&&o?" "+t:t||"",i||0===o||a.plugin?(a.l++,a.type=a.setRatio?2:1,a["xs"+a.l]=r||"",o>0?(a.data["xn"+o]=e+i,a.rxp["xn"+o]=s,a["xn"+o]=e,a.plugin||(a.xfirst=new _e(a,"xn"+o,e,i,a.xfirst||a,0,a.n,s,a.pr),a.xfirst.xs0=0),a):(a.data={s:e+i},a.rxp={},a.s=e,a.c=i,a.r=s,a)):(a["xs"+o]+=e+(r||""),a)};var ve=function(t,e){e=e||{},this.p=e.prefix?q(t)||t:t,l[t]=l[this.p]=this,this.format=e.formatter||fe(e.defaultValue,e.color,e.collapsible,e.multi),e.parser&&(this.parse=e.parser),this.clrs=e.color,this.multi=e.multi,this.keyword=e.keyword,this.dflt=e.defaultValue,this.pr=e.priority||0},ye=I._registerComplexSpecialProp=function(t,e,i){"object"!=typeof e&&(e={parser:i});var r,s,n=t.split(","),a=e.defaultValue;for(i=i||[a],r=0;n.length>r;r++)e.prefix=0===r&&e.prefix,e.defaultValue=i[r]||a,s=new ve(n[r],e)},xe=function(t){if(!l[t]){var e=t.charAt(0).toUpperCase()+t.substr(1)+"Plugin";ye(t,{parser:function(t,i,r,s,n,a,h){var u=o.com.greensock.plugins[e];return u?(u._cssRegister(),l[r].parse(t,i,r,s,n,a,h)):(j("Error: "+e+" js file not loaded."),n)}})}};h=ve.prototype,h.parseComplex=function(t,e,i,r,s,n){var a,o,l,h,u,f,c=this.keyword;if(this.multi&&(D.test(i)||D.test(e)?(o=e.replace(D,"|").split("|"),l=i.replace(D,"|").split("|")):c&&(o=[e],l=[i])),l){for(h=l.length>o.length?l.length:o.length,a=0;h>a;a++)e=o[a]=o[a]||this.dflt,i=l[a]=l[a]||this.dflt,c&&(u=e.indexOf(c),f=i.indexOf(c),u!==f&&(-1===f?o[a]=o[a].split(c).join(""):-1===u&&(o[a]+=" "+c)));e=o.join(", "),i=l.join(", ")}return me(t,this.p,e,i,this.clrs,this.dflt,r,this.pr,s,n)},h.parse=function(t,e,i,r,n,a){return this.parseComplex(t.style,this.format(Q(t,this.p,s,!1,this.dflt)),this.format(e),n,a)},a.registerSpecialProp=function(t,e,i){ye(t,{parser:function(t,r,s,n,a,o){var l=new _e(t,s,0,0,a,2,s,!1,i);return l.plugin=o,l.setRatio=e(t,r,n._tween,s),l},priority:i})},a.useSVGTransformAttr=c||p;var Te,we="scaleX,scaleY,scaleZ,x,y,z,skewX,skewY,rotation,rotationX,rotationY,perspective,xPercent,yPercent".split(","),be=q("transform"),Pe=G+"transform",Se=q("transformOrigin"),Oe=null!==q("perspective"),ke=I.Transform=function(){this.perspective=parseFloat(a.defaultTransformPerspective)||0,this.force3D=a.defaultForce3D!==!1&&Oe?a.defaultForce3D||"auto":!1},Ce=window.SVGElement,Re=function(t,e,i){var r,s=X.createElementNS("http://www.w3.org/2000/svg",t),n=/([a-z])([A-Z])/g;for(r in i)s.setAttributeNS(null,r.replace(n,"$1-$2").toLowerCase(),i[r]);return e.appendChild(s),s},Ae=X.documentElement,Me=function(){var t,e,i,r=d||/Android/i.test(Y)&&!window.chrome;return X.createElementNS&&!r&&(t=Re("svg",Ae),e=Re("rect",t,{width:100,height:50,x:100}),i=e.getBoundingClientRect().width,e.style[Se]="50% 50%",e.style[be]="scaleX(0.5)",r=i===e.getBoundingClientRect().width&&!(p&&Oe),Ae.removeChild(t)),r}(),De=function(t,e,i,r,s){var n,o,l,h,u,f,c,p,_,d,m,g,v,y,x=t._gsTransform,T=Fe(t,!0);x&&(v=x.xOrigin,y=x.yOrigin),(!r||2>(n=r.split(" ")).length)&&(c=t.getBBox(),e=re(e).split(" "),n=[(-1!==e[0].indexOf("%")?parseFloat(e[0])/100*c.width:parseFloat(e[0]))+c.x,(-1!==e[1].indexOf("%")?parseFloat(e[1])/100*c.height:parseFloat(e[1]))+c.y]),i.xOrigin=h=parseFloat(n[0]),i.yOrigin=u=parseFloat(n[1]),r&&T!==Le&&(f=T[0],c=T[1],p=T[2],_=T[3],d=T[4],m=T[5],g=f*_-c*p,o=h*(_/g)+u*(-p/g)+(p*m-_*d)/g,l=h*(-c/g)+u*(f/g)-(f*m-c*d)/g,h=i.xOrigin=n[0]=o,u=i.yOrigin=n[1]=l),x&&(s||s!==!1&&a.defaultSmoothOrigin!==!1?(o=h-v,l=u-y,x.xOffset+=o*T[0]+l*T[2]-o,x.yOffset+=o*T[1]+l*T[3]-l):x.xOffset=x.yOffset=0),t.setAttribute("data-svg-origin",n.join(" "))},Ne=function(t){return!!(Ce&&"function"==typeof t.getBBox&&t.getCTM&&(!t.parentNode||t.parentNode.getBBox&&t.parentNode.getCTM))},Le=[1,0,0,1,0,0],Fe=function(t,e){var i,r,s,n,a,o=t._gsTransform||new ke,l=1e5;if(be?r=Q(t,Pe,null,!0):t.currentStyle&&(r=t.currentStyle.filter.match(A),r=r&&4===r.length?[r[0].substr(4),Number(r[2].substr(4)),Number(r[1].substr(4)),r[3].substr(4),o.x||0,o.y||0].join(","):""),i=!r||"none"===r||"matrix(1, 0, 0, 1, 0, 0)"===r,(o.svg||t.getBBox&&Ne(t))&&(i&&-1!==(t.style[be]+"").indexOf("matrix")&&(r=t.style[be],i=0),s=t.getAttribute("transform"),i&&s&&(-1!==s.indexOf("matrix")?(r=s,i=0):-1!==s.indexOf("translate")&&(r="matrix(1,0,0,1,"+s.match(/(?:\-|\b)[\d\-\.e]+\b/gi).join(",")+")",i=0))),i)return Le;for(s=(r||"").match(/(?:\-|\b)[\d\-\.e]+\b/gi)||[],ge=s.length;--ge>-1;)n=Number(s[ge]),s[ge]=(a=n-(n|=0))?(0|a*l+(0>a?-.5:.5))/l+n:n;return e&&s.length>6?[s[0],s[1],s[4],s[5],s[12],s[13]]:s},Xe=I.getTransform=function(t,i,r,n){if(t._gsTransform&&r&&!n)return t._gsTransform;var o,l,h,u,f,c,p=r?t._gsTransform||new ke:new ke,_=0>p.scaleX,d=2e-5,m=1e5,g=Oe?parseFloat(Q(t,Se,i,!1,"0 0 0").split(" ")[2])||p.zOrigin||0:0,v=parseFloat(a.defaultTransformPerspective)||0;if(p.svg=!(!t.getBBox||!Ne(t)),p.svg&&(De(t,Q(t,Se,s,!1,"50% 50%")+"",p,t.getAttribute("data-svg-origin")),Te=a.useSVGTransformAttr||Me),o=Fe(t),o!==Le){if(16===o.length){var y,x,T,w,b,P=o[0],S=o[1],O=o[2],k=o[3],C=o[4],R=o[5],A=o[6],M=o[7],D=o[8],N=o[9],F=o[10],X=o[12],z=o[13],B=o[14],E=o[11],I=Math.atan2(A,F);p.zOrigin&&(B=-p.zOrigin,X=D*B-o[12],z=N*B-o[13],B=F*B+p.zOrigin-o[14]),p.rotationX=I*L,I&&(w=Math.cos(-I),b=Math.sin(-I),y=C*w+D*b,x=R*w+N*b,T=A*w+F*b,D=C*-b+D*w,N=R*-b+N*w,F=A*-b+F*w,E=M*-b+E*w,C=y,R=x,A=T),I=Math.atan2(D,F),p.rotationY=I*L,I&&(w=Math.cos(-I),b=Math.sin(-I),y=P*w-D*b,x=S*w-N*b,T=O*w-F*b,N=S*b+N*w,F=O*b+F*w,E=k*b+E*w,P=y,S=x,O=T),I=Math.atan2(S,P),p.rotation=I*L,I&&(w=Math.cos(-I),b=Math.sin(-I),P=P*w+C*b,x=S*w+R*b,R=S*-b+R*w,A=O*-b+A*w,S=x),p.rotationX&&Math.abs(p.rotationX)+Math.abs(p.rotation)>359.9&&(p.rotationX=p.rotation=0,p.rotationY+=180),p.scaleX=(0|Math.sqrt(P*P+S*S)*m+.5)/m,p.scaleY=(0|Math.sqrt(R*R+N*N)*m+.5)/m,p.scaleZ=(0|Math.sqrt(A*A+F*F)*m+.5)/m,p.skewX=0,p.perspective=E?1/(0>E?-E:E):0,p.x=X,p.y=z,p.z=B,p.svg&&(p.x-=p.xOrigin-(p.xOrigin*P-p.yOrigin*C),p.y-=p.yOrigin-(p.yOrigin*S-p.xOrigin*R))}else if(!(Oe&&!n&&o.length&&p.x===o[4]&&p.y===o[5]&&(p.rotationX||p.rotationY)||void 0!==p.x&&"none"===Q(t,"display",i))){var Y=o.length>=6,W=Y?o[0]:1,V=o[1]||0,j=o[2]||0,G=Y?o[3]:1;p.x=o[4]||0,p.y=o[5]||0,h=Math.sqrt(W*W+V*V),u=Math.sqrt(G*G+j*j),f=W||V?Math.atan2(V,W)*L:p.rotation||0,c=j||G?Math.atan2(j,G)*L+f:p.skewX||0,Math.abs(c)>90&&270>Math.abs(c)&&(_?(h*=-1,c+=0>=f?180:-180,f+=0>=f?180:-180):(u*=-1,c+=0>=c?180:-180)),p.scaleX=h,p.scaleY=u,p.rotation=f,p.skewX=c,Oe&&(p.rotationX=p.rotationY=p.z=0,p.perspective=v,p.scaleZ=1),p.svg&&(p.x-=p.xOrigin-(p.xOrigin*W+p.yOrigin*j),p.y-=p.yOrigin-(p.xOrigin*V+p.yOrigin*G))}p.zOrigin=g;for(l in p)d>p[l]&&p[l]>-d&&(p[l]=0)}return r&&(t._gsTransform=p,p.svg&&(Te&&t.style[be]?e.delayedCall(.001,function(){Ie(t.style,be)}):!Te&&t.getAttribute("transform")&&e.delayedCall(.001,function(){t.removeAttribute("transform")}))),p},ze=function(t){var e,i,r=this.data,s=-r.rotation*N,n=s+r.skewX*N,a=1e5,o=(0|Math.cos(s)*r.scaleX*a)/a,l=(0|Math.sin(s)*r.scaleX*a)/a,h=(0|Math.sin(n)*-r.scaleY*a)/a,u=(0|Math.cos(n)*r.scaleY*a)/a,f=this.t.style,c=this.t.currentStyle;if(c){i=l,l=-h,h=-i,e=c.filter,f.filter="";var p,_,m=this.t.offsetWidth,g=this.t.offsetHeight,v="absolute"!==c.position,y="progid:DXImageTransform.Microsoft.Matrix(M11="+o+", M12="+l+", M21="+h+", M22="+u,w=r.x+m*r.xPercent/100,b=r.y+g*r.yPercent/100;if(null!=r.ox&&(p=(r.oxp?.01*m*r.ox:r.ox)-m/2,_=(r.oyp?.01*g*r.oy:r.oy)-g/2,w+=p-(p*o+_*l),b+=_-(p*h+_*u)),v?(p=m/2,_=g/2,y+=", Dx="+(p-(p*o+_*l)+w)+", Dy="+(_-(p*h+_*u)+b)+")"):y+=", sizingMethod='auto expand')",f.filter=-1!==e.indexOf("DXImageTransform.Microsoft.Matrix(")?e.replace(M,y):y+" "+e,(0===t||1===t)&&1===o&&0===l&&0===h&&1===u&&(v&&-1===y.indexOf("Dx=0, Dy=0")||T.test(e)&&100!==parseFloat(RegExp.$1)||-1===e.indexOf("gradient("&&e.indexOf("Alpha"))&&f.removeAttribute("filter")),!v){var P,S,O,k=8>d?1:-1;for(p=r.ieOffsetX||0,_=r.ieOffsetY||0,r.ieOffsetX=Math.round((m-((0>o?-o:o)*m+(0>l?-l:l)*g))/2+w),r.ieOffsetY=Math.round((g-((0>u?-u:u)*g+(0>h?-h:h)*m))/2+b),ge=0;4>ge;ge++)S=ee[ge],P=c[S],i=-1!==P.indexOf("px")?parseFloat(P):Z(this.t,S,parseFloat(P),P.replace(x,""))||0,O=i!==r[S]?2>ge?-r.ieOffsetX:-r.ieOffsetY:2>ge?p-r.ieOffsetX:_-r.ieOffsetY,f[S]=(r[S]=Math.round(i-O*(0===ge||2===ge?1:k)))+"px"}}},Be=I.set3DTransformRatio=I.setTransformRatio=function(t){var e,i,r,s,n,a,o,l,h,u,f,c,_,d,m,g,v,y,x,T,w,b,P,S=this.data,O=this.t.style,k=S.rotation,C=S.rotationX,R=S.rotationY,A=S.scaleX,M=S.scaleY,D=S.scaleZ,L=S.x,F=S.y,X=S.z,z=S.svg,B=S.perspective,E=S.force3D;if(!(((1!==t&&0!==t||"auto"!==E||this.tween._totalTime!==this.tween._totalDuration&&this.tween._totalTime)&&E||X||B||R||C)&&(!Te||!z)&&Oe))return k||S.skewX||z?(k*=N,b=S.skewX*N,P=1e5,e=Math.cos(k)*A,s=Math.sin(k)*A,i=Math.sin(k-b)*-M,n=Math.cos(k-b)*M,b&&"simple"===S.skewType&&(v=Math.tan(b),v=Math.sqrt(1+v*v),i*=v,n*=v,S.skewY&&(e*=v,s*=v)),z&&(L+=S.xOrigin-(S.xOrigin*e+S.yOrigin*i)+S.xOffset,F+=S.yOrigin-(S.xOrigin*s+S.yOrigin*n)+S.yOffset,Te&&(S.xPercent||S.yPercent)&&(d=this.t.getBBox(),L+=.01*S.xPercent*d.width,F+=.01*S.yPercent*d.height),d=1e-6,d>L&&L>-d&&(L=0),d>F&&F>-d&&(F=0)),x=(0|e*P)/P+","+(0|s*P)/P+","+(0|i*P)/P+","+(0|n*P)/P+","+L+","+F+")",z&&Te?this.t.setAttribute("transform","matrix("+x):O[be]=(S.xPercent||S.yPercent?"translate("+S.xPercent+"%,"+S.yPercent+"%) matrix(":"matrix(")+x):O[be]=(S.xPercent||S.yPercent?"translate("+S.xPercent+"%,"+S.yPercent+"%) matrix(":"matrix(")+A+",0,0,"+M+","+L+","+F+")",void 0;if(p&&(d=1e-4,d>A&&A>-d&&(A=D=2e-5),d>M&&M>-d&&(M=D=2e-5),!B||S.z||S.rotationX||S.rotationY||(B=0)),k||S.skewX)k*=N,m=e=Math.cos(k),g=s=Math.sin(k),S.skewX&&(k-=S.skewX*N,m=Math.cos(k),g=Math.sin(k),"simple"===S.skewType&&(v=Math.tan(S.skewX*N),v=Math.sqrt(1+v*v),m*=v,g*=v,S.skewY&&(e*=v,s*=v))),i=-g,n=m;else{if(!(R||C||1!==D||B||z))return O[be]=(S.xPercent||S.yPercent?"translate("+S.xPercent+"%,"+S.yPercent+"%) translate3d(":"translate3d(")+L+"px,"+F+"px,"+X+"px)"+(1!==A||1!==M?" scale("+A+","+M+")":""),void 0;e=n=1,i=s=0}h=1,r=a=o=l=u=f=0,c=B?-1/B:0,_=S.zOrigin,d=1e-6,T=",",w="0",k=R*N,k&&(m=Math.cos(k),g=Math.sin(k),o=-g,u=c*-g,r=e*g,a=s*g,h=m,c*=m,e*=m,s*=m),k=C*N,k&&(m=Math.cos(k),g=Math.sin(k),v=i*m+r*g,y=n*m+a*g,l=h*g,f=c*g,r=i*-g+r*m,a=n*-g+a*m,h*=m,c*=m,i=v,n=y),1!==D&&(r*=D,a*=D,h*=D,c*=D),1!==M&&(i*=M,n*=M,l*=M,f*=M),1!==A&&(e*=A,s*=A,o*=A,u*=A),(_||z)&&(_&&(L+=r*-_,F+=a*-_,X+=h*-_+_),z&&(L+=S.xOrigin-(S.xOrigin*e+S.yOrigin*i)+S.xOffset,F+=S.yOrigin-(S.xOrigin*s+S.yOrigin*n)+S.yOffset),d>L&&L>-d&&(L=w),d>F&&F>-d&&(F=w),d>X&&X>-d&&(X=0)),x=S.xPercent||S.yPercent?"translate("+S.xPercent+"%,"+S.yPercent+"%) matrix3d(":"matrix3d(",x+=(d>e&&e>-d?w:e)+T+(d>s&&s>-d?w:s)+T+(d>o&&o>-d?w:o),x+=T+(d>u&&u>-d?w:u)+T+(d>i&&i>-d?w:i)+T+(d>n&&n>-d?w:n),C||R?(x+=T+(d>l&&l>-d?w:l)+T+(d>f&&f>-d?w:f)+T+(d>r&&r>-d?w:r),x+=T+(d>a&&a>-d?w:a)+T+(d>h&&h>-d?w:h)+T+(d>c&&c>-d?w:c)+T):x+=",0,0,0,0,1,0,",x+=L+T+F+T+X+T+(B?1+-X/B:1)+")",O[be]=x};h=ke.prototype,h.x=h.y=h.z=h.skewX=h.skewY=h.rotation=h.rotationX=h.rotationY=h.zOrigin=h.xPercent=h.yPercent=h.xOffset=h.yOffset=0,h.scaleX=h.scaleY=h.scaleZ=1,ye("transform,scale,scaleX,scaleY,scaleZ,x,y,z,rotation,rotationX,rotationY,rotationZ,skewX,skewY,shortRotation,shortRotationX,shortRotationY,shortRotationZ,transformOrigin,svgOrigin,transformPerspective,directionalRotation,parseTransform,force3D,skewType,xPercent,yPercent,smoothOrigin",{parser:function(t,e,i,r,n,o,l){if(r._lastParsedTransform===l)return n;r._lastParsedTransform=l;var h,u,f,c,p,_,d,m,g,v=t._gsTransform,y=r._transform=Xe(t,s,!0,l.parseTransform),x=t.style,T=1e-6,w=we.length,b=l,P={},S="transformOrigin";if("string"==typeof b.transform&&be)f=B.style,f[be]=b.transform,f.display="block",f.position="absolute",X.body.appendChild(B),h=Xe(B,null,!1),X.body.removeChild(B),null!=b.xPercent&&(h.xPercent=ne(b.xPercent,y.xPercent)),null!=b.yPercent&&(h.yPercent=ne(b.yPercent,y.yPercent));else if("object"==typeof b){if(h={scaleX:ne(null!=b.scaleX?b.scaleX:b.scale,y.scaleX),scaleY:ne(null!=b.scaleY?b.scaleY:b.scale,y.scaleY),scaleZ:ne(b.scaleZ,y.scaleZ),x:ne(b.x,y.x),y:ne(b.y,y.y),z:ne(b.z,y.z),xPercent:ne(b.xPercent,y.xPercent),yPercent:ne(b.yPercent,y.yPercent),perspective:ne(b.transformPerspective,y.perspective)},d=b.directionalRotation,null!=d)if("object"==typeof d)for(f in d)b[f]=d[f];else b.rotation=d;"string"==typeof b.x&&-1!==b.x.indexOf("%")&&(h.x=0,h.xPercent=ne(b.x,y.xPercent)),"string"==typeof b.y&&-1!==b.y.indexOf("%")&&(h.y=0,h.yPercent=ne(b.y,y.yPercent)),h.rotation=ae("rotation"in b?b.rotation:"shortRotation"in b?b.shortRotation+"_short":"rotationZ"in b?b.rotationZ:y.rotation,y.rotation,"rotation",P),Oe&&(h.rotationX=ae("rotationX"in b?b.rotationX:"shortRotationX"in b?b.shortRotationX+"_short":y.rotationX||0,y.rotationX,"rotationX",P),h.rotationY=ae("rotationY"in b?b.rotationY:"shortRotationY"in b?b.shortRotationY+"_short":y.rotationY||0,y.rotationY,"rotationY",P)),h.skewX=null==b.skewX?y.skewX:ae(b.skewX,y.skewX),h.skewY=null==b.skewY?y.skewY:ae(b.skewY,y.skewY),(u=h.skewY-y.skewY)&&(h.skewX+=u,h.rotation+=u)}for(Oe&&null!=b.force3D&&(y.force3D=b.force3D,_=!0),y.skewType=b.skewType||y.skewType||a.defaultSkewType,p=y.force3D||y.z||y.rotationX||y.rotationY||h.z||h.rotationX||h.rotationY||h.perspective,p||null==b.scale||(h.scaleZ=1);--w>-1;)i=we[w],c=h[i]-y[i],(c>T||-T>c||null!=b[i]||null!=F[i])&&(_=!0,n=new _e(y,i,y[i],c,n),i in P&&(n.e=P[i]),n.xs0=0,n.plugin=o,r._overwriteProps.push(n.n));return c=b.transformOrigin,y.svg&&(c||b.svgOrigin)&&(m=y.xOffset,g=y.yOffset,De(t,re(c),h,b.svgOrigin,b.smoothOrigin),n=de(y,"xOrigin",(v?y:h).xOrigin,h.xOrigin,n,S),n=de(y,"yOrigin",(v?y:h).yOrigin,h.yOrigin,n,S),(m!==y.xOffset||g!==y.yOffset)&&(n=de(y,"xOffset",v?m:y.xOffset,y.xOffset,n,S),n=de(y,"yOffset",v?g:y.yOffset,y.yOffset,n,S)),c=Te?null:"0px 0px"),(c||Oe&&p&&y.zOrigin)&&(be?(_=!0,i=Se,c=(c||Q(t,i,s,!1,"50% 50%"))+"",n=new _e(x,i,0,0,n,-1,S),n.b=x[i],n.plugin=o,Oe?(f=y.zOrigin,c=c.split(" "),y.zOrigin=(c.length>2&&(0===f||"0px"!==c[2])?parseFloat(c[2]):f)||0,n.xs0=n.e=c[0]+" "+(c[1]||"50%")+" 0px",n=new _e(y,"zOrigin",0,0,n,-1,n.n),n.b=f,n.xs0=n.e=y.zOrigin):n.xs0=n.e=c):re(c+"",y)),_&&(r._transformType=y.svg&&Te||!p&&3!==this._transformType?2:3),n},prefix:!0}),ye("boxShadow",{defaultValue:"0px 0px 0px 0px #999",prefix:!0,color:!0,multi:!0,keyword:"inset"}),ye("borderRadius",{defaultValue:"0px",parser:function(t,e,i,n,a){e=this.format(e);var o,l,h,u,f,c,p,_,d,m,g,v,y,x,T,w,b=["borderTopLeftRadius","borderTopRightRadius","borderBottomRightRadius","borderBottomLeftRadius"],P=t.style;for(d=parseFloat(t.offsetWidth),m=parseFloat(t.offsetHeight),o=e.split(" "),l=0;b.length>l;l++)this.p.indexOf("border")&&(b[l]=q(b[l])),f=u=Q(t,b[l],s,!1,"0px"),-1!==f.indexOf(" ")&&(u=f.split(" "),f=u[0],u=u[1]),c=h=o[l],p=parseFloat(f),v=f.substr((p+"").length),y="="===c.charAt(1),y?(_=parseInt(c.charAt(0)+"1",10),c=c.substr(2),_*=parseFloat(c),g=c.substr((_+"").length-(0>_?1:0))||""):(_=parseFloat(c),g=c.substr((_+"").length)),""===g&&(g=r[i]||v),g!==v&&(x=Z(t,"borderLeft",p,v),T=Z(t,"borderTop",p,v),"%"===g?(f=100*(x/d)+"%",u=100*(T/m)+"%"):"em"===g?(w=Z(t,"borderLeft",1,"em"),f=x/w+"em",u=T/w+"em"):(f=x+"px",u=T+"px"),y&&(c=parseFloat(f)+_+g,h=parseFloat(u)+_+g)),a=me(P,b[l],f+" "+u,c+" "+h,!1,"0px",a);return a},prefix:!0,formatter:fe("0px 0px 0px 0px",!1,!0)}),ye("backgroundPosition",{defaultValue:"0 0",parser:function(t,e,i,r,n,a){var o,l,h,u,f,c,p="background-position",_=s||H(t,null),m=this.format((_?d?_.getPropertyValue(p+"-x")+" "+_.getPropertyValue(p+"-y"):_.getPropertyValue(p):t.currentStyle.backgroundPositionX+" "+t.currentStyle.backgroundPositionY)||"0 0"),g=this.format(e);if(-1!==m.indexOf("%")!=(-1!==g.indexOf("%"))&&(c=Q(t,"backgroundImage").replace(k,""),c&&"none"!==c)){for(o=m.split(" "),l=g.split(" "),E.setAttribute("src",c),h=2;--h>-1;)m=o[h],u=-1!==m.indexOf("%"),u!==(-1!==l[h].indexOf("%"))&&(f=0===h?t.offsetWidth-E.width:t.offsetHeight-E.height,o[h]=u?parseFloat(m)/100*f+"px":100*(parseFloat(m)/f)+"%");m=o.join(" ")}return this.parseComplex(t.style,m,g,n,a)},formatter:re}),ye("backgroundSize",{defaultValue:"0 0",formatter:re}),ye("perspective",{defaultValue:"0px",prefix:!0}),ye("perspectiveOrigin",{defaultValue:"50% 50%",prefix:!0}),ye("transformStyle",{prefix:!0}),ye("backfaceVisibility",{prefix:!0}),ye("userSelect",{prefix:!0}),ye("margin",{parser:ce("marginTop,marginRight,marginBottom,marginLeft")}),ye("padding",{parser:ce("paddingTop,paddingRight,paddingBottom,paddingLeft")}),ye("clip",{defaultValue:"rect(0px,0px,0px,0px)",parser:function(t,e,i,r,n,a){var o,l,h;return 9>d?(l=t.currentStyle,h=8>d?" ":",",o="rect("+l.clipTop+h+l.clipRight+h+l.clipBottom+h+l.clipLeft+")",e=this.format(e).split(",").join(h)):(o=this.format(Q(t,this.p,s,!1,this.dflt)),e=this.format(e)),this.parseComplex(t.style,o,e,n,a)}}),ye("textShadow",{defaultValue:"0px 0px 0px #999",color:!0,multi:!0}),ye("autoRound,strictUnits",{parser:function(t,e,i,r,s){return s}}),ye("border",{defaultValue:"0px solid #000",parser:function(t,e,i,r,n,a){return this.parseComplex(t.style,this.format(Q(t,"borderTopWidth",s,!1,"0px")+" "+Q(t,"borderTopStyle",s,!1,"solid")+" "+Q(t,"borderTopColor",s,!1,"#000")),this.format(e),n,a)},color:!0,formatter:function(t){var e=t.split(" ");return e[0]+" "+(e[1]||"solid")+" "+(t.match(ue)||["#000"])[0]}}),ye("borderWidth",{parser:ce("borderTopWidth,borderRightWidth,borderBottomWidth,borderLeftWidth")}),ye("float,cssFloat,styleFloat",{parser:function(t,e,i,r,s){var n=t.style,a="cssFloat"in n?"cssFloat":"styleFloat";return new _e(n,a,0,0,s,-1,i,!1,0,n[a],e)}});var Ee=function(t){var e,i=this.t,r=i.filter||Q(this.data,"filter")||"",s=0|this.s+this.c*t;100===s&&(-1===r.indexOf("atrix(")&&-1===r.indexOf("radient(")&&-1===r.indexOf("oader(")?(i.removeAttribute("filter"),e=!Q(this.data,"filter")):(i.filter=r.replace(b,""),e=!0)),e||(this.xn1&&(i.filter=r=r||"alpha(opacity="+s+")"),-1===r.indexOf("pacity")?0===s&&this.xn1||(i.filter=r+" alpha(opacity="+s+")"):i.filter=r.replace(T,"opacity="+s))};ye("opacity,alpha,autoAlpha",{defaultValue:"1",parser:function(t,e,i,r,n,a){var o=parseFloat(Q(t,"opacity",s,!1,"1")),l=t.style,h="autoAlpha"===i;return"string"==typeof e&&"="===e.charAt(1)&&(e=("-"===e.charAt(0)?-1:1)*parseFloat(e.substr(2))+o),h&&1===o&&"hidden"===Q(t,"visibility",s)&&0!==e&&(o=0),W?n=new _e(l,"opacity",o,e-o,n):(n=new _e(l,"opacity",100*o,100*(e-o),n),n.xn1=h?1:0,l.zoom=1,n.type=2,n.b="alpha(opacity="+n.s+")",n.e="alpha(opacity="+(n.s+n.c)+")",n.data=t,n.plugin=a,n.setRatio=Ee),h&&(n=new _e(l,"visibility",0,0,n,-1,null,!1,0,0!==o?"inherit":"hidden",0===e?"hidden":"inherit"),n.xs0="inherit",r._overwriteProps.push(n.n),r._overwriteProps.push(i)),n}});var Ie=function(t,e){e&&(t.removeProperty?(("ms"===e.substr(0,2)||"webkit"===e.substr(0,6))&&(e="-"+e),t.removeProperty(e.replace(S,"-$1").toLowerCase())):t.removeAttribute(e))},Ye=function(t){if(this.t._gsClassPT=this,1===t||0===t){this.t.setAttribute("class",0===t?this.b:this.e);for(var e=this.data,i=this.t.style;e;)e.v?i[e.p]=e.v:Ie(i,e.p),e=e._next;1===t&&this.t._gsClassPT===this&&(this.t._gsClassPT=null)}else this.t.getAttribute("class")!==this.e&&this.t.setAttribute("class",this.e)};ye("className",{parser:function(t,e,r,n,a,o,l){var h,u,f,c,p,_=t.getAttribute("class")||"",d=t.style.cssText;if(a=n._classNamePT=new _e(t,r,0,0,a,2),a.setRatio=Ye,a.pr=-11,i=!0,a.b=_,u=K(t,s),f=t._gsClassPT){for(c={},p=f.data;p;)c[p.p]=1,p=p._next;f.setRatio(1)}return t._gsClassPT=a,a.e="="!==e.charAt(1)?e:_.replace(RegExp("\\s*\\b"+e.substr(2)+"\\b"),"")+("+"===e.charAt(0)?" "+e.substr(2):""),t.setAttribute("class",a.e),h=J(t,u,K(t),l,c),t.setAttribute("class",_),a.data=h.firstMPT,t.style.cssText=d,a=a.xfirst=n.parse(t,h.difs,a,o)}});var We=function(t){if((1===t||0===t)&&this.data._totalTime===this.data._totalDuration&&"isFromStart"!==this.data.data){var e,i,r,s,n,a=this.t.style,o=l.transform.parse;if("all"===this.e)a.cssText="",s=!0;else for(e=this.e.split(" ").join("").split(","),r=e.length;--r>-1;)i=e[r],l[i]&&(l[i].parse===o?s=!0:i="transformOrigin"===i?Se:l[i].p),Ie(a,i);s&&(Ie(a,be),n=this.t._gsTransform,n&&(n.svg&&this.t.removeAttribute("data-svg-origin"),delete this.t._gsTransform))}};for(ye("clearProps",{parser:function(t,e,r,s,n){return n=new _e(t,r,0,0,n,2),n.setRatio=We,n.e=e,n.pr=-10,n.data=s._tween,i=!0,n}}),h="bezier,throwProps,physicsProps,physics2D".split(","),ge=h.length;ge--;)xe(h[ge]);h=a.prototype,h._firstPT=h._lastParsedTransform=h._transform=null,h._onInitTween=function(t,e,o){if(!t.nodeType)return!1;this._target=t,this._tween=o,this._vars=e,u=e.autoRound,i=!1,r=e.suffixMap||a.suffixMap,s=H(t,""),n=this._overwriteProps;
var h,p,d,m,g,v,y,x,T,b=t.style;if(f&&""===b.zIndex&&(h=Q(t,"zIndex",s),("auto"===h||""===h)&&this._addLazySet(b,"zIndex",0)),"string"==typeof e&&(m=b.cssText,h=K(t,s),b.cssText=m+";"+e,h=J(t,h,K(t)).difs,!W&&w.test(e)&&(h.opacity=parseFloat(RegExp.$1)),e=h,b.cssText=m),this._firstPT=p=e.className?l.className.parse(t,e.className,"className",this,null,null,e):this.parse(t,e,null),this._transformType){for(T=3===this._transformType,be?c&&(f=!0,""===b.zIndex&&(y=Q(t,"zIndex",s),("auto"===y||""===y)&&this._addLazySet(b,"zIndex",0)),_&&this._addLazySet(b,"WebkitBackfaceVisibility",this._vars.WebkitBackfaceVisibility||(T?"visible":"hidden"))):b.zoom=1,d=p;d&&d._next;)d=d._next;x=new _e(t,"transform",0,0,null,2),this._linkCSSP(x,null,d),x.setRatio=be?Be:ze,x.data=this._transform||Xe(t,s,!0),x.tween=o,x.pr=-1,n.pop()}if(i){for(;p;){for(v=p._next,d=m;d&&d.pr>p.pr;)d=d._next;(p._prev=d?d._prev:g)?p._prev._next=p:m=p,(p._next=d)?d._prev=p:g=p,p=v}this._firstPT=m}return!0},h.parse=function(t,e,i,n){var a,o,h,f,c,p,_,d,m,g,v=t.style;for(a in e)p=e[a],o=l[a],o?i=o.parse(t,p,a,this,i,n,e):(c=Q(t,a,s)+"",m="string"==typeof p,"color"===a||"fill"===a||"stroke"===a||-1!==a.indexOf("Color")||m&&P.test(p)?(m||(p=he(p),p=(p.length>3?"rgba(":"rgb(")+p.join(",")+")"),i=me(v,a,c,p,!0,"transparent",i,0,n)):!m||-1===p.indexOf(" ")&&-1===p.indexOf(",")?(h=parseFloat(c),_=h||0===h?c.substr((h+"").length):"",(""===c||"auto"===c)&&("width"===a||"height"===a?(h=ie(t,a,s),_="px"):"left"===a||"top"===a?(h=$(t,a,s),_="px"):(h="opacity"!==a?0:1,_="")),g=m&&"="===p.charAt(1),g?(f=parseInt(p.charAt(0)+"1",10),p=p.substr(2),f*=parseFloat(p),d=p.replace(x,"")):(f=parseFloat(p),d=m?p.replace(x,""):""),""===d&&(d=a in r?r[a]:_),p=f||0===f?(g?f+h:f)+d:e[a],_!==d&&""!==d&&(f||0===f)&&h&&(h=Z(t,a,h,_),"%"===d?(h/=Z(t,a,100,"%")/100,e.strictUnits!==!0&&(c=h+"%")):"em"===d?h/=Z(t,a,1,"em"):"px"!==d&&(f=Z(t,a,f,d),d="px"),g&&(f||0===f)&&(p=f+h+d)),g&&(f+=h),!h&&0!==h||!f&&0!==f?void 0!==v[a]&&(p||"NaN"!=p+""&&null!=p)?(i=new _e(v,a,f||h||0,0,i,-1,a,!1,0,c,p),i.xs0="none"!==p||"display"!==a&&-1===a.indexOf("Style")?p:c):j("invalid "+a+" tween value: "+e[a]):(i=new _e(v,a,h,f-h,i,0,a,u!==!1&&("px"===d||"zIndex"===a),0,c,p),i.xs0=d)):i=me(v,a,c,p,!0,null,i,0,n)),n&&i&&!i.plugin&&(i.plugin=n);return i},h.setRatio=function(t){var e,i,r,s=this._firstPT,n=1e-6;if(1!==t||this._tween._time!==this._tween._duration&&0!==this._tween._time)if(t||this._tween._time!==this._tween._duration&&0!==this._tween._time||this._tween._rawPrevTime===-1e-6)for(;s;){if(e=s.c*t+s.s,s.r?e=Math.round(e):n>e&&e>-n&&(e=0),s.type)if(1===s.type)if(r=s.l,2===r)s.t[s.p]=s.xs0+e+s.xs1+s.xn1+s.xs2;else if(3===r)s.t[s.p]=s.xs0+e+s.xs1+s.xn1+s.xs2+s.xn2+s.xs3;else if(4===r)s.t[s.p]=s.xs0+e+s.xs1+s.xn1+s.xs2+s.xn2+s.xs3+s.xn3+s.xs4;else if(5===r)s.t[s.p]=s.xs0+e+s.xs1+s.xn1+s.xs2+s.xn2+s.xs3+s.xn3+s.xs4+s.xn4+s.xs5;else{for(i=s.xs0+e+s.xs1,r=1;s.l>r;r++)i+=s["xn"+r]+s["xs"+(r+1)];s.t[s.p]=i}else-1===s.type?s.t[s.p]=s.xs0:s.setRatio&&s.setRatio(t);else s.t[s.p]=e+s.xs0;s=s._next}else for(;s;)2!==s.type?s.t[s.p]=s.b:s.setRatio(t),s=s._next;else for(;s;){if(2!==s.type)if(s.r&&-1!==s.type)if(e=Math.round(s.s+s.c),s.type){if(1===s.type){for(r=s.l,i=s.xs0+e+s.xs1,r=1;s.l>r;r++)i+=s["xn"+r]+s["xs"+(r+1)];s.t[s.p]=i}}else s.t[s.p]=e+s.xs0;else s.t[s.p]=s.e;else s.setRatio(t);s=s._next}},h._enableTransforms=function(t){this._transform=this._transform||Xe(this._target,s,!0),this._transformType=this._transform.svg&&Te||!t&&3!==this._transformType?2:3};var Ve=function(){this.t[this.p]=this.e,this.data._linkCSSP(this,this._next,null,!0)};h._addLazySet=function(t,e,i){var r=this._firstPT=new _e(t,e,0,0,this._firstPT,2);r.e=i,r.setRatio=Ve,r.data=this},h._linkCSSP=function(t,e,i,r){return t&&(e&&(e._prev=t),t._next&&(t._next._prev=t._prev),t._prev?t._prev._next=t._next:this._firstPT===t&&(this._firstPT=t._next,r=!0),i?i._next=t:r||null!==this._firstPT||(this._firstPT=t),t._next=e,t._prev=i),t},h._kill=function(e){var i,r,s,n=e;if(e.autoAlpha||e.alpha){n={};for(r in e)n[r]=e[r];n.opacity=1,n.autoAlpha&&(n.visibility=1)}return e.className&&(i=this._classNamePT)&&(s=i.xfirst,s&&s._prev?this._linkCSSP(s._prev,i._next,s._prev._prev):s===this._firstPT&&(this._firstPT=i._next),i._next&&this._linkCSSP(i._next,i._next._next,s._prev),this._classNamePT=null),t.prototype._kill.call(this,n)};var je=function(t,e,i){var r,s,n,a;if(t.slice)for(s=t.length;--s>-1;)je(t[s],e,i);else for(r=t.childNodes,s=r.length;--s>-1;)n=r[s],a=n.type,n.style&&(e.push(K(n)),i&&i.push(n)),1!==a&&9!==a&&11!==a||!n.childNodes.length||je(n,e,i)};return a.cascadeTo=function(t,i,r){var s,n,a,o,l=e.to(t,i,r),h=[l],u=[],f=[],c=[],p=e._internals.reservedProps;for(t=l._targets||l.target,je(t,u,c),l.render(i,!0,!0),je(t,f),l.render(0,!0,!0),l._enabled(!0),s=c.length;--s>-1;)if(n=J(c[s],u[s],f[s]),n.firstMPT){n=n.difs;for(a in r)p[a]&&(n[a]=r[a]);o={};for(a in n)o[a]=u[s][a];h.push(e.fromTo(c[s],i,o,n))}return h},t.activate([a]),a},!0)}),_gsScope._gsDefine&&_gsScope._gsQueue.pop()(),function(t){"use strict";var e=function(){return(_gsScope.GreenSockGlobals||_gsScope)[t]};"function"==typeof define&&define.amd?define(["TweenLite"],e):"undefined"!=typeof module&&module.exports&&(require("../TweenLite.js"),module.exports=e())}("CSSPlugin");

/*!
 * VERSION: beta 0.3.3
 * DATE: 2014-10-29
 * UPDATES AND DOCS AT: http://greensock.com
 *
 * @license Copyright (c) 2008-2015, GreenSock. All rights reserved.
 * SplitText is a Club GreenSock membership benefit; You must have a valid membership to use
 * this code without violating the terms of use. Visit http://www.greensock.com/club/ to sign up or get more details.
 * This work is subject to the software agreement that was issued with your membership.
 * 
 * @author: Jack Doyle, jack@greensock.com
 */
var _gsScope="undefined"!=typeof module&&module.exports&&"undefined"!=typeof global?global:this||window;(function(t){"use strict";var e=t.GreenSockGlobals||t,i=function(t){var i,s=t.split("."),r=e;for(i=0;s.length>i;i++)r[s[i]]=r=r[s[i]]||{};return r},s=i("com.greensock.utils"),r=function(t){var e=t.nodeType,i="";if(1===e||9===e||11===e){if("string"==typeof t.textContent)return t.textContent;for(t=t.firstChild;t;t=t.nextSibling)i+=r(t)}else if(3===e||4===e)return t.nodeValue;return i},n=document,a=n.defaultView?n.defaultView.getComputedStyle:function(){},o=/([A-Z])/g,h=function(t,e,i,s){var r;return(i=i||a(t,null))?(t=i.getPropertyValue(e.replace(o,"-$1").toLowerCase()),r=t||i.length?t:i[e]):t.currentStyle&&(i=t.currentStyle,r=i[e]),s?r:parseInt(r,10)||0},l=function(t){return t.length&&t[0]&&(t[0].nodeType&&t[0].style&&!t.nodeType||t[0].length&&t[0][0])?!0:!1},_=function(t){var e,i,s,r=[],n=t.length;for(e=0;n>e;e++)if(i=t[e],l(i))for(s=i.length,s=0;i.length>s;s++)r.push(i[s]);else r.push(i);return r},u=")eefec303079ad17405c",c=/(?:<br>|<br\/>|<br \/>)/gi,f=n.all&&!n.addEventListener,p="<div style='position:relative;display:inline-block;"+(f?"*display:inline;*zoom:1;'":"'"),m=function(t){t=t||"";var e=-1!==t.indexOf("++"),i=1;return e&&(t=t.split("++").join("")),function(){return p+(t?" class='"+t+(e?i++:"")+"'>":">")}},d=s.SplitText=e.SplitText=function(t,e){if("string"==typeof t&&(t=d.selector(t)),!t)throw"cannot split a null element.";this.elements=l(t)?_(t):[t],this.chars=[],this.words=[],this.lines=[],this._originals=[],this.vars=e||{},this.split(e)},g=function(t,e,i){var s=t.nodeType;if(1===s||9===s||11===s)for(t=t.firstChild;t;t=t.nextSibling)g(t,e,i);else(3===s||4===s)&&(t.nodeValue=t.nodeValue.split(e).join(i))},v=function(t,e){for(var i=e.length;--i>-1;)t.push(e[i])},y=function(t,e,i,s,o){c.test(t.innerHTML)&&(t.innerHTML=t.innerHTML.replace(c,u));var l,_,f,p,d,y,T,w,b,x,P,S,k,C,R=r(t),O=e.type||e.split||"chars,words,lines",A=-1!==O.indexOf("lines")?[]:null,D=-1!==O.indexOf("words"),M=-1!==O.indexOf("chars"),L="absolute"===e.position||e.absolute===!0,z=L?"&#173; ":" ",I=-999,E=a(t),N=h(t,"paddingLeft",E),F=h(t,"borderBottomWidth",E)+h(t,"borderTopWidth",E),B=h(t,"borderLeftWidth",E)+h(t,"borderRightWidth",E),X=h(t,"paddingTop",E)+h(t,"paddingBottom",E),j=h(t,"paddingLeft",E)+h(t,"paddingRight",E),U=h(t,"textAlign",E,!0),Y=t.clientHeight,q=t.clientWidth,V="</div>",G=m(e.wordsClass),Q=m(e.charsClass),W=-1!==(e.linesClass||"").indexOf("++"),Z=e.linesClass,H=-1!==R.indexOf("<"),$=!0,K=[],J=[],te=[];for(W&&(Z=Z.split("++").join("")),H&&(R=R.split("<").join("{{LT}}")),l=R.length,p=G(),d=0;l>d;d++)if(T=R.charAt(d),")"===T&&R.substr(d,20)===u)p+=($?V:"")+"<BR/>",$=!1,d!==l-20&&R.substr(d+20,20)!==u&&(p+=" "+G(),$=!0),d+=19;else if(" "===T&&" "!==R.charAt(d-1)&&d!==l-1&&R.substr(d-20,20)!==u){for(p+=$?V:"",$=!1;" "===R.charAt(d+1);)p+=z,d++;(")"!==R.charAt(d+1)||R.substr(d+1,20)!==u)&&(p+=z+G(),$=!0)}else p+=M&&" "!==T?Q()+T+"</div>":T;for(t.innerHTML=p+($?V:""),H&&g(t,"{{LT}}","<"),y=t.getElementsByTagName("*"),l=y.length,w=[],d=0;l>d;d++)w[d]=y[d];if(A||L)for(d=0;l>d;d++)b=w[d],f=b.parentNode===t,(f||L||M&&!D)&&(x=b.offsetTop,A&&f&&x!==I&&"BR"!==b.nodeName&&(_=[],A.push(_),I=x),L&&(b._x=b.offsetLeft,b._y=x,b._w=b.offsetWidth,b._h=b.offsetHeight),A&&(D!==f&&M||(_.push(b),b._x-=N),f&&d&&(w[d-1]._wordEnd=!0),"BR"===b.nodeName&&b.nextSibling&&"BR"===b.nextSibling.nodeName&&A.push([])));for(d=0;l>d;d++)b=w[d],f=b.parentNode===t,"BR"!==b.nodeName?(L&&(S=b.style,D||f||(b._x+=b.parentNode._x,b._y+=b.parentNode._y),S.left=b._x+"px",S.top=b._y+"px",S.position="absolute",S.display="block",S.width=b._w+1+"px",S.height=b._h+"px"),D?f&&""!==b.innerHTML?J.push(b):M&&K.push(b):f?(t.removeChild(b),w.splice(d--,1),l--):!f&&M&&(x=!A&&!L&&b.nextSibling,t.appendChild(b),x||t.appendChild(n.createTextNode(" ")),K.push(b))):A||L?(t.removeChild(b),w.splice(d--,1),l--):D||t.appendChild(b);if(A){for(L&&(P=n.createElement("div"),t.appendChild(P),k=P.offsetWidth+"px",x=P.offsetParent===t?0:t.offsetLeft,t.removeChild(P)),S=t.style.cssText,t.style.cssText="display:none;";t.firstChild;)t.removeChild(t.firstChild);for(C=!L||!D&&!M,d=0;A.length>d;d++){for(_=A[d],P=n.createElement("div"),P.style.cssText="display:block;text-align:"+U+";position:"+(L?"absolute;":"relative;"),Z&&(P.className=Z+(W?d+1:"")),te.push(P),l=_.length,y=0;l>y;y++)"BR"!==_[y].nodeName&&(b=_[y],P.appendChild(b),C&&(b._wordEnd||D)&&P.appendChild(n.createTextNode(" ")),L&&(0===y&&(P.style.top=b._y+"px",P.style.left=N+x+"px"),b.style.top="0px",x&&(b.style.left=b._x-x+"px")));0===l&&(P.innerHTML="&nbsp;"),D||M||(P.innerHTML=r(P).split(String.fromCharCode(160)).join(" ")),L&&(P.style.width=k,P.style.height=b._h+"px"),t.appendChild(P)}t.style.cssText=S}L&&(Y>t.clientHeight&&(t.style.height=Y-X+"px",Y>t.clientHeight&&(t.style.height=Y+F+"px")),q>t.clientWidth&&(t.style.width=q-j+"px",q>t.clientWidth&&(t.style.width=q+B+"px"))),v(i,K),v(s,J),v(o,te)},T=d.prototype;T.split=function(t){this.isSplit&&this.revert(),this.vars=t||this.vars,this._originals.length=this.chars.length=this.words.length=this.lines.length=0;for(var e=this.elements.length;--e>-1;)this._originals[e]=this.elements[e].innerHTML,y(this.elements[e],this.vars,this.chars,this.words,this.lines);return this.chars.reverse(),this.words.reverse(),this.lines.reverse(),this.isSplit=!0,this},T.revert=function(){if(!this._originals)throw"revert() call wasn't scoped properly.";for(var t=this._originals.length;--t>-1;)this.elements[t].innerHTML=this._originals[t];return this.chars=[],this.words=[],this.lines=[],this.isSplit=!1,this},d.selector=t.$||t.jQuery||function(e){var i=t.$||t.jQuery;return i?(d.selector=i,i(e)):"undefined"==typeof document?e:document.querySelectorAll?document.querySelectorAll(e):document.getElementById("#"===e.charAt(0)?e.substr(1):e)},d.version="0.3.3"})(_gsScope),function(t){"use strict";var e=function(){return(_gsScope.GreenSockGlobals||_gsScope)[t]};"function"==typeof define&&define.amd?define(["TweenLite"],e):"undefined"!=typeof module&&module.exports&&(module.exports=e())}("SplitText");

try{
	window.GreenSockGlobals = null;
	window._gsQueue = null;
	window._gsDefine = null;

	delete(window.GreenSockGlobals);
	delete(window._gsQueue);
	delete(window._gsDefine);	
   } catch(e) {}

try{
	window.GreenSockGlobals = oldgs;
	window._gsQueue = oldgs_queue;
	} catch(e) {}

if (window.tplogs==true)
	try {
		console.groupEnd();
	} catch(e) {}

(function(e,t){
		e.waitForImages={hasImageProperties:["backgroundImage","listStyleImage","borderImage","borderCornerImage"]};e.expr[":"].uncached=function(t){var n=document.createElement("img");n.src=t.src;return e(t).is('img[src!=""]')&&!n.complete};e.fn.waitForImages=function(t,n,r){if(e.isPlainObject(arguments[0])){n=t.each;r=t.waitForAll;t=t.finished}t=t||e.noop;n=n||e.noop;r=!!r;if(!e.isFunction(t)||!e.isFunction(n)){throw new TypeError("An invalid callback was supplied.")}return this.each(function(){var i=e(this),s=[];if(r){var o=e.waitForImages.hasImageProperties||[],u=/url\((['"]?)(.*?)\1\)/g;i.find("*").each(function(){var t=e(this);if(t.is("img:uncached")){s.push({src:t.attr("src"),element:t[0]})}e.each(o,function(e,n){var r=t.css(n);if(!r){return true}var i;while(i=u.exec(r)){s.push({src:i[2],element:t[0]})}})})}else{i.find("img:uncached").each(function(){s.push({src:this.src,element:this})})}var f=s.length,l=0;if(f==0){t.call(i[0])}e.each(s,function(r,s){var o=new Image;e(o).bind("load error",function(e){l++;n.call(s.element,l,f,e.type=="load");if(l==f){t.call(i[0]);return false}});o.src=s.src})})};		
})(jQuery)
;/************************************************************************************
 * jquery.themepunch.essential.js - jQuery Plugin for esg Portfolio Slider
 * @version: 2.1.0 (19.08.2015)
 * @requires jQuery v1.7 or later (tested on 1.9)
 * @author ThemePunch
************************************************************************************/
//! ++++++++++++++++++++++++++++++++++++++

!function(jQuery,undefined){function createCookie(e,t,a){var o;if(a){var i=new Date;i.setTime(i.getTime()+24*a*60*60*1e3),o="; expires="+i.toGMTString()}else o="";document.cookie=encodeURIComponent(e)+"="+encodeURIComponent(t)+o+"; path=/"}function readCookie(e){for(var t=encodeURIComponent(e)+"=",a=document.cookie.split(";"),o=0;o<a.length;o++){for(var i=a[o];" "===i.charAt(0);)i=i.substring(1,i.length);if(0===i.indexOf(t))return decodeURIComponent(i.substring(t.length,i.length))}return null}function eraseCookie(e){createCookie(e,"",-1)}function checkAvailableFilters(){}function checkMoreToLoad(e,t){var a=new Array;fidlist=new Array,searchchanged=jQuery(t.filterGroupClass+".esg-filter-wrapper.eg-search-wrapper .eg-justfilteredtosearch").length,forcesearch=jQuery(t.filterGroupClass+".esg-filter-wrapper.eg-search-wrapper .eg-forcefilter").length,jQuery(t.filterGroupClass+".esg-filter-wrapper .esg-filterbutton.selected, "+t.filterGroupClass+" .esg-filter-wrapper .esg-filterbutton.selected").each(function(){var e=jQuery(this).data("fid");-1==jQuery.inArray(e,fidlist)&&(a.push(e),fidlist.push(e))}),0==jQuery(t.filterGroupClass+".esg-filter-wrapper .esg-filterbutton.selected, "+t.filterGroupClass+" .esg-filter-wrapper .esg-filterbutton.selected").length&&a.push(-1);for(var o=new Array,i=0;i<t.loadMoreItems.length;i++)jQuery.each(t.loadMoreItems[i][1],function(e,r){jQuery.each(a,function(e,a){a==r&&-1!=t.loadMoreItems[i][0]&&(0==forcesearch||1==forcesearch&&"cat-searchresult"===t.loadMoreItems[i][2])&&o.push(t.loadMoreItems[i])})});return addCountSuffix(e,t),o}function addCountSuffix(e,t){var a=jQuery(t.filterGroupClass+".esg-filter-wrapper.eg-search-wrapper .eg-justfilteredtosearch").length,o=jQuery(t.filterGroupClass+".esg-filter-wrapper.eg-search-wrapper .eg-forcefilter").length;jQuery(t.filterGroupClass+".esg-filter-wrapper.eg-show-amount .esg-filterbutton").each(function(){var i=jQuery(this);if(0==i.find(".eg-el-amount").length||a>0){var r=i.data("fid"),n=i.data("filter");o>0&&(n+=".cat-searchresult");for(var s=e.find("."+n).length,l=0;l<t.loadMoreItems.length;l++){0==o?jQuery.each(t.loadMoreItems[l][1],function(e,a){a===r&&-1!=t.loadMoreItems[l][0]&&s++}):-1!=jQuery.inArray(r,t.loadMoreItems[l][1])&&"cat-searchresult"===t.loadMoreItems[l][2]&&s++}0==i.find(".eg-el-amount").length&&i.append('<span class="eg-el-amount">0</span>'),countToTop(i,s)}}),jQuery(t.filterGroupClass+".esg-filter-wrapper.eg-search-wrapper .eg-justfilteredtosearch").removeClass("eg-justfilteredtosearch")}function countToTop(e,t){function a(e,t){o.html(Math.round(e.target[t]))}var o=e.find(".eg-el-amount"),i={value:parseInt(o.text(),0)};punchgs.TweenLite.to(i,2,{value:t,onUpdate:a,onUpdateParams:["{self}","value"],ease:punchgs.Power3.easeInOut})}function buildLoader(e,t,a){return e.find(".esg-loader").length>0?!1:(e.append('<div class="esg-loader '+t.spinner+'"><div class="dot1"></div><div class="dot2"></div><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>'),("spinner1"==t.spinner||"spinner2"==t.spinner)&&e.find(".esg-loader").css({backgroundColor:t.spinnerColor}),("spinner3"==t.spinner||"spinner4"==t.spinner)&&e.find(".bounce1, .bounce2, .bounce3, .dot1, .dot2").css({backgroundColor:t.spinnerColor}),void(a||punchgs.TweenLite.to(e,.3,{minHeight:"100px",zIndex:0})))}function setKeyToNull(e,t){jQuery.each(e.loadMoreItems,function(a,o){o[0]==t&&(e.loadMoreItems[a][0]=-1,e.loadMoreItems[a][2]="already loaded")})}function loadMoreEmpty(e){for(var t=!0,a=0;a<e.loadMoreItems.length;a++)-1!=e.loadMoreItems[a][0]&&(t=!1);return t}function loadMoreItems(e,t){var a=checkMoreToLoad(e,t),o=new Array;jQuery.each(a,function(e,a){o.length<t.loadMoreAmount&&(o.push(a[0]),setKeyToNull(t,a[0]))});var i=checkMoreToLoad(e,t).length;if(o.length>0){var r=e.find(".esg-loadmore");r.length>0&&(punchgs.TweenLite.to(r,.4,{autoAlpha:.2}),r.data("loading",1));var n={action:t.loadMoreAjaxAction,client_action:"load_more_items",token:t.loadMoreAjaxToken,data:o,gridid:t.gridID};jQuery.ajax({type:"post",url:t.loadMoreAjaxUrl,dataType:"json",data:n}).success(function(a){if(a.success){var o=jQuery(a.data);jQuery(t.filterGroupClass+".esg-filter-wrapper.eg-search-wrapper .eg-forcefilter").length>0&&o.addClass("cat-searchresult"),e.find("ul").first().append(o),checkAvailableFilters(e,t),prepareItemsInGrid(t,!0),setItemsOnPages(t),setTimeout(function(){if(t.animDelay="off",organiseGrid(t),prepareSortingAndOrders(e),loadMoreEmpty(t))e.find(".esg-loadmore").remove();else{var a=e.find(".esg-loadmore"),o=t.loadMoreTxt+" ("+i+")";"off"==t.loadMoreNr&&(o=t.loadMoreTxt),a.html(0==i?t.loadMoreEndTxt:o),a.length>0&&(punchgs.TweenLite.to(a,.4,{autoAlpha:1,overwrite:"all"}),a.data("loading",0))}setTimeout(function(){t.animDelay="on"},500)},10)}}).error(function(t,a){e.find(".esg-loadmore").html("FAILURE: "+a)})}else loadMoreEmpty(t)?e.find(".esg-loadmore").remove():e.find(".esg-loadmore").html(t.loadMoreEndTxt)}function killOldCustomAjaxContent(e){var t=e.data("lastposttype"),a=e.data("oldajaxsource"),o=e.data("oldajaxtype"),i=e.data("oldajaxvideoaspect"),r=e.data("oldselector");if(t!=undefined&&""!=t)try{jQuery.each(jQuery.fn.tpessential.defaults.ajaxTypes,function(n,s){s!=undefined&&s.type!=undefined&&s.type==t&&s.killfunc!=undefined&&setTimeout(function(){s.killfunc.call(this,{id:a,type:o,aspectratio:i,selector:r})&&e.empty()},250)})}catch(n){console.log(n)}e.data("lastposttype","")}function addAjaxNavigagtion(e,t){function a(e){var t=new Array;return jQuery.each(e,function(e,a){jQuery(a).closest(".itemtoshow.isvisiblenow").length>0&&t.push(a)}),t}var o=" eg-acp-"+e.ajaxClosePosition;o=o+" eg-acp-"+e.ajaxCloseStyle,o=o+" eg-acp-"+e.ajaxCloseType,loc="eg-icon-left-open-1",roc="eg-icon-right-open-1",xoc='<i class="eg-icon-cancel"></i>',"type1"==e.ajaxCloseType&&(loc="eg-icon-left-open-big",roc="eg-icon-right-open-big",e.ajaxCloseTxt="",xoc="X"),("true"==e.ajaxCloseInner||1==e.ajaxCloseInner)&&(o+=" eg-acp-inner");var i='<div class="eg-ajax-closer-wrapper'+o+'">';switch("tr"==e.ajaxClosePosition||"br"==e.ajaxClosePosition?("on"==e.ajaxNavButton&&(i=i+'<div class="eg-ajax-left eg-ajax-navbt"><i class="'+loc+'"></i></div><div class="eg-ajax-right eg-ajax-navbt"><i class="'+roc+'"></i></div>'),"on"==e.ajaxCloseButton&&(i=i+'<div class="eg-ajax-closer eg-ajax-navbt">'+xoc+e.ajaxCloseTxt+"</div>")):("on"==e.ajaxCloseButton&&(i=i+'<div class="eg-ajax-closer eg-ajax-navbt">'+xoc+e.ajaxCloseTxt+"</div>"),"on"==e.ajaxNavButton&&(i=i+'<div class="eg-ajax-left eg-ajax-navbt"><i class="'+loc+'"></i></div><div class="eg-ajax-right eg-ajax-navbt"><i class="'+roc+'"></i></div>')),i+="</div>",e.ajaxClosePosition){case"tl":case"tr":case"t":t.prepend(i);break;case"bl":case"br":case"b":t.append(i)}t.find(".eg-ajax-closer").click(function(){showHideAjaxContainer(t,!1,null,null,.25,!0)}),t.find(".eg-ajax-right").click(function(){var e=t.data("container").find(".lastclickedajax").closest("li"),o=e.nextAll().find(".eg-ajax-a-button"),i=e.prevAll().find(".eg-ajax-a-button");o=a(o),i=a(i),o.length>0?o[0].click():i[0].click()}),t.find(".eg-ajax-left").click(function(){var e=t.data("container").find(".lastclickedajax").closest("li"),o=e.nextAll().find(".eg-ajax-a-button"),i=e.prevAll().find(".eg-ajax-a-button");o=a(o),i=a(i),i.length>0?i[i.length-1].click():o[o.length-1].click()})}function showHideAjaxContainer(e,t,a,o,i,r){i=i==undefined?.25:i;var n=e.data("container").data("opt"),s=e.data("lastheight")!=undefined?e.data("lastheight"):"100px";t?(i+=1.2,addAjaxNavigagtion(n,e),punchgs.TweenLite.set(e,{height:"auto"}),punchgs.TweenLite.set(e.parent(),{minHeight:0,maxHeight:"none",height:"auto",overwrite:"all"}),punchgs.TweenLite.from(e,i,{height:s,ease:punchgs.Power3.easeInOut,onStart:function(){punchgs.TweenLite.to(e,i,{autoAlpha:1,ease:punchgs.Power3.easeOut})},onComplete:function(){e.data("lastheight",e.height()),jQuery(window).trigger("resize.essg"),0==e.find(".eg-ajax-closer-wrapper").length&&addAjaxNavigagtion(n,e)}}),"off"!=n.ajaxScrollToOnLoad&&jQuery("html, body").animate({scrollTop:e.offset().top-o},{queue:!1,speed:.5})):(r&&(killOldCustomAjaxContent(e),s="0px"),punchgs.TweenLite.to(e.parent(),i,{height:s,ease:punchgs.Power2.easeInOut,onStart:function(){punchgs.TweenLite.to(e,i,{autoAlpha:0,ease:punchgs.Power3.easeOut})},onComplete:function(){setTimeout(function(){r&&e.html("")},300)}}))}function removeLoader(e){e.closest(".eg-ajaxanimwrapper").find(".esg-loader").remove()}function ajaxCallBack(opt,a){if(opt.ajaxCallback==undefined||""==opt.ajaxCallback||opt.ajaxCallback.length<3)return!1;var splitter=opt.ajaxCallback.split(")"),splitter=splitter[0].split("("),callback=splitter[0],arguments=splitter.length>1&&""!=splitter[1]?splitter[1]+",":"",obj=new Object;try{obj.containerid="#"+opt.ajaxContentTarget,obj.postsource=a.data("ajaxsource"),obj.posttype=a.data("ajaxtype"),eval("on"==opt.ajaxCallbackArgument?callback+"("+arguments+"obj)":callback+"("+arguments+")")}catch(e){console.log("Callback Error"),console.log(e)}}function loadMoreContent(e,t,a){e.find(".lastclickedajax").removeClass("lastclickedajax"),a.addClass("lastclickedajax");var o=jQuery("#"+t.ajaxContentTarget).find(".eg-ajax-target"),i=a.data("ajaxsource"),r=a.data("ajaxtype"),n=a.data("ajaxvideoaspect");if(o.data("container",e),n="16:9"==n?"widevideo":"normalvideo",showHideAjaxContainer(o,!1),o.length>0)switch(t.ajaxJsUrl!=undefined&&""!=t.ajaxJsUrl&&t.ajaxJsUrl.length>3&&jQuery.getScript(t.ajaxJsUrl).done(function(){t.ajaxJsUrl=""}).fail(function(){console.log("Loading Error on Ajax jQuery File. Please doublecheck if JS File and Path exist:"+t.ajaxJSUrl),t.ajaxJsUrl=""}),t.ajaxCssUrl!=undefined&&""!=t.ajaxCssUrl&&t.ajaxCssUrl.length>3&&(jQuery("<link>").appendTo("head").attr({type:"text/css",rel:"stylesheet"}).attr("href",t.ajaxCssUrl),""==t.ajaxCssUrl),buildLoader(o.closest(".eg-ajaxanimwrapper"),t),o.data("ajaxload")!=undefined&&o.data("ajaxload").abort(),killOldCustomAjaxContent(o),r){case"postid":var s={action:t.loadMoreAjaxAction,client_action:"load_more_content",token:t.loadMoreAjaxToken,postid:i};setTimeout(function(){o.data("ajaxload",jQuery.ajax({type:"post",url:t.loadMoreAjaxUrl,dataType:"json",data:s})),o.data("ajaxload").success(function(e){e.success&&(jQuery(o).html(e.data),showHideAjaxContainer(o,!0,t.ajaxScrollToOnLoad,t.ajaxScrollToOffset),removeLoader(o),ajaxCallBack(t,a))}),o.data("ajaxload").error(function(e,t){"abort"!=t&&(jQuery(o).append("<p>FAILURE: <strong>"+t+"</strong></p>"),removeLoader(o))})},300);break;case"youtubeid":setTimeout(function(){o.html('<div class="eg-ajax-video-container '+n+'"><iframe width="560" height="315" src="//www.youtube.com/embed/'+i+'?autoplay=1&vq=hd1080" frameborder="0" allowfullscreen></iframe></div>'),removeLoader(o),showHideAjaxContainer(o,!0,t.ajaxScrollToOnLoad,t.ajaxScrollToOffset),ajaxCallBack(t,a)},300);break;case"vimeoid":setTimeout(function(){o.html('<div class="eg-ajax-video-container '+n+'"><iframe src="//player.vimeo.com/video/'+i+'?portrait=0&autoplay=1" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div>'),removeLoader(o),showHideAjaxContainer(o,!0,t.ajaxScrollToOnLoad,t.ajaxScrollToOffset),ajaxCallBack(t,a)},300);break;case"wistiaid":setTimeout(function(){o.html('<div class="eg-ajax-video-container '+n+'"><iframe src="//fast.wistia.net/embed/iframe/'+i+'"allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" allowfullscreen mozallowfullscreen webkitallowfullscreen oallowfullscreen msallowfullscreen width="640" height="388"></iframe></div>'),removeLoader(o),showHideAjaxContainer(o,!0,t.ajaxScrollToOnLoad,t.ajaxScrollToOffset),ajaxCallBack(t,a)},300);break;case"html5vid":i=i.split("|"),setTimeout(function(){o.html('<video autoplay="true" loop="" class="rowbgimage" poster="" width="100%" height="auto"><source src="'+i[0]+'" type="video/mp4"><source src="'+i[1]+'" type="video/webm"><source src="'+i[2]+'" type="video/ogg"></video>'),removeLoader(o),showHideAjaxContainer(o,!0,t.ajaxScrollToOnLoad,t.ajaxScrollToOffset),ajaxCallBack(t,a)},300);break;case"soundcloud":case"soundcloudid":setTimeout(function(){o.html('<iframe width="100%" height="250" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/'+i+'&auto_play=true&hide_related=false&show_comments=true&show_user=true&show_reposts=false&visual=true"></iframe>'),removeLoader(o),showHideAjaxContainer(o,!0,t.ajaxScrollToOnLoad,t.ajaxScrollToOffset),ajaxCallBack(t,a)},300);break;case"imageurl":setTimeout(function(){var e=new Image;e.onload=function(){var e=jQuery(this);o.html(""),e.css({width:"100%",height:"auto"}),o.append(jQuery(this)),removeLoader(o),showHideAjaxContainer(o,!0,t.ajaxScrollToOnLoad,t.ajaxScrollToOffset),ajaxCallBack(t,a)},e.onerror=function(){o.html("Error"),removeLoader(o),showHideAjaxContainer(o,!0,t.ajaxScrollToOnLoad,t.ajaxScrollToOffset)},e.src=i},300);break;default:jQuery.each(jQuery.fn.tpessential.defaults.ajaxTypes,function(e,a){a.openAnimationSpeed==undefined&&(a.openAnimationSpeed=0),a!=undefined&&a.type!=undefined&&a.type==r&&setTimeout(function(){o.data("lastposttype",r),o.data("oldajaxsource",i),o.data("oldajaxtype",r),o.data("oldajaxvideoaspect",n),o.data("oldselector","#"+t.ajaxContentTarget+" .eg-ajax-target"),showHideAjaxContainer(o,!0,t.ajaxScrollToOnLoad,t.ajaxScrollToOffset,0),o.html(a.func.call(this,{id:i,type:r,aspectratio:n})),removeLoader(o)},300)})}}function resetFiltersFromCookies(e,t,a){if("on"==e.cookies.filter){var o=a!==undefined?a:readCookie("grid_"+e.girdID+"_filters");if(o!==undefined&&null!==o&&o.length>0){var i=0;jQuery.each(o.split(","),function(a,o){o!==undefined&&-1!==o&&"-1"!==o&&jQuery(e.filterGroupClass+".esg-filterbutton,"+e.filterGroupClass+" .esg-filterbutton").each(function(){jQuery(this).data("fid")!=o&&parseInt(jQuery(this).data("fid"),0)!==parseInt(o,0)||jQuery(this).hasClass("esg-pagination-button")||(t?jQuery(this).click():jQuery(this).addClass("selected"),i++)})}),i>0&&jQuery(e.filterGroupClass+".esg-filterbutton.esg-allfilter,"+e.filterGroupClass+" .esg-filterbutton.esg-allfilter").removeClass("selected")}}}function resetPaginationFromCookies(e,t){if("on"===e.cookies.pagination){var a=t!==undefined?t:readCookie("grid_"+e.girdID+"_pagination");a!==undefined&&null!==a&&a.length>0&&jQuery(e.filterGroupClass+".esg-filterbutton.esg-pagination-button,"+e.filterGroupClass+" .esg-filterbutton.esg-pagination-button").each(function(){parseInt(jQuery(this).data("page"),0)!==parseInt(a,0)||jQuery(this).hasClass("selected")||jQuery(this).click()})}}function resetSearchFromCookies(e){if("on"===e.cookies.search){var t=readCookie("grid_"+e.gridID+"_search");t!==undefined&&null!=t&&t.length>0&&(jQuery(e.filterGroupClass+".eg-search-wrapper .eg-search-input").val(t).trigger("change"),e.cookies.searchjusttriggered=!0)}}function mainPreparing(e,t){function a(){if(1==t.lastsearchtimer)return!1;t.lastsearchtimer=1,buildLoader(jQuery(t.filterGroupClass+".eg-search-wrapper"),{spinner:"spinner3",spinnerColor:"#fff"},!0),punchgs.TweenLite.fromTo(jQuery(t.filterGroupClass+".eg-search-wrapper").find(".esg-loader"),.3,{autoAlpha:0},{autoAlpha:1,ease:punchgs.Power3.easeInOut});var a=jQuery(t.filterGroupClass+".eg-search-wrapper .eg-search-input"),o=a.val(),i=jQuery(t.filterGroupClass+".eg-search-wrapper.esg-filter-wrapper .hiddensearchfield");if(a.attr("disabled","true"),o.length>0){a.trigger("searchstarting");var r={search:o,id:t.gridID},n={action:t.loadMoreAjaxAction,client_action:"get_grid_search_ids",token:t.loadMoreAjaxToken,data:r};jQuery.ajax({type:"post",url:t.loadMoreAjaxUrl,dataType:"json",data:n}).success(function(a){if("on"===t.cookies.search&&createCookie("grid_"+t.gridID+"_search",o,t.cookies.timetosave*(1/60/60)),t.cookies.searchjusttriggered===!0){var r=readCookie("grid_"+t.girdID+"_pagination"),n=readCookie("grid_"+t.girdID+"_filters");setTimeout(function(){resetFiltersFromCookies(t,!0,n),resetPaginationFromCookies(t,r)},200),t.cookies.searchjusttriggered=!1}setTimeout(function(){t.lastsearchtimer=0,jQuery(t.filterGroupClass+".eg-search-wrapper .eg-search-input").attr("disabled",!1),punchgs.TweenLite.to(jQuery(t.filterGroupClass+".eg-search-wrapper").find(".esg-loader"),.5,{autoAlpha:1,ease:punchgs.Power3.easeInOut,onComplete:function(){jQuery(t.filterGroupClass+".eg-search-wrapper").find(".esg-loader").remove()}}),jQuery(t.filterGroupClass+".eg-search-wrapper .eg-search-input").trigger("searchended")},1e3);var s=new Array;a&&jQuery.each(a,function(e,t){t!=undefined&&jQuery.isNumeric(t)&&s.push(t)}),e.find(".cat-searchresult").removeClass("cat-searchresult");var l=0;jQuery.each(t.loadMoreItems,function(e,t){t[2]="notsearched",jQuery.each(s,function(e,a){return parseInt(t[0],0)===parseInt(a,0)&&-1!=parseInt(t[0],0)?(t[2]="cat-searchresult",l++,!1):void 0})}),jQuery.each(s,function(t,a){e.find(".eg-post-id-"+a).addClass("cat-searchresult")}),i.addClass("eg-forcefilter").addClass("eg-justfilteredtosearch"),jQuery(t.filterGroupClass+".esg-filter-wrapper .esg-allfilter").trigger("click")}).error(function(e,a){console.log("FAILURE: "+a),setTimeout(function(){t.lastsearchtimer=0,jQuery(t.filterGroupClass+".eg-search-wrapper .eg-search-input").attr("disabled",!1),punchgs.TweenLite.to(jQuery(t.filterGroupClass+".eg-search-wrapper").find(".esg-loader"),.5,{autoAlpha:1,ease:punchgs.Power3.easeInOut,onComplete:function(){jQuery(t.filterGroupClass+".eg-search-wrapper").find(".esg-loader").remove()}}),jQuery(t.filterGroupClass+".eg-search-wrapper .eg-search-input").trigger("searchended")},1e3)})}else{jQuery.each(t.loadMoreItems,function(e,t){t[2]="notsearched"}),e.find(".cat-searchresult").removeClass("cat-searchresult");var i=jQuery(t.filterGroupClass+".eg-search-wrapper.esg-filter-wrapper .hiddensearchfield");i.removeClass("eg-forcefilter").addClass("eg-justfilteredtosearch"),"on"===t.cookies.search&&createCookie("grid_"+t.gridID+"_search","",-1),jQuery(t.filterGroupClass+".esg-filter-wrapper .esg-allfilter").trigger("click"),setTimeout(function(){t.lastsearchtimer=0,jQuery(t.filterGroupClass+".eg-search-wrapper .eg-search-input").attr("disabled",!1),punchgs.TweenLite.to(jQuery(t.filterGroupClass+".eg-search-wrapper").find(".esg-loader"),.5,{autoAlpha:1,ease:punchgs.Power3.easeInOut,onComplete:function(){jQuery(t.filterGroupClass+".eg-search-wrapper").find(".esg-loader").remove()}}),jQuery(t.filterGroupClass+".eg-search-wrapper .eg-search-input").trigger("searchended")},1e3)}}resetFiltersFromCookies(t);var o=e.find(".eg-leftright-container"),i=getBestFitColumn(t,jQuery(window).width(),"id");if(t.column=i.column,t.columnindex=i.index,prepareItemsInGrid(t),organiseGrid(t),jQuery(t.filterGroupClass+".eg-search-wrapper").length>0){var r=t.filterGroupClass.replace(".",""),n="Search Result",s=jQuery(t.filterGroupClass+".eg-search-wrapper .eg-search-submit"),l=jQuery(t.filterGroupClass+".eg-search-wrapper .eg-search-clean");jQuery(t.filterGroupClass+".esg-filter-wrapper.eg-search-wrapper").append('<div style="display:none !important" class="esg-filterbutton hiddensearchfield '+r+'" data-filter="cat-searchresult"><span>'+n+"</span></div>"),t.lastsearchtimer=0,s.click(a),jQuery(t.filterGroupClass+".eg-search-wrapper .eg-search-input").on("change",a),l.click(function(){"on"===t.cookies.search&&createCookie("grid_"+t.gridID+"_search","",-1),jQuery.each(t.loadMoreItems,function(e,t){t[2]="notsearched"}),e.find(".cat-searchresult").removeClass("cat-searchresult");var a=jQuery(t.filterGroupClass+".eg-search-wrapper.esg-filter-wrapper .hiddensearchfield");jQuery(t.filterGroupClass+".eg-search-wrapper .eg-search-input").val(""),a.removeClass("eg-forcefilter").addClass("eg-justfilteredtosearch"),jQuery(t.filterGroupClass+".esg-filter-wrapper .esg-allfilter").trigger("click"),setTimeout(function(){t.lastsearchtimer=0,jQuery(t.filterGroupClass+".eg-search-wrapper .eg-search-input").attr("disabled",!1),punchgs.TweenLite.to(jQuery(t.filterGroupClass+".eg-search-wrapper").find(".esg-loader"),.5,{autoAlpha:1,ease:punchgs.Power3.easeInOut,onComplete:function(){jQuery(t.filterGroupClass+".eg-search-wrapper").find(".esg-loader").remove()}}),jQuery(t.filterGroupClass+".eg-search-wrapper .eg-search-input").trigger("searchended")},1e3)})}addCountSuffix(e,t),jQuery(t.filterGroupClass+".esg-filter-wrapper,"+t.filterGroupClass+" .esg-filter-wrapper").each(function(){var e=jQuery(this);e.hasClass("dropdownstyle")&&(e.find(".esg-filter-checked").each(function(){jQuery(this).prependTo(jQuery(this).parent())}),is_mobile()?e.find(".esg-selected-filterbutton").click(function(){var t=e.find(".esg-selected-filterbutton");t.hasClass("hoveredfilter")?(setTimeout(function(){t.removeClass("hoveredfilter")},200),e.find(".esg-dropdown-wrapper").stop().hide()):(setTimeout(function(){t.addClass("hoveredfilter")},200),e.find(".esg-dropdown-wrapper").stop().show())}):"click"==t.showDropFilter?(e.click(function(){var e=jQuery(this).closest(".esg-filter-wrapper");e.find(".esg-selected-filterbutton").addClass("hoveredfilter"),e.find(".esg-dropdown-wrapper").stop().show()}),e.on("mouseleave",function(){var e=jQuery(this).closest(".esg-filter-wrapper");e.find(".esg-selected-filterbutton").removeClass("hoveredfilter"),e.find(".esg-dropdown-wrapper").stop().hide()})):e.hover(function(){var e=jQuery(this).closest(".esg-filter-wrapper");e.find(".esg-selected-filterbutton").addClass("hoveredfilter"),e.find(".esg-dropdown-wrapper").stop().show()},function(){var e=jQuery(this).closest(".esg-filter-wrapper");e.find(".esg-selected-filterbutton").removeClass("hoveredfilter"),e.find(".esg-dropdown-wrapper").stop().hide()}))}),jQuery("body").on("click",t.filterGroupClass+".esg-left,"+t.filterGroupClass+" .esg-left",function(){t=getOptions(e),t.oldpage=t.currentpage,t.currentpage--,t.currentpage<0&&(t.currentpage=t.realmaxpage-1);var a=getBestFitColumn(t,jQuery(window).width(),"id");t.column=a.column,t.columnindex=a.index,setItemsOnPages(t),organiseGrid(t,"right"),setOptions(e,t),stopAllVideos(!0)}),jQuery("body").on("click",t.filterGroupClass+".esg-right,"+t.filterGroupClass+" .esg-right",function(){t=getOptions(e),t.oldpage=t.currentpage,t.currentpage++,t.currentpage>=t.realmaxpage&&(t.currentpage=0);var a=getBestFitColumn(t,jQuery(window).width(),"id");t.column=a.column,t.columnindex=a.index,setItemsOnPages(t),organiseGrid(t,"right"),setOptions(e,t),stopAllVideos(!0)}),jQuery(t.filterGroupClass+".esg-filterbutton, "+t.filterGroupClass+" .esg-filterbutton").each(function(){jQuery(this).hasClass("esg-pagination-button")||jQuery(this).click(function(){var t=getOptions(e);stopAllVideos(!0);var a=jQuery(this);a.hasClass("esg-pagination-button")||(jQuery(t.filterGroupClass+".esg-allfilter, "+t.filterGroupClass+" .esg-allfilter").removeClass("selected"),a.hasClass("esg-allfilter")&&jQuery(t.filterGroupClass+".esg-filterbutton, "+t.filterGroupClass+" .esg-filterbutton").each(function(){jQuery(this).removeClass("selected")})),a.closest(".esg-filters").hasClass("esg-singlefilters")||"single"==t.filterType?(jQuery(t.filterGroupClass+".esg-filterbutton, "+t.filterGroupClass+" .esg-filterbutton").each(function(){jQuery(this).removeClass("selected")}),a.addClass("selected")):a.hasClass("selected")?a.removeClass("selected"):a.addClass("selected");var o=jQuery(t.filterGroupClass+".esg-filter-wrapper .hiddensearchfield");o.hasClass("eg-forcefilter")&&o.addClass("selected");var i=0,r="";jQuery(t.filterGroupClass+".esg-filterbutton.selected,"+t.filterGroupClass+" .esg-filterbutton.selected").each(function(){jQuery(this).hasClass("selected")&&!jQuery(this).hasClass("esg-pagination-button")&&(i++,r=0===i?jQuery(this).data("fid"):r+","+jQuery(this).data("fid"))}),"on"===t.cookies.filter&&t.cookies.searchjusttriggered!==!0&&createCookie("grid_"+t.girdID+"_filters",r,t.cookies.timetosave*(1/60/60)),0==i&&jQuery(t.filterGroupClass+".esg-allfilter,"+t.filterGroupClass+" .esg-allfilter").addClass("selected"),t.filterchanged=!0,t.currentpage=0,1==t.maxpage?(jQuery(t.filterGroupClass+".navigationbuttons,"+t.filterGroupClass+" .navigationbuttons").css({display:"none"}),jQuery(t.filterGroupClass+".esg-pagination,"+t.filterGroupClass+" .esg-pagination").css({display:"none"})):(jQuery(t.filterGroupClass+".navigationbuttons,"+t.filterGroupClass+" .navigationbuttons").css({display:"inline-block"}),jQuery(t.filterGroupClass+".esg-pagination,"+t.filterGroupClass+" .esg-pagination").css({display:"inline-block"}));var n=e.find(".esg-loadmore");if(n.length>0){var s=checkMoreToLoad(e,t).length;n.html(s>0?"off"==t.loadMoreNr?t.loadMoreTxt:t.loadMoreTxt+" ("+s+")":t.loadMoreEndTxt)}setItemsOnPages(t),organiseGrid(t),setOptions(e,t)})});var u;jQuery(window).on("resize.essg",function(){if(clearTimeout(u),"on"==t.forceFullWidth||"on"==t.forceFullScreen){var a=e.parent().parent().find(".esg-relative-placeholder").offset().left;e.closest(".esg-container-fullscreen-forcer").css({left:0-a,width:jQuery(window).width()})}else e.closest(".esg-container-fullscreen-forcer").css({left:0,width:"auto"});if(o.length>0){var i=o.outerWidth(!0);punchgs.TweenLite.set(e.find(".esg-overflowtrick"),{width:e.width()-i,overwrite:"all"})}var r=getBestFitColumn(t,jQuery(window).width(),"id");t.column=r.column,t.columnindex=r.index,setOptions(e,t),u=setTimeout(function(){t=getOptions(e),setItemsOnPages(t),organiseGrid(t),setOptions(e,t)},200)}),e.on("itemsinposition",function(){var e=jQuery(this),t=getOptions(e),a=e.find(".eg-leftright-container");if(clearTimeout(e.data("callednow")),t.maxheight>0&&t.maxheight<9999999999){t.inanimation=!1;var o=e.find(".esg-overflowtrick").first(),i=e.find("ul").first(),a=e.find(".eg-leftright-container"),r=parseInt(o.css("paddingTop"),0);r=r==undefined?0:r,r=null==r?0:r;var n=parseInt(o.css("paddingBottom"),0);n=n==undefined?0:n,n=null==n?0:n;var s=t.maxheight+t.overflowoffset+r+n;if("on"==t.forceFullScreen){var l=jQuery(window).height();if(t.fullScreenOffsetContainer!=undefined)try{var u=t.fullScreenOffsetContainer.split(",");jQuery.each(u,function(e,a){l-=jQuery(a).outerHeight(!0),l<t.minFullScreenHeight&&(l=t.minFullScreenHeight)})}catch(d){}s=l}var c=.3;i.height()<200&&(c=1),punchgs.TweenLite.to(i,c,{force3D:"auto",height:s,ease:punchgs.Power3.easeInOut,clearProps:"transform"}),punchgs.TweenLite.to(o,c,{force3D:!0,height:s,ease:punchgs.Power3.easeInOut,clearProps:"transform",onComplete:function(){e.closest(".eg-grid-wrapper, .myportfolio-container").css({height:"auto"}).removeClass("eg-startheight")}}),a.length>0&&punchgs.TweenLite.set(a,{minHeight:s,ease:punchgs.Power3.easeInOut});var p=jQuery(t.filterGroupClass+".esg-navbutton-solo-left,"+t.filterGroupClass+" .esg-navbutton-solo-left"),h=jQuery(t.filterGroupClass+".esg-navbutton-solo-right,"+t.filterGroupClass+" .esg-navbutton-solo-right");p.length>0&&p.css({marginTop:0-p.height()/2}),h.length>0&&h.css({marginTop:0-h.height()/2})}else if(0==t.maxheight){var o=e.find(".esg-overflowtrick").first(),i=e.find("ul").first();punchgs.TweenLite.to(i,1,{force3D:"auto",height:0,ease:punchgs.Power3.easeInOut,clearProps:"transform"}),punchgs.TweenLite.to(o,1,{force3D:!0,height:0,ease:punchgs.Power3.easeInOut,clearProps:"transform"})}e.data("callednow",setTimeout(function(){e.find(".itemtoshow.isvisiblenow").each(function(){hideUnderElems(jQuery(this))})},250)),t.firstLoadFinnished===undefined&&(e.trigger("essential_grid_ready_to_use"),resetSearchFromCookies(t),resetPaginationFromCookies(t),t.firstLoadFinnished=!0)}),prepareSortingAndOrders(e),prepareSortingClicks(e)}function prepareSortingAndOrders(e){var t=getOptions(e);e.find(".tp-esg-item").each(function(){var e=new Date(jQuery(this).data("date"));jQuery(this).data("date",e.getTime()/1e3)}),jQuery(t.filterGroupClass+".esg-sortbutton-order,"+t.filterGroupClass+" .esg-sortbutton-order").each(function(){var e=jQuery(this);e.removeClass("tp-desc").addClass("tp-asc"),e.data("dir","asc")})}function prepareSortingClicks(e){opt=getOptions(e);var t;jQuery(opt.filterGroupClass+".esg-sortbutton-wrapper .esg-sortbutton-order,"+opt.filterGroupClass+" .esg-sortbutton-wrapper .esg-sortbutton-order").click(function(){var a=jQuery(this);a.hasClass("tp-desc")?(a.removeClass("tp-desc").addClass("tp-asc"),a.data("dir","asc")):(a.removeClass("tp-asc").addClass("tp-desc"),a.data("dir","desc"));var o=a.data("dir");stopAllVideos(!0,!0),jQuery(opt.filterGroupClass+".esg-sorting-select,"+opt.filterGroupClass+" .esg-sorting-select").each(function(){var a=jQuery(this).val();clearTimeout(t),e.find(".tp-esg-item").tsort({data:a,forceStrings:"false",order:o}),t=setTimeout(function(){opt=getOptions(e),setItemsOnPages(opt),organiseGrid(opt),setOptions(e,opt)},200)})}),jQuery(opt.filterGroupClass+".esg-sorting-select,"+opt.filterGroupClass+" .esg-sorting-select").each(function(){var a=jQuery(this);a.change(function(){e.find("iframe").css({visibility:"hidden"}),e.find(".video-eg").css({visibility:"hidden"});var o=jQuery(this).closest(".esg-sortbutton-wrapper").find(".esg-sortbutton-order"),i=a.val(),r=a.find("option:selected").text(),n=o.data("dir");stopAllVideos(!0,!0),clearTimeout(t),a.parent().parent().find(".sortby_data").text(r),e.find(".tp-esg-item").tsort({data:i,forceStrings:"false",order:n}),t=setTimeout(function(){opt=getOptions(e),setItemsOnPages(opt),organiseGrid(opt),setOptions(e,opt)},500)})})}function fixCenteredCoverElement(e,t,a){if(t==undefined&&(t=e.find(".esg-entry-cover")),a==undefined&&(a=e.find(".esg-entry-media")),t&&a){var o=a.height();punchgs.TweenLite.set(t,{height:o});var i=e.find(".esg-cc");punchgs.TweenLite.set(i,{top:(o-i.height())/2})}}function getBestFitColumn(e,t,a){var o=t,i=0,r=9999,n=0,s=e.column,l=(e.column,e.column),u=0,d=0;e.responsiveEntries!=undefined&&e.responsiveEntries.length>0&&jQuery.each(e.responsiveEntries,function(e,t){var a=t.width!=undefined?t.width:0,c=t.amount!=undefined?t.amount:0;r>a&&(r=a,s=c,d=e),a>n&&(n=a,lamount=c),a>i&&o>=a&&(i=a,l=c,u=e)}),r>t&&(l=s,u=d);var c=new Object;return c.index=u,c.column=l,"id"==a?c:l}function getOptions(e){return e.data("opt")}function setOptions(e,t){e.data("opt",t)}function checkMediaListeners(e){e.find("iframe").each(function(){var e=jQuery(this);e.attr("src").toLowerCase().indexOf("youtube")>0?prepareYT(e):e.attr("src").toLowerCase().indexOf("vimeo")>0?prepareVimeo(e):e.attr("src").toLowerCase().indexOf("wistia")>0?prepareWs(e):e.attr("src").toLowerCase().indexOf("soundcloud")>0&&prepareSoundCloud(e)}),e.find("video").each(function(){prepareVideo(jQuery(this))})}function waitMediaListeners(e){var t=setInterval(function(){e.find("iframe").each(function(){var e=jQuery(this);e.attr("src").toLowerCase().indexOf("youtube")>0?prepareYT(e)&&clearInterval(t):e.attr("src").toLowerCase().indexOf("vimeo")>0?prepareVimeo(e)&&clearInterval(t):e.attr("src").toLowerCase().indexOf("wistia")>0?prepareWs(e)&&clearInterval(t):e.attr("src").toLowerCase().indexOf("soundcloud")>0?prepareSoundCloud(e)&&clearInterval(t):clearInterval(t)}),e.find("video").each(function(){prepareVideo(jQuery(this))&&clearInterval(t)})},50)}function directionPrepare(e,t,a,o,i){var r=new Object;switch(e){case 0:r.x=0,r.y="in"==t?0-o:10+o,r.y=i&&"in"==t?r.y-5:r.y;break;case 1:r.y=0,r.x="in"==t?a:-10-a,r.x=i&&"in"==t?r.x+5:r.x;break;case 2:r.y="in"==t?o:-10-o,r.x=0,r.y=i&&"in"==t?r.y+5:r.y;break;case 3:r.y=0,r.x="in"==t?0-a:10+a,r.x=i&&"in"==t?r.x-5:r.x}return r}function getDir(e,t){var a=e.width(),o=e.height(),i=(t.x-e.offset().left-a/2)*(a>o?o/a:1),r=(t.y-e.offset().top-o/2)*(o>a?a/o:1),n=Math.round((Math.atan2(r,i)*(180/Math.PI)+180)/90+3)%4;return n}function hideUnderElems(e){e.find(".eg-handlehideunder").each(function(){var t=jQuery(this),a=t.data("hideunder"),o=t.data("hideunderheight"),i=t.data("hidetype");t.data("knowndisplay")==undefined&&t.data("knowndisplay",t.css("display")),e.width()<a&&a!=undefined||e.height()<o&&o!=undefined?"visibility"==i?t.addClass("forcenotvisible"):"display"==i&&t.addClass("forcenotdisplay"):"visibility"==i?t.removeClass("forcenotvisible"):"display"==i&&t.removeClass("forcenotdisplay")
})}function offsetParrents(e,t){var a=t.closest(".mainul"),o=a.parent();if(t.position().top+t.height()>a.height()+40||0==e||0!=a.data("bh")&&a.data("bh")!=undefined&&t.position().top+t.height()>parseInt(a.data("bh"),0)+40){(a.data("bh")==undefined||0==a.data("bh"))&&a.data("bh",a.height()),(o.data("bh")==undefined||0==o.data("bh"))&&o.data("bh",o.height());var i=a.data("bh"),r=o.data("bh");0!=e?(a.data("alreadyinoff",!1),punchgs.TweenLite.to(a,.2,{height:i+e}),punchgs.TweenLite.to(o,.2,{height:r+e})):a.data("alreadyinoff")||(a.data("alreadyinoff",!0),punchgs.TweenLite.to(a,.3,{height:i,ease:punchgs.Power3.easeIn,onComplete:function(){a.data("bh",0),o.data("bh",0),a.data("alreadyinoff",!1)}}),punchgs.TweenLite.to(o,.3,{height:r,ease:punchgs.Power3.easeIn,onComplete:function(){a.data("bh",0),o.data("bh",0),a.data("alreadyinoff",!1)}}))}}function itemHoverAnim(e,t,a,o){1!=e.data("simplevideo")&&checkMediaListeners(e);var i=!1,r=e.find(".esg-media-poster");if(r.length>0&&0==r.css("opacity")&&(i=!0),e.find(".isplaying, .isinpause").length>0||i)return!1;clearTimeout(e.data("hovertimer"));var n=a.mainhoverdelay;"set"==t&&(n=0),e.data("hovertimer",setTimeout(function(){e.data("animstarted",1),punchgs.TweenLite.set(e,{z:.01,x:0,y:0,rotationX:0,rotationY:0,rotationZ:0}),e.addClass("esg-hovered");var i=e.find(".esg-entry-cover");if(punchgs.TweenLite.set(i,{transformStyle:"flat"}),"set"!=t&&fixCenteredCoverElement(e,i),e.find(".esg-entry-content").length>0&&"set"!=t&&"even"==a.layout){var r=e.data("pt"),n=e.data("pb"),s=e.data("pl"),l=e.data("pr"),u=e.data("bt"),d=e.data("bb"),c=e.data("bl"),p=e.data("br");if(e.data("hhh",e.outerHeight()),e.data("www",e.outerWidth()),punchgs.TweenLite.set(e.find(".esg-entry-content"),{display:"block"}),punchgs.TweenLite.set(e.find(".esg-entry-media"),{height:e.data("hhh")}),punchgs.TweenLite.set(e,{z:.1,zIndex:50,x:0-(s+l+p+c)/2,y:0-(r+n+u+d)/2,height:"auto",width:e.data("www")+s+l+c+p}),"on"==a.evenGridMasonrySkinPusher){var h=e.height()-e.data("hhh");offsetParrents(h,e)}e.css({paddingTop:r+"px",paddingLeft:s+"px",paddingRight:l+"px",paddingBottom:l+"px"}),e.css({borderTopWidth:u+"px",borderBottomWidth:d+"px",borderLeftWidth:c+"px",borderRightWidth:p+"px"}),1!=a.inanimation&&punchgs.TweenLite.set(e.closest(".esg-overflowtrick"),{overflow:"visible",overwrite:"all"})}jQuery.each(esgAnimmatrix,function(a,i){e.find(i[0]).each(function(){var a=jQuery(this),r=a.data("delay")!=undefined?a.data("delay"):0;animfrom=i[2],animto=i[3],animto.delay=r,animto.overwrite="all",animfrom.overwrite="all",animto.transformStyle="flat",animto.force3D=!0;var n=0,s=i[0].indexOf("out")>-1;a.hasClass("esg-entry-media")||s||(animto.clearProps="transform"),s&&(animfrom.clearProps="transform"),animto.z=.001,animfrom.transformPerspective==undefined&&(animfrom.transformPerspective=1e3),a.hasClass("esg-overlay")&&(animfrom.z==undefined&&(animfrom.z=-.002),animto.z=-1e-4);var l=a;if(a.hasClass("esg-entry-media")&&a.find(".esg-media-video").length>0)return!0;var u=punchgs.TweenLite.killTweensOf(l,!1);if("set"==t){var u=punchgs.TweenLite.set(l,animfrom);s&&u.eventCallback("onComplete",resetTransforms,[l])}else switch(i[0]){case".esg-shifttotop":animto.y=0-e.find(".esg-bc.eec").last().height();var u=punchgs.TweenLite.fromTo(a,.5,{y:0},{y:animto.y});break;case".esg-slide":var d=directionPrepare(o,"in",e.width(),e.height()),c=new Object,p=new Object;jQuery.extend(c,animfrom),jQuery.extend(p,animto),c.css.x=d.x,c.css.y=d.y;var u=punchgs.TweenLite.fromTo(l,i[1],c,p,n);break;case".esg-slideout":var d=directionPrepare(o,"out",e.width(),e.height()),c=new Object,p=new Object;jQuery.extend(c,animfrom),jQuery.extend(p,animto),p.x=d.x,p.y=d.y,p.clearProps="";var u=punchgs.TweenLite.fromTo(l,i[1],c,p,n);break;default:var u=punchgs.TweenLite.fromTo(l,i[1],animfrom,animto,n)}})})},n))}function videoClickEvent(e,t,a,o){e.css({transform:"none","-moz-transform":"none","-webkit-transform":"none"}),e.closest(".esg-overflowtrick").css({transform:"none","-moz-transform":"none","-webkit-transform":"none"}),e.closest("ul").css({transform:"none","-moz-transform":"none","-webkit-transform":"none"}),o||e.find(".esg-media-video").each(function(){var t=jQuery(this);if(t.data("youtube")!=undefined&&0==e.find(".esg-youtube-frame").length){var a=e.find(".esg-entry-media"),o="https://www.youtube.com/embed/"+t.data("youtube")+"?version=3&enablejsapi=1&html5=1&controls=1&autohide=1&rel=0&showinfo=0";a.append('<iframe class="esg-youtube-frame" wmode="Opaque" style="position:absolute;top:0px;left:0px;display:none" width="'+t.attr("width")+'" height="'+t.attr("height")+'" data-src="'+o+'" src="about:blank"></iframe>')}if(t.data("vimeo")!=undefined&&0==e.find(".esg-vimeo-frame").length){var a=e.find(".esg-entry-media"),i="http://player.vimeo.com/video/"+t.data("youtube")+"?title=0&byline=0&html5=1&portrait=0&api=1;";a.append('<iframe class="esg-vimeo-frame"  allowfullscreen="false" style="position:absolute;top:0px;left:0px;display:none" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen="" width="'+t.attr("width")+'" height="'+t.attr("height")+'" data-src="'+i+'" src="about:blank"></iframe>')}if(t.data("wistia")!=undefined&&0==e.find(".esg-wistia-frame").length){var a=e.find(".esg-entry-media"),r="https://fast.wistia.net/embed/iframe/"+t.data("wistia")+"?version=3&enablejsapi=1&html5=1&controls=1&autohide=1&rel=0&showinfo=0";a.append('<iframe class="esg-wistia-frame" wmode="Opaque" style="position:absolute;top:0px;left:0px;display:none" width="'+t.attr("width")+'" height="'+t.attr("height")+'" data-src="'+r+'" src="about:blank"></iframe>')}if(t.data("soundcloud")!=undefined&&0==e.find(".esg-soundcloud-frame").length){var a=e.find(".esg-entry-media"),n="https://w.soundcloud.com/player/?url=https://api.soundcloud.com/tracks/"+t.data("soundcloud")+"&auto_play=false&hide_related=false&visual=true&show_artwork=true";a.append('<iframe class="esg-soundcloud-frame" allowfullscreen="false" style="position:absolute;top:0px;left:0px;display:none" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen="" width="'+t.attr("width")+'" height="'+t.attr("height")+'" scrolling="no" frameborder="no" data-src="'+n+'" src="about:blank"></iframe>')}if((t.data("mp4")!=undefined||t.data("webm")!=undefined||t.data("ogv")!=undefined)&&0==e.find(".esg-video-frame").length){var a=e.find(".esg-entry-media");a.append('<video class="esg-video-frame" style="position:absolute;top:0px;left:0px;display:none" width="'+t.attr("width")+'" height="'+t.attr("height")+'" data-origw="'+t.attr("width")+'" data-origh="'+t.attr("height")+'" ></video'),t.data("mp4")!=undefined&&a.find("video").append('<source src="'+t.data("mp4")+'" type="video/mp4" />'),t.data("webm")!=undefined&&a.find("video").append('<source src="'+t.data("webm")+'" type="video/webm" />'),t.data("ogv")!=undefined&&a.find("video").append('<source src="'+t.data("ogv")+'" type="video/ogg" />')}}),adjustMediaSize(e,!0,null,a);var i=e.find(".esg-youtube-frame");0==i.length&&(i=e.find(".esg-vimeo-frame")),0==i.length&&(i=e.find(".esg-wistia-frame")),0==i.length&&(i=e.find(".esg-soundcloud-frame")),0==i.length&&(i=e.find(".esg-video-frame"));var r=e.find(".esg-entry-cover"),n=e.find(".esg-media-poster");if("about:blank"==i.attr("src")){i.attr("src",i.data("src")),loadVideoApis(t,a),o||punchgs.TweenLite.set(i,{opacity:0,display:"block"});var s=setInterval(function(){i.attr("src").toLowerCase().indexOf("youtube")>0?1==prepareYT(i)&&(clearInterval(s),o||(is_mobile()?(punchgs.TweenLite.set(i,{autoAlpha:1}),punchgs.TweenLite.set(n,{autoAlpha:0}),punchgs.TweenLite.set(r,{autoAlpha:0})):(punchgs.TweenLite.to(i,.5,{autoAlpha:1}),punchgs.TweenLite.to(n,.5,{autoAlpha:0}),punchgs.TweenLite.to(r,.5,{autoAlpha:0}),playYT(i,o)))):i.attr("src").toLowerCase().indexOf("vimeo")>0?1==prepareVimeo(i)&&(clearInterval(s),o||(is_mobile()?(punchgs.TweenLite.set(i,{autoAlpha:1}),punchgs.TweenLite.set(n,{autoAlpha:0}),punchgs.TweenLite.set(r,{autoAlpha:0})):(punchgs.TweenLite.to(i,.5,{autoAlpha:1}),punchgs.TweenLite.to(n,.5,{autoAlpha:0}),punchgs.TweenLite.to(r,.5,{autoAlpha:0})),playVimeo(i,o))):i.attr("src").toLowerCase().indexOf("wistia")>0?1==prepareWs(i)&&(clearInterval(s),o||(is_mobile()?(punchgs.TweenLite.set(i,{autoAlpha:1}),punchgs.TweenLite.set(n,{autoAlpha:0}),punchgs.TweenLite.set(r,{autoAlpha:0})):(punchgs.TweenLite.to(i,.5,{autoAlpha:1}),punchgs.TweenLite.to(n,.5,{autoAlpha:0}),punchgs.TweenLite.to(r,.5,{autoAlpha:0}),playYT(i,o)))):i.attr("src").toLowerCase().indexOf("soundcloud")>0&&1==prepareSoundCloud(i)&&(clearInterval(s),o||(is_mobile()?(punchgs.TweenLite.set(i,{autoAlpha:1}),punchgs.TweenLite.set(n,{autoAlpha:0}),punchgs.TweenLite.set(r,{autoAlpha:0})):(punchgs.TweenLite.to(i,.5,{autoAlpha:1}),punchgs.TweenLite.to(n,.5,{autoAlpha:0}),punchgs.TweenLite.to(r,.5,{autoAlpha:0})),playSC(i,o)))},100)}else if(i.hasClass("esg-video-frame")){loadVideoApis(t,a),punchgs.TweenLite.set(i,{opacity:0,display:"block"});var s=setInterval(function(){1==prepareVideo(i)&&(clearInterval(s),o||(is_mobile()?(punchgs.TweenLite.set(i,{autoAlpha:1}),punchgs.TweenLite.set(n,{autoAlpha:0}),punchgs.TweenLite.set(r,{autoAlpha:0})):(punchgs.TweenLite.to(i,.5,{autoAlpha:1}),punchgs.TweenLite.to(n,.5,{autoAlpha:0}),punchgs.TweenLite.to(r,.5,{autoAlpha:0})),playVideo(i,o)))},100)}else o||(is_mobile()?(punchgs.TweenLite.set(i,{autoAlpha:1}),punchgs.TweenLite.set(n,{autoAlpha:0}),punchgs.TweenLite.set(r,{autoAlpha:0})):(punchgs.TweenLite.set(i,{opacity:0,display:"block"}),punchgs.TweenLite.to(i,.5,{autoAlpha:1}),punchgs.TweenLite.to(n,.5,{autoAlpha:0}),punchgs.TweenLite.to(r,.5,{autoAlpha:0}))),i.attr("src")!=undefined&&(i.attr("src").toLowerCase().indexOf("youtube")>0&&playYT(i,o),i.attr("src").toLowerCase().indexOf("vimeo")>0&&playVimeo(i,o),i.attr("src").toLowerCase().indexOf("wistia")>0&&playWs(i,o),i.attr("src").toLowerCase().indexOf("soundcloud")>0&&playSC(i,o))}function prepareItemsInGrid(e,t){var a=e.container;a.addClass("esg-container"),t||a.find(".mainul>li").each(function(){jQuery(this).addClass("eg-newli")});var o=a.find(".mainul>.eg-newli"),i=(100/e.column,e.aspectratio),r=a.find(".esg-overflowtrick").parent().width(),n=(a.find("ul").first(),a.find(".esg-overflowtrick").first(),0);i=i.split(":"),aratio=parseInt(i[0],0)/parseInt(i[1],0),n=r/e.column/aratio,jQuery.each(o,function(t,o){var i=jQuery(o),r=i.find(".esg-entry-media"),n=r.find("img").attr("src");i.removeClass("eg-newli"),punchgs.TweenLite.set(i,{force3D:"auto",autoAlpha:0,opacity:0}),i.addClass("tp-esg-item"),"even"==e.layout?n!=undefined&&(i.find(".esg-entry-media").wrap('<div class="esg-entry-media-wrapper" style="width:100%;height:100%; overflow:hidden;position:relative;"></div>'),r.find("img").css({top:"0px",left:"0px",width:"100%",height:"auto",visibility:"visible",display:"block"})):i.find(".esg-entry-media").wrap('<div class="esg-entry-media-wrapper" style="overflow:hidden;position:relative;"></div>'),i.find(".esg-media-video").each(function(){var t=jQuery(this),o=i.find(".esg-entry-media"),r="display:none;",n="data-src=",s="src=";if(t.data("poster")!=undefined&&t.data("poster").length>3?"even"!=e.layout?(o.append('<img class="esg-media-poster" src="'+t.data("poster")+'" width="'+t.attr("width")+'" height="'+t.attr("height")+'">'),o.find("img").css({top:"0px",left:"0px",width:"100%",height:"auto",visibility:"visible",display:"block"})):(o.append('<div class="esg-media-poster" style="background:url('+t.data("poster")+'); background-size:cover; background-position:center center">'),o.find(".esg-media-poster").css({width:"100%",height:"100%",visibility:"visible",position:"relative",display:"block"})):(i.find(".esg-entry-cover").remove(),r="display:block;",n="src=",s="data-src=",i.data("simplevideo",1)),0==i.find(".esg-click-to-play-video").length&&(i.find(".esg-entry-cover").find("*").each(function(){0==jQuery(this).closest("a").length&&0==jQuery(this).find("a").length&&jQuery(this).addClass("esg-click-to-play-video")}),i.find(".esg-overlay").addClass("esg-click-to-play-video")),t.data("youtube")!=undefined){var l="https://www.youtube.com/embed/"+t.data("youtube")+"?version=3&enablejsapi=1&html5=1&controls=1&autohide=1&rel=0&showinfo=0";o.append('<iframe class="esg-youtube-frame" wmode="Opaque" style="position:absolute;top:0px;left:0px;'+r+'" width="'+t.attr("width")+'" height="'+t.attr("height")+'" '+n+'"'+l+'" '+s+'"about:blank"></iframe>')}if(t.data("vimeo")!=undefined){var u="http://player.vimeo.com/video/"+t.data("vimeo")+"?title=0&byline=0&html5=1&portrait=0&api=1";o.append('<iframe class="esg-vimeo-frame" style="position:absolute;top:0px;left:0px;'+r+'" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen=""  width="'+t.attr("width")+'" height="'+t.attr("height")+'" '+n+'"'+u+'" '+s+'"about:blank"></iframe>')}if(t.data("wistia")!=undefined){var d="https://fast.wistia.net/embed/iframe/"+t.data("wistia")+"?version=3&enablejsapi=1&html5=1&controls=1&autohide=1&rel=0&showinfo=0";o.append('<iframe class="esg-wistia-frame" wmode="Opaque" style="position:absolute;top:0px;left:0px;'+r+'" width="'+t.attr("width")+'" height="'+t.attr("height")+'" '+n+'"'+d+'" '+s+'"about:blank"></iframe>')}if(t.data("soundcloud")!=undefined){var c="https://w.soundcloud.com/player/?url=https://api.soundcloud.com/tracks/"+t.data("soundcloud")+"&auto_play=false&hide_related=false&visual=true&show_artwork=true";o.append('<iframe class="esg-soundcloud-frame" style="position:absolute;top:0px;left:0px;'+r+'" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen="" width="'+t.attr("width")+'" height="'+t.attr("height")+'" '+n+'"'+c+'" '+s+'"about:blank"></iframe>')}(t.data("mp4")!=undefined||t.data("webm")!=undefined||t.data("ogv")!=undefined)&&(o.append('<video class="esg-video-frame" controls style="position:absolute;top:0px;left:0px;'+r+'" width="'+t.attr("width")+'" height="'+t.attr("height")+'" data-origw="'+t.attr("width")+'" data-origh="'+t.attr("height")+'"></video'),t.data("mp4")!=undefined&&o.find("video").append('<source src="'+t.data("mp4")+'" type="video/mp4" />'),t.data("webm")!=undefined&&o.find("video").append('<source src="'+t.data("webm")+'" type="video/webm" />'),t.data("ogv")!=undefined&&o.find("video").append('<source src="'+t.data("ogv")+'" type="video/ogg" />')),i.find(".esg-click-to-play-video").click(function(){var t=jQuery(this).closest(".tp-esg-item");videoClickEvent(t,a,e)}),1==i.data("simplevideo")&&waitMediaListeners(i)}),0==i.find(".esg-media-video").length&&i.find(".esg-click-to-play-video").remove(),adjustMediaSize(i,!0,null,e),i.find(".esg-entry-content").length>0&&i.find(".esg-media-cover-wrapper").length>0&&(i.find(".esg-entry-content").index()<i.find(".esg-media-cover-wrapper").index()||i.find(".esg-entry-content").addClass("esg-notalone")),i.find(".esg-entry-cover").each(function(){var e=jQuery(this),t=e.data("clickable");e.find(".esg-top").wrapAll('<div class="esg-tc eec"></div>'),e.find(".esg-left").wrapAll('<div class="esg-lc eec"></div>'),e.find(".esg-right").wrapAll('<div class="esg-rc eec"></div>'),e.find(".esg-center").wrapAll('<div class="esg-cc eec"></div>'),e.find(".esg-bottom").wrapAll('<div class="esg-bc eec"></div>'),e.find(".eec").append("<div></div>"),"on"==t&&e.find(".esg-overlay").length>=1&&e.click(function(e){0==jQuery(e.target).closest("a").length&&jQuery(this).find(".eg-invisiblebutton")[0].click()}).css({cursor:"pointer"})}),i.data("pt",parseInt(i.css("paddingTop"),0)),i.data("pb",parseInt(i.css("paddingBottom"),0)),i.data("pl",parseInt(i.css("paddingLeft"),0)),i.data("pr",parseInt(i.css("paddingRight"),0)),i.data("bt",parseInt(i.css("borderTopWidth"),0)),i.data("bb",parseInt(i.css("borderBottomWidth"),0)),i.data("bl",parseInt(i.css("borderLeftWidth"),0)),i.data("br",parseInt(i.css("borderRightWidth"),0)),i.find(".esg-entry-content").length>0&&"even"==e.layout&&(i.css({paddingTop:"0px",paddingLeft:"0px",paddingRight:"0px",paddingBottom:"0px"}),i.css({borderTopWidth:"0px",borderBottomWidth:"0px",borderLeftWidth:"0px",borderRightWidth:"0px"})),i.find(".eg-handlehideunder").each(function(){}),e.ajaxContentTarget!=undefined&&jQuery("#"+e.ajaxContentTarget).length>0&&i.find(".eg-ajaxclicklistener, a").each(function(){var t=jQuery(this),o=jQuery("#"+e.ajaxContentTarget).find(".eg-ajax-target");o.parent().hasClass("eg-ajaxanimwrapper")||o.wrap('<div class="eg-ajaxanimwrapper" style="position:relative;overflow:hidden;"></div>'),t.data("ajaxsource")!=undefined&&t.data("ajaxtype")!=undefined&&(t.addClass("eg-ajax-a-button"),t.click(function(){return loadMoreContent(a,e,t),o.length>0?!1:!0}))}),i.find(".eg-triggerfilter").click(function(){var t=jQuery(this).data("filter");return jQuery(e.filterGroupClass+".esg-filterbutton,"+e.filterGroupClass+" .esg-filterbutton").each(function(){jQuery(this).data("filter")==t&&jQuery(this).trigger("click")}),!1}).css({cursor:"pointer"}),i.on("mouseenter.hoverdir, mouseleave.hoverdir",function(t){var a=jQuery(this),o=getDir(a,{x:t.pageX,y:t.pageY});if("mouseenter"===t.type)itemHoverAnim(jQuery(this),"nope",e,o);else{if(clearTimeout(a.data("hovertimer")),1==a.data("animstarted")){a.data("animstarted",0),a.removeClass("esg-hovered");var i=(a.find(".esg-entry-cover"),0);a.find(".esg-entry-content").length>0&&"even"==e.layout&&(punchgs.TweenLite.set(a.find(".esg-entry-content"),{display:"none"}),punchgs.TweenLite.set(a,{zIndex:5}),punchgs.TweenLite.set(a.closest(".esg-overflowtrick"),{overflow:"hidden",overwrite:"all"}),a.css({paddingTop:"0px",paddingLeft:"0px",paddingRight:"0px",paddingBottom:"0px"}),a.css({borderTopWidth:"0px",borderBottomWidth:"0px",borderLeftWidth:"0px",borderRightWidth:"0px"}),a.find(".esg-entry-media").css({height:"100%"}),punchgs.TweenLite.set(a,{z:0,height:a.data("hhh"),width:a.data("www"),x:0,y:0}),"on"==e.evenGridMasonrySkinPusher&&offsetParrents(0,a)),jQuery.each(esgAnimmatrix,function(e,t){a.find(t[0]).each(function(){var e=jQuery(this),r=e.data("delay")!=undefined?e.data("delay"):0,n=t[5],s=0;switch(animobject=e,splitted=!1,isOut=t[0].indexOf("out")>-1,r>i&&(i=r),n.z==undefined&&(n.z=1),t[0]){case".esg-slide":var l=directionPrepare(o,"in",a.width(),a.height(),!0);n.x=l.x,n.y=l.y;var u=punchgs.TweenLite.to(animobject,.5,{y:n.y,x:n.x,overwrite:"all",onCompleteParams:[animobject],onComplete:function(e){punchgs.TweenLite.set(e,{autoAlpha:0})}});break;case".esg-slideout":var l=directionPrepare(o,"out",a.width(),a.height());n.x=0,n.y=0,n.overwrite="all";var u=punchgs.TweenLite.fromTo(animobject,.5,{autoAlpha:1,x:l.x,y:l.y},{x:0,y:0,autoAlpha:1,overwrite:"all"});break;default:n.force3D="auto";var u=punchgs.TweenLite.to(animobject,t[4],n,s)}isOut&&u.eventCallback("onComplete",resetTransforms,[animobject])})})}a.hasClass("esg-demo")&&setTimeout(function(){itemHoverAnim(a)},800)}}),itemHoverAnim(i,"set",e),i.hasClass("esg-demo")&&itemHoverAnim(i)}),loadVideoApis(a,e),setItemsOnPages(e)}function resetTransforms(e){punchgs.TweenLite.set(e,{clearProps:"transform",css:{clearProps:"transform"}})}function adjustMediaSize(e,t,a,o){e.find("iframe").length>0&&e.find("iframe").each(function(){var i=jQuery(this);i.data("origw",i.attr("width")),i.data("origh",i.attr("height"));var r=i.data("origw"),n=i.data("origh");if(a!=undefined)var s=a.itemw;else var s=e.width();ifh=s/r*n,s=Math.round(s),ifh=Math.round(ifh),i.data("neww",s),i.data("newh",ifh),t&&"even"!=o.layout?(punchgs.TweenLite.set(i,{width:s,height:ifh}),punchgs.TweenLite.set(e.find(".esg-media-poster"),{width:s,height:ifh}),punchgs.TweenLite.set(e.find(".esg-entry-media"),{width:s,height:ifh})):(punchgs.TweenLite.set(i,{width:"100%",height:"100%"}),punchgs.TweenLite.set(e.find(".esg-media-poster"),{width:"100%",height:"100%"}),punchgs.TweenLite.set(e.find(".esg-entry-media"),{width:"100%",height:"100%"}))}),e.find(".esg-video-frame").length>0&&e.find(".esg-video-frame").each(function(){var i=jQuery(this);i.parent().data("origw",i.data("origw")),i.parent().data("origh",i.data("origh"));var r=i.data("origw"),n=i.data("origh");if(a!=undefined)var s=a.itemw;else var s=e.width();ifh=s/r*n,s=Math.round(s),ifh=Math.round(ifh),i.data("neww",s),i.data("newh",ifh),t&&"even"!=o.layout?(punchgs.TweenLite.set(i,{width:s,height:ifh}),punchgs.TweenLite.set(e.find(".esg-media-poster"),{width:s,height:ifh}),punchgs.TweenLite.set(e.find(".esg-entry-media"),{width:s,height:ifh})):(punchgs.TweenLite.set(i,{width:"100%",height:"100%"}),punchgs.TweenLite.set(e.find(".esg-media-poster"),{width:"100%",height:"100%"}),punchgs.TweenLite.set(e.find(".esg-entry-media"),{width:"100%",height:"100%"}))})}function setItemsOnPages(e){var t=e.container,a=t.find(".mainul>li"),o=e.column*e.row,i=e.rowItemMultiplier,r=i.length;if(r>0&&"even"==e.layout){o=0;for(var n=0;n<e.row;n++){var s=n-r*Math.floor(n/r);o+=i[s][e.columnindex]}}if("on"==e.evenCobbles&&e.cobblesPattern!=undefined){var l=0,o=0;jQuery.each(a,function(t){var a=jQuery(a),i=a.data("cobblesw"),r=a.data("cobblesh");if(e.cobblesPattern!=undefined&&e.cobblesPattern.length>2){var n=getCobblePat(e.cobblesPattern,t);i=parseInt(n.w,0),r=parseInt(n.h,0)}i=i==undefined?1:i,r=r==undefined?1:r,e.column<i&&(i=e.column),l+=i*r,e.column*e.row>=l&&o++})}var u=o*e.currentpage,d=(t.find(".esg-overflowtrick").parent().width(),u+o),c=jQuery(e.filterGroupClass+".esg-filterbutton.selected:not(.esg-navigationbutton),"+e.filterGroupClass+" .esg-filterbutton.selected:not(.esg-navigationbutton)"),p=0;if(jQuery(e.filterGroupClass+".esg-filter-wrapper, "+e.filterGroupClass+" .esg-filter-wrapper").length>0?jQuery.each(a,function(t,a){var o=jQuery(a);o.find(".esgbox").each(function(){"all"==e.lightBoxMode?jQuery(this).attr("rel","group"):"contentgroup"!=e.lightBoxMode&&jQuery(this).attr("rel","")});var i=!0,r=0;jQuery.each(c,function(e,t){o.hasClass(jQuery(t).data("filter"))&&(i=!1,r++)}),"and"==e.filterLogic&&r<c.length&&(i=!0),hidsbutton=jQuery(e.filterGroupClass+".esg-filter-wrapper .hiddensearchfield"),hidsbutton.hasClass("eg-forcefilter")&&r<c.length&&(i=!0),p>=u&&d>p&&!i?(o.addClass("itemtoshow").removeClass("itemishidden").removeClass("itemonotherpage"),("filterpage"==e.lightBoxMode||"filterall"==e.lightBoxMode)&&o.find(".esgbox").attr("rel","group"),p++):("filterall"==e.lightBoxMode&&o.find(".esgbox").attr("rel","group"),i?o.addClass("itemishidden").removeClass("itemtoshow").removeClass("fitsinfilter"):(u>p||p>=d?(o.addClass("itemonotherpage"),o.removeClass("itemtoshow"),p++):(o.addClass("itemtoshow").removeClass("itemishidden").removeClass("itemonotherpage"),p++),o.addClass("fitsinfilter")))}):jQuery.each(a,function(t,a){var o=jQuery(a);o.find(".esgbox").each(function(){"all"==e.lightBoxMode?jQuery(this).attr("rel","group"):"contentgroup"!=e.lightBoxMode&&jQuery(this).attr("rel","")}),p>=u&&d>p?(o.addClass("itemtoshow").removeClass("itemishidden").removeClass("itemonotherpage"),p++):(u>p||p>=d?(o.addClass("itemonotherpage"),o.removeClass("itemtoshow"),p++):(o.addClass("itemtoshow").removeClass("itemishidden").removeClass("itemonotherpage"),p++),o.addClass("fitsinfilter"))}),e.nonefiltereditems=t.find(".itemtoshow, .fitsinfilter").length,"none"!=e.loadMoreType){var h=0,f=!1;c.each(function(){var e=jQuery(this).data("filter");if(e!=undefined){var a=t.find("."+e).length;h+=a,0==a&&(f=!0)}}),(0==c.length||1==c.length)&&(h=1),(0==h||f)&&loadMoreItems(t,e)}var g=jQuery(e.filterGroupClass+".esg-pagination,"+e.filterGroupClass+" .esg-pagination");g.find(".esg-pagination").remove(),g.html(""),e.maxpage=0;var w,m=Math.ceil(e.nonefiltereditems/o);if(e.realmaxpage=m,m>7&&"on"==e.smartPagination)if(e.currentpage<3){for(var n=0;4>n;n++)w=n==e.currentpage?"selected":"",e.maxpage++,g.append('<div class="esg-navigationbutton esg-filterbutton esg-pagination-button '+w+'" data-page="'+n+'">'+(n+1)+"</div>");g.append('<div class="esg-navigationbutton">...</div>'),g.append('<div class="esg-navigationbutton esg-filterbutton esg-pagination-button '+w+'" data-page="'+(m-1)+'">'+m+"</div>")}else if(m-e.currentpage<4){g.append('<div class="esg-navigationbutton esg-filterbutton esg-pagination-button '+w+'" data-page="0">1</div>'),g.append('<div class="esg-navigationbutton">...</div>');for(var n=m-4;m>n;n++)w=n==e.currentpage?"selected":"",e.maxpage++,g.append('<div class="esg-navigationbutton esg-filterbutton esg-pagination-button '+w+'" data-page="'+n+'">'+(n+1)+"</div>")}else{g.append('<div class="esg-navigationbutton esg-filterbutton esg-pagination-button '+w+'" data-page="0">1</div>'),g.append('<div class="esg-navigationbutton">...</div>');for(var n=e.currentpage-1;n<e.currentpage+2;n++)w=n==e.currentpage?"selected":"",e.maxpage++,g.append('<div class="esg-navigationbutton esg-filterbutton esg-pagination-button '+w+'" data-page="'+n+'">'+(n+1)+"</div>");g.append('<div class="esg-navigationbutton">...</div>'),g.append('<div class="esg-navigationbutton esg-filterbutton esg-pagination-button '+w+'" data-page="'+(m-1)+'">'+m+"</div>")}else for(var n=0;m>n;n++)w=n==e.currentpage?"selected":"",e.maxpage++,g.append('<div class="esg-navigationbutton esg-filterbutton esg-pagination-button '+w+'" data-page="'+n+'">'+(n+1)+"</div>");if(1==e.maxpage?(jQuery(e.filterGroupClass+".esg-navigationbutton,"+e.filterGroupClass+" .esg-navigationbutton").not(".esg-loadmore").css({display:"none"}),g.css({display:"none"})):(jQuery(e.filterGroupClass+".esg-navigationbutton,"+e.filterGroupClass+" .esg-navigationbutton").css({display:"inline-block"}),g.css({display:"inline-block"})),e.currentpage>=Math.ceil(e.nonefiltereditems/o)){e.oldpage=e.currentpage,e.currentpage=0;var v=0;t.find(".itemtoshow, .fitsinfilter").each(function(){v++,d>v&&jQuery(this).removeClass("itemonotherpage")}),g.find(".esg-pagination-button").first().addClass("selected")}e.currentpage<0&&(e.currentpage=0),g.find(".esg-pagination-button").on("click",function(){e.oldpage=e.currentpage,e.currentpage=jQuery(this).data("page"),e=getOptions(t);var a=getBestFitColumn(e,jQuery(window).width(),"id");e.column=a.column,e.columnindex=a.index,"on"===e.cookies.pagination&&e.cookies.searchjusttriggered!==!0&&createCookie("grid_"+e.girdID+"_pagination",e.currentpage,e.cookies.timetosave*(1/60/60)),setItemsOnPages(e),organiseGrid(e),setOptions(t,e),stopAllVideos(!0),"on"==e.paginationScrollToTop&&jQuery("html, body").animate({scrollTop:t.offset().top-e.paginationScrollToTopOffset},{queue:!1,speed:.5})}),e.firstshowever==undefined&&jQuery(e.filterGroupClass+".esg-navigationbutton,"+e.filterGroupClass+" .esg-navigationbutton").css({visibility:"hidden"})}function waittorungGrid(e,t,a){var o=e.closest(".mainul");clearTimeout(o.data("intreorganisier")),o.hasClass("gridorganising")?o.data("intreorganisier",setTimeout(function(){waittorungGrid(e,t,a)},10)):runGrid(t,a)}function loadAllPrepared(e,t){if(1==e.data("preloading"))return!1;var a=t.aspectratio,o=(parseInt(a[0],0)/parseInt(a[1],0),new Image);e.data("lazysrc")!=e.attr("src")&&e.data("lazysrc")!=undefined&&"undefined"!=e.data("lazysrc")&&e.data("lazysrc")!=undefined&&"undefined"!=e.data("lazysrc")&&e.attr("src",e.data("lazysrc")),e.data("preloading",1),o.onload=function(){e.data("lazydone",1),e.data("ww",o.width),e.data("hh",o.height),e.closest(".showmeonload").addClass("itemtoshow").removeClass("showmeonload").addClass("loadedmedia"),evenImageRatio(e,t),"on"==t.lazyLoad&&waittorungGrid(e,t,!0)},o.onerror=function(){e.data("lazydone",1),e.closest(".showmeonload").addClass("itemtoshow").removeClass("showmeonload").addClass("loadedmedia"),"on"==t.lazyLoad&&waittorungGrid(e,t,!0)},o.src=e.attr("src")!=undefined&&"undefined"!=e.attr("src")?e.attr("src"):e.data("src"),o.complete&&(e.data("lazydone",1),e.data("ww",o.width),e.data("hh",o.height),e.closest(".showmeonload").addClass("itemtoshow").removeClass("showmeonload").addClass("loadedmedia"),evenImageRatio(e,t),"on"==t.lazyLoad&&waittorungGrid(e,t,!0))}function organiseGrid(e){var t=e.container;e.listneractivated==undefined&&(t.on("visibleimagesloaded",function(){runGrid(e)}),e.listneractivated=!0,setOptions(t,e)),waitForLoads(t.find(".itemtoshow"),e)}function evenImageRatio(e,t,a){if("even"==t.layout&&e.is(":visible")){var o=t.aspectratio;o=o.split(":");var i=parseInt(o[0],0)/parseInt(o[1],0),r=e.data("ww"),n=e.data("lazydone");if(r==undefined&&1==n||"on"==t.forceFullScreen&&1==n||a&&r!=undefined){var s=e.data("hh"),l=r/s;if(l>=i){var u=s/o[1],d=s/u,c=r/u,p=(c-o[0])/2;p=100/o[0]*p,e.css({position:"absolute",width:"auto",height:"101%",top:"0%",left:0-p+"%"})}else{var u=r/o[0],d=s/u,c=r/u,p=(d-o[1])/2;p=100/o[1]*p,e.css({position:"absolute",width:"101%",height:"auto",left:"0%",top:0-p+"%"})}removeLLCover(e)}}else removeLLCover(e)}function removeLLCover(e){!e.hasClass("coverremoved")&&e.parent().find(".lazyloadcover").length>0&&(e.addClass("coverremoved"),punchgs.TweenLite.to(e.parent().find(".lazyloadcover"),.5,{autoAlpha:0,ease:punchgs.Power3.easeInOut,onComplete:function(){e.parent().find(".lazyloadcover").remove()}}))}function runGrid(e,t){var a=e.container;e.firstshowever==undefined?(a.is(":hidden")?(punchgs.TweenLite.set(a,{autoAlpha:1,display:"block"}),setTimeout(function(){runGridMain(e,t),jQuery(e.filterGroupClass+".esg-navigationbutton, "+e.filterGroupClass+" .esg-navigationbutton").css({visibility:"visible"})},300)):(runGridMain(e,t),jQuery(e.filterGroupClass+".esg-navigationbutton, "+e.filterGroupClass+" .esg-navigationbutton").css({visibility:"visible"})),e.firstshowever=1):(runGridMain(e,t),jQuery(e.filterGroupClass+".esg-navigationbutton, "+e.filterGroupClass+" .esg-navigationbutton").css({visibility:"visible"}))}function getCobblePat(e,t){var a=new Object;return a.w=1,a.h=1,e=e.split(","),e!=undefined&&(e=e[t-Math.floor(t/e.length)*e.length].split("x"),a.w=e[0],a.h=e[1]),a}function runGridMain(e,t){var a,o=e.container,i=o.find(".itemtoshow, .isvisiblenow").not(".ui-sortable-helper"),r=new Object,n=o.find("ul").first(),s=(o.find(".esg-overflowtrick").first(),e.aspectratio),l=0;e.aspectratioOrig=e.aspectratio,o.find(".mainul").addClass("gridorganising"),s=s.split(":"),a=parseInt(s[0],0)/parseInt(s[1],0),r.item=0,r.pagetoanimate=0-e.currentpage,r.col=0,r.row=0,r.pagecounter=0,r.itemcounter=0,r.fakecol=0,r.fakerow=0,r.maxheight=0,r.allcol=0,r.allrow=0,r.ulcurheight=0,r.ulwidth=n.width(),r.verticalsteps=1,r.currentcolumnheight=new Array;for(var u=0;u<e.column;u++)r.currentcolumnheight[u]=0;r.pageitemcounterfake=0,r.pageitemcounter=0,r.delaybasic=e.delayBasic!=undefined?e.delayBasic:.08,r.anim=e.pageAnimation,r.itemtowait=0,r.itemouttowait=0,r.ease="punchgs.Power1.easeInOut",r.easeout=r.ease,r.row=0,r.col=0;{var d=e.rowItemMultiplier,c=d.length;e.column}if(r.y=0,r.fakey=0,o.find(".esg-overflowtrick").css({width:"100%"}),100==o.find(".esg-overflowtrick").width()&&o.find(".esg-overflowtrick").css({width:o.find(".esg-overflowtrick").parent().width()}),r.cwidth=o.find(".esg-overflowtrick").width()-2*e.overflowoffset,e.inanimation=!0,r.cwidth_n_spaces=r.cwidth-(e.column-1)*e.space,r.itemw=Math.round(r.cwidth_n_spaces/e.column),r.originalitemw=r.itemw,"on"==e.forceFullScreen&&(l=jQuery(window).height(),e.fullScreenOffsetContainer!=undefined))try{var p=e.fullScreenOffsetContainer.split(",");jQuery.each(p,function(t,a){l-=jQuery(a).outerHeight(!0),l<e.minFullScreenHeight&&(l=e.minFullScreenHeight)})}catch(h){}"even"==e.layout?(r.itemh=Math.round(0==Math.round(l)?r.cwidth_n_spaces/e.column/a:l/e.row),e.aspectratio=0==l?e.aspectratio:r.itemw+":"+r.itemh,c>0?punchgs.TweenLite.set(i,{display:"block",visibility:"visible",overwrite:"auto"}):punchgs.TweenLite.set(i,{display:"block",width:r.itemw,height:r.itemh,visibility:"visible",overwrite:"auto"})):punchgs.TweenLite.set(i,{display:"block",width:r.itemw,height:"auto",visibility:"visible",overwrite:"auto"}),t||punchgs.TweenLite.killTweensOf(i),r.originalitemh=r.itemh;for(var f=new Array,g=e.row*e.column*2,w=0;g>w;w++){for(var m=new Array,v=0;v<e.column;v++)m.push(0);f.push(m)}var y=0;0==i.length&&o.trigger("itemsinposition"),jQuery.each(i,function(t,i){var s=jQuery(i);if(r.itemw=r.originalitemw,"on"!=e.evenCobbles||s.hasClass("itemonotherpage")||s.hasClass("itemishidden")){var u=r.row-c*Math.floor(r.row/c);
"even"==e.layout&&c>0&&(e.column=d[u][e.columnindex],r.cwidth=o.find(".esg-overflowtrick").width()-2*e.overflowoffset,r.cwidth_n_spaces=r.cwidth-(e.column-1)*e.space,r.itemw=Math.round(r.cwidth_n_spaces/e.column),r.itemh=0==l?r.cwidth_n_spaces/e.column/a:l/e.row,e.aspectratio=0==l?e.aspectratio:r.itemw+":"+r.itemh,punchgs.TweenLite.set(s,{width:r.itemw,height:r.itemh,overwrite:"auto"}))}else{var p=s.data("cobblesw"),h=s.data("cobblesh");if(e.cobblesPattern!=undefined&&e.cobblesPattern.length>2){var w=getCobblePat(e.cobblesPattern,y);p=parseInt(w.w,0),h=parseInt(w.h,0),y++}p=p==undefined?1:p,h=h==undefined?1:h,e.column<p&&(p=e.column),r.cobblesorigw=r.originalitemw,r.cobblesorigh=r.originalitemh,r.itemw=r.itemw*p+(p-1)*e.space,r.itemh=r.originalitemh,r.itemh=r.itemh*h+(h-1)*e.space;var m=p+":"+h,v=!1,b=0,x=0;switch(m){case"1:1":do 0==f[b][x]&&(f[b][x]="1:1",v=!0,r.cobblesx=x,r.cobblesy=b),x++,x==e.column&&(x=0,b++),b>=g&&(v=!0);while(!v);break;case"1:2":do 0==f[b][x]&&g-1>b&&0==f[b+1][x]&&(f[b][x]="1:2",f[b+1][x]="1:2",r.cobblesx=x,r.cobblesy=b,v=!0),x++,x==e.column&&(x=0,b++),b>=g&&(v=!0);while(!v);break;case"1:3":do 0==f[b][x]&&g-2>b&&0==f[b+1][x]&&0==f[b+2][x]&&(f[b][x]="1:3",f[b+1][x]="1:3",f[b+2][x]="1:3",r.cobblesx=x,r.cobblesy=b,v=!0),x++,x==e.column&&(x=0,b++),b>=g&&(v=!0);while(!v);break;case"2:1":do 0==f[b][x]&&x<e.column-1&&0==f[b][x+1]&&(f[b][x]="2:1",f[b][x+1]="2:1",r.cobblesx=x,r.cobblesy=b,v=!0),x++,x==e.column&&(x=0,b++),b>=g&&(v=!0);while(!v);break;case"3:1":do 0==f[b][x]&&x<e.column-2&&0==f[b][x+1]&&0==f[b][x+2]&&(f[b][x]="3:1",f[b][x+1]="3:1",f[b][x+2]="3:1",r.cobblesx=x,r.cobblesy=b,v=!0),x++,x==e.column&&(x=0,b++),b>=g&&(v=!0);while(!v);break;case"2:2":do x<e.column-1&&g-1>b&&0==f[b][x]&&0==f[b][x+1]&&0==f[b+1][x]&&0==f[b+1][x+1]&&(f[b][x]="2:2",f[b+1][x]="2:2",f[b][x+1]="2:2",f[b+1][x+1]="2:2",r.cobblesx=x,r.cobblesy=b,v=!0),x++,x==e.column&&(x=0,b++),b>=g&&(v=!0);while(!v);break;case"3:2":do x<e.column-2&&g-1>b&&0==f[b][x]&&0==f[b][x+1]&&0==f[b][x+2]&&0==f[b+1][x]&&0==f[b+1][x+1]&&0==f[b+1][x+2]&&(f[b][x]="3:2",f[b][x+1]="3:2",f[b][x+2]="3:2",f[b+1][x]="3:2",f[b+1][x+1]="3:2",f[b+1][x+2]="3:2",r.cobblesx=x,r.cobblesy=b,v=!0),x++,x==e.column&&(x=0,b++),b>=g&&(v=!0);while(!v);break;case"2:3":do x<e.column-1&&g-2>b&&0==f[b][x]&&0==f[b][x+1]&&0==f[b+1][x]&&0==f[b+1][x+1]&&0==f[b+2][x+1]&&0==f[b+2][x+1]&&(f[b][x]="2:3",f[b][x+1]="2:3",f[b+1][x]="2:3",f[b+1][x+1]="2:3",f[b+2][x]="2:3",f[b+2][x+1]="2:3",r.cobblesx=x,r.cobblesy=b,v=!0),x++,x==e.column&&(x=0,b++),b>=g&&(v=!0);while(!v);break;case"3:3":do x<e.column-2&&g-2>b&&0==f[b][x]&&0==f[b][x+1]&&0==f[b][x+2]&&0==f[b+1][x]&&0==f[b+1][x+1]&&0==f[b+1][x+2]&&0==f[b+2][x]&&0==f[b+2][x+1]&&0==f[b+2][x+2]&&(f[b][x]="3:3",f[b][x+1]="3:3",f[b][x+2]="3:3",f[b+1][x]="3:3",f[b+1][x+1]="3:3",f[b+1][x+2]="3:3",f[b+2][x]="3:3",f[b+2][x+1]="3:3",f[b+2][x+2]="3:3",r.cobblesx=x,r.cobblesy=b,v=!0),x++,x==e.column&&(x=0,b++),b>=g&&(v=!0);while(!v)}e.aspectratio=r.itemw+":"+r.itemh,punchgs.TweenLite.set(s,{width:r.itemw,height:r.itemh,overwrite:"auto"});var j=s.find(".esg-entry-media").find("img");j.length>0&&evenImageRatio(j,e,!0)}if("even"==e.layout){var j=s.find(".esg-entry-media").find("img");j.length>0&&evenImageRatio(j,e,!0)}else s.hasClass("itemtoshow")&&(s.width()!=r.itemw||0==s.css("opacity")||"hidden"==s.css("visibility"))?r=prepareItemToMessure(s,r,o):(adjustMediaSize(s,!0,r,e),r.itemh=s.height());r=animateGrid(i,e,r),r.itemcounter++,n.height()<r.maxheight&&o.trigger("itemsinposition")}),e.aspectratio=e.aspectratioOrig,0==r.itemtowait&&(e.container.trigger("itemsinposition"),o.find(".mainul").removeClass("gridorganising"));var b=getBestFitColumn(e,jQuery(window).width(),"id");e.column=b.column,e.columnindex=b.index,e.maxheight=r.maxheight,e.container.trigger("itemsinposition"),e.inanimation=!0,e.started=!1,e.filterchanged=!1,e.silent=!1,e.silentout=!1,e.changedAnim="",setOptions(o,e);var x=o.parent().parent().find(".esg-loader");x.length>0&&punchgs.TweenLite.to(x,.2,{autoAlpha:0})}function prepareItemToMessure(e,t,a){return adjustMediaSize(e,!0,t,a.data("opt")),t.itemh=e.outerHeight(!0),t}function animateGrid(e,t,a){var o=jQuery(e);if(a.skipanim=!1,a.x=Math.round(a.col*a.itemw),"even"==t.layout);else{a.idealcol=0,a.backupcol=a.col;for(var i=0;i<t.column;i++)a.currentcolumnheight[a.idealcol]>a.currentcolumnheight[i]&&(a.idealcol=i);a.y=a.currentcolumnheight[a.idealcol],a.x=Math.round(a.idealcol*a.itemw)+a.idealcol*t.space,a.col=a.idealcol,a.itemh==undefined&&(a.itemh=0)}if(a.cobblesx!=undefined&&(a.x=a.cobblesx*a.cobblesorigw,a.y=a.cobblesy*a.cobblesorigh),a.waits=a.col*a.delaybasic+a.row*a.delaybasic*t.column,a.speed=t.animSpeed,a.inxrot=0,a.inyrot=0,a.outxrot=0,a.outyrot=0,a.inorigin="center center",a.outorigin="center center",a.itemh=Math.round(a.itemh),a.scale=1,a.outfade=0,a.infade=0,o.hasClass("itemonotherpage")&&(a.skipanim=!0),"horizontal-slide"==a.anim?(a.waits=0,a.hsoffset=0-a.cwidth-parseInt(t.space,10),a.hsoffsetout=0-a.cwidth-parseInt(t.space,10),t.oldpage!=undefined&&t.oldpage>t.currentpage&&(a.hsoffset=a.cwidth+parseInt(t.space,10),a.hsoffsetout=a.cwidth+parseInt(t.space,10))):"vertical-slide"==a.anim&&(a.waits=0,a.maxcalcheight=t.row*t.space+t.row*a.itemh,a.vsoffset=a.maxcalcheight+parseInt(t.space,10),a.vsoffsetout=a.maxcalcheight+parseInt(t.space,10),t.oldpage!=undefined&&t.oldpage>t.currentpage&&(a.vsoffset=0-a.maxcalcheight-parseInt(t.space,10),a.vsoffsetout=0-a.maxcalcheight-parseInt(t.space,10))),a.outwaits=a.waits,"even"==t.layout&&a.cobblesx==undefined&&(a.x=a.x+a.col*t.space),a.cobblesx!=undefined&&(a.x=a.x+a.cobblesx*t.space,a.y=a.y+a.cobblesy*t.space),("vertical-flip"==a.anim||"horizontal-flip"==a.anim||"vertical-flipbook"==a.anim||"horizontal-flipbook"==a.anim)&&(a=fakePositions(o,a,t)),"vertical-flip"==a.anim?(a.inxrot=-180,a.outxrot=180):"horizontal-flip"==a.anim&&(a.inyrot=-180,a.outyrot=180),a.outspeed=a.speed,"off"==t.animDelay&&(a.waits=0,a.outwaits=0),"scale"==a.anim?a.scale=0:"vertical-flipbook"==a.anim?(a.inxrot=-90,a.outxrot=90,a.inorigin="center top",a.outorigin="center bottom",a.waits=a.waits+a.speed/3,a.outfade=1,a.infade=1,a.outspeed=a.speed/1.2,a.ease="Sine.easeOut",a.easeout="Sine.easeIn","off"==t.animDelay&&(a.waits=a.speed/3,a.outwaits=0)):"horizontal-flipbook"==a.anim?(a.inyrot=-90,a.outyrot=-90,a.inorigin="left center",a.outorigin="right center",a.waits=a.waits+a.speed/2.4,a.outfade=1,a.infade=1,a.outspeed=a.speed/1.2,a.ease="Sine.easeOut",a.easeout="Sine.easeIn","off"==t.animDelay&&(a.waits=a.speed/3,a.outwaits=0)):("fall"==a.anim||"rotatefall"==a.anim)&&(a.outoffsety=100,a=fakePositions(o,a,t),a.outfade=0),"rotatefall"==a.anim?(a.rotatez=20,a.outorigin="left top",a.outfade=1,a.outoffsety=600):"rotatescale"==a.anim?(a.scale=0,a.inorigin="left bottom",a.outorigin="center center",a.faeout=1,a.outoffsety=100,a=fakePositions(o,a,t)):"stack"==a.anim&&(a.scale=0,a.inorigin="center center",a.faeout=1,a.ease="punchgs.Power3.easeOut",a=fakePositions(o,a,t),a.ease="Back.easeOut"),t.silent&&(a.waits=0,a.outwaits=0,a.speed=0,a.outspeed=0),t.silentout&&(a.outwaits=0,a.outspeed=.4,a.speed=.4,a.ease="punchgs.Power3.easeOut",a.easeout=a.ease),a.hooffset=t.overflowoffset,a.vooffset=t.overflowoffset,a.itemw+a.x-a.cwidth<20&&a.itemw+a.x-a.cwidth>-20){var r=a.itemw+a.x-a.cwidth;a.itemw=a.itemw-r}if(!o.hasClass("itemtoshow")&&!o.hasClass("fitsinfilter")||a.skipanim)a.itemouttowait++,punchgs.TweenLite.set(o,{zIndex:5}),o.removeClass("isvisiblenow"),o.css("opacity")>0?"stack"==a.anim?(punchgs.TweenLite.set(o,{zIndex:a.pageitemcounterfake+100}),punchgs.TweenLite.to(o,a.outspeed/2,{force3D:"auto",x:-20-a.itemw,rotationY:30,rotationX:10,ease:Sine.easeInOut,delay:a.outwaits}),punchgs.TweenLite.to(o,.01,{force3D:"auto",zIndex:a.pageitemcounterfake,delay:a.outwaits+a.outspeed/3}),punchgs.TweenLite.to(o,.2*a.outspeed,{force3D:"auto",delay:a.outwaits+.9*a.outspeed,autoAlpha:0,ease:Sine.easeInOut}),punchgs.TweenLite.to(o,a.outspeed/3,{zIndex:2,force3D:"auto",x:0,scale:.9,rotationY:0,rotationX:0,ease:Sine.easeInOut,delay:a.outwaits+a.outspeed/1.4,onComplete:function(){o.hasClass("itemtoshow")||punchgs.TweenLite.set(o,{autoAlpha:0,overwrite:"all",display:"none"}),a.itemouttowait--,0==a.itemouttowait&&t.container.trigger("itemsinposition")}})):"horizontal-flipbook"==a.anim||"vertical-flipbook"==a.anim?punchgs.TweenLite.to(o,a.outspeed,{force3D:"auto",zIndex:2,scale:a.scale,autoAlpha:a.outfade,transformOrigin:a.outorigin,rotationX:a.outxrot,rotationY:a.outyrot,ease:a.easeout,delay:a.outwaits,onComplete:function(){o.hasClass("itemtoshow")||punchgs.TweenLite.set(o,{autoAlpha:0,overwrite:"all",display:"none"}),a.itemouttowait--,0==a.itemouttowait&&t.container.trigger("itemsinposition")}}):"fall"==a.anim?punchgs.TweenLite.to(o,a.outspeed,{zIndex:2,force3D:"auto",y:a.outoffsety,autoAlpha:0,ease:a.easeout,delay:a.outwaits,onComplete:function(){o.hasClass("itemtoshow")||punchgs.TweenLite.set(o,{autoAlpha:0,overwrite:"all",display:"none"}),a.itemouttowait--,0==a.itemouttowait&&t.container.trigger("itemsinposition")}}):"horizontal-slide"==a.anim?punchgs.TweenLite.to(o,a.outspeed,{zIndex:2,force3D:"auto",autoAlpha:1,left:a.hooffset+o.position().left+a.hsoffsetout,top:a.vooffset+o.position().top,ease:a.easeout,delay:a.outwaits,onComplete:function(){punchgs.TweenLite.set(o,{autoAlpha:0,overwrite:"all",display:"none"}),a.itemouttowait--,0==a.itemouttowait&&t.container.trigger("itemsinposition")}}):"vertical-slide"==a.anim?punchgs.TweenLite.to(o,a.outspeed,{zIndex:2,force3D:"auto",autoAlpha:1,left:a.hooffset+o.position().left,top:a.vooffset+o.position().top+a.vsoffsetout,ease:a.easeout,delay:a.outwaits,onComplete:function(){punchgs.TweenLite.set(o,{autoAlpha:0,overwrite:"all",display:"none"}),a.itemouttowait--,0==a.itemouttowait&&t.container.trigger("itemsinposition")}}):"rotatefall"==a.anim&&o.css("opacity")>0?(punchgs.TweenLite.set(o,{zIndex:300-a.item}),punchgs.TweenLite.to(o,a.outspeed/2,{force3D:"auto",transformOrigin:a.outorigin,ease:"punchgs.Bounce.easeOut",rotationZ:a.rotatez,delay:a.outwaits}),punchgs.TweenLite.to(o,a.outspeed/2,{zIndex:2,force3D:"auto",autoAlpha:0,y:a.outoffsety,ease:punchgs.Power3.easeIn,delay:a.outwaits+a.outspeed/3})):punchgs.TweenLite.to(o,a.outspeed,{force3D:"auto",zIndex:2,scale:a.scale,autoAlpha:a.outfade,transformOrigin:a.outorigin,rotationX:a.outxrot,rotationY:a.outyrot,ease:a.easeout,delay:a.outwaits,onComplete:function(){o.hasClass("itemtoshow")||punchgs.TweenLite.set(o,{autoAlpha:0,overwrite:"all",display:"none"}),a.itemouttowait--,0==a.itemouttowait&&t.container.trigger("itemsinposition")}}):punchgs.TweenLite.set(o,{zIndex:2,scale:a.scale,autoAlpha:0,transformOrigin:a.outorigin,rotationX:a.outxrot,rotationY:a.outyrot,onComplete:function(){o.hasClass("itemtoshow")||punchgs.TweenLite.set(o,{autoAlpha:0,overwrite:"all",display:"none"}),a.itemouttowait--,0==a.itemouttowait&&t.container.trigger("itemsinposition")}}),a=shiftGridFake(a,t);else{o.addClass("isvisiblenow"),"even"!=t.layout?(a.currentcolumnheight[a.idealcol]=a.currentcolumnheight[a.idealcol]+a.itemh+parseInt(t.space,10),a.ulcurheight<a.currentcolumnheight[a.idealcol]&&(a.ulcurheight=a.currentcolumnheight[a.idealcol])):a.ulcurheight=a.y+a.itemh,a.maxheight<a.ulcurheight&&(a.maxheight=a.ulcurheight),a.itemtowait++;var n=Math.round(a.hooffset+a.x),s=Math.round(a.vooffset+a.y);"on"==t.rtl&&(n=a.ulwidth-n-a.itemw),0==o.css("opacity")&&"fade"==a.anim?punchgs.TweenLite.set(o,{opacity:0,autoAlpha:0,width:a.itemw,height:a.itemh,scale:1,left:n,y:0,top:s,overwrite:"all"}):0==o.css("opacity")&&"scale"==a.anim?punchgs.TweenLite.set(o,{width:a.itemw,height:a.itemh,scale:0,left:n,y:0,top:s,overwrite:"all"}):0==o.css("opacity")&&"rotatescale"==a.anim?punchgs.TweenLite.set(o,{width:a.itemw,height:a.itemh,scale:1,left:n,top:s,xPercent:150,yPercent:150,rotationZ:20,overwrite:"all"}):0==o.css("opacity")&&"fall"==a.anim?punchgs.TweenLite.set(o,{width:a.itemw,height:a.itemh,scale:.5,left:n,top:s,y:0,overwrite:"all"}):0==o.css("opacity")&&"rotatefall"==a.anim&&punchgs.TweenLite.set(o,{autoAlpha:0,width:a.itemw,height:a.itemh,left:n,rotationZ:0,top:s,y:0,overwrite:"all"}),0!=o.css("opacity")||"vertical-flip"!=a.anim&&"horizontal-flip"!=a.anim&&"vertical-flipbook"!=a.anim&&"horizontal-flipbook"!=a.anim||punchgs.TweenLite.set(o,{autoAlpha:a.infade,zIndex:10,scale:1,y:0,transformOrigin:a.inorigin,rotationX:a.inxrot,rotationY:a.inyrot,width:a.itemw,height:a.itemh,left:n,top:s,overwrite:"all"}),"stack"==a.anim&&punchgs.TweenLite.set(o,{zIndex:a.pageitemcounter,scale:.5,autoAlpha:1,left:n,top:s}),"horizontal-slide"==a.anim&&0==o.css("opacity")&&punchgs.TweenLite.set(o,{autoAlpha:1,left:Math.round(a.hooffset+(a.x-a.hsoffset)),top:s,width:a.itemw,height:a.itemh}),"vertical-slide"==a.anim&&0==o.css("opacity")&&punchgs.TweenLite.set(o,{autoAlpha:1,left:n,top:Math.round(a.vooffset+a.y-a.vsoffset),width:a.itemw,height:a.itemh});var l=o.find(".esg-entry-cover"),u=o.find(".esg-entry-media");if(l&&u){var d=u.height(),c=o.find(".esg-cc");punchgs.TweenLite.to(l,.01,{height:d,ease:a.ease,delay:a.waits}),punchgs.TweenLite.to(c,.01,{top:(d-c.height())/2,ease:a.ease,delay:a.waits})}punchgs.TweenLite.to(o,a.speed,{force3D:"auto",autoAlpha:1,scale:1,transformOrigin:a.inorigin,rotationX:0,rotationY:0,y:0,x:0,xPercent:0,yPercent:0,z:.1,rotationZ:0,left:n,top:s,ease:a.ease,delay:a.waits,onComplete:function(){o.hasClass("itemtoshow")&&punchgs.TweenLite.set(o,{autoAlpha:1,overwrite:"all"}),a.itemtowait--,0==a.itemtowait&&(t.container.trigger("itemsinposition"),o.closest(".mainul").removeClass("gridorganising"))}}),o.find("iframe").length>0&&o.find("iframe").each(function(){var e=jQuery(this),a=Math.round(e.data("neww")),i=Math.round(e.data("newh"));"even"!=t.layout?(punchgs.TweenLite.set(o.find(".esg-media-poster"),{width:a,height:i}),punchgs.TweenLite.set(o.find("iframe"),{width:a,height:i})):(punchgs.TweenLite.set(o.find(".esg-media-poster"),{width:"100%",height:"100%"}),punchgs.TweenLite.set(o.find("iframe"),{width:"100%",height:"100%"}))}),o.find(".video-eg").length>0&&o.find(".video-eg").each(function(){var e=jQuery(this),a=e.data("neww"),i=e.data("newh");"even"!=t.layout?(punchgs.TweenLite.set(o.find(".esg-media-poster"),{width:a,height:i}),punchgs.TweenLite.set(o.find(".esg-entry-media"),{width:a,height:i}),punchgs.TweenLite.set(o.find(".video-eg"),{width:a,height:i})):(punchgs.TweenLite.set(o.find(".esg-media-poster"),{width:"100%",height:"100%"}),punchgs.TweenLite.set(o.find(".esg-entry-media"),{width:"100%",height:"100%"}),punchgs.TweenLite.set(o.find(".video-eg"),{width:"100%",height:"100%"}))}),"masonry"==t.layout&&(a.col=a.backupcol),a=shiftGrid(a,t,o)}return a}function fakePositions(e,t,a){if(!e.hasClass("itemtoshow")&&!e.hasClass("fitsinfilter")||t.skipanim){var o=e.data("col"),i=e.data("row");(o==undefined||i==undefined)&&0!=t.x&&0!=t.y&&(t.x=Math.round(t.fakecol*t.itemw),t.y=t.fakey,o=t.fakecol,i=t.fakerow,e.data("col",t.fakecol),e.data("row",t.fakerow)),t.outwaits="rotatefall"==t.anim?(a.column-o)*t.delaybasic+i*t.delaybasic*a.column:o*t.delaybasic+i*t.delaybasic*a.column}else;return t}function shiftGrid(e,t,a){if(a.data("col",e.col),a.data("row",e.row),e.pageitemcounter++,e.col=e.col+e.verticalsteps,e.allcol++,e.col==t.column&&(e.col=0,e.row++,e.allrow++,e.y=parseFloat(e.y)+parseFloat(e.itemh)+parseFloat(t.space),e.row==t.row&&(e.row=0,e.pageitemcounter>=t.column*t.row&&(e.pageitemcounter=0),e.pagetoanimate=e.pagetoanimate+1,e.pagecounter++,0==e.pageitemcounter)))for(var o=0;o<t.column;o++)e.currentcolumnheight[o]=0;return e}function shiftGridFake(e,t){return e.fakecol=e.fakecol+1,e.pageitemcounterfake++,e.fakecol==t.column&&(e.fakecol=0,e.fakerow++,e.fakey=e.fakey+e.itemh+t.space,e.fakerow==t.row&&(e.fakerow=0,e.pageitemcounterfake=0)),e}function loadVideoApis(e){var t=0,a=0,o=0,i=0,r=0,n="http";"https:"===location.protocol&&(n="https"),e.find("iframe").each(function(){try{if(jQuery(this).attr("src").indexOf("you")>0&&0==t){t=1;var e=document.createElement("script"),a="https";e.src=a+"://www.youtube.com/iframe_api";var o=document.getElementsByTagName("script")[0],i=!0;jQuery("head").find("*").each(function(){jQuery(this).attr("src")==a+"://www.youtube.com/iframe_api"&&(i=!1)}),i&&o.parentNode.insertBefore(e,o)}}catch(r){}}),e.find("iframe").each(function(){try{if(jQuery(this).attr("src").indexOf("ws")>0&&0==o){o=1;var e=document.createElement("script");e.src=n+"://fast.wistia.com/assets/external/E-v1.js";var t=document.getElementsByTagName("script")[0],a=!0;jQuery("head").find("*").each(function(){jQuery(this).attr("src")==n+"://fast.wistia.com/assets/external/E-v1.js"&&(a=!1)}),a&&t.parentNode.insertBefore(e,t)}}catch(i){}}),e.find("iframe").each(function(){try{if(jQuery(this).attr("src").indexOf("vim")>0&&0==a){a=1;var e=document.createElement("script");e.src=n+"://a.vimeocdn.com/js/froogaloop2.min.js";var t=document.getElementsByTagName("script")[0],o=!0;jQuery("head").find("*").each(function(){jQuery(this).attr("src")==n+"://a.vimeocdn.com/js/froogaloop2.min.js"&&(o=!1)}),o&&t.parentNode.insertBefore(e,t)}}catch(i){}}),e.find("iframe").each(function(){try{if(jQuery(this).attr("src").indexOf("soundcloud")>0&&0==r){r=1;var e=document.createElement("script");e.src=n+"://w.soundcloud.com/player/api.js";var t=document.getElementsByTagName("script")[0],a=!0;jQuery("head").find("*").each(function(){jQuery(this).attr("src")==n+"://w.soundcloud.com/player/api.js"&&(a=!1)}),a&&t.parentNode.insertBefore(e,t)}}catch(o){}});var s={youtube:t,vimeo:a,wistia:o,soundcloud:r,htmlvid:i};return s}function toHHMMSS(){var e=new Date,t=Math.floor(e)/1e3,a=Math.floor(t/60),o=Math.floor(a/60),i=Math.floor(o/24),o=o-24*i,a=a-24*i*60-60*o,t=t-24*i*60*60-60*o*60-60*a;return o+":"+a+":"+t}function stopAllVideos(e,t,a){var o=".isplaying";e&&(o=""),jQuery(".esg-youtubevideo.haslistener"+o).each(function(){var t=jQuery(this),o=t.data("player");a!=t.attr("id")&&(o.pauseVideo(),e&&forceVideoInPause(t,!0,o,"youtube"))}),jQuery(".esg-vimeovideo.haslistener"+o).each(function(){var t=jQuery(this),o=t.attr("id"),i=$f(o);a!=o&&(i.api("pause"),a===undefined&&e&&forceVideoInPause(t,!0,i,"vimeo"))}),jQuery(".esg-wistiavideo.haslistener"+o).each(function(){var t=jQuery(this),o=t.data("player");a!=t.attr("id")&&(t.wistiaApi.pause(),e&&forceVideoInPause(t,!0,o,"wistia"))}),jQuery(".esg-htmlvideo.haslistener"+o).each(function(){var t=jQuery(this),o=t.attr("id"),i=document.getElementById(o);a!=o&&(i.pause(),e&&forceVideoInPause(t,!0,i,"html5vid"))}),jQuery(".esg-soundcloud"+o).each(function(){var t=jQuery(this),o=t.data("player");a!=t.attr("id")&&(o.pause(),e&&forceVideoInPause(t,!0,o,"soundcloud"))})}function forceVideoInPause(e,t,a,o){e.removeClass("isplaying");var i=e.closest(".tp-esg-item");if(i.find(".esg-media-video").length>0&&!jQuery("body").data("fullScreenMode")){var r=i.find(".esg-entry-cover"),n=i.find(".esg-media-poster");if(n.length>0)if(is_mobile()?(punchgs.TweenLite.set(r,{autoAlpha:1}),punchgs.TweenLite.set(n,{autoAlpha:1}),punchgs.TweenLite.set(e,{autoAlpha:0})):(punchgs.TweenLite.to(r,.5,{autoAlpha:1}),punchgs.TweenLite.to(n,.5,{autoAlpha:1}),punchgs.TweenLite.to(e,.5,{autoAlpha:0})),t)if("youtube"==o)try{a.destroy()}catch(s){}else if("vimeo"==o)try{a.api("unload")}catch(s){}else if("wistia"==o)try{a.end()}catch(s){}else"html5vid"!=o&&(e.removeClass("haslistener"),e.removeClass("readytoplay"));else setTimeout(function(){is_mobile()||e.css({display:"none"})},500)}}function onPlayerStateChange(e){var t=e.target.getVideoEmbedCode(),a=jQuery("#"+t.split('id="')[1].split('"')[0]),o=a.data("player");e.data==YT.PlayerState.PLAYING&&(o.setPlaybackQuality("hd1080"),stopAllVideos(!0,!1,a.attr("id")),a.addClass("isplaying"),a.removeClass("isinpause")),2==e.data&&forceVideoInPause(a),0==e.data&&forceVideoInPause(a)}function vimeoready_auto(e){var t=$f(e),a=jQuery("#"+e);t.addEvent("ready",function(){a.addClass("readytoplay"),t.addEvent("play",function(){stopAllVideos(!0,!1,e),a.addClass("isplaying"),a.removeClass("isinpause")}),t.addEvent("finish",function(){forceVideoInPause(a),a.removeClass("isplaying")}),t.addEvent("pause",function(){forceVideoInPause(a),a.removeClass("isplaying")})})}function addEvent(e,t,a){e.addEventListener?e.addEventListener(t,a,!1):e.attachEvent(t,a,!1)}function html5vidready(e,t,a){t.addClass("readytoplay"),t.on("play",function(){stopAllVideos(!0,!1,a),t.addClass("isplaying"),t.removeClass("isinpause")}),t.on("pause",function(){forceVideoInPause(t),t.removeClass("isplaying")}),t.on("ended",function(){forceVideoInPause(t),t.removeClass("isplaying")})}function prepareYT(e){var t="ytiframe"+Math.round(1e5*Math.random()+1);if(e.hasClass("haslistener")||"undefined"==typeof YT){var a=e.data("player");return a!=undefined&&"function"==typeof a.playVideo?!0:!1}try{e.attr("id",t);var a=new YT.Player(t,{events:{onStateChange:onPlayerStateChange}});e.data("player",a),e.addClass("haslistener").addClass("esg-youtubevideo")}catch(o){return!1}}function playYT(e){var t=e.data("player");t!=undefined&&"function"==typeof t.playVideo&&t.playVideo()}function prepareVimeo(e){if(e.hasClass("haslistener")||"undefined"==typeof $f){if(typeof $f!=undefined&&"undefined"!=typeof $f){var t=$f(e.attr("id"));return"function"==typeof t.api&&e.hasClass("readytoplay")?!0:!1}return!1}try{var a="vimeoiframe"+Math.round(1e5*Math.random()+1);e.attr("id",a);for(var o,i=e.attr("src"),r={},n=i,s=/([^&=]+)=([^&]*)/g;o=s.exec(n);)r[decodeURIComponent(o[1])]=decodeURIComponent(o[2]);i=r.player_id!=undefined?i.replace(r.player_id,a):i+"&player_id="+a;try{i=i.replace("api=0","api=1")}catch(l){}i+="&api=1",e.attr("src",i);var u=e[0];$f(u).addEvent("ready",function(){vimeoready_auto(a)}),e.addClass("haslistener").addClass("esg-vimeovideo")}catch(l){return!1}}function playVimeo(e){var t=$f(e.attr("id"));t.api("play")}function prepareWs(e){var t="wsiframe"+Math.round(1e5*Math.random()+1);if(e.hasClass("haslistener")||"undefined"==typeof Ws){var a=e.data("player");return a!=undefined&&"function"==typeof a.playVideo?!0:!1}try{e.attr("id",t);var a=new Ws.Player(t,{events:{onStateChange:onPlayerStateChange}});e.data("player",a),e.addClass("haslistener").addClass("esg-wistiavideo")}catch(o){return!1}}function playWs(e){var t=e.data("player");t!=undefined&&"function"==typeof t.playVideo&&t.wistiaApi.Plau()}function prepareSoundCloud(e){if(e.data("player")!=undefined||"undefined"==typeof SC){var t=e.data("player");return t!=undefined&&"function"==typeof t.getVolume?!0:!1}var a="sciframe"+Math.round(1e5*Math.random()+1);try{e.attr("id",a);var t=SC.Widget(a);t.bind(SC.Widget.Events.PLAY,function(){stopAllVideos(!0,!1,e.attr("id")),e.addClass("isplaying"),e.removeClass("isinpause")}),t.bind(SC.Widget.Events.PAUSE,function(){forceVideoInPause(e),e.removeClass("isplaying")}),t.bind(SC.Widget.Events.FINISH,function(){forceVideoInPause(e),e.removeClass("isplaying")}),e.data("player",t),e.addClass("haslistener").addClass("esg-soundcloud")}catch(o){return!1}}function playSC(e){var t=e.data("player");t!=undefined&&"function"==typeof t.getVolume&&setTimeout(function(){t.play()},500)}function prepareVideo(e){if(e.hasClass("haslistener"))try{var t=e.attr("id"),a=document.getElementById(t);return"function"==typeof a.play&&e.hasClass("readytoplay")?!0:!1}catch(o){return!1}else{var i="videoid_"+Math.round(1e5*Math.random()+1);e.attr("id",i);var a=document.getElementById(i);a.oncanplay=html5vidready(a,e,i),e.addClass("haslistener").addClass("esg-htmlvideo")}}function playVideo(e){var t=e.attr("id"),a=document.getElementById(t);a.play()}var esgAnimmatrix=[[".esg-none",0,{autoAlpha:1,rotationZ:0,x:0,y:0,scale:1,rotationX:0,rotationY:0,skewX:0,skewY:0},{autoAlpha:1,ease:punchgs.Power2.easeOut,overwrite:"all"},0,{autoAlpha:1,overwrite:"all"}],[".esg-fade",.3,{autoAlpha:0,rotationZ:0,x:0,y:0,scale:1,rotationX:0,rotationY:0,skewX:0,skewY:0},{autoAlpha:1,ease:punchgs.Power2.easeOut,overwrite:"all"},.3,{autoAlpha:0,ease:punchgs.Power2.easeOut,overwrite:"all"}],[".esg-fadeout",.3,{autoAlpha:1,ease:punchgs.Power2.easeOut,overwrite:"all"},{autoAlpha:0,rotationZ:0,x:0,y:0,scale:1,rotationX:0,rotationY:0,skewX:0,skewY:0},.3,{autoAlpha:1,rotationZ:0,x:0,y:0,scale:1,rotationX:0,rotationY:0,skewX:0,skewY:0,ease:punchgs.Power2.easeOut,overwrite:"all"}],[".esg-covergrowup",.3,{autoAlpha:1,top:"100%",marginTop:-10,rotationZ:0,x:0,y:0,scale:1,rotationX:0,rotationY:0,skewX:0,skewY:0},{autoAlpha:1,top:"0%",marginTop:0,ease:punchgs.Power2.easeOut,overwrite:"all"},.3,{autoAlpha:1,top:"100%",marginTop:-10,bottom:0,z:0,ease:punchgs.Power2.easeOut,overwrite:"all"},!0],[".esg-flipvertical",.5,{x:0,y:0,scale:1,rotationZ:0,rotationX:0,rotationY:0,skewX:0,skewY:0,rotationX:180,autoAlpha:0,z:-.001,transformOrigin:"50% 50%"},{rotationX:0,autoAlpha:1,scale:1,z:.001,ease:punchgs.Power3.easeInOut,overwrite:"all"},.5,{rotationX:180,autoAlpha:0,scale:1,z:-.001,ease:punchgs.Power3.easeInOut,overwrite:"all"},!0],[".esg-flipverticalout",.5,{x:0,y:0,scale:1,rotationZ:0,rotationX:0,rotationY:0,skewX:0,skewY:0,rotationX:0,autoAlpha:1,z:.001,transformOrigin:"50% 50%"},{rotationX:-180,scale:1,autoAlpha:0,z:-150,ease:punchgs.Power3.easeInOut,overwrite:"all"},.5,{rotationX:0,autoAlpha:1,scale:1,z:0,ease:punchgs.Power3.easeInOut,overwrite:"all"}],[".esg-fliphorizontal",.5,{x:0,y:0,scale:1,rotationZ:0,rotationX:0,rotationY:0,skewX:0,skewY:0,rotationY:180,autoAlpha:0,z:-.001,transformOrigin:"50% 50%"},{rotationY:0,autoAlpha:1,scale:1,z:.001,ease:punchgs.Power3.easeInOut,overwrite:"all"},.5,{rotationY:180,autoAlpha:0,scale:1,z:-.001,ease:punchgs.Power3.easeInOut,overwrite:"all"},!0],[".esg-fliphorizontalout",.5,{x:0,y:0,scale:1,rotationZ:0,rotationX:0,rotationY:0,skewX:0,skewY:0,autoAlpha:1,z:.001,transformOrigin:"50% 50%"},{rotationY:-180,scale:1,autoAlpha:0,z:-150,ease:punchgs.Power3.easeInOut,overwrite:"all"},.5,{rotationY:0,autoAlpha:1,scale:1,z:.001,ease:punchgs.Power3.easeInOut,overwrite:"all"}],[".esg-flipup",.5,{x:0,y:0,scale:.8,rotationZ:0,rotationX:90,rotationY:0,skewX:0,skewY:0,autoAlpha:0,z:.001,transformOrigin:"50% 100%"},{scale:1,rotationX:0,autoAlpha:1,z:.001,ease:punchgs.Power2.easeOut,overwrite:"all"},.3,{scale:.8,rotationX:90,autoAlpha:0,z:.001,ease:punchgs.Power2.easeOut,overwrite:"all"},!0],[".esg-flipupout",.5,{rotationX:0,autoAlpha:1,y:0,ease:punchgs.Bounce.easeOut,overwrite:"all"},{x:0,y:0,scale:1,rotationZ:0,rotationX:-90,rotationY:0,skewX:0,skewY:0,autoAlpha:1,z:.001,transformOrigin:"50% 0%"},.3,{rotationX:0,autoAlpha:1,y:0,ease:punchgs.Bounce.easeOut,overwrite:"all"}],[".esg-flipdown",.5,{x:0,y:0,scale:1,rotationZ:0,rotationX:-90,rotationY:0,skewX:0,skewY:0,autoAlpha:0,z:.001,transformOrigin:"50% 0%"},{rotationX:0,autoAlpha:1,y:0,ease:punchgs.Bounce.easeOut,overwrite:"all"},.3,{rotationX:-90,z:0,ease:punchgs.Power2.easeOut,autoAlpha:0,overwrite:"all"},!0],[".esg-flipdownout",.5,{scale:1,rotationX:0,autoAlpha:1,z:0,ease:punchgs.Power2.easeOut,overwrite:"all"},{x:0,y:0,scale:.8,rotationZ:0,rotationX:90,rotationY:0,skewX:0,skewY:0,autoAlpha:0,z:.001,transformOrigin:"50% 100%"},.3,{scale:1,rotationX:0,autoAlpha:1,z:0,ease:punchgs.Power2.easeOut,overwrite:"all"}],[".esg-flipright",.5,{x:0,y:0,scale:.8,rotationZ:0,rotationX:0,rotationY:90,skewX:0,skewY:0,autoAlpha:0,transformOrigin:"0% 50%"},{scale:1,rotationY:0,autoAlpha:1,ease:punchgs.Power2.easeOut,overwrite:"all"},.3,{autoAlpha:0,scale:.8,rotationY:90,ease:punchgs.Power3.easeOut,overwrite:"all"},!0],[".esg-fliprightout",.5,{x:0,y:0,scale:1,rotationZ:0,rotationX:0,rotationY:0,skewX:0,skewY:0,rotationY:0,autoAlpha:1,transformOrigin:"100% 50%"},{scale:1,rotationY:-90,autoAlpha:0,ease:punchgs.Power2.easeOut,overwrite:"all"},.3,{scale:1,z:0,rotationY:0,autoAlpha:1,ease:punchgs.Power3.easeOut,overwrite:"all"}],[".esg-flipleft",.5,{x:0,y:0,scale:1,rotationZ:0,rotationX:0,rotationY:-90,skewX:0,skewY:0,autoAlpha:1,transformOrigin:"100% 50%"},{rotationY:0,autoAlpha:1,z:.001,scale:1,ease:punchgs.Power3.easeOut,overwrite:"all"},.3,{autoAlpha:0,rotationY:-90,z:0,ease:punchgs.Power2.easeOut,overwrite:"all"},!0],[".esg-flipleftout",.5,{x:0,y:0,scale:1,rotationZ:0,rotationX:0,rotationY:0,skewX:0,skewY:0,rotationY:0,autoAlpha:1,transformOrigin:"0% 50%"},{scale:1,rotationY:90,autoAlpha:0,ease:punchgs.Power2.easeOut,overwrite:"all"},.3,{scale:1,z:0,rotationY:0,autoAlpha:1,ease:punchgs.Power3.easeOut,overwrite:"all"}],[".esg-turn",.5,{x:50,y:0,scale:0,rotationZ:0,rotationX:0,rotationY:-40,skewX:0,skewY:0,autoAlpha:1,transformOrigin:"50% 50%"},{scale:1,x:0,rotationY:0,autoAlpha:1,ease:punchgs.Power3.easeInOut,overwrite:"all"},.3,{scale:0,rotationY:-40,autoAlpha:1,z:0,x:50,ease:punchgs.Power3.easeInOut,overwrite:"all"},!0],[".esg-turnout",.5,{x:0,y:0,scale:1,rotationZ:0,rotationX:0,rotationY:0,skewX:0,skewY:0,autoAlpha:1,transformOrigin:"50% 50%"},{scale:1,rotationY:40,scale:.6,autoAlpha:0,x:-50,ease:punchgs.Power3.easeInOut,overwrite:"all"},.3,{scale:1,rotationY:0,z:0,autoAlpha:1,x:0,rotationX:0,rotationZ:0,ease:punchgs.Power3.easeInOut,overwrite:"all"}],[".esg-slide",.5,{x:-1e4,y:0,scale:1,rotationZ:0,rotationX:0,rotationY:0,skewX:0,skewY:0,autoAlpha:1,transformOrigin:"50% 50%"},{autoAlpha:1,x:0,y:0,ease:punchgs.Power3.easeOut,overwrite:"all"},.3,{autoAlpha:1,x:-1e4,y:0,z:0,ease:punchgs.Power2.easeOut,overwrite:"all"}],[".esg-slideout",.5,{x:0,y:0,scale:1,rotationZ:0,rotationX:0,rotationY:0,skewX:0,skewY:0,autoAlpha:1,transformOrigin:"50% 50%"},{autoAlpha:1,x:0,y:0,ease:punchgs.Power3.easeOut,overwrite:"all"},.3,{autoAlpha:1,x:0,y:0,z:0,ease:punchgs.Power2.easeOut,overwrite:"all"}],[".esg-slideright",.5,{xPercent:-50,y:0,scale:1,rotationZ:0,rotationX:0,rotationY:0,skewX:0,skewY:0,autoAlpha:0,transformOrigin:"50% 50%"},{autoAlpha:1,xPercent:0,ease:punchgs.Power3.easeOut,overwrite:"all"},.3,{autoAlpha:0,xPercent:-50,z:0,ease:punchgs.Power2.easeOut,overwrite:"all"}],[".esg-sliderightout",.5,{autoAlpha:1,xPercent:0,ease:punchgs.Power3.easeOut,overwrite:"all"},{xPercent:50,y:0,scale:1,rotationZ:0,rotationX:0,rotationY:0,skewX:0,skewY:0,autoAlpha:0,transformOrigin:"50% 50%"},.3,{autoAlpha:1,xPercent:0,ease:punchgs.Power3.easeOut,overwrite:"all"}],[".esg-scaleleft",.5,{x:0,y:0,scaleX:0,rotationZ:0,rotationX:0,rotationY:0,skewX:0,skewY:0,autoAlpha:1,transformOrigin:"100% 50%"},{autoAlpha:1,x:0,scaleX:1,ease:punchgs.Power3.easeOut,overwrite:"all"},.3,{autoAlpha:1,x:0,z:0,scaleX:0,ease:punchgs.Power2.easeOut,overwrite:"all"}],[".esg-scaleright",.5,{x:0,y:0,scaleX:0,rotationZ:0,rotationX:0,rotationY:0,skewX:0,skewY:0,autoAlpha:1,transformOrigin:"0% 50%"},{autoAlpha:1,x:0,scaleX:1,ease:punchgs.Power3.easeOut,overwrite:"all"},.3,{autoAlpha:1,x:0,z:0,scaleX:0,ease:punchgs.Power2.easeOut,overwrite:"all"}],[".esg-slideleft",.5,{xPercent:50,y:0,scale:1,rotationZ:0,rotationX:0,rotationY:0,skewX:0,skewY:0,autoAlpha:0,transformOrigin:"50% 50%"},{autoAlpha:1,xPercent:0,ease:punchgs.Power3.easeOut,overwrite:"all"},.3,{autoAlpha:0,xPercent:50,z:0,ease:punchgs.Power2.easeOut,overwrite:"all"}],[".esg-slideleftout",.5,{autoAlpha:1,xPercent:0,ease:punchgs.Power3.easeOut,overwrite:"all"},{xPercent:-50,y:0,scale:1,rotationZ:0,rotationX:0,rotationY:0,skewX:0,skewY:0,autoAlpha:0,transformOrigin:"50% 50%"},.3,{autoAlpha:1,xPercent:0,ease:punchgs.Power3.easeOut,overwrite:"all"}],[".esg-slideup",.5,{x:0,yPercent:50,scale:1,rotationZ:0,rotationX:0,rotationY:0,skewX:0,skewY:0,autoAlpha:0,transformOrigin:"50% 50%"},{autoAlpha:1,yPercent:0,ease:punchgs.Power3.easeOut,overwrite:"all"},.3,{autoAlpha:0,yPercent:50,z:0,ease:punchgs.Power2.easeOut,overwrite:"all"}],[".esg-slideupout",.5,{autoAlpha:1,yPercent:0,ease:punchgs.Power3.easeOut,overwrite:"all"},{x:0,yPercent:-50,scale:1,rotationZ:0,rotationX:0,rotationY:0,skewX:0,skewY:0,autoAlpha:0,transformOrigin:"50% 50%"},.3,{autoAlpha:1,yPercent:0,ease:punchgs.Power3.easeOut,overwrite:"all"}],[".esg-slidedown",.5,{x:0,yPercent:-50,scale:1,rotationZ:0,rotationX:0,rotationY:0,skewX:0,skewY:0,autoAlpha:0,transformOrigin:"50% 50%"},{autoAlpha:1,yPercent:0,ease:punchgs.Power3.easeOut,overwrite:"all"},.3,{autoAlpha:0,yPercent:-50,z:0,ease:punchgs.Power2.easeOut,overwrite:"all"}],[".esg-slidedownout",.5,{autoAlpha:1,yPercent:0,z:0,ease:punchgs.Power3.easeOut,overwrite:"all"},{x:0,yPercent:50,scale:1,rotationZ:0,rotationX:0,z:10,rotationY:0,skewX:0,skewY:0,autoAlpha:0,transformOrigin:"50% 50%"},.3,{autoAlpha:1,yPercent:0,z:0,ease:punchgs.Power3.easeOut,overwrite:"all"}],[".esg-slideshortright",.5,{x:-30,y:0,scale:1,rotationZ:0,rotationX:0,rotationY:0,skewX:0,skewY:0,autoAlpha:0,transformOrigin:"50% 50%"},{autoAlpha:1,x:0,ease:punchgs.Power3.easeOut,overwrite:"all"},.3,{autoAlpha:0,x:-30,z:0,ease:punchgs.Power2.easeOut,overwrite:"all"}],[".esg-slideshortrightout",.5,{autoAlpha:1,x:0,ease:punchgs.Power3.easeOut,overwrite:"all"},{x:30,y:0,scale:1,rotationZ:0,rotationX:0,rotationY:0,skewX:0,skewY:0,autoAlpha:0,transformOrigin:"50% 50%"},.3,{autoAlpha:1,x:30,ease:punchgs.Power3.easeOut,overwrite:"all"}],[".esg-slideshortleft",.5,{x:30,y:0,scale:1,rotationZ:0,rotationX:0,rotationY:0,skewX:0,skewY:0,autoAlpha:0,transformOrigin:"50% 50%"},{autoAlpha:1,x:0,ease:punchgs.Power3.easeOut,overwrite:"all"},.3,{autoAlpha:0,x:30,z:0,ease:punchgs.Power2.easeOut,overwrite:"all"}],[".esg-slideshortleftout",.5,{autoAlpha:1,x:0,ease:punchgs.Power3.easeOut,overwrite:"all"},{x:-30,y:0,scale:1,rotationZ:0,rotationX:0,rotationY:0,skewX:0,skewY:0,autoAlpha:0,transformOrigin:"50% 50%"},.3,{autoAlpha:1,x:0,ease:punchgs.Power3.easeOut,overwrite:"all"}],[".esg-slideshortup",.5,{x:0,y:30,scale:1,rotationZ:0,rotationX:0,rotationY:0,skewX:0,skewY:0,autoAlpha:0,transformOrigin:"50% 50%"},{autoAlpha:1,y:0,ease:punchgs.Power3.easeOut,overwrite:"all"},.3,{autoAlpha:0,y:30,z:0,ease:punchgs.Power2.easeOut,overwrite:"all"}],[".esg-slideshortupout",.5,{autoAlpha:1,y:0,ease:punchgs.Power3.easeOut,overwrite:"all"},{x:0,y:-30,scale:1,rotationZ:0,rotationX:0,rotationY:0,skewX:0,skewY:0,autoAlpha:0,transformOrigin:"50% 50%"},.3,{autoAlpha:1,y:0,ease:punchgs.Power3.easeOut,overwrite:"all"}],[".esg-slideshortdown",.5,{x:0,y:-30,scale:1,rotationZ:0,rotationX:0,rotationY:0,skewX:0,skewY:0,autoAlpha:0,transformOrigin:"50% 50%"},{autoAlpha:1,y:0,ease:punchgs.Power3.easeOut,overwrite:"all"},.3,{autoAlpha:0,y:-30,z:0,ease:punchgs.Power2.easeOut,overwrite:"all"}],[".esg-slideshortdownout",.5,{autoAlpha:1,y:0,ease:punchgs.Power3.easeOut,overwrite:"all"},{x:0,y:30,scale:1,rotationZ:0,rotationX:0,rotationY:0,skewX:0,skewY:0,autoAlpha:0,transformOrigin:"50% 50%"},.3,{autoAlpha:1,y:0,ease:punchgs.Power3.easeOut,overwrite:"all"}],[".esg-skewright",.5,{xPercent:-100,y:0,scale:1,rotationZ:0,rotationX:0,rotationY:0,skewX:60,skewY:0,autoAlpha:0,transformOrigin:"50% 50%"},{autoAlpha:1,xPercent:0,skewX:0,ease:punchgs.Power3.easeOut,overwrite:"all"},.3,{autoAlpha:0,skewX:-60,xPercent:-100,z:0,ease:punchgs.Power2.easeOut,overwrite:"all"}],[".esg-skewrightout",.5,{autoAlpha:1,xPercent:0,skewX:0,ease:punchgs.Power3.easeOut,overwrite:"all"},{xPercent:100,y:0,scale:1,rotationZ:0,rotationX:0,rotationY:0,skewX:-60,skewY:0,autoAlpha:0,transformOrigin:"50% 50%"},.3,{autoAlpha:1,xPercent:0,skewX:0,ease:punchgs.Power3.easeOut,overwrite:"all"}],[".esg-skewleft",.5,{xPercent:100,y:0,scale:1,rotationZ:0,rotationX:0,rotationY:0,skewX:-60,skewY:0,autoAlpha:0,transformOrigin:"50% 50%"},{autoAlpha:1,xPercent:0,skewX:0,ease:punchgs.Power3.easeOut,overwrite:"all"},.3,{autoAlpha:0,xPercent:100,z:0,skewX:60,ease:punchgs.Power2.easeOut,overwrite:"all"}],[".esg-skewleftout",.5,{autoAlpha:1,xPercent:0,skewX:0,ease:punchgs.Power3.easeOut,overwrite:"all"},{xPercent:-100,y:0,scale:1,rotationZ:0,rotationX:0,rotationY:0,skewX:60,skewY:0,autoAlpha:0,transformOrigin:"50% 50%"},.3,{autoAlpha:1,xPercent:0,skewX:0,ease:punchgs.Power3.easeOut,overwrite:"all"}],[".esg-shifttotop",.5,{x:0,y:0,scale:1,rotationZ:0,rotationX:0,rotationY:0,skewX:0,skewY:0,autoAlpha:1,transformOrigin:"50% 50%"},{autoAlpha:1,y:0,ease:punchgs.Power3.easeOut,overwrite:"all"},.3,{autoAlpha:1,y:0,z:0,ease:punchgs.Power2.easeOut,overwrite:"all"}],[".esg-rollleft",.5,{xPercent:50,y:0,scale:1,rotationX:0,rotationY:0,skewX:0,skewY:0,autoAlpha:0,rotationZ:90,transformOrigin:"50% 50%"},{autoAlpha:1,xPercent:0,rotationZ:0,ease:punchgs.Power3.easeOut,overwrite:"all"},.3,{autoAlpha:0,xPercent:50,z:0,rotationZ:90,ease:punchgs.Power2.easeOut,overwrite:"all"}],[".esg-rollleftout",.5,{autoAlpha:1,xPercent:0,rotationZ:0,ease:punchgs.Power3.easeOut,overwrite:"all"},{xPercent:50,y:0,scale:1,rotationX:0,rotationY:0,skewX:0,skewY:0,autoAlpha:0,rotationZ:90,transformOrigin:"50% 50%"},.3,{autoAlpha:1,xPercent:0,rotationZ:0,ease:punchgs.Power3.easeOut,overwrite:"all"}],[".esg-rollright",.5,{xPercent:-50,y:0,scale:1,rotationX:0,rotationY:0,skewX:0,skewY:0,autoAlpha:0,rotationZ:-90,transformOrigin:"50% 50%"},{autoAlpha:1,xPercent:0,rotationZ:0,ease:punchgs.Power3.easeOut,overwrite:"all"},.3,{autoAlpha:0,xPercent:-50,rotationZ:-90,z:0,ease:punchgs.Power2.easeOut,overwrite:"all"}],[".esg-rollrightout",.5,{autoAlpha:1,xPercent:0,rotationZ:0,ease:punchgs.Power3.easeOut,overwrite:"all"},{xPercent:-50,y:0,scale:1,rotationX:0,rotationY:0,skewX:0,skewY:0,autoAlpha:0,rotationZ:-90,transformOrigin:"50% 50%"},.3,{autoAlpha:1,xPercent:0,rotationZ:0,ease:punchgs.Power3.easeOut,overwrite:"all"}],[".esg-falldown",.4,{x:0,scale:1,rotationZ:0,rotationX:0,rotationY:0,skewX:0,skewY:0,autoAlpha:0,yPercent:-100},{autoAlpha:1,yPercent:0,ease:punchgs.Power3.easeOut,overwrite:"all"},.4,{yPercent:-100,autoAlpha:0,z:0,ease:punchgs.Power2.easeOut,delay:.2,overwrite:"all"}],[".esg-falldownout",.4,{autoAlpha:1,yPercent:0,ease:punchgs.Back.easeOut,overwrite:"all"},{x:0,scale:1,rotationZ:0,rotationX:0,rotationY:0,skewX:0,skewY:0,autoAlpha:0,yPercent:100},.4,{autoAlpha:1,yPercent:0,ease:punchgs.Power3.easeOut,overwrite:"all"}],[".esg-rotatescale",.3,{x:0,y:0,rotationX:0,rotationY:0,skewX:0,skewY:0,autoAlpha:0,rotationZ:80,scale:.6,transformOrigin:"50% 50%"},{autoAlpha:1,scale:1,rotationZ:0,ease:punchgs.Back.easeOut,overwrite:"all"},.3,{autoAlpha:0,scale:.6,z:0,rotationZ:80,ease:punchgs.Power2.easeOut,overwrite:"all"}],[".esg-rotatescaleout",.3,{autoAlpha:1,scale:1,rotationZ:0,ease:punchgs.Back.easeOut,overwrite:"all"},{x:0,y:0,rotationX:0,rotationY:0,skewX:0,skewY:0,autoAlpha:0,rotationZ:80,scale:.6,transformOrigin:"50% 50%"},.3,{autoAlpha:1,scale:1,rotationZ:0,ease:punchgs.Back.easeOut,overwrite:"all"}],[".esg-zoomintocorner",.5,{x:0,y:0,scale:1,rotationZ:0,rotationX:0,rotationY:0,skewX:0,skewY:0,autoAlpha:1,transformOrigin:"20% 50%"},{autoAlpha:1,scale:1.2,x:0,y:0,ease:punchgs.Power3.easeOut,overwrite:"all"},.5,{autoAlpha:0,x:0,y:0,scale:1,autoAlpha:1,z:0,ease:punchgs.Power2.easeOut,overwrite:"all"}],[".esg-zoomouttocorner",.5,{x:0,y:0,scale:1.2,rotationZ:0,rotationX:0,rotationY:0,skewX:0,skewY:0,autoAlpha:1,transformOrigin:"80% 50%"},{autoAlpha:1,scale:1,x:0,y:0,ease:punchgs.Power3.easeOut,overwrite:"all"},.5,{autoAlpha:0,x:0,y:0,scale:1.2,autoAlpha:1,z:0,ease:punchgs.Power2.easeOut,overwrite:"all"}],[".esg-zoomtodefault",.5,{x:0,y:0,scale:1.2,rotationZ:0,rotationX:0,rotationY:0,skewX:0,skewY:0,autoAlpha:1,transformOrigin:"50% 50%"},{autoAlpha:1,scale:1,x:0,y:0,ease:punchgs.Power3.easeOut,overwrite:"all"},.5,{autoAlpha:0,x:0,y:0,scale:1.2,autoAlpha:1,z:0,ease:punchgs.Power2.easeOut,overwrite:"all"}],[".esg-zoomback",.5,{x:0,y:0,scale:.2,rotationZ:0,rotationX:0,rotationY:0,skewX:0,skewY:0,autoAlpha:0,transformOrigin:"50% 50%"},{autoAlpha:1,scale:1,x:0,y:0,ease:punchgs.Back.easeOut,overwrite:"all"},.5,{autoAlpha:0,x:0,y:0,scale:.2,autoAlpha:0,z:0,ease:punchgs.Power2.easeOut,overwrite:"all"}],[".esg-zoombackout",.5,{autoAlpha:1,scale:1,x:0,y:0,ease:punchgs.Back.easeOut,overwrite:"all"},{x:0,y:0,scale:.2,rotationZ:0,rotationX:0,rotationY:0,skewX:0,skewY:0,autoAlpha:0,transformOrigin:"50% 50%"},.5,{autoAlpha:1,scale:1,x:0,y:0,ease:punchgs.Back.easeOut,overwrite:"all"}],[".esg-zoomfront",.5,{x:0,y:0,scale:1.5,rotationZ:0,rotationX:0,rotationY:0,skewX:0,skewY:0,autoAlpha:0,transformOrigin:"50% 50%"},{autoAlpha:1,scale:1,x:0,y:0,ease:punchgs.Power3.easeOut,overwrite:"all"},.5,{autoAlpha:0,x:0,y:0,scale:1.5,z:0,ease:punchgs.Power2.easeOut,overwrite:"all"}],[".esg-zoomfrontout",.5,{autoAlpha:1,scale:1,x:0,y:0,ease:punchgs.Back.easeOut,overwrite:"all"},{x:0,y:0,scale:1.5,rotationZ:0,rotationX:0,rotationY:0,skewX:0,skewY:0,autoAlpha:0,transformOrigin:"50% 50%"},.5,{autoAlpha:1,scale:1,x:0,y:0,ease:punchgs.Back.easeOut,overwrite:"all"}],[".esg-flyleft",.8,{x:-80,y:0,z:0,scale:.3,rotationZ:0,rotationY:75,rotationX:10,skewX:0,skewY:0,autoAlpha:.01,transformOrigin:"30% 10%"},{x:0,y:0,rotationY:0,z:.001,rotationX:0,rotationZ:0,autoAlpha:1,scale:1,x:0,y:0,z:0,ease:punchgs.Power3.easeInOut,overwrite:"all"},.8,{autoAlpha:.01,x:-40,y:0,z:300,rotationY:60,rotationX:20,overwrite:"all"}],[".esg-flyleftout",.8,{x:0,y:0,rotationY:0,z:.001,rotationX:0,rotationZ:0,autoAlpha:1,scale:1,x:0,y:0,z:0,ease:punchgs.Power3.easeInOut,overwrite:"all"},{x:-80,y:0,z:0,scale:.3,rotationZ:0,rotationY:75,rotationX:10,skewX:0,skewY:0,autoAlpha:.01,transformOrigin:"30% 10%"},.8,{x:0,y:0,rotationY:0,z:.001,rotationX:0,rotationZ:0,autoAlpha:1,scale:1,x:0,y:0,z:0,ease:punchgs.Power3.easeInOut,overwrite:"all"}],[".esg-flyright",.8,{scale:1,skewX:0,skewY:0,autoAlpha:0,x:80,y:0,z:0,scale:.3,rotationZ:0,rotationY:-75,rotationX:10,transformOrigin:"70% 20%"},{x:0,y:0,rotationY:0,z:.001,rotationX:0,autoAlpha:1,scale:1,x:0,y:0,z:0,ease:punchgs.Power3.easeInOut,overwrite:"all"},.8,{autoAlpha:0,x:40,y:-40,z:300,rotationY:-60,rotationX:-40,overwrite:"all"}],[".esg-flyrightout",.8,{x:0,y:0,rotationY:0,z:.001,rotationX:0,autoAlpha:1,scale:1,x:0,y:0,z:0,ease:punchgs.Power3.easeInOut,overwrite:"all"},{scale:1,skewX:0,skewY:0,autoAlpha:0,x:80,y:0,z:0,scale:.3,rotationZ:0,rotationY:-75,rotationX:10,transformOrigin:"70% 20%"},.8,{x:0,y:0,rotationY:0,z:.001,rotationX:0,autoAlpha:1,scale:1,x:0,y:0,z:0,ease:punchgs.Power3.easeInOut,overwrite:"all"}],[".esg-mediazoom",.3,{x:0,y:0,scale:1,rotationZ:0,rotationX:0,rotationY:0,skewX:0,skewY:0,autoAlpha:1,transformOrigin:"50% 50%"},{autoAlpha:1,scale:1.4,x:0,y:0,ease:punchgs.Back.easeOut,overwrite:"all"},.3,{autoAlpha:0,x:0,y:0,scale:1,z:0,ease:punchgs.Power2.easeOut,overwrite:"all"}],[".esg-zoomandrotate",.6,{x:0,y:0,scale:1,rotationZ:0,rotationX:0,rotationY:0,skewX:0,skewY:0,autoAlpha:1,transformOrigin:"50% 50%"},{autoAlpha:1,scale:1.4,x:0,y:0,rotationZ:30,ease:punchgs.Power2.easeOut,overwrite:"all"},.4,{x:0,y:0,scale:1,z:0,rotationZ:0,ease:punchgs.Power2.easeOut,overwrite:"all"}],[".esg-pressback",.5,{x:0,y:0,scale:1,rotationZ:0,rotationX:0,rotationY:0,skewX:0,skewY:0,autoAlpha:1,transformOrigin:"50% 50%"},{rotationY:0,autoAlpha:1,scale:.8,ease:punchgs.Power3.easeOut,overwrite:"all"},.3,{rotationY:0,autoAlpha:1,z:0,scale:1,ease:punchgs.Power2.easeOut,overwrite:"all"}],[".esg-3dturnright",.5,{x:0,y:0,scale:1,rotationZ:0,rotationX:0,rotationY:0,skewX:0,skewY:0,autoAlpha:1,transformPerspective:600},{x:-40,y:0,scale:.8,rotationZ:2,rotationX:5,rotationY:-28,skewX:0,skewY:0,autoAlpha:1,transformOrigin:"100% 50% 40%",transformPerspective:600,ease:punchgs.Power3.easeOut,overwrite:"all"},.3,{z:0,x:0,y:0,scale:1,rotationZ:0,rotationX:0,rotationY:0,skewX:0,skewY:0,autoAlpha:1,force3D:"auto",ease:punchgs.Power2.easeOut,overwrite:"all"},!0]];
jQuery.fn.extend({tpessential:function(e){return jQuery.fn.tpessential.defaults={forceFullWidth:"off",forceFullScreen:"off",fullScreenOffsetContainer:"",row:3,column:4,space:10,pageAnimation:"fade",animSpeed:600,delayBasic:.08,smartPagination:"on",paginationScrollToTop:"off",paginationScrollToTopOffset:200,layout:"even",rtl:"off",aspectratio:"16:9",bgPosition:"center center",bgSize:"cover",videoJsPath:"",overflowoffset:0,mainhoverdelay:0,rowItemMultiplier:[],filterGroupClass:"",filterType:"",filterLogic:"or",showDropFilter:"hover",evenGridMasonrySkinPusher:"on",loadMoreType:"none",loadMoreItems:[],loadMoreAmount:5,loadMoreTxt:"Load More",loadMoreNr:"on",loadMoreEndTxt:"No More Items for the Selected Filter",loadMoreAjaxUrl:"",loadMoreAjaxToken:"",loadMoreAjaxAction:"",lazyLoad:"off",lazyLoadColor:"#ff0000",gridID:0,spinner:"",spinnerColor:"",lightBoxMode:"single",cobblesPattern:"",searchInput:".faqsearch",googleFonts:"",googleFontJS:"//ajax.googleapis.com/ajax/libs/webfont/1/webfont.js",ajaxContentTarget:"",ajaxScrollToOnLoad:"off",ajaxScrollToOffset:100,ajaxCallback:"",ajaxCallbackArgument:"on",ajaxCssUrl:"",ajaxJsUrl:"",ajaxCloseButton:"on",ajaxNavButton:"on",ajaxCloseTxt:"Close",ajaxCloseType:"type1",ajaxClosePosition:"tr",ajaxCloseInner:"true",ajaxCloseStyle:"light",ajaxTypes:[],cookies:{search:"off",filter:"off",pagination:"off",loadmore:"off",timetosave:"30"}},e=jQuery.extend({},jQuery.fn.tpessential.defaults,e),"undefined"==typeof WebFontConfig&&(WebFontConfig=new Object),this.each(function(){function t(e,t){mainPreparing(e,t),t.initialised="ready",jQuery("body").trigger("essentialready",e.attr("id"))}function a(e,t){var a=e.offset().top+e.height()-jQuery(document).scrollTop();jQuery(window).height()>a&&1!=t.data("loading")&&(t.data("loading",1),loadMoreItems(e,o))}var o=e,i=jQuery(this);if(i==undefined)return!1;if(i.parent().css({position:"relative"}),"cobbles"==o.layout?(o.layout="even",o.evenCobbles="on"):o.evenCobbles="off","true"!=o.get&&1!=o.get){if(o.get=!0,o.filterGroupClass=o.filterGroupClass==undefined||0==o.filterGroupClass.length?"#"+i.attr("id"):"."+o.filterGroupClass,1==window.tplogs)try{console.groupCollapsed("Essential Grid  2.0.5 Initialisation on "+i.attr("id")),console.groupCollapsed("Used Options:"),console.info(e),console.groupEnd(),console.groupCollapsed("Tween Engine:")}catch(r){}if(punchgs.TweenLite==undefined){if(1==window.tplogs)try{console.error("GreenSock Engine Does not Exist!")}catch(r){}return!1}if(punchgs.force3D=!0,1==window.tplogs)try{console.info("GreenSock Engine Version in Essential Grid:"+punchgs.TweenLite.version)}catch(r){}if(punchgs.TweenLite.lagSmoothing(2e3,16),punchgs.force3D="auto",1==window.tplogs)try{console.groupEnd(),console.groupEnd()}catch(r){}jQuery("body").data("fullScreenMode",!1),jQuery(window).on("mozfullscreenchange webkitfullscreenchange fullscreenchange",function(){jQuery("body").data("fullScreenMode",!jQuery("body").data("fullScreenMode"))}),buildLoader(i.parent(),o),o.firstshowever==undefined&&jQuery(o.filterGroupClass+".esg-navigationbutton,"+o.filterGroupClass+" .esg-navigationbutton").css({visibility:"hidden"}),i.parent().append('<div class="esg-relative-placeholder" style="width:100%;height:auto"></div>'),i.wrap('<div class="esg-container-fullscreen-forcer" style="position:relative;left:0px;top:0px;width:100%;height:auto;"></div>');var n=i.parent().parent().find(".esg-relative-placeholder").offset().left;("on"==o.forceFullWidth||"on"==o.forceFullScreen)&&i.closest(".esg-container-fullscreen-forcer").css({left:0-n,width:jQuery(window).width()}),o.animDelay=0==o.delayBasic?"off":"on",o.container=i,i.find("ul").first().addClass("mainul").wrap('<div class="esg-overflowtrick"></div>');var s=jQuery(o.filterGroupClass+".esg-navbutton-solo-left,"+o.filterGroupClass+" .esg-navbutton-solo-left"),l=jQuery(o.filterGroupClass+".esg-navbutton-solo-right,"+o.filterGroupClass+" .esg-navbutton-solo-right");s.length>0&&(s.css({marginTop:0-s.height()/2}),s.appendTo(i.find(".esg-overflowtrick"))),l.length>0&&(l.css({marginTop:0-l.height()/2}),l.appendTo(i.find(".esg-overflowtrick"))),punchgs.CSSPlugin.defaultTransformPerspective=1200,o.animSpeed=o.animSpeed/1e3,o.delayBasic=o.delayBasic/100,setOptions(i,o),o.filter=o.statfilter,o.origcolumn=o.column,o.currentpage=0,i.addClass("esg-layout-"+o.layout);{loadVideoApis(i,o)}if("even"==o.layout&&"on"==o.forceFullScreen){var u=jQuery(window).height();if(o.fullScreenOffsetContainer!=undefined)try{var d=o.fullScreenOffsetContainer.split(",");d&&jQuery.each(d,function(e,t){u-=jQuery(t).outerHeight(!0),u<o.minFullScreenHeight&&(u=o.minFullScreenHeight)})}catch(r){}var c=i.find(".esg-overflowtrick").first(),p=i.find("ul").first();c.css({display:"block",height:u+"px"}),p.css({display:"block",height:u+"px"}),i.closest(".eg-grid-wrapper, .myportfolio-container").css({height:"auto"}).removeClass("eg-startheight")}if(0!=o.googleFonts.length&&"masonry"==o.layout){var h=(o.googleFonts.length,!0);if(jQuery("head").find("*").each(function(){jQuery(this).attr("src")!=undefined&&jQuery(this).attr("src").indexOf("webfont.js")>0&&(h=!1)}),WebFontConfig.active==undefined&&h){WebFontConfig={google:{families:o.googleFonts},active:function(){t(i,o)},inactive:function(){t(i,o)},timeout:1500};var f=document.createElement("script");f.src=("https:"==document.location.protocol?"https":"http")+"://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js",f.type="text/javascript",f.async="true";var g=document.getElementsByTagName("script")[0];g.parentNode.insertBefore(f,g)}else t(i,o)}else t(i,o);if("button"==o.loadMoreType){i.append('<div class="esg-loadmore-wrapper" style="text-align:center"><div class="esg-navigationbutton esg-loadmore">LOAD MORE</div></div>');var w=i.find(".esg-loadmore"),m=o.loadMoreTxt+" ("+checkMoreToLoad(i,o).length+")";"off"==o.loadMoreNr&&(m=o.loadMoreTxt),w.html(m),w.click(function(){1!=w.data("loading")&&loadMoreItems(i,o)}),0==checkMoreToLoad(i,o).length&&w.remove()}else if("scroll"==o.loadMoreType){i.append('<div style="display:inline-block" class="esg-navigationbutton esg-loadmore">LOAD MORE</div>');var w=i.find(".esg-loadmore"),m=o.loadMoreTxt+" ("+checkMoreToLoad(i,o).length+")";"off"==o.loadMoreNr&&(m=o.loadMoreTxt),w.html(m),jQuery(document,window).scroll(function(){a(i,w)}),a(i,w),0==checkMoreToLoad(i,o).length&&w.remove()}checkAvailableFilters(i,o),tabBlurringCheck(i,o)}})},esappend:function(){var e=jQuery(this);return prepareItemsInGrid(opt,!0),organiseGrid(opt),prepareSortingAndOrders(e),opt.lastslide},esskill:function(){var e=jQuery(this);e.find("*").each(function(){jQuery(this).off("click, focus, focusin, hover, play, ended, stop, pause, essentialready"),jQuery(this).remove()}),e.remove(),e.html(),e=null},esreadsettings:function(e){e=e==undefined?new Object:e;var t=jQuery(this),a=getOptions(t);return a},esredraw:function(e){e=e==undefined?new Object:e;var t=jQuery(this),a=getOptions(t);if(e.space!=undefined&&(a.space=parseInt(e.space,0)),e.row!=undefined&&(a.row=parseInt(e.row,0)),e.rtl!=undefined&&(a.rtl=e.rtl),e.aspectratio!=undefined&&(a.aspectratio=e.aspectratio),e.forceFullWidth!=undefined)if(a.forceFullWidth=e.forceFullWidth,"on"==a.forceFullWidth){var o=t.parent().parent().find(".esg-relative-placeholder").offset().left;t.closest(".esg-container-fullscreen-forcer").css({left:0-o,width:jQuery(window).width()})}else t.closest(".esg-container-fullscreen-forcer").css({left:0,width:"auto"});if(e.rowItemMultiplier!=undefined&&(a.rowItemMultiplier=e.rowItemMultiplier),e.responsiveEntries!=undefined&&(a.responsiveEntries=e.responsiveEntries),e.column!=undefined){if(e.column<=0||e.column>=20){var i=getBestFitColumn(a,jQuery(window).width(),"id");a.column=i.column,a.columnindex=i.index}else a.column=parseInt(e.column,0);a.origcolumn=a.column}e.animSpeed!=undefined&&(a.animSpeed=e.animSpeed/1e3),e.delayBasic!=undefined&&(a.delayBasic=e.delayBasic/100),e.pageAnimation!=undefined&&(a.pageAnimation=e.pageAnimation),e.changedAnim!=undefined&&(a.changedAnim=e.changedAnim),a.started=!0,1==e.silent&&(a.silent=!0),setOptions(t,a),setItemsOnPages(a),organiseGrid(a)},esquickdraw:function(){var e=jQuery(this),t=getOptions(e);t.silent=!0,setOptions(e,t),setItemsOnPages(t),organiseGrid(t)},esreinit:function(){var e=jQuery(this);return prepareItemsInGrid(opt,!0),organiseGrid(opt),prepareSortingAndOrders(e),opt.lastslide},somemethodb:function(){return this.each(function(){jQuery(this)})}});var vis=function(){var e,t,a={hidden:"visibilitychange",webkitHidden:"webkitvisibilitychange",mozHidden:"mozvisibilitychange",msHidden:"msvisibilitychange"};for(e in a)if(e in document){t=a[e];break}return function(a){return a&&document.addEventListener(t,a),!document[e]}}(),tabBlurringCheck=function(){var e=document.documentMode===undefined,t=window.chrome;jQuery("body").hasClass("esg-blurlistenerexists")||(jQuery("body").addClass("esg-blurlistenerexists"),e&&!t?jQuery(window).on("focusin",function(){setTimeout(function(){jQuery("body").find(".esg-grid.esg-container").each(function(){jQuery(this).esquickdraw()})},300)}).on("focusout",function(){}):window.addEventListener?window.addEventListener("focus",function(){setTimeout(function(){jQuery("body").find(".esg-grid.esg-container").each(function(){jQuery(this).esquickdraw()})},300)},!1):window.attachEvent("focus",function(){setTimeout(function(){jQuery("body").find(".esg-grid.esg-container").each(function(){jQuery(this).esquickdraw()})},300)}))},is_mobile=function(){var e=["android","webos","iphone","ipad","blackberry","Android","webos",,"iPod","iPhone","iPad","Blackberry","BlackBerry"],t=!1;for(i in e)navigator.userAgent.split(e[i]).length>1&&(t=!0);return t},waitForLoads=function(e,t){var a=e.closest(".esg-grid").parent().parent().find(".esg-loader");jQuery.each(e,function(e,o){o=jQuery(o),a.length>0&&punchgs.TweenLite.to(a,.2,{autoAlpha:1,delay:.5}),!o.hasClass("loadedmedia")&&"even"!=t.layout&&o.find("img").length>0&&o.hasClass("itemtoshow")&&o.removeClass("itemtoshow").addClass("showmeonload"),o.hasClass("loadedmedia")||"even"==t.layout||0!=o.find("img").length||(evenImageRatio(o.find("img"),t),waittorungGrid(o,t,!0))});var o=setInterval(function(){t.bannertimeronpause=!0,t.cd=0;var i=0;e.find("img").each(function(){var e=jQuery(this);1!=e.data("lazydone")&&"on"==t.lazyLoad&&e.parent().find(".lazyloadcover").length<1&&e.parent().append('<div class="lazyloadcover" style="position:absolute;top:0px;left:0px;z-index:10;width:100%;height:100%;background:'+t.lazyLoadColor+'"></div>'),1!=e.data("lazydone")&&3>i&&(i++,loadAllPrepared(jQuery(this),t))}),0==i&&a.length>0&&(punchgs.TweenLite.killTweensOf(a,!1),punchgs.TweenLite.to(a,.2,{autoAlpha:0})),0!=i||e.closest(".mainul").hasClass("gridorganising")||(clearInterval(o),runGrid(t))},50)}}(jQuery),function(e,t){"use strict";function a(e){return e&&e.toLowerCase?e.toLowerCase():e}function o(e,t){for(var a=0,o=e.length;o>a;a++)if(e[a]==t)return!i;return i}var i=!1,r=null,n=parseFloat,s=Math.min,l=/(-?\d+\.?\d*)$/g,u=/(\d+\.?\d*)$/g,d=[],c=[],p=function(e){return"string"==typeof e},h=function(e,t){for(var a,o=e.length,i=o;i--;)a=o-i-1,t(e[a],a)},f=Array.prototype.indexOf||function(e){var t=this.length,a=Number(arguments[1])||0;for(a=0>a?Math.ceil(a):Math.floor(a),0>a&&(a+=t);t>a;a++)if(a in this&&this[a]===e)return a;return-1};e.tinysort={id:"TinySort",version:"1.5.6",copyright:"Copyright (c) 2008-2013 Ron Valstar",uri:"http://tinysort.sjeiti.com/",licensed:{MIT:"http://www.opensource.org/licenses/mit-license.php",GPL:"http://www.gnu.org/licenses/gpl.html"},plugin:function(){var e=function(e,t){d.push(e),c.push(t)};return e.indexOf=f,e}(),defaults:{order:"asc",attr:r,data:r,useVal:i,place:"start",returns:i,cases:i,forceStrings:i,ignoreDashes:i,sortFunction:r}},e.fn.extend({tinysort:function(){var g,w,m,v,y=this,b=[],x=[],j=[],C=[],k=0,A=[],T=[],O=function(e){h(d,function(t){t.call(t,e)})},P=function(e,t){return"string"==typeof t&&(e.cases||(t=a(t)),t=t.replace(/^\s*(.*?)\s*$/i,"$1")),t},Q=function(e,t){var a=0;for(0!==k&&(k=0);0===a&&v>k;){var o=C[k],r=o.oSettings,s=r.ignoreDashes?u:l;if(O(r),r.sortFunction)a=r.sortFunction(e,t);else if("rand"==r.order)a=Math.random()<.5?1:-1;else{var d=i,f=P(r,e.s[k]),g=P(r,t.s[k]);if(!r.forceStrings){var w=p(f)?f&&f.match(s):i,m=p(g)?g&&g.match(s):i;if(w&&m){var y=f.substr(0,f.length-w[0].length),b=g.substr(0,g.length-m[0].length);y==b&&(d=!i,f=n(w[0]),g=n(m[0]))}}a=o.iAsc*(g>f?-1:f>g?1:0)}h(c,function(e){a=e.call(e,d,f,g,a)}),0===a&&k++}return a};for(g=0,m=arguments.length;m>g;g++){var L=arguments[g];p(L)?A.push(L)-1>T.length&&(T.length=A.length-1):T.push(L)>A.length&&(A.length=T.length)}for(A.length>T.length&&(T.length=A.length),v=A.length,0===v&&(v=A.length=1,T.push({})),g=0,m=v;m>g;g++){var I=A[g],Y=e.extend({},e.tinysort.defaults,T[g]),X=!(!I||""===I),z=X&&":"===I[0];C.push({sFind:I,oSettings:Y,bFind:X,bAttr:!(Y.attr===r||""===Y.attr),bData:Y.data!==r,bFilter:z,$Filter:z?y.filter(I):y,fnSort:Y.sortFunction,iAsc:"asc"==Y.order?1:-1})}return y.each(function(a,o){var i,r=e(o),n=r.parent().get(0),s=[];for(w=0;v>w;w++){var l=C[w],u=l.bFind?l.bFilter?l.$Filter.filter(o):r.find(l.sFind):r;s.push(l.bData?u.data(l.oSettings.data):l.bAttr?u.attr(l.oSettings.attr):l.oSettings.useVal?u.val():u.text()),i===t&&(i=u)}var d=f.call(j,n);0>d&&(d=j.push(n)-1,x[d]={s:[],n:[]}),i.length>0?x[d].s.push({s:s,e:r,n:a}):x[d].n.push({e:r,n:a})}),h(x,function(e){e.s.sort(Q)}),h(x,function(e){var t=e.s,a=e.n,r=t.length,n=a.length,l=r+n,u=[],d=l,c=[0,0];switch(Y.place){case"first":h(t,function(e){d=s(d,e.n)});break;case"org":h(t,function(e){u.push(e.n)});break;case"end":d=n;break;default:d=0}for(g=0;l>g;g++){var p=o(u,g)?!i:g>=d&&d+r>g,f=p?0:1,w=(p?t:a)[c[f]].e;w.parent().append(w),(p||!Y.returns)&&b.push(w.get(0)),c[f]++}}),y.length=0,Array.prototype.push.apply(y,b),y}}),e.fn.TinySort=e.fn.Tinysort=e.fn.tsort=e.fn.tinysort}(jQuery);