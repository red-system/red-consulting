-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 17, 2020 at 10:59 AM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 7.0.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rs_redconsulting`
--

-- --------------------------------------------------------

--
-- Table structure for table `about`
--

CREATE TABLE `about` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keywords` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`, `email`, `name`) VALUES
(2, 'mahendrawardana', '6001c26274f43ac7c6b2be2662a027f6', 'mahendra.adi.wardana@gmail.com', 'Mahendra Wardana'),
(32, 'agus', '5f4dcc3b5aa765d61d8327deb882cf99', 'agusdownload5293@gmail.com', 'Agus Arimbawa');

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE `blog` (
  `id` int(11) NOT NULL,
  `id_team` int(11) DEFAULT NULL,
  `id_blog_category` int(11) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `thumbnail` text,
  `thumbnail_alt` text,
  `description` text NOT NULL,
  `use` enum('yes','no') DEFAULT 'no',
  `views` int(11) DEFAULT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keywords` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `blog_category`
--

CREATE TABLE `blog_category` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `thumbnail` varchar(255) DEFAULT NULL,
  `thumbnail_alt` varchar(255) DEFAULT NULL,
  `use` enum('yes','no') DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blog_category`
--

INSERT INTO `blog_category` (`id`, `title`, `description`, `thumbnail`, `thumbnail_alt`, `use`, `meta_keywords`, `meta_title`, `meta_description`) VALUES
(1, 'Perpajakan', '<p>Pajak</p>', 'perpajakan.png', 'Pajak', 'yes', 'pajak, artikel', 'Perpajakan', 'Artikel mengenai perpajakan'),
(2, 'Manajemen Laporan Keuagan', '<p>Manajemen Laporan Keuangan</p>', 'manajemen-laporan-keuagan.png', 'Manajemen Laporan Keuangan', 'yes', 'laporan keuangan, manajemen', 'Manajemen Laporan Keuangan', 'Manajemen Laporan Keuangan'),
(3, 'Finance Controller', '<p>Finance Controller</p>', 'finance-controller.png', 'Finance Controller', 'yes', 'finance controller', 'Finance Controller', 'Finance Controller');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `language_id` int(11) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `thumbnail` varchar(255) DEFAULT NULL,
  `thumbnail_alt` varchar(255) DEFAULT NULL,
  `use` enum('yes','no') DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE `comment` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `thumbnail` varchar(255) DEFAULT NULL,
  `thumbnail_alt` varchar(255) DEFAULT NULL,
  `use` enum('yes','no') NOT NULL,
  `date` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comment`
--

INSERT INTO `comment` (`id`, `title`, `description`, `thumbnail`, `thumbnail_alt`, `use`, `date`) VALUES
(1, 'Lisa Larson', 'Being a company''s director requires maximum attention and devotion. This was exactly what I felt when turned to your products and services. All our questions and inquiries were answered effectively and right away. Our website has never looked better, ever', 'lisa-larson.jpg', 'Lisa Larson', 'yes', '');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `position` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `publish` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE `email` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `use` enum('yes','no') NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE `gallery` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `thumbnail` varchar(255) NOT NULL,
  `thumbnail_alt` text,
  `use` enum('yes','no') DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `language`
--

CREATE TABLE `language` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `thumbnail` varchar(255) DEFAULT NULL,
  `code` char(10) DEFAULT NULL,
  `use` enum('yes','no') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `language`
--

INSERT INTO `language` (`id`, `title`, `thumbnail`, `code`, `use`) VALUES
(1, 'English', 'english.png', 'en', 'no'),
(2, 'Indonesia', 'indonesia.png', 'id', 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(11) NOT NULL,
  `language_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `title_sub` varchar(255) DEFAULT NULL,
  `description` text,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `seo` enum('yes','no') DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `language_id`, `title`, `title_sub`, `description`, `meta_title`, `meta_keywords`, `meta_description`, `type`, `seo`) VALUES
(1, 2, 'Testimonial', 'Kami sudah mendapatkan banyak testimonial dari klien / partner kami untuk membagikan pengalaman mereka saat melakukan kerjasama dengan kami.', '<p>te</p>', 'Testimonial', 'testimonial, klien, partner, kerjasama', 'Kami sudah mendapatkan banyak testimonial dari klien / partner kami untuk membagikan pengalaman mereka saat melakukan kerjasama dengan kami.', 'testimonial', 'yes'),
(2, 2, 'Artikel Terbaru Kami', 'Baca artkel terbaru dari kami yang meliputi bidang perpajakan dan keuangan.', '<p>-</p>', NULL, NULL, NULL, 'blog', 'yes'),
(3, 2, 'Selamat Datang di RED Consulting', 'Kami ada untuk memberikan pelayanan yang terbaik untuk anda.', '<p align="justify">Derasnya persaingan bisnis yang nyaris tanpa kompromi, membuat management bisnis dan keuangan yang tepat dan efektif seakan menjadi sesuatu yang wajib dan harus diberikan ruang istimewa dalam sebuah perusahaan atau bisnis. Atas dasar itulah <span class="color-active">RED</span> Consulting hadir untuk menjadi solusi terbaik dalam menghadirkan manajemen bisnis yang tepat dan efektif untuk perusahaan atau bisnis anda sehingga perusahaan atau bisnis anda bisa bertumbuh dengan kuat dan stabil.</p>\r\n<p align="justify"><span class="color-active">RED</span> Consulting adalah perusahaan Konsultan Pajak dan Keuangan yang bernaung di bawah PT. Guna Artha Kencana dan berkantor di Jalan Ratna No. 68 G, Denpasa-Bali.</p>\r\n<p align="justify">Didukung oleh tim bisnis yang telah berpengalaman lebih dari 10 tahun, <span class="color-active">RED</span> Consulting siap untuk menjadi pilihan terbaik bagi pelaku usaha (individu atau perusahaan) yang ingin menjadikan manajemen bisnis dan keuangan yang tepat dan efektif sebagai pondasi dasar yang kuat dalam berbisnis.</p>', 'RED Consulting', 'consulting, konsultan pajak, konsultan keuangan, manajemen bisnis ', 'RED Consulting adalah perusahaan Konsultan Pajak dan Keuangan yang bernaung di bawah PT. Guna Artha Kencana. RED Consulting hadir untuk menjadi solusi terbaik dalam menghadirkan manajemen bisnis yang tepat dan efektif untuk perusahaan atau bisnis anda seh', 'profile', 'yes'),
(4, 2, 'Layanan Utama Kami', 'Layanan terbaik kami yang dapat menjadi solusi perusahaan anda', '<p>-</p>', NULL, NULL, NULL, 'services', 'yes'),
(5, 2, '-', '-', '<p>R.E.D. CONSULTING hadir untuk membantu perusahaan dengan membuka seluruh potensi yang dimiliki untuk tumbuh lebih besar, dengan turut menghadirkan solusi terbaik dalam memenuhi penataan keuangan perusahaan. Kami melayani perusahaan di berbagai bidang industri dengan mengedepankan layanan</p>', NULL, NULL, NULL, 'footer', 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `reservation`
--

CREATE TABLE `reservation` (
  `id` int(11) NOT NULL,
  `id_tour` int(11) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` int(50) NOT NULL,
  `address` text,
  `country` varchar(255) NOT NULL,
  `tour_start` varchar(255) NOT NULL,
  `total_adult` int(50) NOT NULL,
  `total_children` int(50) NOT NULL,
  `message` int(50) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `id` int(11) NOT NULL,
  `language_id` int(11) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `thumbnail` varchar(255) NOT NULL,
  `thumbnail_alt` varchar(255) DEFAULT NULL,
  `url` varchar(255) NOT NULL,
  `use` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id`, `language_id`, `title`, `description`, `thumbnail`, `thumbnail_alt`, `url`, `use`) VALUES
(1, 2, ' Memberikan yang terbaik dalam Konsultasi', 'Dengan memiliki tim yang berpengalaman dibidangnya anda mendapatkan solusi yang terbaik dan tepat.', '-memberikan-yang-terbaik-dalam-konsultasi.jpg', 'Mendapatkan solusi yang terbaik dan tepat dalam masalah anda.', '', 'yes'),
(2, 2, 'Menyelesaikan setiap permasalahan perusahaan atau bisnis', 'Membantu dalam pengelolaan manajemen keuangan hingga permasalahan perpajakan.', 'menyelesaikan-setiap-permasalahan-perusahaan-atau-bisnis.jpg', 'Membantu dalam pengelolaan manajemen keuangan hingga permasalahan perpajakan', '', 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `team`
--

CREATE TABLE `team` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `position` varchar(255) NOT NULL,
  `description` text,
  `thumbnail` varchar(255) NOT NULL,
  `thumbnail_alt` text,
  `use` enum('yes','no') DEFAULT 'no',
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_description` text,
  `meta_keywords` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `team`
--

INSERT INTO `team` (`id`, `title`, `position`, `description`, `thumbnail`, `thumbnail_alt`, `use`, `meta_title`, `meta_description`, `meta_keywords`) VALUES
(1, 'I Made Dwi Harmana SE., M.Si., BKP', 'Tax Consultant (No Izin Praktek : KEP-3580/IP.B/PJ/2018)', '<p>I Made Dwi Harmana SE., M.Si., BKP menjabat sebagai Tax Consultant (No Izin Praktek : KEP-3580/IP.B/PJ/2018)</p>', 'i-made-dwi-harmana-se.--m.si.--bkp', NULL, 'yes', 'I Made Dwi Harmana SE., M.Si., BKP', 'I Made Dwi Harmana SE., M.Si., BKP', 'I Made Dwi Harmana SE., M.Si., BKP'),
(2, 'A.A Sagung Alit Dwijayanti', 'Tax & Accounting Manager', '<p>A.A Sagung Alit Dwijayanti menjabat sebagai Tax & Accounting Manager</p>', 'a.a-sagung-alit-dwijayanti', NULL, 'yes', 'A.A Sagung Alit Dwijayanti', 'A.A Sagung Alit Dwijayanti', 'A.A Sagung Alit Dwijayanti'),
(3, 'Ni Kadek Miantari', 'Tax Officer', '<p>Ni Kadek Miantari menjabat sebagai Tax Officer</p>', 'ni-kadek-miantari.jpg', NULL, 'yes', 'Ni Kadek Miantari', 'Ni Kadek Miantari', 'Ni Kadek Miantari'),
(4, 'I Gede Wahyu Indrawan', 'Tax Officer', '<p>I Gede Wahyu Indrawan menjabat sebagai Tax Officer</p>', 'i-gede-wahyu-indrawan.jpg', NULL, 'yes', 'I Gede Wahyu Indrawan', 'I Gede Wahyu Indrawan', 'I Gede Wahyu Indrawan'),
(5, 'Ni Putu Juliastari', 'Tax Officer', '<p>Ni Putu Juliastari menjabat sebagai Tax Officer</p>', 'ni-putu-juliastari.jpg', NULL, 'yes', 'Ni Putu Juliastari', 'Ni Putu Juliastari', 'Ni Putu Juliastari');

-- --------------------------------------------------------

--
-- Table structure for table `tour`
--

CREATE TABLE `tour` (
  `id` int(11) NOT NULL,
  `language_id` int(11) DEFAULT NULL,
  `id_category` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `title_sub` varchar(255) DEFAULT NULL,
  `use` enum('yes','no') DEFAULT NULL,
  `description` text NOT NULL,
  `thumbnail` varchar(255) NOT NULL,
  `thumbnail_alt` varchar(255) NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keywords` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tour`
--

INSERT INTO `tour` (`id`, `language_id`, `id_category`, `title`, `title_sub`, `use`, `description`, `thumbnail`, `thumbnail_alt`, `meta_title`, `meta_description`, `meta_keywords`, `created_at`, `updated_at`) VALUES
(1, 2, '', 'Konsultan Pajak', 'Kami dapat membantu dalam konsultasi perpajakan, perencanaan pajak, review kewajiban wajib pajak, pengurusan SPT Masa dan SPT Tahunan, penyelesaian masalah perpajakan, administrasi Pajak, dan lain sebagainya.', 'yes', '', 'konsultan-pajak.png', 'Konsultan Pajak', 'Konsultan Pajak', 'Kami dapat membantu dalam konsultasi perpajakan, perencanaan pajak, review kewajiban wajib pajak, pengurusan SPT Masa dan SPT Tahunan, penyelesaian masalah perpajakan, administrasi Pajak, dan lain sebagainya.', 'konsultan pajak, perencanaan pajak, review kewajiban wajib pajak, pengurusan SPT Masa dan SPT Tahunan, penyelesaian masalah perpajakan, administrasi Pajak, dan lain sebagainya', '2020-02-17 08:53:04', '2020-02-17 09:28:12'),
(2, 2, '', 'Manajemen Laporan Keuangan', '-', 'yes', '<p>Menyusun laporan keuangan sehingga kondisi perusahaan dapat terbaca dengan jelas, Bagian Laporan Keuangan yang disajikan meliputi, laporan laba rugi, laporan neraca, laporan penyusutan & amortisasi, summery, advice.</p>', 'manajemen-laporan-keuangan.png', 'Manajemen Laporan Keuangan', 'Manajemen Laporan Keuangan', 'Menyusun laporan keuangan sehingga kondisi perusahaan dapat terbaca dengan jelas, Bagian Laporan Keuangan yang disajikan meliputi, laporan laba rugi, laporan neraca, laporan penyusutan & amortisasi, summery, advice.', 'manajemen laporan keuangan,  laporan laba rugi, laporan neraca, laporan penyusutan & amortisasi, summery, advice', '2020-02-17 08:56:11', NULL),
(3, 2, '', 'Finance Controller', '-', 'yes', '<p>Menerapkan & Menyusun laporan Keuangan kepada manajemen sesuai dengan PSAK yang berlaku, Berkolaborasi dalam penyusunan inisiatif strategi, target perusahaan dan penyusunan anggaran keuangan untuk mencapai efektifitas operasional.</p>', 'finance-controller1.png', 'Finance Controller', 'Finance Controller', 'Menerapkan & Menyusun laporan Keuangan kepada manajemen sesuai dengan PSAK yang berlaku, Berkolaborasi dalam penyusunan inisiatif strategi, target perusahaan dan penyusunan anggaran keuangan untuk mencapai efektifitas operasional.', 'finance controller, menerapkan & menyusun laporan keuangan', '2020-02-17 08:58:47', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tour_gallery`
--

CREATE TABLE `tour_gallery` (
  `id` int(11) NOT NULL,
  `id_tour` int(11) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `thumbnail` varchar(255) NOT NULL,
  `thumbnail_alt` text,
  `use` enum('yes','no') DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `video`
--

CREATE TABLE `video` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `video` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about`
--
ALTER TABLE `about`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `blog_category`
--
ALTER TABLE `blog_category`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `email`
--
ALTER TABLE `email`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `language`
--
ALTER TABLE `language`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `reservation`
--
ALTER TABLE `reservation`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `team`
--
ALTER TABLE `team`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `tour`
--
ALTER TABLE `tour`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `tour_gallery`
--
ALTER TABLE `tour_gallery`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `video`
--
ALTER TABLE `video`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about`
--
ALTER TABLE `about`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `blog_category`
--
ALTER TABLE `blog_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `email`
--
ALTER TABLE `email`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `gallery`
--
ALTER TABLE `gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `language`
--
ALTER TABLE `language`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `reservation`
--
ALTER TABLE `reservation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `team`
--
ALTER TABLE `team`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tour`
--
ALTER TABLE `tour`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tour_gallery`
--
ALTER TABLE `tour_gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `video`
--
ALTER TABLE `video`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
