<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kebijakan_privasi extends CI_Controller {

	function __construct(){
		parent:: __construct();
		$this->load->helper(array('form', 'url', 'html','language'));
	}
    
	public function index()
	{
		$this->session->unset_userdata('menu');
        $this->session->set_userdata('menu', 'kebijakan_privasi');

        $data = $this->general->data_general();
        $data['title'] = 'Kebijakan privasi - Red Consulting';
        $data['description'] = ' R.E.D. CONSULTING hadir untuk membantu perusahaan dengan membuka seluruh potensi yang dimiliki untuk tumbuh lebih besar, dengan turut menghadirkan solusi terbaik dalam memenuhi penataan keuangan perusahaan.';
        $data['keywords'] = 'consulting, keuangan, perpajakan';

        $data['title_act'] = "Kebijakan Privasi";
		
        $this->load->view('templates/header',$data);
        $this->load->view('kebijakan_privasi');
		$this->load->view('templates/footer');        
	}

}
