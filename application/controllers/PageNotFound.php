<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class PageNotFound extends CI_Controller {

	public function index()
	{
		$this->session->unset_userdata('menu');
        $this->session->set_userdata('menu', 'notfound');

		$data = $this->general->data_general();
        $data['title'] = 'Red Consulting';
        $data['description'] = 'R.E.D. CONSULTING hadir untuk membantu perusahaan dengan membuka seluruh potensi  yang dimiliki untuk tumbuh lebih besar, dengan turut menghadirkan solusi terbaik dalam memenuhi penataan keuangan perusahaan.';
        $data['keywords'] = 'consulting, keuangan, perpajakan, perusahaan, bisnis';

        $data['title_act'] = "Halam Tidak Ditemukan";

		$this->load->view('templates/header',$data);
		$this->load->view('notfound');	
		$this->load->view('templates/footer');	
	}
 
}