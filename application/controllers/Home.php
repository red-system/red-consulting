<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller
{

    function __construct()
    {
        parent:: __construct();
    }

    public function index()
    {
        $this->session->unset_userdata('menu');
        $this->session->set_userdata('menu', 'home');

        $data = $this->general->data_general();
        $data['title'] = 'Red Consulting';
        $data['description'] = 'R.E.D. CONSULTING hadir untuk membantu perusahaan dengan membuka seluruh potensi  yang dimiliki untuk tumbuh lebih besar, dengan turut menghadirkan solusi terbaik dalam memenuhi penataan keuangan perusahaan.';
        $data['keywords'] = 'consulting, keuangan, perpajakan, perusahaan, bisnis';

        $this->load->view('templates/header', $data);
        $this->load->view('home');
        $this->load->view('templates/footer');
    }

}
