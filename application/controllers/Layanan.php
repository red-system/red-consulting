<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Layanan extends CI_Controller {

    function __construct(){
        parent:: __construct();
        $this->load->helper(array('form', 'url', 'html','language'));
    $this->load->library(array('form_validation','email','session'));
    }

    function captcha() {
        $this->load->helper('captcha');
        $vals = array(
            'img_path'   => './assets/captcha_mwz/',
            'img_url'    => base_url().'assets/captcha_mwz/',
            'font_path'  => './assets/css/fonts/mvboli.ttf',
            'font_size'     => 20,
            'img_width'=>'270', 
            'img_height'=>'45',
            'border' => 0, 
            'expiration' => 7200,
            'pool' => '0123456789',
            'colors'        => array(
                'background' => array(255, 255, 255),
                'border' => array(255, 255, 255),
                'text' => array(0, 0, 0),
                'grid' => array(255, 40, 40)
            ),
            'word_length'   => '5' );
        $cap = create_captcha($vals);
        $captcha = $cap['image'];
        $this->session->set_userdata('captcha_mwz', $cap['word']);
        return $captcha;
    }

    function captcha_check($str) {
        if($str == $this->session->userdata('captcha_mwz')) return TRUE;    
        else {
            $this->form_validation->set_message('captcha_check', 'Security Code was wrong');
            return FALSE;   
        }
    }

	public function manajemen_perpajakan()
	{
		$this->session->unset_userdata('menu');
        $this->session->set_userdata('menu', 'layanan-pajak');

        $data = $this->general->data_general();
        $data['title'] = 'Manajemen Perpajakan - RED Consulting';
        $data['description'] = 'RED Consulting akan melakakukan yang pendampingan restitusi, pemeriksaan, laporan, dan administrasi perpajakan.';
        $data['keywords'] = 'consulting, pajak, laporan, restitusi';
		
        //content
        $data['title_act'] = 'Manajemen Perpajakan';
        $data['description_layanan'] = 'Kami membuat strategi manajemen untuk mengendalikan, merencanakan, dan mengorganisasikan aspek-aspek perpajakan dari sisi yang dapat menguntungkan nilai bisnis perusahaan dengan tetap melaksanakan kewajiban perpajakan secara peraturan dan perundang-undangan.';
        //content
		
        $this->load->view('templates/header',$data);
        $this->load->view('layanan_perpajakan');
		$this->load->view('templates/footer');   
	}

	public function detail_manajemen_perpajakan($slug)
    {
        $this->session->unset_userdata('menu');
    	$this->session->set_userdata('menu', 'layanan-pajak-detail');

    	$data = $this->general->data_general();
	   	$data['title'] = 'Pendampingan Restitusi Pajak - RED Consulting';
        $data['description'] = 'Kami siap mendampingi Anda untuk melakukan proses restitusi pajak dengan cermat dan tepat sehingga hak Anda atas pengembalian kelebihan pembayaran pajak tersebut bisa terlaksana sesuai dengan peraturan perpajakan yang berlaku di Indonesia.';
        $data['keywords'] = 'pajak, pendampingan, restitusi, konsultasi';
		
		$data['kategori'] = 'Manajemen Perpajakan';
        $data['title_act'] = 'Pendampingan Restitusi Pajak';

        $data['captcha'] = $this->captcha();

        $this->load->view('templates/header',$data);
	    $this->load->view('layanan_perpajakan_detail');
        $this->load->view('templates/footer'); 
    }

    public function manajemen_laporan_keuangan()
	{
		$this->session->unset_userdata('menu');
        $this->session->set_userdata('menu', 'layanan-laporan-keuangan');

        $data = $this->general->data_general();
        $data['title'] = 'Manajemen Laporan Keuangan - RED Consulting';
        $data['description'] = 'RED Consulting akan melakakukan yang pendampingan restitusi, pemeriksaan, laporan, dan administrasi perpajakan.';
        $data['keywords'] = 'consulting, laporan, keuanagn';
		
        //content
        $data['title_act'] = 'Manajemen Laporan Keuangan';
        $data['description_layanan'] = 'Kami dapat memberikan pemahaman secara mendalam tentang kondisi spesifik perusahaan tersebut. Laporan manajemen bisa memberikan informasi secara lebih spesifik dalam bentuk data keuntungan dan kerugian yang berasal dari setiap departemen, tim atau kelompok kerja, serta jenis proyek; data pencapaian (realization rate); dan data penggunaan (utilization rate)';
        //content
		
        $this->load->view('templates/header',$data);
        $this->load->view('layanan_laporan_keuangan');
		$this->load->view('templates/footer');   
	}

	public function detail_manajemen_manajemen_laporan_keuangan($slug)
    {
        $this->session->unset_userdata('menu');
    	$this->session->set_userdata('menu', 'layanan-laporan-keuangan-detail');

    	$data = $this->general->data_general();
	   	$data['title'] = 'Penyusunan Laporan Keuangan - RED Consulting';
        $data['description'] = 'Kami memberi perhatian penting terhadap keberadaan Laporan Keuangan perusahaan Anda, mengingat peran pentingnya sebagai media pengambilan keputusan manajemen dan berhubungan dengan pihak ketiga.';
        $data['keywords'] = 'keuangan, pendampingan, laporan, konsultasi';
		
		$data['kategori'] = 'Manajemen Laporan Keuangan';
        $data['title_act'] = 'Penyusunan Laporan Keuangan';

        $data['captcha'] = $this->captcha();

        $this->load->view('templates/header',$data);
	    $this->load->view('layanan_laporan_keuangan_detail');
        $this->load->view('templates/footer'); 
    }

   public function manajemen_keuangan()
	{
		$this->session->unset_userdata('menu');
        $this->session->set_userdata('menu', 'layanan-keuangan');

        $data = $this->general->data_general();
        $data['title'] = 'Finance Controller - RED Consulting';
        $data['description'] = 'RED Consulting akan melakakukan yang pendampingan restitusi, pemeriksaan, laporan, dan administrasi perpajakan.';
        $data['keywords'] = 'consulting, laporan, keuanagn';
		
        //content
        $data['title_act'] = 'Finance Controller';
        $data['description_layanan'] = 'Kami akan membantu Anda dalam mengelola dana maupun aset-aset yang dimiliki perusahaan untuk dimanfaatkan pada hal-hal atau kegiatan yang membantu tercapainya tujuan utama perusahaan tersebut, yaitu profit.';
        //content
		
        $this->load->view('templates/header',$data);
        $this->load->view('layanan_keuangan');
		$this->load->view('templates/footer');   
	}

	public function detail_manajemen_manajemen_keuangan($slug)
    {
        $this->session->unset_userdata('menu');
    	$this->session->set_userdata('menu', 'layanan-keuangan-detail');

    	$data = $this->general->data_general();
	   	$data['title'] = 'Monitoring Anggaran Perusahaan - RED Consulting';
        $data['description'] = 'Kami sebagai mitra bisnis jangka panjang Anda, berkomitmen membantu Anda dalam melakukan monitoring anggaran perusahaan. Kami memberi perhatian penting terhadap kesuksesan dan aspirasi pertumbuhan bisnis Anda.';
        $data['keywords'] = 'keuangan, pendampingan, anggran, konsultasi';
		
		$data['kategori'] = 'Finance Controller';
        $data['title_act'] = 'Monitoring Anggaran Perusahaan';

        $data['captcha'] = $this->captcha();

        $this->load->view('templates/header',$data);
	    $this->load->view('layanan_keuangan_detail');
        $this->load->view('templates/footer'); 
    }

    function get_subkategori(){
        $kategori=$this->input->post('id');
        $data = array();
        if ($kategori=="Manajemen Perpajakan") {
           $data = array('Pendampingan Restitusi Pajak','Pendampingan Pemeriksaan Pajak','Penyusunan Laporan Perpajakan','Pendampingan Administrasi Perpajakan' );
        } elseif ($kategori=="Manajemen Laporan Keuangan") {
            $data = array('Penyusunan Laporan Keuangan','Menyusun Proyeksi Keuangan' );
        } elseif ($kategori=="Finance Controller") {
            $data = array('Monitoring Anggaran Perusahaan','Menyusun Anggaran Perusahaan' );
        }
        echo json_encode($data);
    }

    function kirim_konsultasi() 
    {
        $this->form_validation->set_rules('nama', 'Full Name', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('phone', 'Phone', 'trim|required');
        $this->form_validation->set_rules('comment', 'Message', 'trim|required');
        $this->form_validation->set_rules('captcha', 'Security Code', 'trim|required|callback_captcha_check');
        $this->form_validation->set_error_delimiters('<span style="color:red">', '</span>');

        $emailsmtp = "inforedconsulting69@gmail.com";
        $passsmtp = "teamwork69SOLID!!!";

        if ($this->form_validation->run() == FALSE) {
            $alert = 'Fill the Form correctly';
            $str   = array('nama','email','phone','comment','captcha');
            $error = array();
            foreach($str as $row) $error[$row] = form_error($row);
            $json  = array_merge(array('status'=>'error', 'alert'=>$alert), $error);
            echo json_encode($json);
        } else {
            
            $config = [
               'useragent' => 'CodeIgniter',
               'protocol'  => 'smtp',
               'mailpath'  => '/usr/sbin/sendmail',
               'smtp_host' => 'ssl://smtp.gmail.com',
               'smtp_user' => $emailsmtp,   // Ganti dengan email gmail Anda.
               'smtp_pass' => $passsmtp,             // Password gmail Anda.
               'smtp_port' => 465,
               'smtp_keepalive' => TRUE,
               'smtp_crypto' => 'SSL',
               'wordwrap'  => TRUE,
               'wrapchars' => 80,
               'mailtype'  => 'html',
               'charset'   => 'utf-8',
               'validate'  => TRUE,
               'crlf'      => "\r\n",
               'newline'   => "\r\n",
            ];

            // Load library email dan konfigurasinya.
            $this->email->initialize($config);
            
            //Penerima
            $this->email->From($this->input->post('email'),$this->input->post('nama')); //pengirim
            $this->email->to($emailsmtp);

            // Subject email.
            $this->email->subject('Konsultasi '.$this->input->post('subkategori')); //subject 

            $this->email->message("Kepada Admin RED Consulting,<br />
                Anda baru saja mendapatkan pesan email dengan pesan sebagai berikut : <br><br>
                --------------- MESSAGE DATA ---------------<br />
                <table width='100%'>
                    <tr>
                        <td width='80'>Nama</td>
                        <td width='920'>: <strong>".$this->input->post('nama'). "</strong></td>
                    </tr>
                    <tr>
                        <td width='80'>Email</td>
                        <td width='920'>: <strong>".$this->input->post('email'). "</strong></td>
                    </tr>
                    <tr>
                        <td width='80'>No HP</td>
                        <td width='920'>: <strong>".$this->input->post('phone'). "</strong></td>
                    </tr>
                    <tr>
                        <td width='80'>Layanan</td>
                        <td width='920'>: <strong>".$this->input->post('kategori'). "</strong></td>
                    </tr>
                    <tr>
                        <td width='80'>Sub Layanan</td>
                        <td width='920'>: <strong>".$this->input->post('subkategori'). "</strong></td>
                    </tr>
                    <tr>
                        <td width='80'>Message</td>
                        <td width='920'>: <strong>".$this->input->post('comment'). "</strong></td>
                    </tr>
                </table>
                <br>
                Terima kasih atas perhatian Anda yang cepat pada hal di atas, saya berharap dapat menerima email yang mengkonfirmasikan pesan saya.<br><br>
                Salam,<br>
                ".$this->input->post('nama')."<br>
                (".$this->input->post('email'). ")<br>
                --------------- MESSAGE DATA ---------------<br />
                "); //isi pesan 
                
                if ($this->email->send()) {
                    $config = [
                       'useragent' => 'CodeIgniter',
                       'protocol'  => 'smtp',
                       'mailpath'  => '/usr/sbin/sendmail',
                       'smtp_host' => 'ssl://smtp.gmail.com',
                       'smtp_user' => $emailsmtp,   // Ganti dengan email gmail Anda.
                       'smtp_pass' => $passsmtp,             // Password gmail Anda.
                       'smtp_port' => 465,
                       'smtp_keepalive' => TRUE,
                       'smtp_crypto' => 'SSL',
                       'wordwrap'  => TRUE,
                       'wrapchars' => 80,
                       'mailtype'  => 'html',
                       'charset'   => 'utf-8',
                       'validate'  => TRUE,
                       'crlf'      => "\r\n",
                       'newline'   => "\r\n",
                    ];
                    // Load library email dan konfigurasinya.
                    $this->email->initialize($config);

                    //Penerima
                    $this->email->From($emailsmtp,'Admin Red Consulting'); //pengirim
                    $this->email->to($this->input->post('email'));

                    // Subject email.
                    $this->email->subject('Auto-Replay dari Red Consulting'); //subject 

                    $this->email->message(
                        "Kepada ".$this->input->post('nama').",<br /><br />
                        Anda baru saja mengirimkan email ke Admin RED Consulting.
                        Tim kami akan segera membalas email Anda.<br><br>
                        Salam,<br>
                        Admin RED Consulting
                        "); //isi pesan 

                    $this->email->send();
                    $alert = 'Email Anda telah terkirim, terima kasih sudah mengirimkan email kepada kami, tunggu balasan dari kami :)';
                
                        echo json_encode(array(
                            'status'=>'success',
                            'alert'=>$alert
                        ));

                }
        }
    }


}
