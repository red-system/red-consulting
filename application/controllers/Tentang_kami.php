<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tentang_kami extends CI_Controller {

	function __construct(){
		parent:: __construct();
		$this->load->helper(array('form', 'url', 'html','language'));
	}
    
	public function index()
	{
		$this->session->unset_userdata('menu');
        $this->session->set_userdata('menu', 'tentang-kami');

        $data = $this->general->data_general();
        $data['title'] = 'Tentang Kami - Red Consulting';
        $data['description'] = ' R.E.D. CONSULTING hadir untuk membantu perusahaan dengan membuka seluruh potensi yang dimiliki untuk tumbuh lebih besar, dengan turut menghadirkan solusi terbaik dalam memenuhi penataan keuangan perusahaan.';
        $data['keywords'] = 'consulting, keuangan, perpajakan';

        $data['title_act'] = "Tentang Kami";
		
        $this->load->view('templates/header',$data);
        $this->load->view('tentang_kami');
		$this->load->view('templates/footer');        
	}

}
