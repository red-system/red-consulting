<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Artikel extends CI_Controller {

	function __construct(){
		parent:: __construct();
		$this->load->helper(array('form', 'url', 'html','language'));
	}
    
	public function index()
	{
		$this->session->unset_userdata('menu');
        $this->session->set_userdata('menu', 'artikel');

        $data = $this->general->data_general();
        $data['title'] = 'Artikel - RED Consulting';
        $data['description'] = ' R.E.D. CONSULTING hadir untuk membantu perusahaan dengan membuka seluruh potensi yang dimiliki untuk tumbuh lebih besar, dengan turut menghadirkan solusi terbaik dalam memenuhi penataan keuangan perusahaan.';
        $data['keywords'] = 'consulting, keuangan, perpajakan';

        $data['title_act'] = "Artikel";
		
        $this->load->view('templates/header',$data);
        $this->load->view('artikel');
		$this->load->view('templates/footer');        
	}

	public function detail($slug)
	{
		$this->session->unset_userdata('menu');
        $this->session->set_userdata('menu', 'artikel-detail');

        $this->load->library('disqus');
		
        $data = $this->general->data_general();
        $data['title'] = 'Money Market Rates Finding the Best Accounts in 2016 - RED Consulting';
        $data['description'] = ' R.E.D. CONSULTING hadir untuk membantu perusahaan dengan membuka seluruh potensi yang dimiliki untuk tumbuh lebih besar, dengan turut menghadirkan solusi terbaik dalam memenuhi penataan keuangan perusahaan.';
        $data['keywords'] = 'keuangan, pendampingan, laporan, konsultasi';
        
        $data['disqus'] = $this->disqus->get_html();
		$data['kategori'] = 'Perpajakan';
		$data['title_act'] = "Money Market Rates Finding the Best Accounts in 2016";

        $this->load->view('templates/header',$data);
        $this->load->view('artikel_detail');
		$this->load->view('templates/footer'); 
	}

}
