<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kontak extends CI_Controller {

	function __construct(){
		parent:: __construct();
		$this->load->helper(array('form', 'url', 'html','language'));
    $this->load->library(array('form_validation','email','session'));
	}

    function captcha() {
        $this->load->helper('captcha');
        $vals = array(
            'img_path'   => './assets/captcha_mwz/',
            'img_url'    => base_url().'assets/captcha_mwz/',
            'font_path'  => './assets/css/fonts/mvboli.ttf',
            'font_size'     => 20,
            'img_width'=>'270', 
            'img_height'=>'45',
            'border' => 0, 
            'expiration' => 7200,
            'pool' => '0123456789',
            'colors'        => array(
                'background' => array(255, 255, 255),
                'border' => array(255, 255, 255),
                'text' => array(0, 0, 0),
                'grid' => array(255, 40, 40)
            ),
            'word_length'   => '5' );
        $cap = create_captcha($vals);
        $captcha = $cap['image'];
        $this->session->set_userdata('captcha_mwz', $cap['word']);
        return $captcha;
    }

    function captcha_check($str) {
        if($str == $this->session->userdata('captcha_mwz')) return TRUE;    
        else {
            $this->form_validation->set_message('captcha_check', 'Security Code was wrong');
            return FALSE;   
        }
    }
    
	public function index()
	{
		$this->session->unset_userdata('menu');
        $this->session->set_userdata('menu', 'kontak');

        $data = $this->general->data_general();
        $data['title'] = 'Kontak - Red Consulting';
        $data['description'] = ' R.E.D. CONSULTING hadir untuk membantu perusahaan dengan membuka seluruh potensi yang dimiliki untuk tumbuh lebih besar, dengan turut menghadirkan solusi terbaik dalam memenuhi penataan keuangan perusahaan.';
        $data['keywords'] = 'consulting, keuangan, perpajakan';

        $data['title_act'] = "Kontak";

        $data['captcha'] = $this->captcha();
		
        $this->load->view('templates/header',$data);
        $this->load->view('kontak');
		$this->load->view('templates/footer');        
	}

	function kirim_pesan() 
    {
        $this->form_validation->set_rules('nama', 'Full Name', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('phone', 'Phone', 'trim|required');
        $this->form_validation->set_rules('message', 'Message', 'trim|required');
        $this->form_validation->set_rules('captcha', 'Security Code', 'trim|required|callback_captcha_check');
        $this->form_validation->set_error_delimiters('<span style="color:red">', '</span>');

        $emailsmtp = "inforedconsulting69@gmail.com";
        $passsmtp = "teamwork69SOLID!!!";

        if ($this->form_validation->run() == FALSE) {
            $alert = 'Fill the Form correctly';
            $str   = array('nama','email','phone','message','captcha');
            $error = array();
            foreach($str as $row) $error[$row] = form_error($row);
            $json  = array_merge(array('status'=>'error', 'alert'=>$alert), $error);
            echo json_encode($json);
        } else {
            
            $config = [
               'useragent' => 'CodeIgniter',
               'protocol'  => 'smtp',
               'mailpath'  => '/usr/sbin/sendmail',
               'smtp_host' => 'ssl://smtp.gmail.com',
               'smtp_user' => $emailsmtp,   // Ganti dengan email gmail Anda.
               'smtp_pass' => $passsmtp,             // Password gmail Anda.
               'smtp_port' => 465,
               'smtp_keepalive' => TRUE,
               'smtp_crypto' => 'SSL',
               'wordwrap'  => TRUE,
               'wrapchars' => 80,
               'mailtype'  => 'html',
               'charset'   => 'utf-8',
               'validate'  => TRUE,
               'crlf'      => "\r\n",
               'newline'   => "\r\n",
            ];

            // Load library email dan konfigurasinya.
            $this->email->initialize($config);
            
            //Penerima
            $this->email->From($this->input->post('email'),$this->input->post('nama')); //pengirim
            $this->email->to($emailsmtp);

            // Subject email.
            $this->email->subject('Pesan dari pengunjung RED Consulting'); //subject 

            $this->email->message("Kepada Admin RED Consulting,<br />
                Anda baru saja mendapatkan pesan email dengan pesan sebagai berikut : <br><br>
                --------------- MESSAGE DATA ---------------<br />
                <table width='100%'>
                    <tr>
                        <td width='80'>Nama</td>
                        <td width='920'>: <strong>".$this->input->post('nama'). "</strong></td>
                    </tr>
                    <tr>
                        <td width='80'>Email</td>
                        <td width='920'>: <strong>".$this->input->post('email'). "</strong></td>
                    </tr>
                    <tr>
                        <td width='80'>No HP</td>
                        <td width='920'>: <strong>".$this->input->post('phone'). "</strong></td>
                    </tr>
                    <tr>
                        <td width='80'>Message</td>
                        <td width='920'>: <strong>".$this->input->post('message'). "</strong></td>
                    </tr>
                </table>
                <br>
                Terima kasih atas perhatian Anda yang cepat pada hal di atas, saya berharap dapat menerima email yang mengkonfirmasikan pesan saya.<br><br>
                Salam,<br>
                ".$this->input->post('nama')."<br>
                (".$this->input->post('email'). ")<br>
                --------------- MESSAGE DATA ---------------<br />
                "); //isi pesan 
                
                if ($this->email->send()) {
                    $config = [
                       'useragent' => 'CodeIgniter',
                       'protocol'  => 'smtp',
                       'mailpath'  => '/usr/sbin/sendmail',
                       'smtp_host' => 'ssl://smtp.gmail.com',
                       'smtp_user' => $emailsmtp,   // Ganti dengan email gmail Anda.
                       'smtp_pass' => $passsmtp,             // Password gmail Anda.
                       'smtp_port' => 465,
                       'smtp_keepalive' => TRUE,
                       'smtp_crypto' => 'SSL',
                       'wordwrap'  => TRUE,
                       'wrapchars' => 80,
                       'mailtype'  => 'html',
                       'charset'   => 'utf-8',
                       'validate'  => TRUE,
                       'crlf'      => "\r\n",
                       'newline'   => "\r\n",
                    ];
                    // Load library email dan konfigurasinya.
                    $this->email->initialize($config);

                    //Penerima
                    $this->email->From($emailsmtp,'Admin Red Consulting'); //pengirim
                    $this->email->to($this->input->post('email'));

                    // Subject email.
                    $this->email->subject('Auto-Replay dari Red Consulting'); //subject 

                    $this->email->message(
                        "Kepada ".$this->input->post('nama').",<br /><br />
                        Anda baru saja mengirimkan email ke Admin RED Consulting.
                        Tim kami akan segera membalas email Anda.<br><br>
                        Salam,<br>
                        Admin RED Consulting
                        "); //isi pesan 

                    $this->email->send();
                    $alert = 'Email Anda telah terkirim, terima kasih sudah mengirimkan email kepada kami, tunggu balasan dari kami :)';
                
                        echo json_encode(array(
                            'status'=>'success',
                            'alert'=>$alert
                        ));

                }
        }
    }


}
