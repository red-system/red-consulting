<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Syarat_ketentuan extends CI_Controller {

	function __construct(){
		parent:: __construct();
		$this->load->helper(array('form', 'url', 'html','language'));
	}
    
	public function index()
	{
		$this->session->unset_userdata('menu');
        $this->session->set_userdata('menu', 'Syarat_ketentuan');

        $data = $this->general->data_general();
        $data['title'] = 'Syarat ketentuan - Red Consulting';
        $data['description'] = ' R.E.D. CONSULTING hadir untuk membantu perusahaan dengan membuka seluruh potensi yang dimiliki untuk tumbuh lebih besar, dengan turut menghadirkan solusi terbaik dalam memenuhi penataan keuangan perusahaan.';
        $data['keywords'] = 'consulting, keuangan, perpajakan';

        $data['title_act'] = "Syarat & Ketentuan";
		
        $this->load->view('templates/header',$data);
        $this->load->view('syarat_ketentuan');
		$this->load->view('templates/footer');        
	}

}
