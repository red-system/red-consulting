<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/


$connection = mysqli_connect('localhost', 'root', '', 'gtn_redconsulting');


$route['default_controller'] = 'home';


$route['proweb'] = 'proweb/login';
$route['proweb/login'] = 'proweb/login/process';
$route['layanan/konsultan-pajak'] = 'layanan/manajemen_perpajakan';
$route['layanan/konsultan-pajak/(:any)'] = "layanan/detail_manajemen_perpajakan/$1";
$route['layanan/manajemen-laporan-keuangan'] = 'layanan/manajemen_laporan_keuangan';
$route['layanan/manajemen-laporan-keuangan/(:any)'] = "layanan/detail_manajemen_manajemen_laporan_keuangan/$1";
$route['layanan/finance-controller'] = 'layanan/manajemen_keuangan';
$route['layanan/finance-controller/(:any)'] = "layanan/detail_manajemen_manajemen_keuangan/$1";
$route['layanan/kirim-konsultasi'] = "layanan/kirim_konsultasi";
$route['testimonial'] = 'testimonial';
$route['galeri'] = 'galeri';
$route['artikel'] = 'artikel';
$route['artikel/detail/(:any)'] = "artikel/detail/$1";
$route['tentang-kami'] = 'tentang_kami';
$route['kontak'] = 'kontak';
$route['kontak/kirim-pesan'] = 'kontak/kirim_pesan';
$route['syarat-ketentuan'] = 'syarat_ketentuan';
$route['kebijakan-privasi'] = 'kebijakan_privasi';

$route['404_override'] = 'PageNotFound';

$route['translate_uri_dashes'] = FALSE;
