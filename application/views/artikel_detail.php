<div class="top_panel_title top_panel_style_3 title_present scheme_original is_page_paddings_yes">
    <div class="top_panel_title_inner top_panel_inner_style_3 title_present_inner">
        <div class="content_wrap">
            <h1 class="page_title">Money Market Rates Finding the Best Accounts in 2016</h1>
            <div class="cat_post_info">
                <span class="post_categories"><a class="category_link" href="all-posts.html" title="<?php echo $kategori ;?>"><?php echo $kategori ;?></a></span>
            </div>
        </div>
    </div>
</div>
<div class="page_content_wrap page_paddings_yes">
    <div class="content_wrap">
        <div class="content">
            <article class="itemscope post_item post_item_single post_featured_default post_format_gallery">
                <section class="post_content">
                    <div class="sc_slider sc_slider_swiper swiper-slider-container sc_slider_controls sc_slider_pagination" data-interval="7783" data-old-height="659" data-old-width="1170" id="sc_1920x1079804840962">
                        <div class="slides swiper-wrapper" data-style="height:659px;">
                            <div class="swiper-slide" data-style="background-image: url(<?php echo base_url();?>assets/images/artikel/post-3-370x270.jpg);width:1170px;height:659px;">
                            </div>
                            
                        </div>
                        <div class="sc_slider_controls_wrap">
                            <a class="sc_slider_prev" href="#"></a> <a class="sc_slider_next" href="#"></a>
                        </div>
                        <div class="sc_slider_pagination_wrap"></div>
                    </div>
                    <p align="justify"><br>
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum.</p>
                    <p align="justify">Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.</p>
                    <div class="single_footer_info">
                        <div class="post_info_bottom width-post-bottom">
                            <span class="post_info_item post_info_tags"><span class="icon icon-lightbulb-light"></span> keuangan, pendampingan, laporan, konsultasi</span>
                        </div>
                        <div class="post_info_bottom border-post-bottom">
                            <div class="sc_socials sc_socials_size_small sc_socials_share sc_socials_dir_horizontal">
                                <div class="share_caption">
                                    <span class="share_caption_text">Share It</span>
                                    <div class="sc_social_items_block">
                                        <div class="sc_socials_item social_item_popup">
                                            <a class="color-black social_icons social_facebook" data-link="#" href=""><span class="icon-facebook"></span></a>
                                        </div>
                                        <div class="sc_socials_item social_item_popup">
                                            <a class="color-black social_icons social_twitter" data-link="#" href=""><span class="icon-twitter"></span></a>
                                        </div>
                                        <div class="sc_socials_item social_item_popup">
                                            <a class="color-black social_icons social_gplus" data-link="#" href=""><span class="icon-gplus"></span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </article>
            <section class="comments_wrap">
                <div class="comments_form_wrap">
                    <h2 class="section_title comments_form_title">Join Discussion</h2>
                    <div class="comments_form">
                        <div class="comment-respond" id="respond">
                            <?php echo $disqus ?>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <div class="sidebar widget_area scheme_original">
            <div class="sidebar_inner widget_area_inner">
                <aside class="widget widget_search">
                    <h5 class="widget_title">Search</h5>
                    <form action="#" class="search_form" method="get">
                        <input class="search_field" name="s" placeholder="Search &hellip;" title="Search for:" type="text" value=""> <button class="search_button icon-search-light" type="submit"></button>
                    </form>
                </aside>
                <aside class="widget widget_categories">
                    <h5 class="widget_title">Kategori</h5>
                    <ul>
                        <li class="cat-item">
                            <a href="#">Perpajakan</a>
                        </li>
                        <li class="cat-item">
                            <a href="#">Keuangan</a>
                        </li>
                    </ul>
                </aside>
                <aside class="widget widget_recent_posts">
                    <h5 class="widget_title">Artikel Lainnya</h5>
                    <article class="post_item with_thumb">
                        <div class="post_thumb"><img class="height-100" alt="" src="<?php echo base_url();?>assets/images/artikel/post-2-370x270.jpg"></div>
                        <div class="post_content">
                            <h6 class="post_title"><a href="<?php echo site_url('artikel/detail/money-market-rates-finding-the-best-accounts-in-2016') ?>">Sustainable Investing from Bugs to Biodrying</a></h6>
                            <div class="post_info">
                                <span class="post_info_item"><a class="post_info_date" href="<?php echo site_url('artikel/detail/money-market-rates-finding-the-best-accounts-in-2016') ?>">April 20, 2016</a></span> 
                                <span class="post_info_item post_info_counters"><a class="post_counters_item icon-pencil-light" href="single-post.html"><span class="post_counters_number">Perpajakan</span></a></span>
                            </div>
                        </div>
                    </article>
                    <article class="post_item with_thumb">
                        <div class="post_thumb"><img class="height-100" alt="" src="<?php echo base_url();?>assets/images/artikel/post-6-370x270.jpg"></div>
                        <div class="post_content">
                            <h6 class="post_title"><a href="<?php echo site_url('artikel/detail/money-market-rates-finding-the-best-accounts-in-2016') ?>">Stay in Trend: How to Be Ahead of Stock Changes</a></h6>
                            <div class="post_info">
                                <span class="post_info_item"><a class="post_info_date" href="<?php echo site_url('artikel/detail/money-market-rates-finding-the-best-accounts-in-2016') ?>">April 20, 2016</a></span> 
                                <span class="post_info_item post_info_counters"><a class="post_counters_item icon-pencil-light" href="single-post.html"><span class="post_counters_number">Keuangan</span></a></span>
                            </div>
                        </div>
                    </article>
                </aside>
            </div>
        </div>
    </div>
</div>