<div class="page_content_wrap page_paddings_no">
    <div class="sc_section custom_bg_2">
        <div class="content_wrap">
            <div class="sc_empty_space" data-height="3em"></div>
            <div class="sc_services_wrap">
                <div class="sc_services sc_services_style_services-3 sc_services_type_images margin_top_huge margin_bottom_huge">
                    <h2 class="sc_section_title sc_item_title">Testimonial</h2>
                    <div class="sc_section_descr sc_item_descr">
                        Kami sudah mendapatkan banyak testimonial dari klien / partner kami untuk membagikan pengalaman mereka saat melakukan kerjasama dengan kami.
                    </div>
                    <div class="sc_columns columns_wrap">
                        <div class="column-1_3 column_padding_bottom padding-left-30">
                            <div class="sc_services_item sc_services_item_1">
                                <div class="sc_testimonial_avatar aligncenter">
                                    <img class="border-radius-50" alt="Lisa Larson" title="Lisa Larson" width="80" height="80" src="<?php echo base_url();?>assets/images/testimonial/testimonials-1-80x80.jpg">
                                </div>
                                <div class="sc_services_item_content">
                                    <h4 class="sc_services_item_title  aligncenter">Lisa Larson</h4>
                                    <span class="sc_services_item_subtitle  aligncenter">Company Director</span>
                                    <div class="sc_services_item_description">
                                        <p align="justify">Being a company's director requires maximum attention and devotion. This was exactly what I felt when turned to your products and services. All our questions and inquiries were answered effectively and right away. Our website has never looked better, ever. Thank you.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="column-1_3 column_padding_bottom padding-left-30">
                            <div class="sc_services_item sc_services_item_1">
                                <div class="sc_testimonial_avatar aligncenter">
                                    <img class="border-radius-50" alt="James Watson" title="James Watson" width="80" height="80" src="<?php echo base_url();?>assets/images/testimonial/testimonials-2-80x80.jpg">
                                </div>
                                <div class="sc_services_item_content">
                                    <h4 class="sc_services_item_title  aligncenter">James Watson</h4>
                                    <span class="sc_services_item_subtitle  aligncenter">Senior Manager</span>
                                    <div class="sc_services_item_description">
                                        <p align="justify">You never know what is gooing to happen until you try. But let me tell you that taking risk with these guys was totally worth it. Now we are a regular client, and this was probably the best decision we ever made! Our company appreciates your assistance and great work!</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="column-1_3 column_padding_bottom padding-left-30">
                            <div class="sc_services_item sc_services_item_1">
                                <div class="sc_testimonial_avatar aligncenter">
                                    <img class="border-radius-50" alt="Chris Doe" title="Chris Doe" width="80" height="80" src="<?php echo base_url();?>assets/images/testimonial/testimonials-3-80x80.jpg">
                                </div>
                                <div class="sc_services_item_content">
                                    <h4 class="sc_services_item_title  aligncenter">Chris Doe</h4>
                                    <span class="sc_services_item_subtitle  aligncenter">Startup SEO</span>
                                    <div class="sc_services_item_description">
                                        <p align="justify">Highly recommend these guys to everyone who is in search of fresh ideas, quality products and excellent customer support. You will be able to find what you need and more right here with no hassle. Thanks for your time and efford toward our project. We will stay in touch!</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="column-1_3 column_padding_bottom padding-left-30">
                            <div class="sc_services_item sc_services_item_1">
                                <div class="sc_testimonial_avatar aligncenter">
                                    <img class="border-radius-50" alt="Susan Smith" title="Susan Smith" width="80" height="80" src="<?php echo base_url();?>assets/images/testimonial/testimonials-4-80x80.jpg">
                                </div>
                                <div class="sc_services_item_content">
                                    <h4 class="sc_services_item_title  aligncenter">Susan Smith</h4>
                                    <span class="sc_services_item_subtitle  aligncenter">Handmade Designer</span>
                                    <div class="sc_services_item_description">
                                        <p align="justify">I enjoyed working with you guys very much! No matter what issues or questions popped up, you were always there to assist. I appreciate your service, quality products and professional approach in every aspect. Keep up the excellent job and let us know what comes next!</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>