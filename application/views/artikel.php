<div class="page_content_wrap page_paddings_no" id="myDiv">
    <div class="sc_section custom_bg_2">
        <div class="content_wrap">
            <div class="sc_empty_space" data-height="2em"></div>
            <div class="sc_blogger layout_classic_3 template_masonry no_padding_post margin_top_huge margin_bottom_huge sc_blogger_horizontal">
                <h2 class="sc_blogger_title sc_item_title">Artikel Terbaru Kami</h2>
                <div class="sc_blogger_descr sc_item_descr">
                    Baca artkel terbaru dari kami yang meliputi bidang perpajakan dan keuangan.
                </div>
                <div class="isotope_wrap" data-columns="3">
                    <div class="isotope_item isotope_item_classic isotope_item_classic_3 isotope_column_3">
                        <div class="post_item post_item_classic post_item_classic_3 post_format_standard">
                            <div class="post_featured">
                                <div class="post_thumb">
                                    <a class="hover_icon hover_icon_link" href="<?php echo site_url('artikel/detail/money-market-rates-finding-the-best-accounts-in-2016') ?>"><img alt="" src="<?php echo base_url();?>assets/images/artikel/post-3-370x270.jpg"></a>
                                </div>
                                <div class="cat_post_info">
                                    <span class="post_categories"><a class="category_link" href="#">Perpajakan</a></span>
                                </div>
                            </div>
                            <div class="post_content isotope_item_content">
                                <h5 class="post_title"><a href="<?php echo site_url('artikel/detail/money-market-rates-finding-the-best-accounts-in-2016') ?>">Money Market Rates Finding the Best Accounts in 2016</a></h5>
                                <div class="post_info">
                                    <span class="post_info_item"><a class="post_info_date" href="<?php echo site_url('artikel/detail/money-market-rates-finding-the-best-accounts-in-2016') ?>">April 26, 2016</a></span> 
                                </div>
                                <div class="post_descr">
                                    <p>Perspiciatis unde omnis iste natus sit volupt tem accusantium doloremque...</p><a class="post_readmore readmore" href="<?php echo site_url('artikel/detail/money-market-rates-finding-the-best-accounts-in-2016') ?>">Lihat lengkap di sini</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="isotope_item isotope_item_classic isotope_item_classic_3 isotope_column_3">
                        <div class="post_item post_item_classic post_item_classic_3 post_format_standard even">
                            <div class="post_featured">
                                <div class="post_thumb">
                                    <a class="hover_icon hover_icon_link" href="<?php echo site_url('artikel/detail/money-market-rates-finding-the-best-accounts-in-2016') ?>"><img alt="" src="<?php echo base_url();?>assets/images/artikel/post-2-370x270.jpg"></a>
                                </div>
                                <div class="cat_post_info">
                                    <span class="post_categories"><a class="category_link" href="#">Perpajakan</a></span>
                                </div>
                            </div>
                            <div class="post_content isotope_item_content">
                                <h5 class="post_title"><a href="<?php echo site_url('artikel/detail/money-market-rates-finding-the-best-accounts-in-2016') ?>">Sustainable Investing from Bugs to Biodrying</a></h5>
                                <div class="post_info">
                                    <span class="post_info_item"><a class="post_info_date" href="<?php echo site_url('artikel/detail/money-market-rates-finding-the-best-accounts-in-2016') ?>">April 20, 2016</a></span>
                                </div>
                                <div class="post_descr">
                                    <p>Perspiciatis unde omnis iste natus sit volupt tem accusantium doloremque...</p><a class="post_readmore readmore" href="<?php echo site_url('artikel/detail/money-market-rates-finding-the-best-accounts-in-2016') ?>">Lihat lengkap di sini</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="isotope_item isotope_item_classic isotope_item_classic_3 isotope_column_3">
                        <div class="post_item post_item_classic post_item_classic_3 post_format_standard last">
                            <div class="post_featured">
                                <div class="post_thumb">
                                    <a class="hover_icon hover_icon_link" href="<?php echo site_url('artikel/detail/money-market-rates-finding-the-best-accounts-in-2016') ?>"><img alt="" src="<?php echo base_url();?>assets/images/artikel/post-6-370x270.jpg"></a>
                                </div>
                                <div class="cat_post_info">
                                    <span class="post_categories"><a class="category_link" href="#">Keuangan</a></span>
                                </div>
                            </div>
                            <div class="post_content isotope_item_content">
                                <h5 class="post_title"><a href="<?php echo site_url('artikel/detail/money-market-rates-finding-the-best-accounts-in-2016') ?>">Stay in Trend: How to Be Ahead of Stock Changes</a></h5>
                                <div class="post_info">
                                    <span class="post_info_item"><a class="post_info_date" href="<?php echo site_url('artikel/detail/money-market-rates-finding-the-best-accounts-in-2016') ?>">January 4, 2016</a></span>
                                </div>
                                <div class="post_descr">
                                    <p>Perspiciatis unde omnis iste natus sit volupt tem accusantium doloremque...</p><a class="post_readmore readmore" href="<?php echo site_url('artikel/detail/money-market-rates-finding-the-best-accounts-in-2016') ?>">Lihat lengkap di sini</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="sc_empty_space" data-height="2.3em"></div>
        </div>
    </div>
</div>