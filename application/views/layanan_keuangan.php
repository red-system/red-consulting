<div class="page_content_wrap page_paddings_no">
    <div class="sc_section custom_bg_2">
        <div class="content_wrap">
            <div class="sc_empty_space" data-height="2.2em"></div>
            <div class="sc_section margin_top_huge ">
                <div class="sc_section_inner">
                   <h2 class="sc_section_title sc_item_title"><?php echo $title_act; ?></h2>
                    <div class="sc_section_descr sc_item_descr">
                        <?php echo $description_layanan; ?>
                    </div>
                    <div class="sc_empty_space" data-height="0.7em"></div>
                    <div class="columns_wrap sc_columns margin_top_tiny">
                        <div class="column-1_2 sc_column_item">
                            <figure class="sc_image style_img">
                               <img width="346" height="264" alt="Manajemen Keuangan" title="Manajemen Keuangan"  class="first" src="<?php echo base_url();?>assets/images/home_1.jpg"> <img width="364" height="380" alt="Manajemen Keuangan" title="Manajemen Keuangan"  class="second" src="<?php echo base_url();?>assets/images/img1.jpg">
                            </figure>
                        </div>
                        <div class="column-1_2 sc_column_item">
                            <p align="justify">Manajemen keuangan merupakan segala kegiatan ataupun aktivitas pada perusahaan yang berhubungan dengan bagaimanakah caranya agar bisa mendapatkan pendanaan modal kerka, menggunakan atau mengalokasikan dana tersebut serta mengelola asset yang telah dimiliki perusahaan guna mencapai tujuan utama pada suatu perusahaan.</p>
                            <p align="justify">Manajemen keuangan mempunyai kepentingan dalam bagaimana cara menciptakan serta menjaga nilai ekonomis suatu perusahaan. Alhasil, semua pengambilan keputusan tentu harus di fokuskan kepada penciptaan kesejahteraan para pegawainya.</p>
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="sc_empty_space" data-height="2em"></div>
        </div>
    </div>
    <div class="sc_section custom_bg_1">
        <div class="content_wrap">
            <div class="sc_empty_space" data-height="2em"></div>
            <div class="sc_services_wrap">
                <div class="sc_services sc_services_style_services-3 sc_services_type_images margin_top_huge margin_bottom_huge">
                    <h2 class="sc_services_title sc_item_title">Layanan <?php echo $title_act; ?></h2>
                    <div class="sc_services_descr sc_item_descr">
                        Berikut ini layanan yang akan kami berikan terkait Manajemen Keuangan.
                    </div>
                    <div class="sc_columns columns_wrap">
                        <div class="column-1_3 column_padding_bottom">
                            <div class="sc_services_item sc_services_item_1">
                                <div class="sc_services_item_featured post_featured">
                                    <div class="post_thumb">
                                        <a class="hover_icon hover_icon_link" href="<?php echo site_url('layanan/finance-controller/monitoring-anggaran-perusahaan') ?>"><img width="370" height="240" alt="Monitoring Anggaran Perusahaan" title="Monitoring Anggaran Perusahaan" src="<?php echo base_url();?>assets/images/layanan/monitoring-anggaran-perusahaan-red-consulting.jpeg"></a>
                                    </div>
                                    <div class="sc_services_item_count">
                                        1
                                    </div>
                                </div>
                                <div class="sc_services_item_content">
                                    <h4 class="sc_services_item_title"><a href="<?php echo site_url('layanan/finance-controller/monitoring-anggaran-perusahaan') ?>">Monitoring Anggaran Perusahaan</a></h4>
                                    <div class="sc_services_item_description">
                                        <p>Proses monitoring anggaran/budget perusahaan merupakan...</p>
                                    </div>
                                    <div class="sc_services_item_decoration"></div>
                                </div>
                            </div>
                        </div>
                        <div class="column-1_3 column_padding_bottom">
                            <div class="sc_services_item sc_services_item_2">
                                <div class="sc_services_item_featured post_featured">
                                    <div class="post_thumb">
                                        <a class="hover_icon hover_icon_link" href="<?php echo site_url('layanan/finance-controller/monitoring-anggaran-perusahaan') ?>"><img width="370" height="240" alt="Menyusun Anggaran Perusahaan" title="Menyusun Anggaran Perusahaan" src="<?php echo base_url();?>assets/images/layanan/menyusun-anggaran-perusahaan-red-consulting.jpeg"></a>
                                    </div>
                                    <div class="sc_services_item_count">
                                        2
                                    </div>
                                </div>
                                <div class="sc_services_item_content">
                                    <h4 class="sc_services_item_title"><a href="<?php echo site_url('layanan/finance-controller/monitoring-anggaran-perusahaan') ?>">Menyusun Anggaran Perusahaan</a></h4>
                                    <div class="sc_services_item_description">
                                        <p>Perencanaan anggaran atau biasa disebut dengan budgeting...</p>
                                    </div>
                                    <div class="sc_services_item_decoration"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>