<div class="page_content_wrap page_paddings_no">
    <div class="sc_section custom_bg_2">
        <div class="content_wrap">
            <div class="sc_empty_space" data-height="2.2em"></div>
            <div class="sc_section margin_top_huge ">
                <div class="sc_section_inner">
                    <h2 class="sc_section_title sc_item_title">FAQ</h2>
                    <div class="sc_section_descr sc_item_descr">
                        Berikut ini adalah pertanyaan yang sering diajukan orang dan jawaban yang sudah disediakan.
                    </div>
                    <div class="sc_empty_space" data-height="0.7em"></div>
                    <div class="margin_top_tiny">
                        <button class="accordion">Bagaimana prosedurnya untuk bergabung dengan perusahaan Anda?</button>
                        <div class="panel">
                          <p>Pembina utama kebahagiaan manusia. Tidak ada yang menolak, tidak suka, atau menghindari kesenangan itu sendiri, karena itu adalah kesenangan, tetapi karena mereka yang tidak tahu bagaimana mengejar kesenangan.</p>
                        </div>

                        <button class="accordion">Apakah Anda memberikan penawaran untuk pelanggan premium?</button>
                        <div class="panel">
                          <p>Pembina utama kebahagiaan manusia. Tidak ada yang menolak, tidak suka, atau menghindari kesenangan itu sendiri, karena itu adalah kesenangan, tetapi karena mereka yang tidak tahu bagaimana mengejar kesenangan.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="sc_empty_space" data-height="2em"></div>
        </div>
    </div>
</div>