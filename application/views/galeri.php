<div class="page_content_wrap page_paddings_yes">
    <div class="sc_section custom_bg_2">
        <div class="content_wrap">
            <div class="sc_empty_space" data-height="2.2em"></div>
            <div class="sc_section margin_top_huge ">
                <div class="sc_section_inner">
                    <h2 class="sc_section_title sc_item_title">Galeri</h2>
                    <div class="sc_section_descr sc_item_descr">
                        Dalam galeri ini terdapat tips-tips untuk memajukan bisnis / perusahaan. Serta hasil dokumentasi yang dapat kami abadikan bersama partner / klien kami sewaktu kami sedang meeting dan tanda tangan kerjasama.
                    </div>
                </div>
            </div>
            <div class="sc_empty_space" data-height="2em"></div>
        </div>
    </div>
    <div class="content_wrap">
        <div class="content">
            <article class="itemscope post_item post_item_single post_featured_default post_format_standard">
                <section class="post_content">
                    <article class="myportfolio-container minimal-light" id="esg-grid-3-1-wrap">
                        <div class="esg-grid" id="esg-grid-3-1">
                            <div id="pixlee_container"></div>
                            <script type="text/javascript">window.PixleeAsyncInit = function() {Pixlee.init({apiKey:'KC3MZEVbAls61PRFsC67'});Pixlee.addSimpleWidget({widgetId:'24658'});};</script>
                            <script src="//instafeed.assets.pixlee.com/assets/pixlee_widget_1_0_0.js"></script>
                        </div>
                    </article>
                </section>
            </article>
        </div>
    </div>
</div>