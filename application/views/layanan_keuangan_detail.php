<div class="loader-send display-none"></div>
<div class="top_panel_title top_panel_style_3 title_present scheme_original is_page_paddings_yes">
    <div class="top_panel_title_inner top_panel_inner_style_3 title_present_inner">
        <div class="content_wrap">
            <h1 class="page_title">Monitoring Anggaran Perusahaan</h1>
            <div class="cat_post_info">
                <span class="post_categories"><a class="category_link" href="<?php echo base_url() ?>layanan/finance-controller" title="<?php echo $kategori ;?>"><?php echo $kategori ;?></a></span>
            </div>
        </div>
    </div>
</div>
<div class="page_content_wrap page_paddings_yes">
    <div class="content_wrap">
        <div class="content">
            <article class="itemscope post_item post_item_single post_featured_default post_format_gallery">
                <section class="post_content">
                    <div class="sc_slider sc_slider_swiper swiper-slider-container sc_slider_controls sc_slider_pagination" data-interval="7783" data-old-height="659" data-old-width="1170" id="sc_1920x1079804840962">
                        <div class="slides swiper-wrapper" data-style="height:659px;">
                            <div class="swiper-slide" title="Monitoring Anggaran Perusahaan" alt="Monitoring Anggaran Perusahaan" data-style="background-image: url(<?php echo base_url();?>assets/images/layanan/monitoring-anggaran-perusahaan-red-consulting.jpeg);width:1170px;height:659px;">
                            </div>
                            
                        </div>
                        <div class="sc_slider_controls_wrap">
                            <a class="sc_slider_prev" href="#"></a> <a class="sc_slider_next" href="#"></a>
                        </div>
                        <div class="sc_slider_pagination_wrap"></div>
                    </div>
                    <p align="justify"><br>
                    Proses monitoring anggaran/budget perusahaan merupakan proses yang penting untuk dilakukan pasca penyusunan anggaran telah dilakukan. Hal ini penting dilakukan agar perusahaan Anda terhindar dari pengeluaran-pengeluaran yang tidak sesuai dengan rencana awal dan dapat berpotensi menekan keuntungan perusahaan. Disamping itu, monitoring dilakukan untuk menjaga efektivitas beban keuangan perusahaan yang telah dikeluarkan. Secara umum, kegiatan monitoring dilakukan secara rutin pada periode tertentu, misalnya bulanan atau triwulanan, dan membandingkannya antara Realisasi Anggaran dengan dengan Anggaran awal yang telah disusun.</p>
                    <p align="justify">Kami sebagai mitra bisnis jangka panjang Anda, berkomitmen membantu Anda dalam melakukan monitoring anggaran perusahaan. Kami memberi perhatian penting terhadap kesuksesan dan aspirasi pertumbuhan bisnis Anda.</p>
                    <div class="single_footer_info">
                        <div class="post_info_bottom width-post-bottom">
                            <span class="post_info_item post_info_tags"><span class="icon icon-lightbulb-light"></span> keuangan, pendampingan, anggran, konsultasi</span>
                        </div>
                        <div class="post_info_bottom border-post-bottom">
                            <div class="sc_socials sc_socials_size_small sc_socials_share sc_socials_dir_horizontal">
                                <div class="share_caption">
                                    <span class="share_caption_text">Share It</span>
                                    <div class="sc_social_items_block">
                                        <div class="sc_socials_item social_item_popup">
                                            <a class="color-black social_icons social_facebook" data-link="#" href=""><span class="icon-facebook"></span></a>
                                        </div>
                                        <div class="sc_socials_item social_item_popup">
                                            <a class="color-black social_icons social_twitter" data-link="#" href=""><span class="icon-twitter"></span></a>
                                        </div>
                                        <div class="sc_socials_item social_item_popup">
                                            <a class="color-black social_icons social_gplus" data-link="#" href=""><span class="icon-gplus"></span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </article>
            <section class="comments_wrap">
                <div class="comments_form_wrap">
                    <h6 class="comments_subtitle">Hubungi Kami</h6>
                    <h2 class="section_title comments_form_title">Konsultasi Gratis</h2>
                    <div class="comments_form">
                        <div class="comment-respond" id="respond">
                            <?php echo form_open_multipart('layanan/kirim-konsultasi','class="comment-form sc_input_hover_default" id="kirim_email_konsultasi" '); ?>

                                <div class="comments_field comments_site">
                                    <input id="nama" name="nama" placeholder="Name *" size="30" type="text" value="">
                                    <span class="form-error"></span>
                                </div>
                                <div class="comments_field comments_email">
                                    <input id="email" name="email" placeholder="Email *" size="30" type="email" value="">
                                    <span class="form-error"></span>
                                </div>
                                <div class="comments_field comments_author float-right">
                                    <input id="phone" name="phone" placeholder="Phone *" size="30" type="text" value="">
                                    <span class="form-error"></span>
                                </div>
                                <div class="comments_field comments_site">
                                    <select class="text-capitalize selectpicker form-control required" required data-style="g-select" data-width="100%" name="kategori" id="kategori">
                                        <option value="0">Pilih Layanan</option>
                                        <option value="Manajemen Perpajakan">Manajemen Perpajakan</option>
                                        <option value="Manajemen Laporan Keuangan">Manajemen Laporan Keuangan</option>
                                        <option value="Manajemen Keuangan">Manajemen Keuangan</option>
                                    </select>
                                </div>
                                <div class="comments_field comments_site">
                                    <select class="text-capitalize selectpicker form-control required subkategori" required data-style="g-select" data-width="100%" name="subkategori" id="kategori">
                                        <option value="0">Pilih Sub Layanan</option>
                                    </select>
                                </div>
                                <div class="comments_field comments_message">
                                    <textarea id="comment" name="comment" placeholder="Comment"></textarea>
                                    <span class="form-error"></span>
                                </div>
                                <div class="comments_field comments_author">
                                    <?php echo $captcha; ?>
                                </div>
                                <div class="comments_field comments_author float-right">
                                    <input  id="captcha" placeholder="Input Security Code *" name="captcha" size="30" type="text" value="">
                                    <span class="form-error"></span>
                                </div>
                                <div class="text_align_right">
                                    <button type="submit" class="submit">Kirim Sekarang</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <div class="sidebar widget_area scheme_original">
            <div class="sidebar_inner widget_area_inner">
                <aside class="widget widget_search">
                    <h5 class="widget_title">Search</h5>
                    <form action="#" class="search_form" method="get">
                        <input class="search_field" name="s" placeholder="Search &hellip;" title="Search for:" type="text" value=""> <button class="search_button icon-search-light" type="submit"></button>
                    </form>
                </aside>
                <aside class="widget widget_categories">
                    <h5 class="widget_title">Kategori</h5>
                    <ul>
                        <li class="cat-item">
                            <a href="<?php echo base_url() ?>layanan/konsultan-pajak">Manajemen Perpajakan</a>
                        </li>
                        <li class="cat-item">
                            <a href="<?php echo base_url() ?>layanan/manajemen-laporan-keuangan">Manajemen Laporan Keuangan</a>
                        </li>
                        <li class="cat-item">
                            <a href="<?php echo base_url() ?>layanan/finance-controller">Manajemen Keuangan</a>
                        </li>
                    </ul>
                </aside>
                <aside class="widget widget_recent_posts">
                    <h5 class="widget_title">Layanan Lainnya</h5>
                    <article class="post_item with_thumb">
                        <div class="post_thumb"><img class="height-100" width="68" alt="Menyusun Anggaran Perusahaan" title="Menyusun Anggaran Perusahaan"  src="<?php echo base_url();?>assets/images/layanan/thumb/menyusun-anggaran-perusahaan-red-consulting_thumb.jpeg"></div>
                        <div class="post_content">
                            <h6 class="post_title"><a href="<?php echo site_url('layanan/finance-controller/monitoring-anggaran-perusahaan') ?>">Menyusun Anggaran Perusahaan</a></h6>
                        </div>
                    </article>
                    
                </aside>
            </div>
        </div>
    </div>
</div>