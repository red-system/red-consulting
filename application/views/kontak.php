<div class="loader-send display-none"></div>
<div class="page_content_wrap page_paddings_no">
    <div class="sc_section custom_bg_2">
        <div class="content_wrap">
            <div class="sc_empty_space" data-height="2.2em"></div>
            <div class="sc_section margin_top_huge ">
                <div class="sc_section_inner">
                    <div class="sc_services_item sc_services_item_1 text_align_center">
                        <div class="sc_services_item_featured">
                            <div class="post_thumb">
                                 <a href="<?php echo base_url();?>assets/images/red-consulting.png" target="_blank"><img class="margin-bottom-15" src="<?php echo base_url();?>assets/images/red-consulting-thumb.png" alt="Red Consulting" title="Red Consulting" width="250" height="87" ></a>
                            </div>
                        </div>
                        <div class="sc_empty_space" data-height="2em"></div>
                        <div class="sc_services_item_content">
                            <div class="sc_services_item_description">
                                <p>Apapun permasalahan Anda, kami siap membantu. Silakan mengisi form di bawah ini untuk mengirim pertanyaan dan meminta konsultasi awal secara gratis</p>
                                <p>Jl. Ratna No 68 G, Tonja, Denpasar Utara, Denpasar - Bali . 80239.</p>
                                <p>Sen - Jum : 08:00 - 17:00 </p>
                                <p>Phone: (0361) 4746102 </p>
                                <p>Email: inforedconsulting69@gmail.com<p>
                                
                            </div>
                            <div class="content_wrap">
                                <div class="sc_socials sc_socials_type_icons sc_socials_shape_square sc_socials_size_tiny">
                                    <div class="sc_socials_item">
                                        <a class="social_icons social_facebook" href="<?php echo $facebook_link ?>" title="<?php echo $facebook_title ?>" target="_blank"><span class="icon-facebook"></span></a>
                                    </div>
                                    <div class="sc_socials_item">
                                        <a class="social_icons social_instagram" href="<?php echo $instagram_link ?>" title="<?php echo $instagram_title ?>" target="_blank"><span class="icon-instagramm"></span></a>
                                    </div>
                                    <div class="sc_socials_item">
                                        <a class="social_icons social_linkedin" href="<?php echo $linkedin_link ?>" title="<?php echo $linkedin_title ?>" target="_blank"><span class="icon-linkedin"></span></a>
                                    </div>
                                </div>
                            </div>
                            <div class="sc_services_item_decoration"></div>
                        </div>
                    </div>
                    
                </div>
            </div>
            <div class="sc_empty_space" data-height="2em"></div>
        </div>
    </div>
    <div class="sc_section">
        <div class="content_wrap">
            <div class="sc_empty_space" data-height="0.8em"></div>
            <div class="sc_section margin_top_huge margin_bottom_huge">
                <div class="sc_section_inner">
                    <div class="sc_section_content_wrap">
                        <h2 class="sc_section_title sc_item_title">Hubungi Kami</h2>
                        <div class="sc_section_descr sc_item_descr">
                            Kami ada untuk memberikan pelayanan yang terbaik untuk Anda dan akan selalu siap membalas email yang Anda kirimkan dengan cepat.
                        </div>
                        <div class="sc_empty_space" data-height="0.4em"></div>
                        <div class="columns_wrap sc_columns margin_top_tiny">
                            <div class="column-1_2 sc_column_item">
                                <div class="sc_form_wrap">
                                    <div class="sc_form sc_form_style_form_2 aligncenter margin_top_medium">
                                        <div class="sc_form_fields">
                                            <iframe data-src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3944.535980234358!2d115.22603191433608!3d-8.640464890248698!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd23f7a779a6533%3A0x28f52169c1dbb072!2sPT%20Guna%20Artha%20kencana%20%22Red%20Consulting%22!5e0!3m2!1sid!2sid!4v1566890455589!5m2!1sid!2sid" width="100%" height="450" frameborder="0"  allowfullscreen=""></iframe>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="column-1_2 sc_column_item">
                                <div class="sc_form_wrap">
                                    <div class="sc_form sc_form_style_form_2 aligncenter margin_top_medium">
                                        <div class="sc_form_fields">
                                            <?php echo form_open_multipart('kontak/kirim_pesan','class="sc_input_hover_default" id="kirim_email" '); ?>
                                                <div class="sc_columns columns_wrap sc_form_info">
                                                    <div class="sc_form_address column-1_1">
                                                        <div class="sc_form_item sc_form_field">
                                                            <input class="sc_form_username" name="nama" placeholder="Name *" type="text">
                                                            <span class="form-error"></span>
                                                        </div>
                                                    </div>
                                                    <div class="column-1_2">
                                                        <div class="sc_form_item sc_form_field">
                                                            <input class="sc_form_email" name="email" placeholder="E-mail *" type="text">
                                                            <span class="form-error"></span>
                                                        </div>
                                                    </div>
                                                    <div class="sc_form_address column-1_2">
                                                        <div class="sc_form_item sc_form_field">
                                                            <input class="sc_form_subj" name="phone" placeholder="Phone" type="text">
                                                            <span class="form-error"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="sc_form_item sc_form_message">
                                                    <textarea class="sc_form_message" name="message" placeholder="Message"></textarea>
                                                    <span class="form-error"></span>
                                                </div>
                                                <div class="sc_columns columns_wrap sc_form_info">
                                                    <div class="column-1_2">
                                                        <div class="sc_form_item sc_form_field">
                                                            <?php echo $captcha; ?>
                                                        </div>
                                                    </div>
                                                    <div class="sc_form_address column-1_2">
                                                        <div class="sc_form_item sc_form_field">
                                                            <input class="sc_form_subj" id="captcha" placeholder="Input Security Code *" name="captcha" type="text">
                                                            <span class="form-error"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="sc_form_item sc_form_button">
                                                    <button class="sc_button sc_button_size_medium sc_button_iconed icon-paper-plane-light">Kirim Pesan</button>
                                                </div>
                                                
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
            <div class="sc_empty_space" data-height="11.19px"></div>
        </div>
    </div>
   
</div>