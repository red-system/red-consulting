<div class="page_content_wrap page_paddings_no">
    <div class="sc_section custom_bg_2">
        <div class="content_wrap">
            <div class="sc_empty_space" data-height="2.2em"></div>
            <div class="sc_section margin_top_huge ">
                <div class="sc_section_inner">
                    <h2 class="sc_section_title sc_item_title">Selamat Datang di <span class="color-active">RED</span> Consulting</h2>
                    <div class="sc_section_descr sc_item_descr">
                        Kami ada untuk memberikan pelayanan yang terbaik untuk anda.
                    </div>
                    <div class="sc_empty_space" data-height="0.7em"></div>
                    <div class="columns_wrap sc_columns margin_top_tiny">
                        <div class="column-1_2 sc_column_item">
                            <figure class="sc_image style_img">
                                <img width="346" height="264" alt="RED Consulting" title="RED Consulting"  class="first" src="<?php echo base_url();?>assets/images/home_1.jpg"> <img width="364" height="380" alt="RED Consulting" title="RED Consulting"  class="second" src="<?php echo base_url();?>assets/images/img1.jpg">
                            </figure>
                        </div>
                        <div class="column-1_2 sc_column_item">
                            <p align="justify">Derasnya persaingan bisnis yang nyaris tanpa kompromi, membuat management bisnis dan keuangan yang tepat dan efektif seakan menjadi sesuatu yang wajib dan harus diberikan ruang istimewa dalam sebuah perusahaan atau bisnis. Atas dasar itulah <span class="color-active">RED</span> Consulting hadir untuk menjadi solusi terbaik dalam menghadirkan manajemen bisnis yang tepat dan efektif untuk perusahaan atau bisnis anda sehingga perusahaan atau bisnis anda bisa bertumbuh dengan kuat dan stabil.</p>
                            <p align="justify"><span class="color-active">RED</span> Consulting adalah perusahaan Konsultan Pajak dan Keuangan yang bernaung di bawah PT. Guna Artha Kencana dan berkantor di Jalan Ratna No. 68 G, Denpasa-Bali.</p> 
                            <p align="justify">Didukung oleh tim bisnis yang telah berpengalaman lebih dari 10 tahun, <span class="color-active">RED</span> Consulting siap untuk menjadi pilihan terbaik bagi pelaku usaha (individu atau perusahaan) yang ingin menjadikan manajemen bisnis dan keuangan yang tepat dan efektif sebagai pondasi dasar yang kuat dalam berbisnis.<br/></p> 
                            <a class="sc_button sc_button_style_filled sc_button_size_medium margin_top_small margin_bottom_small" href="<?php echo base_url() ?>tentang-kami">Cari Tau di sini</a>
                        </div>
                    </div>
            </div>
            <div class="sc_empty_space" data-height="2em"></div>
        </div>
    </div>
    <div class="sc_section">
        <div class="content_wrap">
            <div class="sc_empty_space" data-height="2em"></div>
            <div class="sc_services_wrap">
                <div class="sc_services sc_services_style_services-1 sc_services_type_icons_img margin_top_huge margin_bottom_huge">
                    <h2 class="sc_services_title sc_item_title">Layanan Utama Kami</h2>
                    <div class="sc_services_descr sc_item_descr">
                        Layanan terbaik kami yang dapat menjadi solusi perusahaan anda
                    </div>
                    <div class="sc_columns columns_wrap">
                        <div class="column-1_3 column_padding_bottom">
                            <div class="sc_services_item">
                                <div class="top_post_image">
                                    <a href="<?php echo base_url() ?>layanan/konsultan-pajak"><img alt="Manajemen Perpajakan" title="Manajemen Perpajakan" width="58" height="66" class="services-post-image" src="<?php echo base_url();?>assets/images/icon-manajemen-perpajakan.png"></a>
                                </div>
                                <div class="sc_services_item_content">
                                    <h4 class="sc_services_item_title"><a href="<?php echo base_url() ?>layanan/konsultan-pajak">Konsultan Pajak</a></h4>
                                    <div class="sc_services_item_description">
                                        <p>Kami dapat membantu dalam konsultasi perpajakan, perencanaan pajak, review kewajiban wajib pajak, pengurusan SPT Masa dan SPT Tahunan, penyelesaian masalah perpajakan, administrasi Pajak, dan lain sebagainya.</p>
                                    </div>
                                    <div class="sc_services_item_decoration"></div>
                                </div>
                            </div>
                        </div>
                        <div class="column-1_3 column_padding_bottom">
                            <div class="sc_services_item">
                                <div class="top_post_image">
                                    <a href="<?php echo base_url() ?>layanan/manajemen-laporan-keuangan"><img alt="Manajemen Laporan Keuangan" title="Manajemen Laporan Keuangan" width="58" height="66" class="services-post-image" src="<?php echo base_url();?>assets/images/icon-manajemen-laporan keuangan.png"></a>
                                </div>
                                <div class="sc_services_item_content">
                                    <h4 class="sc_services_item_title"><a href="<?php echo base_url() ?>layanan/manajemen-laporan-keuangan">Manajemen Laporan Keuangan</a></h4>
                                    <div class="sc_services_item_description">
                                        <p>Menyusun laporan keuangan sehingga kondisi perusahaan dapat terbaca dengan jelas, Bagian Laporan Keuangan yang disajikan meliputi, laporan laba rugi, laporan neraca, laporan penyusutan & amortisasi, summery, advice.</p>
                                    </div>
                                    <div class="sc_services_item_decoration"></div>
                                </div>
                            </div>
                        </div>
                        <div class="column-1_3 column_padding_bottom">
                            <div class="sc_services_item">
                                <div class="top_post_image">
                                    <a href="<?php echo base_url() ?>layanan/finance-controller"><img  alt="Manajemen Keuangan" title="Manajemen Keuangan" width="58" height="66"class="services-post-image" src="<?php echo base_url();?>assets/images/icon-manajemen-keuangan.png"></a>
                                </div>
                                <div class="sc_services_item_content">
                                    <h4 class="sc_services_item_title"><a href="<?php echo base_url() ?>layanan/finance-controller">Finance Controller</a></h4>
                                    <div class="sc_services_item_description">
                                        <p>Menerapkan & Menyusun laporan Keuangan kepada manajemen sesuai dengan PSAK yang berlaku, Berkolaborasi dalam penyusunan inisiatif strategi, target perusahaan dan penyusunan anggaran keuangan untuk mencapai efektifitas operasional.</p>
                                    </div>
                                    <div class="sc_services_item_decoration"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="sc_section">
        <div class="columns_wrap sc_columns no_margins width_1_3" data-equal-height=".sc_column_item">
            <div class="column-1_3 sc_column_item sc_column_item_1 first text_align_center">
                <div class="sc_column_item_inner" id="video_player">
                    <div class="sc_popup mfp-with-anim mfp-hide" id="video_go">
                        <div class="sc_video_player">
                            <div class="sc_video_frame" data-height="659" data-width="1170">
                                
                            </div>
                        </div>
                    </div>
                    <div class="sc_section extra_vertical_align aligncenter">
                        <div class="sc_section_inner">
                            <div class="sc_section_content_wrap">
                                <div class="sc_services_wrap scheme_dark">
                                    <div class="sc_services sc_services_style_services-2 sc_services_type_icons margin_top_tiny-">
                                        <div class="sc_services_item">
                                            <div class="sc_services_item_featured post_featured"></div>
                                            <div class="sc_services_item_content">
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="column-2_3 sc_column_item">
                <div class="sc_empty_space" data-height="2.2em"></div>
                <div class="sc_services_wrap">
                    <div class="sc_services sc_services_style_services-2 sc_services_type_icons_img margin_top_huge margin_right_medium margin_bottom_huge margin_left_medium">
                        <h2 class="sc_services_title sc_item_title">Mengapa memilih Kami ?</h2>
                        <div class="sc_services_descr sc_item_descr">
                            Kelebihan Kami
                        </div>
                        <div class="sc_columns columns_wrap">
                            <div class="column-1_2 column_padding_bottom">
                                <div class="sc_services_item">
                                    <div class="top_post_image">
                                        <a href="#"><img alt="Dukungan Bisnis" title="Dukungan Bisnis" width="43" height="47" class="services-post-image" src="<?php echo base_url();?>assets/images/icon-solusi.png"></a>
                                    </div>
                                    <div class="sc_services_item_content">
                                        <h4 class="sc_services_item_title"><a href="#">Solusi Cerdas dan Cermat</a></h4>
                                        <div class="sc_services_item_description">
                                            <p align="justify">Kami selalu memberikan solusi yang cerdas dan cermat dalam menyelesaikan setiap permasalahan perpajakan maupun keuangan.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="column-1_2 column_padding_bottom">
                                <div class="sc_services_item">
                                    <div class="top_post_image">
                                        <a href="#"><img alt="Konsultasi Keuangan" title="Konsultasi Keuangan" width="43" height="47" class="services-post-image" src="<?php echo base_url();?>assets/images/icon-konsultasi keuangan.png"></a>
                                    </div>
                                    <div class="sc_services_item_content">
                                        <h4 class="sc_services_item_title"><a href="#">Responsible</a></h4>
                                        <div class="sc_services_item_description">
                                            <p align="justify">Memberikan tanggung jawab kepada seluruh klien dalam pelayanan yang cerdas dan cermat.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="column-1_2 column_padding_bottom">
                                <div class="sc_services_item">
                                    <div class="top_post_image">
                                        <a href="#"><img alt="Konsultasi Manajemen" title="Konsultasi Manajemen" width="43" height="47" class="services-post-image" src="<?php echo base_url();?>assets/images/icon-konsultasi-manajemen.png"></a>
                                    </div>
                                    <div class="sc_services_item_content">
                                        <h4 class="sc_services_item_title"><a href="#">Sistem Kerja Inovatif</a></h4>
                                        <div class="sc_services_item_description">
                                            <p align="justify">Kami memiliki system kerja yang inovatif dalam berkerjasama dengan klien sehingga kenyamanan dan keamanan klien bisa selalu terjaga dengan baik.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="column-1_2 column_padding_bottom">
                                <div class="sc_services_item">
                                    <div class="top_post_image">
                                        <a href="#"><img alt="Konsultasi Manajemen" title="Konsultasi Manajemen" width="43" height="47" class="services-post-image" src="<?php echo base_url();?>assets/images/icon-education.png"></a>
                                    </div>
                                    <div class="sc_services_item_content">
                                        <h4 class="sc_services_item_title"><a href="#">Education</a></h4>
                                        <div class="sc_services_item_description">
                                            <p align="justify">Memberikan Edukasi manajemen keuangan dan bisnis sehingga dapat memberikan saran yang tepat dan akurat.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="column-1_2 column_padding_bottom">
                                <div class="sc_services_item">
                                    <div class="top_post_image">
                                        <a href="#"><img alt="Pelatihan Keuangan" title="Pelatihan Keuangan" width="43" height="47" class="services-post-image" src="<?php echo base_url();?>assets/images/icon-pelatihan-keuangan.png"></a>
                                    </div>
                                    <div class="sc_services_item_content">
                                        <h4 class="sc_services_item_title"><a href="#">SDM (Sumber Daya Manusia) Berkualitas</a></h4>
                                        <div class="sc_services_item_description">
                                            <p align="justify">Kami memiliki SDM yang cakap dan ahli dibidangnya masing-masing sehingga setiap permasalahan klien bisa tertangani dengan baik dan tepat.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="column-1_2 column_padding_bottom">
                                <div class="sc_services_item">
                                    <div class="top_post_image">
                                        <a href="#"><img alt="Konsultasi Keuangan" title="Konsultasi Keuangan" width="43" height="47" class="services-post-image" src="<?php echo base_url();?>assets/images/icon-dedication.png"></a>
                                    </div>
                                    <div class="sc_services_item_content">
                                        <h4 class="sc_services_item_title"><a href="#">Dedication</a></h4>
                                        <div class="sc_services_item_description">
                                            <p align="justify">Kami berkomitmen memberikan seluruh energi kepada seluruh client, sehingga setiap permasalahan klien dapat tertangani dengan baik tanpa harus khawatir.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="sc_empty_space" data-height="0.3em"></div>
            </div>
        </div>
    </div>
    <div class="sc_section custom_bg_2">
        <div class="content_wrap">
            <div class="sc_empty_space" data-height="2em"></div>
            <div class="sc_team_wrap">
                <div class="sc_team sc_team_style_team-3 margin_top_huge margin_bottom_huge aligncenter">
                    <h2 class="sc_team_title sc_item_title">Staff Kami</h2>
                    <div class="sc_team_descr sc_item_descr">
                       Kami berupaya agar bisnis Anda mulai bekerja secara efektif untuk Anda.
                    </div>
                    <div class="sc_columns columns_wrap">
                        <div class="column-1_3 column_padding_bottom">
                            <div class="sc_team_item">
                                <div class="sc_team_item_avatar">
                                    <img alt="I Made Dwi Harmana SE., M.Si., BKP" title="I Made Dwi Harmana SE., M.Si., BKP" src="<?php echo base_url();?>assets/images/team/i-made-dwi-harmana.jpg" widht="370" height="463">
                                    <div class="sc_team_item_hover">
                                        <div class="sc_team_item_socials">
                                            <div class="sc_socials sc_socials_type_icons sc_socials_shape_round sc_socials_size_tiny">
                                                <div class="sc_socials_item">
                                                    <a class="social_icons social_facebook" href="#" target="_blank"><span class="icon-facebook"></span></a>
                                                </div>
                                                <div class="sc_socials_item">
                                                    <a class="social_icons social_gplus" href="#" target="_blank"><span class="icon-gplus"></span></a>
                                                </div>
                                                <div class="sc_socials_item">
                                                    <a class="social_icons social_linkedin" href="#" target="_blank"><span class="icon-linkedin"></span></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="sc_team_item_info">
                                    <h5 class="sc_team_item_title"><a href="single-team.html">I Made Dwi Harmana SE., M.Si., BKP</a></h5>
                                    <div class="sc_team_item_position">
                                        Tax Consultant (No Izin Praktek : KEP-3580/IP.B/PJ/2018)
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="column-1_3 column_padding_bottom">
                            <div class="sc_team_item">
                                <div class="sc_team_item_avatar">
                                    <img alt="A.A Sagung Alit Dwijayanti" title="A.A Sagung Alit Dwijayanti" src="<?php echo base_url();?>assets/images/team/a-a-sagung-alit-dwijayanti.jpg" widht="370" height="463">
                                    <div class="sc_team_item_hover">
                                        <div class="sc_team_item_socials">
                                            <div class="sc_socials sc_socials_type_icons sc_socials_shape_round sc_socials_size_tiny">
                                                <div class="sc_socials_item">
                                                    <a class="social_icons social_facebook" href="#" target="_blank"><span class="icon-facebook"></span></a>
                                                </div>
                                                <div class="sc_socials_item">
                                                    <a class="social_icons social_gplus" href="#" target="_blank"><span class="icon-gplus"></span></a>
                                                </div>
                                                <div class="sc_socials_item">
                                                    <a class="social_icons social_linkedin" href="#" target="_blank"><span class="icon-linkedin"></span></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="sc_team_item_info">
                                    <h5 class="sc_team_item_title"><a href="single-team.html">A.A Sagung Alit Dwijayanti</a></h5>
                                    <div class="sc_team_item_position">
                                        Tax & Accounting Manager
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="column-1_3 column_padding_bottom">
                            <div class="sc_team_item">
                                <div class="sc_team_item_avatar">
                                    <img alt="Ni Kadek Miantari" title="Ni Kadek Miantari" src="<?php echo base_url();?>assets/images/team/ni-kadek-miantari.jpg" widht="370" height="463">
                                    <div class="sc_team_item_hover">
                                        <div class="sc_team_item_socials">
                                            <div class="sc_socials sc_socials_type_icons sc_socials_shape_round sc_socials_size_tiny">
                                                <div class="sc_socials_item">
                                                    <a class="social_icons social_facebook" href="#" target="_blank"><span class="icon-facebook"></span></a>
                                                </div>
                                                <div class="sc_socials_item">
                                                    <a class="social_icons social_gplus" href="#" target="_blank"><span class="icon-gplus"></span></a>
                                                </div>
                                                <div class="sc_socials_item">
                                                    <a class="social_icons social_linkedin" href="#" target="_blank"><span class="icon-linkedin"></span></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="sc_team_item_info">
                                    <h5 class="sc_team_item_title"><a href="single-team.html">Ni Kadek Miantari</a></h5>
                                    <div class="sc_team_item_position">
                                        Tax Officer
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="column-1_3 column_padding_bottom">
                            <div class="sc_team_item">
                                <div class="sc_team_item_avatar">
                                    <img alt="I Gede Wahyu Indrawan" title="I Gede Wahyu Indrawan" src="<?php echo base_url();?>assets/images/team/i-gede-wahyu-indrawan.jpg" widht="370" height="463">
                                    <div class="sc_team_item_hover">
                                        <div class="sc_team_item_socials">
                                            <div class="sc_socials sc_socials_type_icons sc_socials_shape_round sc_socials_size_tiny">
                                                <div class="sc_socials_item">
                                                    <a class="social_icons social_facebook" href="#" target="_blank"><span class="icon-facebook"></span></a>
                                                </div>
                                                <div class="sc_socials_item">
                                                    <a class="social_icons social_gplus" href="#" target="_blank"><span class="icon-gplus"></span></a>
                                                </div>
                                                <div class="sc_socials_item">
                                                    <a class="social_icons social_linkedin" href="#" target="_blank"><span class="icon-linkedin"></span></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="sc_team_item_info">
                                    <h5 class="sc_team_item_title"><a href="single-team.html">I Gede Wahyu Indrawan</a></h5>
                                    <div class="sc_team_item_position">
                                        Tax Officer
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="column-1_3 column_padding_bottom">
                            <div class="sc_team_item">
                                <div class="sc_team_item_avatar">
                                    <img alt="Ni Putu Juliastari" title="Ni Putu Juliastari" src="<?php echo base_url();?>assets/images/team/ni-putu-juliastari.jpg" widht="370" height="463">
                                    <div class="sc_team_item_hover">
                                        <div class="sc_team_item_socials">
                                            <div class="sc_socials sc_socials_type_icons sc_socials_shape_round sc_socials_size_tiny">
                                                <div class="sc_socials_item">
                                                    <a class="social_icons social_facebook" href="#" target="_blank"><span class="icon-facebook"></span></a>
                                                </div>
                                                <div class="sc_socials_item">
                                                    <a class="social_icons social_gplus" href="#" target="_blank"><span class="icon-gplus"></span></a>
                                                </div>
                                                <div class="sc_socials_item">
                                                    <a class="social_icons social_linkedin" href="#" target="_blank"><span class="icon-linkedin"></span></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="sc_team_item_info">
                                    <h5 class="sc_team_item_title"><a href="single-team.html">Ni Putu Juliastari</a></h5>
                                    <div class="sc_team_item_position">
                                        Tax Officer
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="sc_empty_space" data-height="0.5em"></div>
    </div>
    <div class="sc_section skills_bg_1">
        <div class="content_wrap">
            <div class="sc_empty_space" data-height="3.7em"></div>
            <h2 class="sc_title sc_title_regular sc_align_center margin_top_huge margin_bottom_null text_align_center custom_cl_4">Kami selalu di depan.</h2>
            <h2 class="sc_title sc_title_underline sc_align_center margin_top_null text_align_center custom_cl_4">Solusi Profesional untuk Bisnis Anda.</h2>
            <div class="sc_skills sc_skills_counter margin_top_medium margin_bottom_huge" data-caption="Skills" data-type="counter">
                <div class="columns_wrap sc_skills_columns sc_skills_columns_4">
                    <div class="sc_skills_column column-1_3">
                        <div class="sc_skills_item sc_skills_style_4">
                            <div class="sc_skills_info_2">
                                <div class="sc_skills_label">
                                    klien
                                </div>
                            </div>
                            <div class="sc_skills_count">
                                <div data-duration="2940" data-ed="" data-max="100" data-speed="30" data-start="0" data-step="1" data-stop="198">
                                    198
                                </div>
                            </div>
                            <div class="sc_skills_info">
                                <div class="sc_skills_label">
                                    Klien & Mitra yang Puas
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="sc_skills_column column-1_3">
                        <div class="sc_skills_item sc_skills_style_4">
                            <div class="sc_skills_info_2">
                                <div class="sc_skills_label">
                                    Partner
                                </div>
                            </div>
                            <div class="sc_skills_count">
                                <div data-duration="2475" data-ed="" data-max="256" data-speed="29" data-start="0" data-step="3" data-stop="30">
                                    30
                                </div>
                            </div>
                            <div class="sc_skills_info">
                                <div class="sc_skills_label">
                                    Partner
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="sc_skills_column column-1_3">
                        <div class="sc_skills_item sc_skills_style_4">
                            <div class="sc_skills_info_2">
                                <div class="sc_skills_label">
                                    Proyek Konsulting
                                </div>
                            </div>
                            <div class="sc_skills_count">
                                <div data-duration="325" data-ed="" data-max="256" data-speed="15" data-start="0" data-step="3" data-stop="24">
                                    24
                                </div>
                            </div>
                            <div class="sc_skills_info">
                                <div class="sc_skills_label">
                                    Proyek Konsulting
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="sc_empty_space" data-height="2em"></div>
        </div>
    </div>
    <div class="sc_section custom_bg_2">
        <div class="sc_empty_space" data-height="1em"></div>
        <div class="sc_testimonials_wrap sc_section scheme_original_in">
            <div class="sc_section_overlay">
                <div class="content_wrap">
                    <div class="sc_testimonials sc_testimonials_style_testimonials-4 margin_top_huge margin_bottom_huge aligncenter">
                        <h2 class="sc_testimonials_title sc_item_title">Testimonials</h2>
                        <div class="sc_testimonials_descr sc_item_descr">
                            Testimoni dari pelanggan yang puas dengan layanan yang telah digunakan dalam pemasaran selama pemasaran ada.
                        </div>
                        <div class="sc_section_button sc_item_button">
                            <a class="sc_button sc_button_style_border sc_button_size_medium" href="<?php echo base_url() ?>testimonial">Lihat Lebih Banyak Testimonial di sini</a>
                        </div>
                        <div class="sc_empty_space" data-height="2em"></div>
                        <div class="sc_slider_swiper swiper-slider-container sc_slider_pagination sc_slider_pagination_bottom sc_slider_nocontrols" data-interval="8082" data-slides-min-width="250" data-slides-per-view="2" data-slides-space="30">
                            <div class="slides swiper-wrapper">
                                <div class="swiper-slide">
                                    <div class="sc_testimonial_item">
                                        <div class="sc_testimonial_avatar"><img alt="Lisa Larson" title="Lisa Larson" width="80" height="80" src="<?php echo base_url();?>assets/images/testimonial/testimonials-1-80x80.jpg"></div>
                                        <div class="sc_testimonial_content">
                                            <p>Being a company's director requires maximum attention and devotion. This was exactly what I felt when turned to your products and services. All our questions and inquiries were answered effectively and right away. Our website has never looked better, ever. Thank you.</p>
                                        </div>
                                        <div class="sc_testimonial_author">
                                            <span class="sc_testimonial_author_name">Lisa Larson</span> <span class="sc_testimonial_author_position">Company Director</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="sc_testimonial_item">
                                        <div class="sc_testimonial_avatar"><img alt="James Watson" title="James Watson" width="80" height="80" src="<?php echo base_url();?>assets/images/testimonial/testimonials-2-80x80.jpg"></div>
                                        <div class="sc_testimonial_content">
                                            <p>You never know what is gooing to happen until you try. But let me tell you that taking risk with these guys was totally worth it. Now we are a regular client, and this was probably the best decision we ever made! Our company appreciates your assistance and great work!</p>
                                        </div>
                                        <div class="sc_testimonial_author">
                                            <span class="sc_testimonial_author_name">James Watson</span> <span class="sc_testimonial_author_position">Senior Manager</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="sc_testimonial_item">
                                        <div class="sc_testimonial_avatar"><img alt="Chris Doe" title="Chris Doe" width="80" height="80" src="<?php echo base_url();?>assets/images/testimonial/testimonials-3-80x80.jpg"></div>
                                        <div class="sc_testimonial_content">
                                            <p>Highly recommend these guys to everyone who is in search of fresh ideas, quality products and excellent customer support. You will be able to find what you need and more right here with no hassle. Thanks for your time and efford toward our project. We will stay in touch!</p>
                                        </div>
                                        <div class="sc_testimonial_author">
                                            <span class="sc_testimonial_author_name">Chris Doe</span> <span class="sc_testimonial_author_position">Startup SEO</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="sc_testimonial_item">
                                        <div class="sc_testimonial_avatar"><img alt="Susan Smith" title="Susan Smith" width="80" height="80" src="<?php echo base_url();?>assets/images/testimonial/testimonials-4-80x80.jpg"></div>
                                        <div class="sc_testimonial_content">
                                            <p>I enjoyed working with you guys very much! No matter what issues or questions popped up, you were always there to assist. I appreciate your service, quality products and professional approach in every aspect. Keep up the excellent job and let us know what comes next!</p>
                                        </div>
                                        <div class="sc_testimonial_author">
                                            <span class="sc_testimonial_author_name">Susan Smith</span> <span class="sc_testimonial_author_position">Handmade Designer</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="sc_slider_controls_wrap">
                                <a class="sc_slider_prev" href="#"></a> <a class="sc_slider_next" href="#"></a>
                            </div>
                            <div class="sc_slider_pagination_wrap"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="sc_empty_space" data-height="0.7em"></div>
    </div>
    
</div>