<div class="page_content_wrap page_paddings_no">
    <div class="sc_section custom_bg_2">
        <div class="content_wrap">
            <div class="sc_empty_space" data-height="2.2em"></div>
            <div class="sc_section margin_top_huge ">
                <div class="sc_section_inner">
                   <h2 class="sc_section_title sc_item_title"><?php echo $title_act; ?></h2>
                    <div class="sc_section_descr sc_item_descr">
                        <?php echo $description_layanan; ?>
                    </div>
                    <div class="sc_empty_space" data-height="0.7em"></div>
                    <div class="columns_wrap sc_columns margin_top_tiny">
                        <div class="column-1_2 sc_column_item">
                            <figure class="sc_image style_img">
                                <img width="346" height="264" alt="Manajemen Laporan Keuangan" title="Manajemen Laporan Keuangan"  class="first" src="<?php echo base_url();?>assets/images/home_1.jpg"> <img width="364" height="380" alt="Manajemen Laporan Keuangan" title="Manajemen Laporan Keuangan"  class="second" src="<?php echo base_url();?>assets/images/img1.jpg">
                            </figure>
                        </div>
                        <div class="column-1_2 sc_column_item">
                            <p align="justify">Laporan keuangan ialah susunan data keuangan yang berasal dari segala jenis transaksi baik pemasukan maupun pengeluaran dalam periode tertentu. Data yang dicantumkan pada laporan keuangan meliputi catatan transaksi pengeluaran, catatan transaksi pemasukan atau pendapatan, pernyataan (statement) dari cash flow, balance sheet, dan total profit atau keuntungan dan kerugian.</p>
                            <p align="justify">Pada dasarnya, tujuan laporan keuangan adalah menunjukkan kondisi keuangan sebuah perusahaan pada periode tertentu sehingga performa sebuah perusahaan bisa diketahui. Setiap perusahaan wajib membuat laporan keuangan untuk kebutuhan internal maupun eksternal seperti pengajuan pinjaman.</p>
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="sc_empty_space" data-height="2em"></div>
        </div>
    </div>
    <div class="sc_section custom_bg_1">
        <div class="content_wrap">
            <div class="sc_empty_space" data-height="2em"></div>
            <div class="sc_services_wrap">
                <div class="sc_services sc_services_style_services-3 sc_services_type_images margin_top_huge margin_bottom_huge">
                    <h2 class="sc_services_title sc_item_title">Layanan <?php echo $title_act; ?></h2>
                    <div class="sc_services_descr sc_item_descr">
                        Berikut ini layanan yang akan kami berikan terkait Manajemen Laporan Keuangan.
                    </div>
                    <div class="sc_columns columns_wrap">
                        <div class="column-1_3 column_padding_bottom">
                            <div class="sc_services_item sc_services_item_1">
                                <div class="sc_services_item_featured post_featured">
                                    <div class="post_thumb">
                                        <a class="hover_icon hover_icon_link" href="<?php echo site_url('layanan/manajemen-laporan-keuangan/penyusunan-laporan-keuangan') ?>"><img width="370" height="240" alt="Penyusunan Laporan Keuangan" title="Penyusunan Laporan Keuangan" src="<?php echo base_url();?>assets/images/layanan/penyusunan-laporan-keuangan-red-consulting.jpeg"></a>
                                    </div>
                                    <div class="sc_services_item_count">
                                        1
                                    </div>
                                </div>
                                <div class="sc_services_item_content">
                                    <h4 class="sc_services_item_title"><a href="<?php echo site_url('layanan/manajemen-laporan-keuangan/penyusunan-laporan-keuangan') ?>">Penyusunan Laporan Keuangan</a></h4>
                                    <div class="sc_services_item_description">
                                        <p>Laporan Keuangan adalah media tertulis yang berisikan laporan...</p>
                                    </div>
                                    <div class="sc_services_item_decoration"></div>
                                </div>
                            </div>
                        </div>
                        <div class="column-1_3 column_padding_bottom">
                            <div class="sc_services_item sc_services_item_2">
                                <div class="sc_services_item_featured post_featured">
                                    <div class="post_thumb">
                                        <a class="hover_icon hover_icon_link" href="<?php echo site_url('layanan/manajemen-laporan-keuangan/penyusunan-laporan-keuangan') ?>"><img width="370" height="240" alt="Menyusun Proyeksi Keuangan" title="Menyusun Proyeksi Keuangan" src="<?php echo base_url();?>assets/images/layanan/menyusun-proyeksi-keuangan-red-consulting.jpeg"></a>
                                    </div>
                                    <div class="sc_services_item_count">
                                        2
                                    </div>
                                </div>
                                <div class="sc_services_item_content">
                                    <h4 class="sc_services_item_title"><a href="<?php echo site_url('layanan/manajemen-laporan-keuangan/penyusunan-laporan-keuangan') ?>">Menyusun Proyeksi Keuangan</a></h4>
                                    <div class="sc_services_item_description">
                                        <p>Menyusun proyeksi keuangan merupakan kegiatan penting...</p>
                                    </div>
                                    <div class="sc_services_item_decoration"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>