<div class="page_content_wrap page_paddings_yes padding-error">
    <div class="content_wrap">
        <div class="content content-100">
            <article class="post_item post_item_404">
                <div class="post_content">
                    <h1 class="page_title">404</h1>
                    <h2 class="page_subtitle">Halaman yang diminta tidak dapat ditemukan</h2>
                    <p class="page_description">Tidak dapat menemukan yang Anda butuhkan? Luangkan waktu sejenak dan mulai dari <a class="sc_button sc_button_style_border sc_button_size_medium" href="<?php echo base_url() ?>">Beranda</a></p>
                </div>
            </article>
        </div>
    </div>
</div>