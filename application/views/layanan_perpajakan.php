<div class="page_content_wrap page_paddings_no">
    <div class="sc_section custom_bg_2">
        <div class="content_wrap">
            <div class="sc_empty_space" data-height="2.2em"></div>
            <div class="sc_section margin_top_huge ">
                <div class="sc_section_inner">
                    <h2 class="sc_section_title sc_item_title"><?php echo $title_act; ?></h2>
                    <div class="sc_section_descr sc_item_descr">
                        <?php echo $description_layanan; ?>
                    </div>
                    <div class="sc_empty_space" data-height="0.7em"></div>
                    <div class="columns_wrap sc_columns margin_top_tiny">
                        <div class="column-1_2 sc_column_item">
                            <figure class="sc_image style_img">
                                <img width="346" height="264" alt="Manajemen Perpajakan" title="Manajemen Perpajakan"  class="first" src="<?php echo base_url();?>assets/images/home_1.jpg"> <img width="364" height="380" alt="Manajemen Perpajakan" title="Manajemen Perpajakan"  class="second" src="<?php echo base_url();?>assets/images/img1.jpg">
                            </figure>
                        </div>
                        <div class="column-1_2 sc_column_item">
                            <p align="justify">Upaya dalam melakukan penghematan pajak secara legal dapat dilakukan melalui manajemenpajak. Legalitas manajemen pajak tergantung dari instrumen yang dipakai yaitu setelah adaputusan Pengadilan. Manajemen pajak adalah sarana untuk memenuhi kewajiban perpajakan dengan benar, tetapi jumlah pajak yang dibayar dapat ditekan serendah mungkin untuk memperoleh laba dan likuiditas yang diharapkan.</p>
                            <p align="justify">Maka dari kami RED Consulting hadir untuk membantu bisnis Anda. Kami akan menggunakan strategi yang efektif dan efisien agar perusahaan/bisnis mendapatkan laba dan likuiditas yang diharapkan.</p>
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="sc_empty_space" data-height="2em"></div>
        </div>
    </div>
    <div class="sc_section custom_bg_1">
        <div class="content_wrap">
            <div class="sc_empty_space" data-height="2em"></div>
            <div class="sc_services_wrap">
                <div class="sc_services sc_services_style_services-3 sc_services_type_images margin_top_huge margin_bottom_huge">
                    <h2 class="sc_services_title sc_item_title">Layanan <?php echo $title_act; ?></h2>
                    <div class="sc_services_descr sc_item_descr">
                        Berikut ini layanan yang akan kami berikan terkait Manajemen Perpajakan.
                    </div>
                    <div class="sc_columns columns_wrap">
                        <div class="column-1_3 column_padding_bottom">
                            <div class="sc_services_item sc_services_item_1">
                                <div class="sc_services_item_featured post_featured">
                                    <div class="post_thumb">
                                        <a class="hover_icon hover_icon_link" href="<?php echo site_url('layanan/konsultan-pajak/pendampingan-restitusi-pajak') ?>"><img width="370" height="240" alt="Pendampingan Restitusi Pajak" title="Pendampingan Restitusi Pajak" src="<?php echo base_url();?>assets/images/layanan/pendampingan-restitusi-pajak-red-consulting.jpeg"></a>
                                    </div>
                                    <div class="sc_services_item_count">
                                        1
                                    </div>
                                </div>
                                <div class="sc_services_item_content">
                                    <h4 class="sc_services_item_title"><a href="<?php echo site_url('layanan/konsultan-pajak/pendampingan-restitusi-pajak') ?>">Pendampingan Restitusi Pajak</a></h4>
                                    <div class="sc_services_item_description">
                                        <p>Sering terjadi wajib pajak kelebihan melakukan pembayaran pajak...</p>
                                    </div>
                                    <div class="sc_services_item_decoration"></div>
                                </div>
                            </div>
                        </div>
                        <div class="column-1_3 column_padding_bottom">
                            <div class="sc_services_item sc_services_item_2">
                                <div class="sc_services_item_featured post_featured">
                                    <div class="post_thumb">
                                        <a class="hover_icon hover_icon_link" href="<?php echo site_url('layanan/konsultan-pajak/pendampingan-restitusi-pajak') ?>"><img width="370" height="240" alt="Pendampingan Pemeriksaan Pajak" title="Pendampingan Pemeriksaan Pajak" src="<?php echo base_url();?>assets/images/layanan/pendampingan-pemeriksaan-pajak-red-consulting.jpeg"></a>
                                    </div>
                                    <div class="sc_services_item_count">
                                        2
                                    </div>
                                </div>
                                <div class="sc_services_item_content">
                                    <h4 class="sc_services_item_title"><a href="<?php echo site_url('layanan/konsultan-pajak/pendampingan-restitusi-pajak') ?>">Pendampingan Pemeriksaan Pajak</a></h4>
                                    <div class="sc_services_item_description">
                                        <p>Pemeriksaan pajak bisa hadir sewaktu-waktu, baik diberitahukan sebelumnya ataupun...</p>
                                    </div>
                                    <div class="sc_services_item_decoration"></div>
                                </div>
                            </div>
                        </div>
                        <div class="column-1_3 column_padding_bottom">
                            <div class="sc_services_item sc_services_item_3">
                                <div class="sc_services_item_featured post_featured">
                                    <div class="post_thumb">
                                        <a class="hover_icon hover_icon_link" href="<?php echo site_url('layanan/konsultan-pajak/pendampingan-restitusi-pajak') ?>"><img width="370" height="240" alt="Penyusunan Laporan Perpajakan" title="Penyusunan Laporan Perpajakan" src="<?php echo base_url();?>assets/images/layanan/pengurusan-laporan-perpajakan-red-consulting.jpeg"></a>
                                    </div>
                                    <div class="sc_services_item_count">
                                        3
                                    </div>
                                </div>
                                <div class="sc_services_item_content">
                                    <h4 class="sc_services_item_title"><a href="<?php echo site_url('layanan/konsultan-pajak/pendampingan-restitusi-pajak') ?>">Penyusunan Laporan Perpajakan</a></h4>
                                    <div class="sc_services_item_description">
                                        <p>Sebagai pengusaha baik itu berbentuk orang pribadi ataupun perusahaan, ada 2 (dua) jenis...</p>
                                    </div>
                                    <div class="sc_services_item_decoration"></div>
                                </div>
                            </div>
                        </div>
                        <div class="column-1_3 column_padding_bottom">
                            <div class="sc_services_item sc_services_item_3">
                                <div class="sc_services_item_featured post_featured">
                                    <div class="post_thumb">
                                        <a class="hover_icon hover_icon_link" href="<?php echo site_url('layanan/konsultan-pajak/pendampingan-restitusi-pajak') ?>"><img width="370" height="240" alt="Pendampingan Administrasi Perpajakan" title="Pendampingan Administrasi Perpajakan" src="<?php echo base_url();?>assets/images/layanan/pendampingan-administrasi-perpajakan-red-consulting.jpeg"></a>
                                    </div>
                                    <div class="sc_services_item_count">
                                        4
                                    </div>
                                </div>
                                <div class="sc_services_item_content">
                                    <h4 class="sc_services_item_title"><a href="<?php echo site_url('layanan/konsultan-pajak/pendampingan-restitusi-pajak') ?>">Pendampingan Administrasi Perpajakan</a></h4>
                                    <div class="sc_services_item_description">
                                        <p>Untuk bisa menjalankan kewajiban perpajakan di Indonesia dengan benar dan tepat...</p>
                                    </div>
                                    <div class="sc_services_item_decoration"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>