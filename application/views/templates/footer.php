            <div class="sticky-container hide--social--media-asside">
                <ul class="sticky sticky--padding-left">
                    <li>
                        <img src="<?php echo base_url();?>assets/images/icon-facebook.png" width="32" height="32">
                        <p><a href="<?php echo $facebook_link ?>" title="<?php echo $facebook_title ?>" alt="<?php echo $facebook_title ?>" target="_blank">Sukai kami di<br>Facebook</a></p>
                    </li>
                    <li>
                        <img src="<?php echo base_url();?>assets/images/icon-instagram.png" width="32" height="32">
                        <p><a href="<?php echo $instagram_link ?>" title="<?php echo $instagram_title ?>" alt="<?php echo $instagram_title ?>" target="_blank">Ikuti kami di<br>Instagram</a></p>
                    </li>
                    <li>
                        <img src="<?php echo base_url();?>assets/images/icon-linkedin.png" width="32" height="32">
                        <p><a href="<?php echo $linkedin_link ?>" title="<?php echo $linkedin_title ?>" alt="<?php echo $linkedin_title ?>" target="_blank">Ikuti kami di<br>LinkedIn</a></p>
                    </li>
               </ul>
            </div>
            <footer class="footer_wrap widget_area scheme_dark">
                <div class="footer_wrap_inner widget_area_inner">
                    <div class="content_wrap">
                        <div class="columns_wrap">
                            <aside class="column-1_3 widget widget_socials">
                                <div class="widget_inner">
                                    <div class="logo">
                                        <a href="<?php echo base_url() ?>"><img alt="RED Consulting" title="RED Consulting" width="134" height="47" class="logo_main" src="<?php echo base_url();?>assets/images/red-consulting-logo-footer.png"></a>
                                    </div>
                                    <div class="logo_descr">
                                       <p align="justify"> 
                                            R.E.D. CONSULTING hadir untuk membantu perusahaan dengan membuka seluruh potensi yang dimiliki untuk tumbuh lebih besar, dengan turut menghadirkan solusi terbaik dalam memenuhi penataan keuangan perusahaan. Kami melayani perusahaan di berbagai bidang industri dengan mengedepankan layanan
                                            <br>
                                        </p>
                                        
                                    </div>
                                    <div class="content_wrap">
                                        <div class="sc_socials sc_socials_type_icons sc_socials_shape_square sc_socials_size_tiny">
                                            <div class="sc_socials_item">
                                                <a class="social_icons social_facebook" href="<?php echo $facebook_link ?>" title="<?php echo $facebook_title ?>" target="_blank"><span class="icon-facebook"></span></a>
                                            </div>
                                            <div class="sc_socials_item">
                                                <a class="social_icons social_instagram" href="<?php echo $instagram_link ?>" title="<?php echo $instagram_title ?>" target="_blank"><span class="icon-instagramm"></span></a>
                                            </div>
                                            <div class="sc_socials_item">
                                                <a class="social_icons social_linkedin" href="<?php echo $linkedin_link ?>" title="<?php echo $linkedin_title ?>" target="_blank"><span class="icon-linkedin"></span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </aside>
                            <aside class="column-1_3 widget widget_text">
                                <h5 class="widget_title">Link Lainnya</h5>
                                <div class="textwidget">
                                    <ul class="sc_list sc_list_style_iconed custom_cl_1">
                                        <li><a href="<?php echo base_url() ?>faq"><span>FAQ</span></a></li>
                                        <li><a href="<?php echo base_url() ?>syarat-ketentuan"><span>Syarat & Ketentuan</span></a></li>
                                        <li><a href="<?php echo base_url() ?>kebijakan-privasi"><span>Kebijakan Privasi</span></a></li>
                                        <li><a href="<?php echo base_url() ?>tentang-kami"><span>Tentang Kami</span></a></li>
                                        <li><a href="<?php echo base_url() ?>kontak"><span>Kontak Kami</span></a></li>
                                    </ul>
                                </div>
                            </aside>
                            <aside class="column-1_3 widget widget_text">
                                <h5 class="widget_title">Kontak Kami</h5>
                                <div class="textwidget">
                                    <ul class="sc_list sc_list_style_iconed custom_cl_1">
                                        <li class="sc_list_item"><span class="sc_list_icon icon-location-light custom_cl_2"></span>Jl. Ratna No 68 G, Tonja, Denpasar Utara, Denpasar - Bali . 80239.</li>
                                        <li class="sc_list_item"><span class="sc_list_icon icon-clock-light custom_cl_2"></span>Sen - Jum : 08:00 - 17:00</li>
                                        <li class="sc_list_item"><span class="sc_list_icon icon-mobile-light custom_cl_2"></span>Phone: <a href="tel:(0361) 4746102">(0361) 4746102</a></li>
                                        <li class="sc_list_item"><span class="sc_list_icon icon-mail-light custom_cl_2"></span>Email: <a href="mailto:inforedconsulting69@gmail.com">inforedconsulting69@gmail.com</a></li>
                                    </ul>
                                </div>
                            </aside>
                        </div>
                    </div>
                </div>
            </footer>
            <div class="copyright_wrap copyright_style_soc scheme_dark">
                <div class="copyright_wrap_inner">
                    <div class="content_wrap">
                        <span>&copy; <?php echo date ('Y');?> | PT. Guna Artha kencana (Red Consulting) | by <a href="http://redsystem.id" target="_blank">Red System</a></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <span id="base-value" data-base-url="<?php echo base_url(); ?>"></span>

    <div class="popup_wrap_bg"></div><a class="scroll_to_top icon-up" href="#" title="Scroll to top"></a> 
    
    <?php if (($this->session->menu=='home')== 'active') { ?>
        <script async defer type="text/javascript" src="<?php echo base_url();?>assets/js/beranda.min.js "></script>
    <?php } elseif (($this->session->menu=='layanan-pajak-detail'||$this->session->menu=='layanan-laporan-keuangan-detail'||$this->session->menu=='layanan-keuangan-detail')== 'active') { ?>
        <script async defer type="text/javascript" src="<?php echo base_url();?>assets/js/layanan_detail.min.js "></script>
    <?php } elseif (($this->session->menu=='galeri')== 'active') { ?>
        <script async defer type="text/javascript" src="<?php echo base_url();?>assets/js/galeri.min.js "></script>
    <?php } elseif (($this->session->menu=='artikel')== 'active') { ?>
        <script async defer type="text/javascript" src="<?php echo base_url();?>assets/js/artikel.min.js "></script>
    <?php } elseif (($this->session->menu=='artikel-detail')== 'active') { ?>
        <script async defer type="text/javascript" src="<?php echo base_url();?>assets/js/artikel_detail.min.js "></script>
    <?php } elseif (($this->session->menu=='tentang-kami')== 'active') { ?>
        <script async defer type="text/javascript" src="<?php echo base_url();?>assets/js/tentang_kami.min.js "></script>
    <?php } elseif (($this->session->menu=='faq')== 'active') { ?>
        <script async defer type="text/javascript" src="<?php echo base_url();?>assets/js/faq.min.js "></script>
    <?php } elseif (($this->session->menu=='kontak')== 'active') { ?>
        <script async defer type="text/javascript" src="<?php echo base_url();?>assets/js/kontak.min.js "></script>
    <?php } else {  ?>
        <script async defer type="text/javascript" src="<?php echo base_url();?>assets/js/default.min.js "></script>
    <?php } ?>
    
    

    
    
    
</body>
</html>