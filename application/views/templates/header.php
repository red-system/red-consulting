<!DOCTYPE html>
<html class="scheme_original" lang="en-US">
<head>
    <title><?php echo $title; ?></title>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport">
    <meta content="telephone=no" name="format-detection">
    <meta name="description" content="<?php echo $description; ?>">
    <meta name="keywords" content="<?php echo $keywords; ?>">
    <meta name="author" content="www.redsystem.id" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="revisit-after" content="2 days"/>
    <meta name="robots" content="index, follow"/>
    <meta name="rating" content="General"/>
    <meta name="author" content="http://redsystem.id/"/>
    <meta name="MSSmartTagsPreventParsing" content="true"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="reply-to" content="info@redsystem.id">
    <meta http-equiv="expires" content="Mon, 28 Jul <?php echo date('Y')+1;?> 11:12:01 GMT">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

    <?php if (($this->session->menu=='home')== 'active') { ?>
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/beranda.min.css" type="text/css">
    <?php } elseif (($this->session->menu=='layanan-pajak-detail'||$this->session->menu=='layanan-laporan-keuangan-detail'||$this->session->menu=='layanan-keuangan-detail')== 'active') { ?>
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/layanan_detail.min.css" type="text/css">
    <?php } elseif (($this->session->menu=='galeri')== 'active') { ?>
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/galeri.min.css" type="text/css">
    <?php } elseif (($this->session->menu=='artikel-detail'||$this->session->menu=='tentang-kami')== 'active') { ?>
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/artikel_detail.min.css" type="text/css">
    <?php } elseif (($this->session->menu=='kontak')== 'active') { ?>
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/kontak.min.css" type="text/css">
    <?php } else { ?>
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/default.min.css" type="text/css">
    <?php } ?>
    <link href="<?php echo base_url();?>assets/images/logo-favicon-red-consulting.png" rel="icon" sizes="192x192">
</head>

<?php if (($this->session->menu=='layanan-pajak'||$this->session->menu=='layanan-pajak-detail'||$this->session->menu=='layanan-laporan-keuangan'||$this->session->menu=='layanan-laporan-keuangan-detail'||$this->session->menu=='layanan-keuangan'||$this->session->menu=='layanan-keuangan-detail'||$this->session->menu=='artikel-detail')=='active') { ?>
<body class="body_style_wide body_filled scheme_original top_panel_show top_panel_over sidebar_show sidebar_right sidebar_outer_show sidebar_outer_yes">
<?php } else { ?>
<body class="body_style_wide body_filled scheme_original top_panel_show top_panel_over sidebar_hide sidebar_outer_show sidebar_outer_yes">
<?php } ?>
    <div class="body_wrap">
        <div class="page_wrap">
            <div class="top_panel_fixed_wrap"></div>
            <?php if (($this->session->menu=='home'||$this->session->menu=='notfound')== 'active') { ?>
                <header class="top_panel_wrap top_panel_style_3 boxed_style scheme_original">
                    <div class="top_panel_wrap_inner top_panel_inner_style_3 top_panel_position_over">
                        <div class="top_panel_top">
                            <div class="content_wrap clearfix">
                                <div class="top_panel_top_contact_area icon-location-light">
                                    Jl. Ratna No 68 G, Tonja, Denpasar Utara, Denpasar - Bali . 80239.
                                </div>
                                <div class="top_panel_top_open_hours icon-mail-light">
                                    <a href="mailto:inforedconsulting69@gmail.com">inforedconsulting69@gmail.com</a>
                                </div>
                                <div class="top_panel_top_ophone icon-call-out kontak-header">
                                    <a href="tel:(0361) 4746102">(0361) 4746102</a>
                                </div>
                                <div class="top_panel_top_user_area cart_show">
                                    <div class="menu_pushy_wrap clearfix">
                                        <a class="menu_pushy_button icon-1460034782_menu2" href="#"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="top_panel_middle">
                            <div class="content_wrap">
                                <div class="contact_logo">
                                    <div class="logo">
                                        <a href="<?php echo base_url() ?>"><img alt="RED Consulting" title="RED Consulting" width="200" height="70" class="logo_main" src="<?php echo base_url();?>assets/images/red-consulting-logo.png"> <img alt="RED Consulting" title="RED Consulting" width="200" height="70" class="logo_fixed" src="<?php echo base_url();?>assets/images/red-consulting-logo.png"></a>
                                    </div>
                                </div>
                                <div class="menu_main_wrap hide--social--media">
                                    <div class="search_wrap search_style_fullscreen search_state_closed top_panel_el top_panel_icon">
                                        <div class="search_form_wrap">
                                            <form action="#" class="search_form" method="get">
                                                <button class="search_submit icon-search-light" type="submit"></button> <input class="search_field" name="s" placeholder="Search" type="text" value=""> <a class="search_close icon-1460034721_close"></a>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="top_panel_top_socials top_panel_el">
                                        <div class="sc_socials sc_socials_type_icons sc_socials_shape_square sc_socials_size_tiny">
                                            <div class="sc_socials_item">
                                                <a class="social_icons social_facebook" href="<?php echo $facebook_link ?>" title="<?php echo $facebook_title ?>" target="_blank"><span class="icon-facebook"></span></a>
                                            </div>
                                            <div class="sc_socials_item">
                                                <a class="social_icons social_instagram" href="<?php echo $instagram_link ?>" title="<?php echo $instagram_title ?>" target="_blank"><span class="icon-instagramm"></span></a>
                                            </div>
                                            <div class="sc_socials_item">
                                                <a class="social_icons social_linkedin" href="<?php echo $linkedin_link ?>" title="<?php echo $linkedin_title ?>" target="_blank"><span class="icon-linkedin"></span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="menu_main_wrap">
                                    <nav class="menu_main_nav_area menu_hover_fade">
                                        <ul class="menu_main_nav" id="menu_main">
                                            <li class="menu-item">
                                                <a href="<?php echo base_url() ?>"><span class="<?php if($this->session->menu=='home'){echo('color-active');} ?>">Beranda</span></a>
                                            </li>
                                            <li class="menu-item menu-item-has-children <?php if ($this->session->menu=='layanan-pajak'||$this->session->menu=='layanan-pajak-detail'||$this->session->menu=='layanan-laporan-keuangan'||$this->session->menu=='layanan-laporan-keuangan-detail'||$this->session->menu=='layanan-keuangan'||$this->session->menu=='layanan-keuangan-detail'){echo('current-menu-ancestor current-menu-parent');} ?>">
                                                <a href="#" class="sf-with-ul"><span>Layanan</span></a>
                                                <ul class="sub-menu fadeOut animated fast">
                                                    <li class="menu-item <?php if($this->session->menu=='layanan-pajak'||$this->session->menu=='layanan-pajak-detail'){echo('current-menu-item');} ?>">
                                                        <a href="<?php echo base_url() ?>layanan/konsultan-pajak"><span>Konsultan Pajak</span></a>
                                                    </li>
                                                    <li class="menu-item <?php if($this->session->menu=='layanan-laporan-keuangan'||$this->session->menu=='layanan-laporan-keuangan-detail'){echo('current-menu-item');} ?>">
                                                        <a href="<?php echo base_url() ?>layanan/manajemen-laporan-keuangan"><span>Manajemen Laporan Keuangan</span></a>
                                                    </li>
                                                    <li class="menu-item <?php if($this->session->menu=='layanan-keuangan'||$this->session->menu=='layanan-keuangan-detail'){echo('current-menu-item');} ?>">
                                                        <a href="<?php echo base_url() ?>layanan/finance-controller"><span>Finance Controller</span></a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li class="menu-item <?php if($this->session->menu=='testimonial'){echo('current-menu-item');} ?>">
                                                <a href="<?php echo base_url() ?>testimonial"><span>Testimonial</span></a>
                                            </li>
                                            <li class="menu-item <?php if($this->session->menu=='galeri'){echo('current-menu-item');} ?>">
                                                <a href="<?php echo base_url() ?>galeri"><span>Galeri</span></a>
                                            </li>
                                            <li class="menu-item <?php if($this->session->menu=='artikel'||$this->session->menu=='artikel-detail'){echo('current-menu-item');} ?>">
                                                <a href="<?php echo base_url() ?>artikel"><span>Artikel</span></a>
                                            </li>
                                            <li class="menu-item <?php if($this->session->menu=='tentang-kami'){echo('current-menu-item');} ?>">
                                                <a href="<?php echo base_url() ?>tentang-kami"><span>Tentang Kami</span></a>
                                            </li>
                                            <li class="menu-item <?php if($this->session->menu=='kontak'){echo('current-menu-item');} ?>">
                                                <a href="<?php echo base_url() ?>kontak"><span>Kontak</span></a>
                                            </li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>
                <nav class="menu_pushy_nav_area pushy pushy-left scheme_dark">
                    <div class="pushy_inner">
                        <a class="close-pushy" href="#"></a>
                        <div class="sidebar_outer widget_area scheme_dark">
                            <div class="sidebar_outer_inner widget_area_inner">
                                <div class="sidebar_outer_widgets">
                                    <aside class="widget widget_socials">
                                        <h5 class="widget_title">Tentang Kami</h5>
                                        <div class="widget_inner">
                                            <div class="logo">
                                                <a href="<?php echo base_url() ?>"><img alt="RED Consulting" title="RED Consulting" class="logo_main" src="<?php echo base_url();?>assets/images/red-consulting-logo-footer.png" width="134" height="47"></a>
                                            </div>
                                            <div class="logo_descr">
                                               <p align="justify"> 
                                                    R.E.D. CONSULTING hadir untuk membantu perusahaan dengan membuka seluruh potensi yang dimiliki untuk tumbuh lebih besar, dengan turut menghadirkan solusi terbaik dalam memenuhi penataan keuangan perusahaan. Kami melayani perusahaan di berbagai bidang industri dengan mengedepankan layanan
                                                    <br>
                                                </p>
                                                <ul class="sc_list sc_list_style_iconed custom_cl_1">
                                                    <li class="sc_list_item"><span class="sc_list_icon icon-location-light custom_cl_2"></span>Jl. Ratna No 68 G, Tonja, Denpasar Utara, Denpasar - Bali . 80239.</li>
                                                    <li class="sc_list_item"><span class="sc_list_icon icon-clock-light custom_cl_2"></span>Sen - Jum : 08:00 - 17:00</li>
                                                    <li class="sc_list_item"><span class="sc_list_icon icon-mobile-light custom_cl_2"></span><a href="tel:(0361) 4746102" class="padding-0">Phone: (0361) 4746102</a></li>
                                                    <li class="sc_list_item"><span class="sc_list_icon icon-mail-light custom_cl_2"></span><a href="mailto:inforedconsulting69@gmail.com" class="padding-0">Email: inforedconsulting69@gmail.com</a></li>
                                                </ul>
                                                
                                            </div>
                                            
                                            <div class="content_wrap">
                                                <div class="sc_socials sc_socials_type_icons sc_socials_shape_square sc_socials_size_tiny">
                                                    <div class="sc_socials_item">
                                                        <a class="social_icons social_facebook" href="<?php echo $facebook_link ?>" title="<?php echo $facebook_title ?>" target="_blank"><span class="icon-facebook"></span></a>
                                                    </div>
                                                    <div class="sc_socials_item">
                                                        <a class="social_icons social_instagram" href="<?php echo $instagram_link ?>" title="<?php echo $instagram_title ?>" target="_blank"><span class="icon-instagramm"></span></a>
                                                    </div>
                                                    <div class="sc_socials_item">
                                                        <a class="social_icons social_linkedin" href="<?php echo $linkedin_link ?>" title="<?php echo $linkedin_title ?>" target="_blank"><span class="icon-linkedin"></span></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </aside>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </nav>
            <?php } else {  ?>
                <header class="top_panel_wrap top_panel_style_3 boxed_style scheme_original">
                    <div class="top_panel_wrap_inner top_panel_inner_style_3 top_panel_position_over">
                        <div class="top_panel_top">
                            <div class="content_wrap clearfix">
                                <div class="top_panel_top_contact_area icon-location-light">
                                    Jl. Ratna No 68 G, Tonja, Denpasar Utara, Denpasar - Bali . 80239.
                                </div>
                                <div class="top_panel_top_open_hours icon-mail-light">
                                    <a href="mailto:inforedconsulting69@gmail.com">inforedconsulting69@gmail.com</a>
                                </div>
                                <div class="top_panel_top_ophone icon-call-out">
                                    <a href="tel:(0361) 4746102">(0361) 4746102</a>
                                </div>
                                <div class="top_panel_top_user_area cart_show">
                                    <div class="menu_pushy_wrap clearfix">
                                        <a class="menu_pushy_button icon-1460034782_menu2" href="#"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="top_panel_middle">
                            <div class="content_wrap padding-0">
                                <div class="contact_logo">
                                    <div class="logo">
                                        <a href="<?php echo base_url() ?>"><img alt="RED Consulting" title="RED Consulting" width="200" height="70" class="logo_main" src="<?php echo base_url();?>assets/images/red-consulting-logo.png""> <img alt="RED Consulting" title="RED Consulting" width="200" height="70" class="logo_fixed" src="<?php echo base_url();?>assets/images/red-consulting-logo.png"></a>
                                    </div>
                                </div>
                                <div class="menu_main_wrap hide--social--media">
                                    <div class="search_wrap search_style_fullscreen search_state_closed top_panel_el top_panel_icon">
                                        <div class="search_form_wrap">
                                            <form action="#" class="search_form" method="get">
                                                <button class="search_submit icon-search-light" type="submit"></button> <input class="search_field" name="s" placeholder="Search" type="text" value=""> <a class="search_close icon-1460034721_close"></a>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="top_panel_top_socials top_panel_el">
                                        <div class="sc_socials sc_socials_type_icons sc_socials_shape_square sc_socials_size_tiny">
                                            <div class="sc_socials_item">
                                                <a class="social_icons social_facebook" href="<?php echo $facebook_link ?>" title="<?php echo $facebook_title ?>" target="_blank"><span class="icon-facebook"></span></a>
                                            </div>
                                            <div class="sc_socials_item">
                                                <a class="social_icons social_instagram" href="<?php echo $instagram_link ?>" title="<?php echo $instagram_title ?>" target="_blank"><span class="icon-instagramm"></span></a>
                                            </div>
                                            <div class="sc_socials_item">
                                                <a class="social_icons social_linkedin" href="<?php echo $linkedin_link ?>" title="<?php echo $linkedin_title ?>" target="_blank"><span class="icon-linkedin"></span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="menu_main_wrap">
                                    <nav class="menu_main_nav_area menu_hover_fade">
                                        <ul class="menu_main_nav" id="menu_main">
                                            <li class="menu-item <?php if($this->session->menu=='home'){echo('current-menu-item');} ?>">
                                                <a href="<?php echo base_url() ?>"><span class="<?php if($this->session->menu=='home'){echo('color-active');} ?>">Beranda</span></a>
                                            </li>
                                            <li class="menu-item menu-item-has-children <?php if ($this->session->menu=='layanan-pajak'||$this->session->menu=='layanan-pajak-detail'||$this->session->menu=='layanan-laporan-keuangan'||$this->session->menu=='layanan-laporan-keuangan-detail'||$this->session->menu=='layanan-keuangan'||$this->session->menu=='layanan-keuangan-detail'){echo('current-menu-ancestor current-menu-parent');} ?>">
                                                <a href="#" class="sf-with-ul"><span class="<?php if($this->session->menu=='layanan-pajak'||$this->session->menu=='layanan-pajak-detail'||$this->session->menu=='layanan-laporan-keuangan'||$this->session->menu=='layanan-laporan-keuangan-detail'||$this->session->menu=='layanan-keuangan'||$this->session->menu=='layanan-keuangan-detail'){echo('color-active');} ?>">Layanan</span></a>
                                                <ul class="sub-menu fadeOut animated fast">
                                                    <li class="menu-item <?php if($this->session->menu=='layanan-pajak'||$this->session->menu=='layanan-pajak-detail'){echo('current-menu-item');} ?>">
                                                        <a href="<?php echo base_url() ?>layanan/konsultan-pajak"><span class="<?php if($this->session->menu=='layanan-pajak'||$this->session->menu=='layanan-pajak-detail'){echo('color-active');} ?>">Konsultan Pajak</span></a>
                                                    </li>
                                                    <li class="menu-item <?php if($this->session->menu=='layanan-laporan-keuangan'||$this->session->menu=='layanan-laporan-keuangan-detail'){echo('current-menu-item');} ?>">
                                                        <a href="<?php echo base_url() ?>layanan/manajemen-laporan-keuangan"><span class="<?php if($this->session->menu=='layanan-laporan-keuangan'||$this->session->menu=='layanan-laporan-keuangan-detail'){echo('color-active');} ?>">Manajemen Laporan Keuangan</span></a>
                                                    </li>
                                                    <li class="menu-item <?php if($this->session->menu=='layanan-keuangan'||$this->session->menu=='layanan-keuangan-detail'){echo('current-menu-item');} ?>">
                                                        <a href="<?php echo base_url() ?>layanan/finance-controller"><span class="<?php if($this->session->menu=='layanan-keuangan'||$this->session->menu=='layanan-keuangan-detail'){echo('color-active');} ?>">Finance Controller</span></a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li class="menu-item <?php if($this->session->menu=='testimonial'){echo('current-menu-item');} ?>">
                                                <a href="<?php echo base_url() ?>testimonial"><span class="<?php if($this->session->menu=='testimonial'){echo('color-active');} ?>">Testimonial</span></a>
                                            </li>
                                            <li class="menu-item <?php if($this->session->menu=='galeri'){echo('current-menu-item');} ?>">
                                                <a href="<?php echo base_url() ?>galeri"><span class="<?php if($this->session->menu=='galeri'){echo('color-active');} ?>">Galeri</span></a>
                                            </li>
                                            <li class="menu-item <?php if($this->session->menu=='artikel'||$this->session->menu=='artikel-detail'){echo('current-menu-item');} ?>">
                                                <a href="<?php echo base_url() ?>artikel"><span class="<?php if($this->session->menu=='artikel'||$this->session->menu=='artikel-detail'){echo('color-active');} ?>">Artikel</span></a>
                                            </li>
                                            <li class="menu-item <?php if($this->session->menu=='tentang-kami'){echo('current-menu-item');} ?>">
                                                <a href="<?php echo base_url() ?>tentang-kami"><span class="<?php if($this->session->menu=='tentang-kami'){echo('color-active');} ?>">Tentang Kami</span></a>
                                            </li>
                                            <li class="menu-item <?php if($this->session->menu=='kontak'){echo('current-menu-item');} ?>">
                                                <a href="<?php echo base_url() ?>kontak"><span class="<?php if($this->session->menu=='kontak'){echo('color-active');} ?>">Kontak</span></a>
                                            </li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>
                <?php if (($this->session->menu=='layanan-pajak'||$this->session->menu=='layanan-laporan-keuangan'||$this->session->menu=='layanan-keuangan')== 'active') { ?>
                    <section class="top_panel_image top_panel_image_1">
                        <div class="top_panel_image_hover"></div>
                        <div class="top_panel_image_header">
                            <h1 class="top_panel_image_title"><?php echo $title_act; ?></h1>
                            <div class="breadcrumbs">
                                <a class="breadcrumbs_item home" href="<?php echo base_url() ?>">Beranda</a> <span class="breadcrumbs_delimiter"></span> Layanan <span class="breadcrumbs_delimiter"></span>  <span class="breadcrumbs_item current"><?php echo $title_act; ?></span>
                            </div>
                        </div>
                    </section>
                <?php } elseif  (($this->session->menu=='artikel-detail')== 'active') { ?>
                    <section class="top_panel_image top_panel_image_1">
                        <div class="top_panel_image_hover"></div>
                        <div class="top_panel_image_header">
                            <h1 class="top_panel_image_title"><?php echo $title_act; ?></h1>
                            <div class="breadcrumbs">
                                <a class="breadcrumbs_item home" href="<?php echo base_url() ?>">Beranda</a> <span class="breadcrumbs_delimiter"></span> <a class="breadcrumbs_item home" href="<?php echo base_url() ?>artikel">Artikel</a> <span class="breadcrumbs_delimiter"></span>  <span class="breadcrumbs_item current"><?php echo $title_act; ?></span>
                            </div>
                        </div>
                    </section>
               <?php } else {  ?>
                    <section class="top_panel_image top_panel_image_1">
                        <div class="top_panel_image_hover"></div>
                        <div class="top_panel_image_header">
                            <h1 class="top_panel_image_title"><?php echo $title_act; ?></h1>
                            <div class="breadcrumbs">
                                <a class="breadcrumbs_item home" href="<?php echo base_url() ?>">Beranda</a> <span class="breadcrumbs_delimiter"></span> <span class="breadcrumbs_item current"><?php echo $title_act; ?></span>
                            </div>
                        </div>
                    </section>
                <?php } ?>
                <nav class="menu_pushy_nav_area pushy pushy-left scheme_dark">
                    <div class="pushy_inner">
                        <a class="close-pushy" href="#"></a>
                        <div class="sidebar_outer widget_area scheme_dark">
                            <div class="sidebar_outer_inner widget_area_inner">
                                <div class="sidebar_outer_widgets">
                                    <aside class="widget widget_socials">
                                        <h5 class="widget_title">Tentang Kami</h5>
                                        <div class="widget_inner">
                                            <div class="logo">
                                                <a href="<?php echo base_url() ?>"><img alt="RED Consulting" title="RED Consulting" width="134" height="47" class="logo_main" src="<?php echo base_url();?>assets/images/red-consulting-logo-footer.png"></a>
                                            </div>
                                            <div class="logo_descr">
                                               <p align="justify"> 
                                                    R.E.D. CONSULTING hadir untuk membantu perusahaan dengan membuka seluruh potensi yang dimiliki untuk tumbuh lebih besar, dengan turut menghadirkan solusi terbaik dalam memenuhi penataan keuangan perusahaan. Kami melayani perusahaan di berbagai bidang industri dengan mengedepankan layanan
                                                    <br>
                                                </p>
                                                <ul class="sc_list sc_list_style_iconed custom_cl_1">
                                                    <li class="sc_list_item"><span class="sc_list_icon icon-location-light custom_cl_2"></span>Jl. Ratna No 68 G, Tonja, Denpasar Utara, Denpasar - Bali . 80239.</li>
                                                    <li class="sc_list_item"><span class="sc_list_icon icon-clock-light custom_cl_2"></span>Sen - Jum : 08:00 - 17:00</li>
                                                    <li class="sc_list_item"><span class="sc_list_icon icon-mobile-light custom_cl_2"></span><a href="tel:(0361) 4746102" class="padding-0">Phone: (0361) 4746102</a></li>
                                                    <li class="sc_list_item"><span class="sc_list_icon icon-mail-light custom_cl_2"></span><a href="mailto:inforedconsulting69@gmail.com" class="padding-0">Email: inforedconsulting69@gmail.com</a></li>
                                                </ul>
                                                
                                            </div>
                                            
                                            <div class="content_wrap">
                                                <div class="sc_socials sc_socials_type_icons sc_socials_shape_square sc_socials_size_tiny">
                                                    <div class="sc_socials_item">
                                                        <a class="social_icons social_facebook" href="<?php echo $facebook_link ?>" title="<?php echo $facebook_title ?>" target="_blank"><span class="icon-facebook"></span></a>
                                                    </div>
                                                    <div class="sc_socials_item">
                                                        <a class="social_icons social_instagram" href="<?php echo $instagram_link ?>" title="<?php echo $instagram_title ?>" target="_blank"><span class="icon-instagramm"></span></a>
                                                    </div>
                                                    <div class="sc_socials_item">
                                                        <a class="social_icons social_linkedin" href="<?php echo $linkedin_link ?>" title="<?php echo $linkedin_title ?>" target="_blank"><span class="icon-linkedin"></span></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </aside>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </nav>

            <?php } ?>
            
            <div class="site-overlay"></div>
            <div class="header_mobile header_mobile_style_3 boxed_style position-fixed">
                <div class="content_wrap">
                    <div class="menu_button icon-menu"></div>
                    <div class="logo">
                        <a href="<?php echo base_url() ?>"><img alt="RED Consulting" title="RED Consulting" width="200" height="70" class="logo_main" src="<?php echo base_url();?>assets/images/red-consulting-logo.png"></a>
                    </div>
                </div>
                <div class="side_wrap">
                    <div class="close">
                        Close
                    </div>
                    <div class="panel_top">
                        <nav class="menu_main_nav_area">
                            <ul class="menu_main_nav" id="menu_mobile">
                                <li class="menu-item <?php if($this->session->menu=='home'){echo('current-menu-item');} ?>">
                                    <a href="<?php echo base_url() ?>"><span >Beranda</span></a>
                                </li>
                                <li class="menu-item menu-item-has-children <?php if ($this->session->menu=='layanan-pajak'||$this->session->menu=='layanan-laporan-keuangan'||$this->session->menu=='layanan-keuangan'){echo('current-menu-ancestor current-menu-parent');} ?>">
                                    <a href="#"><span >Layanan</span></a>
                                    <ul class="sub-menu">
                                        <li class="menu-item <?php if($this->session->menu=='layanan-pajak'){echo('current-menu-item');} ?>">
                                            <a href="<?php echo base_url() ?>layanan/konsultan-pajak"><span >Manajemen Perpajakan</span></a>
                                        </li>
                                        <li class="menu-item <?php if($this->session->menu=='layanan-laporan-keuangan'){echo('current-menu-item');} ?>">
                                            <a href="<?php echo base_url() ?>layanan/manajemen-laporan-keuangan"><span >Manajemen Laporan Keuangan</span></a>
                                        </li>
                                        <li class="menu-item <?php if($this->session->menu=='layanan-keuangan'){echo('current-menu-item');} ?>">
                                            <a href="<?php echo base_url() ?>layanan/finance-controller"><span >Finance Controller</span></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="menu-item <?php if($this->session->menu=='testimonial'){echo('current-menu-item');} ?>">
                                    <a href="<?php echo base_url() ?>testimonial"><span >Testimonial</span></a>
                                </li>
                                <li class="menu-item <?php if($this->session->menu=='galeri'){echo('current-menu-item');} ?>">
                                    <a href="<?php echo base_url() ?>galeri"><span >Galeri</span></a>
                                </li>
                                <li class="menu-item <?php if($this->session->menu=='artikel'||$this->session->menu=='artikel-detail'){echo('current-menu-item');} ?>">
                                    <a href="<?php echo base_url() ?>artikel"><span >Artikel</span></a>
                                </li>
                                <li class="menu-item <?php if($this->session->menu=='tentang-kami'){echo('current-menu-item');} ?>">
                                    <a href="<?php echo base_url() ?>tentang-kami"><span >Tentang Kami</span></a>
                                </li>
                                <li class="menu-item <?php if($this->session->menu=='kontak'){echo('current-menu-item');} ?>">
                                    <a href="<?php echo base_url() ?>kontak"><span >Kontak</span></a>
                                </li>
                            </ul>
                        </nav>
                        <div class="search_wrap search_state_fixed search_ajax">
                            <div class="search_form_wrap">
                                <form action="#" class="search_form" method="get">
                                    <button class="search_submit icon-search-light" type="submit"></button> <input class="search_field" name="s" placeholder="Search" type="text" value="">
                                </form>
                            </div>
                            <div class="search_results widget_area scheme_original">
                                <a class="search_results_close icon-cancel"></a>
                                <div class="search_results_content"></div>
                            </div>
                        </div>
                       
                    </div>
                    <div class="panel_middle">
                        <div class="contact_field contact_phone">
                            <span class="contact_icon icon-location-light"></span> <span class="contact_label contact_phone">Jl. Ratna No 68 G, Tonja, Denpasar Utara, Denpasar - Bali . 80239</span>
                        </div>
                        <div class="contact_field contact_phone">
                            <span class="contact_icon icon-call-out"></span> <span class="contact_label contact_phone"><a href="tel:(0361) 4746102">(0361) 4746102</a></span>
                        </div>
                        <div class="contact_field contact_phone">
                            <span class="contact_icon icon-mail-light"></span> <span class="contact_label contact_phone"><a href="mailto:inforedconsulting69@gmail.com">inforedconsulting69@gmail.com</a></span>
                        </div>
                        <div class="top_panel_top_open_hours icon-clock-light">
                            Mon - Fri : 09:00 - 17:00
                        </div>
                        <div class="top_panel_top_user_area cart_hide">
                            <div class="menu_pushy_wrap clearfix">
                                <a class="menu_pushy_button icon-1460034782_menu2" href="#"></a>
                            </div>
                        </div>
                    </div>
                    <div class="panel_bottom">
                        <div class="contact_socials">
                            <div class="sc_socials sc_socials_type_icons sc_socials_shape_square sc_socials_size_small">
                                <div class="sc_socials_item">
                                    <a class="social_icons social_facebook" href="<?php echo $facebook_link ?>" title="<?php echo $facebook_title ?>" target="_blank"><span class="icon-facebook"></span></a>
                                </div>
                                <div class="sc_socials_item">
                                    <a class="social_icons social_instagram" href="<?php echo $instagram_link ?>" title="<?php echo $instagram_title ?>" target="_blank"><span class="icon-instagramm"></span></a>
                                </div>
                                <div class="sc_socials_item">
                                    <a class="social_icons social_linkedin" href="<?php echo $linkedin_link ?>" title="<?php echo $linkedin_title ?>" target="_blank"><span class="icon-linkedin"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mask"></div>
            </div>